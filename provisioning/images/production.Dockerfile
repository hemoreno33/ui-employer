ARG APPLICATION_NAME='ui-employer'
FROM govtechsg/mcf-ui-server:0.0.34
ENV APPLICATION_NAME=${APPLICATION_NAME}
COPY ./dist /server/static
COPY ./.version /server/static/version
COPY ./.version /server/.version
USER root
RUN chown app:app -R ./
