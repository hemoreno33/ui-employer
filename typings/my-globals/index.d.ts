declare module 'my-globals' {
  global {
    interface Window {
      __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
      msCrypto: Crypto;
    }
  }
}

declare module '*.scss';
