/**
 * fix: `matchMedia` not present, legacy browsers require a polyfill. Used for tests with react-slick
 */

/* tslint:disable:no-empty */
const matchMediaMock = (): MediaQueryList =>
  ({
    addListener: () => {},
    matches: false,
    removeListener: () => {},
  } as any);

window.matchMedia = window.matchMedia || matchMediaMock;
