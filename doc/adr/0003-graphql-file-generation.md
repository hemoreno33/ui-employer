# 3. Graphql File Generation

Date: 2019-01-18

## Status

Accepted

## Context

Manually updating graphql schema (through a command) and types is cumbersome and error prone (schema may be outdated, types may be different, creating types that match graphql schema is tedious).
On top of that, developer are required to push changes every time manually

## Decision

Generate graphql schema and types automatically and make git ignore the generated files. They both will be generated automatically by the CI matching the version deployed in QA environment. Thus the project will always be up to date with the latest available version and any breaking change (that will affect the project) will be caught and make the pipeline fail.

## Consequences

We don't have to write new types for graphql. But we have to be aware that it's generated, and where to find this generated definitions. Which can be explained in README.md

Once the project is installed, developers have to make sure that files are up to date by running `yarn dev:install` (they already have to care about node dependencies for new packages or upgraded packages) and when developing they must be careful about any changes that could happen to generated files (in case of updates for gql queries). To ensure locally the project is working correctly:
- Run `yarn dev:install` manually
- Use File Watchers in your IDE to run actions (Watch for graphql folder and run `yarn dev:install`)

