# 2. Use Redux Sagas for side effect actions like, triggering survey sequences or analytics

Date: 2018-12-11

## Status

Accepted

## Context

We need to have the way of handling actions not directly related to our system. Which not suppose to be tied or coupled with our system. Such as triggering sequences, example are showing survey depends on how many clicks was done. Or google analytics requests.

## Decision

We will use Redux Sagas, in order to handle these events. Some specifics, to differentiate them from regular actions we will use action names starting with `on` or `ON_`. Examples are,

action name `ON_APPLICATION_CLICKED` corresponding dispatch will be `onApplicationClicked`

## Consequences

1. Redux action namespace pollution;
2. Using containers components in order to dispatch events. Having additional prop for such dispatch in our stateless components.
