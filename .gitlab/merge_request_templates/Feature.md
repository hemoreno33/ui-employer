Pivotal Story:

<!-- Make sure Merge Request title following "<type>: <subject> [#<pivotal_story_id>]" -->

<!-- Brief description. Examples are, screenshots, steps to reproduce, links to dependent MRs -->

### General Checklist:

- [ ] Don't forget to check changes in IE11
- [ ] Exact versions in package.json
- [ ] Testing instructions?
- [ ] Docs? Examples are, update `README.md` file, or add ADR in `doc/adr`
- [ ] Tests?
- [ ] Put in copy at least two potential reviewers

/cc

/label ~"Review Me"
