# Development

## Getting Started
To get started with default setings run:

```sh
yarn dev:init;
```

The above will trigger the build, install and start steps as follows:

- `yarn dev:build` to initialise Docker images
- `yarn dev:install` to install dependencies
- `yarn dev:start` to start with development environment
- `yarn dev:clean` to clear everything related to this
