// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const fs = require('fs-extra');
const path = require('path');
const {exec} = require('child_process');
const mysql = require('mysql');
const dotenv = require('dotenv');

// getting Feature flag details and writing to a JSON file
const portals = ['jobseeker', 'employer'];
for (const portal of portals) {
  const featureURL = `https://www.mycareersfuture.sg/features/${portal}/latest.json`;
  exec(`curl '${featureURL}'`, (error, stdout) => {
    if (!error) {
      // eslint-disable-next-line no-console
      console.log(`${featureURL}\n${stdout}`);
      fs.outputFile(path.resolve(process.cwd(), 'cypress/fixtures/featureFlags', `${portal}.json`), stdout);
    }
  });
}

function getConfigurationByFile(file) {
  const pathToConfigFile = path.resolve(process.cwd(), 'cypress/config', `${file}.json`);
  return fs.readJson(pathToConfigFile);
}

function dbConnectAndQuery(query, dbConfig) {
  return new Promise((resolve, reject) => {
    const connection = mysql.createConnection(dbConfig);
    connection.connect();
    connection.query(query, (error, results) => {
      if (error) reject(error);
      else {
        connection.end();
        return resolve(results);
      }
    });
  });
}

module.exports = (on, config) => {
  const environment = config.env.configFile || 'qa-external';

  // load db config from dotenv file
  dotenv.config({path: `./cypress/config/dbConfig/.env.${environment}`});

  on('task', {
    jobDB: (query) => {
      return dbConnectAndQuery(query, {
        host: process.env.CYPRESS_JOB_MYSQL_HOST || 'localhost',
        user: process.env.CYPRESS_JOB_MYSQL_USER || 'mcf_job',
        password: process.env.CYPRESS_JOB_MYSQL_PW || 'mcf_job',
        database: process.env.CYPRESS_JOB_MYSQL_NAME || 'mcf_job',
      });
    },
    profileDB: (query) => {
      return dbConnectAndQuery(query, {
        host: process.env.CYPRESS_PROFILE_MYSQL_HOST || 'localhost',
        user: process.env.CYPRESS_PROFILE_MYSQL_USER || 'mcf_profile',
        password: process.env.CYPRESS_PROFILE_MYSQL_PW || 'mcf_profile',
        database: process.env.CYPRESS_PROFILE_MYSQL_NAME || 'mcf_profile',
      });
    },
  });
  return getConfigurationByFile(environment);
};
