const cypress = require('cypress');
const {merge} = require('mochawesome-merge');
const generator = require('mochawesome-report-generator');
const fse = require('fs-extra');

const spec = process.env.CYPRESS_SPEC;
const baseUrl = process.env.CYPRESS_baseUrl;

async function runTests() {
  await fse.remove('mochawesome-report'); // remove the report folder
  const {totalFailed} = await cypress.run({
    spec,
    config: {
      baseUrl,
    },
  }); // get the number of failed tests
  const jsonReport = await merge(); // generate JSON report
  await generator.create(jsonReport);
  process.exit(totalFailed); // exit with the number of failed tests
}

runTests();
