const faker = require('faker');

export const getApplicationsCount = (jobUuid, count, unviewedTotal) => {
  return {
    variables: {jobId: jobUuid},
    response: {
      data: {applicationsForJob: {total: count, unviewedTotal: unviewedTotal, __typename: 'Applications'}},
    },
  };
};

export const getApplicationsMock = (jobUuid, numberOfApplications) => {
  const applications = [];
  for (let i = 0; i < numberOfApplications; i++) {
    const application = {
      job: {
        uuid: jobUuid,
        __typename: 'Job',
      },
      applicant: {
        email: faker.internet.email(),
        name: faker.name.findName(),
        id: faker.random.uuid(),
        lastLogin: faker.date.recent(),
        mobileNumber: faker.phone.phoneNumber('9########'),
        education: [
          {
            name: 'Degree',
            isHighest: true,
            institution: 'Verified University',
            yearAttained: 1970,
            ssecEqaCode: '70',
            ssecEqaDescription: "Bachelor's Degree or equivalent",
            ssecFosDescription: 'Business Management',
            isVerified: true,
            __typename: 'Education',
          },
          {
            name: 'Degree',
            isHighest: false,
            institution: 'University of Eastern Australia',
            yearAttained: 2000,
            ssecEqaCode: '70',
            ssecEqaDescription: "Bachelor's Degree or equivalent",
            ssecFosDescription: 'Business Management',
            isVerified: false,
            __typename: 'Education',
          },
          {
            name: 'Degree',
            isHighest: false,
            institution: 'NUS',
            yearAttained: 2000,
            ssecEqaCode: '70',
            ssecEqaDescription: "Bachelor's Degree or equivalent",
            ssecFosDescription: 'Business Management',
            isVerified: null,
            __typename: 'Education',
          },
        ],
        skills: [
          {id: 66, skill: 'Accounting', __typename: 'Skill'},
          {id: 565, skill: 'Auditing', __typename: 'Skill'},
          {id: 924, skill: 'Budget', __typename: 'Skill'},
          {id: 3209, skill: 'External Audit', __typename: 'Skill'},
          {id: 3361, skill: 'Financial Accounting', __typename: 'Skill'},
          {id: 3364, skill: 'Financial Audits', __typename: 'Skill'},
        ],
        workExperiences: [
          {
            jobTitle: faker.name.jobTitle(),
            companyName: faker.company.companyName(),
            startDate: '2012-09-01',
            endDate: '2018-08-01',
            jobDescription: faker.name.jobDescriptor(),
            __typename: 'WorkExperience',
          },
        ],
        resume: {
          fileName: '+Cheryl Wong CV Aug 2019.pdf',
          filePath:
            '/documents/b75bb3fe8852a22210d1b245488b00ca070b8f2844e083f2440d82c8796a33967c5cc3f50112d42fb3ed4110fee36168e1ab26844335e32aa2e53a56b30d51a3c380e168fb3bd99e0c0f7e98475ba592',
          lastDownloadedAt: null,
          __typename: 'Resume',
        },
        __typename: 'Candidate',
      },
      createdOn: faker.date.past(),
      id: faker.random.uuid(),
      isViewed: true,
      isShortlisted: null,
      bookmarkedOn: null,
      statusId: 4,
      scores: {
        wcc: 0.632,
        jobkred: 0.553,
        __typename: 'ApplicationScore',
      },
      __typename: 'Application',
    };
    applications.push(application);
  }

  return {
    variables: {
      jobId: jobUuid,
    },
    response: {
      data: {
        applicationsForJob: {
          applications,
          total: numberOfApplications,
          __typename: 'Applications',
        },
      },
    },
  };
};
