const faker = require('faker');
export const getSuggestedTalentMock = (jobUuid, numberOfTalents = 1) => {
  return {
    ok: true,
    variables: {jobId: jobUuid},
    response: {
      data: {
        suggestedTalentsForJob: {
          talents: getTalents(numberOfTalents),
          total: numberOfTalents,
          __typename: 'SuggestedTalents',
        },
      },
    },
  };
};

export const getSuggestedTalentsCount = (count) => {
  return {
    response: {
      data: {suggestedTalentsForJob: {total: count, __typename: 'SuggestedTalents'}},
    },
  };
};

const getTalents = (numberOfTalents) => {
  const talents = [];
  for (let i = 0; i < numberOfTalents; i++) {
    const talent = {
      talent: {
        email: faker.internet.email(),
        name: faker.name.findName(),
        id: faker.random.uuid(),
        lastLogin: faker.date.recent(),
        mobileNumber: faker.phone.phoneNumber('9########'),
        education: [
          {
            name: 'Degree',
            isHighest: true,
            institution: 'University of Western Australia',
            yearAttained: 2001,
            ssecEqaCode: '70',
            ssecEqaDescription: "Bachelor's Degree or equivalent",
            ssecFosDescription: 'Business Management',
            isVerified: null,
            __typename: 'Education',
          },
        ],
        skills: [
          {id: 66, skill: 'Accounting', __typename: 'Skill'},
          {id: 565, skill: 'Auditing', __typename: 'Skill'},
          {id: 924, skill: 'Budget', __typename: 'Skill'},
          {id: 3209, skill: 'External Audit', __typename: 'Skill'},
          {id: 3361, skill: 'Financial Accounting', __typename: 'Skill'},
          {id: 3364, skill: 'Financial Audits', __typename: 'Skill'},
        ],
        workExperiences: [
          {
            jobTitle: faker.name.jobTitle(),
            companyName: faker.company.companyName(),
            startDate: '2012-09-01',
            endDate: '2018-08-01',
            jobDescription: faker.name.jobDescriptor(),
            __typename: 'WorkExperience',
          },
        ],
        resume: {
          fileName: '+Cheryl Wong CV Aug 2019.pdf',
          filePath:
            '/documents/b75bb3fe8852a22210d1b245488b00ca070b8f2844e083f2440d82c8796a33967c5cc3f50112d42fb3ed4110fee36168e1ab26844335e32aa2e53a56b30d51a3c380e168fb3bd99e0c0f7e98475ba592',
          __typename: 'Resume',
        },
        __typename: 'Candidate',
      },
      scores: {wcc: 0.5853646, __typename: 'TalentScore'},
      bookmarkedOn: null,
      __typename: 'Talent',
    };
    talents.push(talent);
  }
  return talents;
};
