const moment = require('moment');
const faker = require('faker');

export const apiJobData = {
  defaultJobPost: {
    address: {
      postalCode: '738964',
      block: '9',
      street: 'WOODLANDS AVENUE 9',
      building: '',
      isOverseas: false,
    },
    categories: [{id: 11}],
    description: faker.lorem.sentences(25),
    employmentTypes: [{id: 7}],
    metadata: {
      expiryDate: moment()
        .add(21, 'days')
        .format('YYYY-MM-DD'),
    },
    minimumYearsExperience: 15,
    numberOfVacancies: 1,
    positionLevels: [{id: 1}],
    postedCompany: {uen: '100000008C'},
    salary: {maximum: 15000, minimum: 12000, type: {id: 4}},
    schemes: [],
    skills: [
      {id: 189},
      {id: 191},
      {id: 192},
      {id: 2653},
      {id: 3487},
      {id: 2412},
      {id: 100084},
      {id: 100085},
      {id: 8747},
      {id: 7935},
      {id: 5319},
      {id: 100041},
    ],
    ssecEqa: '92',
    ssecFos: '1152',
    ssocCode: 2284,
    status: {id: 102},
    title: faker.name.jobTitle(),
  },
};
