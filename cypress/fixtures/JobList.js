const moment = require('moment');
const SINGAPORE_TIMEZONE = '+08:00';
const getLocalMoment = () => moment.utc().utcOffset(SINGAPORE_TIMEZONE);
const formatPostedDate = (date) => `Posted on ${date.format('D MMM YYYY')}`;

const sharedJobPostData = {
  jobOccupationSelection: 'Adjunct Lecturer',
  previewUenOrCompanyName: 'CYPRESS06',
  previewTypeOfRegistration: 'Registry of Company',
  previewIndustry: 'Assembly and testing of semiconductors',
  jobDescriptionLabel: {
    jobTitleLabel: 'Job Title',
    jobOccupationLabel: 'Occupation',
    jobDescriptionLabel: 'Job Description & Requirements',
    thirdPartyEmployerLabel: "You're posting on behalf of another company",
  },
  thirdPartyEmployerEnabledFieldLabel: {
    uenOrCompanyNameLabel: 'Company Name / UEN',
    typeOfRegistrationLabel: 'Type of Registration',
    industryLabel: 'Industry',
  },
  keyInformation: {
    jobPostDurationLabelCheck: 'Job Post Duration',
    previewJobPostDurationValue: '7 Calendar Days',
    jobPostNoOfVacancyLabelCheck: 'Number of Vacancies',
    jobPostNoOfVacancyValueFill: '1',
    jobPostCategoryLabelPreview: 'Job Category',
    jobPostCategoryPreviewValue: 'Customer Service',
    jobPostPositionLevelLabelCheck: 'Position Level',
    jobPostPositionLevelValueFill: 'Senior Executive',
    jobPostMinNoOfExpLabelCheck: 'Minimum Years of Experience',
    jobPostMinNoOfExpValueFill: '0',
    jobPostEmploymentTypeLabelCheck: 'Employment Type',
    jobPostEmploymentTypeValueFill: 'Part Time',
    jobPostEmploymentTypeValueSelect: ['Part Time'],
    jobPostMinOfQualificationLevelLabelCheck: 'Minimum Qualification Level',
    jobPostMinOfQualificationValueFill: 'Doctorate or equivalent',
    jobPostFieldOfStudyLabelCheck: 'Field of Study',
    jobPostFieldOfStudyValueFill: 'Accountancy',
    jobPostSalaryRangeLabelCheck: 'Gross Monthly Salary Range (SGD)',
    jobPostSalaryMaxMinPreviewValue: '$1 - 9999',
  },
  jobWorkplaceDetail: {
    jobPostWorkLocationLabel: 'Workplace Address',
    jobPostWorkLocationValue: 'Local',
    jobPostWorkLocationSameLocationLabel: 'The workplace address is the same as your company address',
    jobPostWorkLocationMultipleLocationLabel: 'This position involves multiple workplace locations in Singapore',
    jobPostWorkLocation: ['Local', 'Overseas'],
    jobPostPostalCodeLabel: 'Postal Code',
    jobPostPostalCodeFillValue: '738964',
    jobPostBlockOrHouseNoLabel: 'Block/House No.',
    jobPostBlockOrHouseNoFillValue: '9',
    jobPostStreetNameLabel: 'Street Name',
    jobPostStreetNameFillValue: 'Test street',
    jobPostBuildingNameLabel: 'Building Name (optional)',
    jobPostBuildingNameFileValue: 'Test Building',
  },
};

const editJobPostUpdateData = {
  hiringUEN: '100000007C',
  jobTitle: 'Senior Developer',
  jobOccupationPartial: 'Developer',
  jobOccupationSelection: 'Application Developer',
  jobDescriptionValueFill:
    'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu, Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus, cucumber',
  jobDescriptionLabel: {
    jobTitleLabel: 'Job Title',
    jobOccupationLabel: 'Occupation',
    jobDescriptionLabel: 'Job Description & Requirements',
    thirdPartyEmployerLabel: "You're posting on behalf of another company",
  },
  recommendedSelectSkillsSection: ['Software Development', 'SQL', 'Agile Methodologies', 'XML', 'MySQL', 'Cucumber'],
  addMoreUnSelectSkillsSection: ['JavaSE'],
  keyInformation: {
    jobPostDurationLabelCheck: 'Job Post Duration',
    jobPostDurationValueFill: '21 days',
    previewJobPostDurationValue: '21 Calendar Days',
    jobPostNoOfVacancyLabelCheck: 'Number of Vacancies',
    jobPostNoOfVacancyValueFill: '3',
    jobPostCategoryLabelCheck: 'Job Category (Max 5 categories)',
    jobPostCategoryLabelPreview: 'Job Category',
    jobPostCategoryValueSelect: ['Consulting', 'Design'],
    jobPostCategoryPreviewValue:
      'Accounting / Auditing / Taxation, Architecture / Interior Design, Consulting, Customer Service',
    jobPostPositionLevelLabelCheck: 'Position Level',
    jobPostPositionLevelValueFill: 'Manager',
    jobPostMinNoOfExpLabelCheck: 'Minimum Years of Experience',
    jobPostMinNoOfExpValueFill: '3',
    jobPostEmploymentTypeLabelCheck: 'Employment Type',
    jobPostEmploymentTypeValueFill: 'Contract, Part Time, Permanent',
    jobPostEmploymentTypeValueSelect: ['Contract'],
    jobPostMinOfQualificationLevelLabelCheck: 'Minimum Qualification Level',
    jobPostMinOfQualificationValueFill: 'No Formal Qualification',
    jobPostFieldOfStudyLabelCheck: 'Field of Study',
    jobPostFieldOfStudyValueFill: 'Blank',
    jobPostSalaryRangeLabelCheck: 'Gross Monthly Salary Range (SGD)',
    jobPostSalaryMaxMinPreviewValue: '$4000 - 7000',
    jobPostSalaryMinValueFill: '4000',
    jobPostSalaryMaxValueFill: '7000',
    jobPostGovSupportOptionalCheck: 'Government support (Optional)',
    jobPostGovSchemeSelected: 'Career Support Programme',
  },
  jobWorkplaceDetail: {
    jobPostWorkplaceLabel: 'Workplace Address',
    jobPostWorkLocationValue: 'Overseas',
    jobPostCountryLabel: 'Country',
    jobPostCountryFillValue: 'Malaysia',
    jobPostOverseasAddress1Label: 'Overseas Address 1',
    jobPostOverseasAddress1FillValue: 'Test1, xxxyyzz, Malaysia',
    jobPostOverseasAddress2Label: 'Overseas Address 2',
    jobPostOverseasAddress2FillValue: 'Test2, aaabbcc, Malaysia',
  },
  acknowledgementSuccessLabel: 'You have successfully edited the following job:',
  acknowlegementExpiryDateLabel: 'Auto-closes on:',
  acknowlegementViewJobPostLabel: 'View Posted Job',
  acknowlegementReturnAllJobsLabel: 'Return to All Jobs',
  previewSkills: [
    'Aerospace',
    'Java',
    'Java Application Development',
    'Java Enterprise Edition',
    'Java Software Development',
    'Java Web Services',
    'JavaBeans',
    'JavaFX',
    'JavaScript',
    'JavaScript Frameworks',
    'JavaScript Libraries',
    'Software Development',
    'SQL',
    'Agile Methodologies',
    'XML',
    'MySQL',
    'Cucumber',
  ],
};
export default {
  // Those jobs is in UEN 100000000C
  sortingOptionsOpenJobs: ['Auto-close Date', 'Posted Date'],
  sortingOptionsClosedJobs: ['Closed Date', 'Posted Date'],
  J1: {
    // 1-for applicants pagination test
    Title: '03不要动这个工作，我的我的我的 - 开着的',
    Id: 'MCF-2018-9111113',
    PostDate: formatPostedDate(getLocalMoment()),
    ExpiryDate: 'Auto-closes in 12 days',
    Applicants: 126,
    Unviewed: 126,
  },
  J2: {
    // 2-For applicants data validation test, 6 applicants with different data in each applicant
    Title: '01Do not touch this job, this is for integration test - open',
    Id: 'MCF-2018-9111111',
    PostDate: formatPostedDate(
      getLocalMoment()
        .add(3, 'months')
        .subtract(7, 'days'),
    ),
    ExpiryDate: 'Auto-closes in 3 months',
    Applicants: 6,
    Unviewed: 5,
  },
  J3: {
    // 3-For 0 applicant test
    Title: '02Do not touch this job, this is for integration test - open',
    Id: 'MCF-2017-9111112',
    PostDate: formatPostedDate(
      getLocalMoment()
        .add(3, 'years')
        .subtract(7, 'days'),
    ),
    ExpiryDate: 'Auto-closes in 3 years',
    Applicants: 0,
    Unviewed: 0,
  },
  J4: {
    // 3-For 0 applicant test
    Title: '04Do not touch this job, this is for integration test - closed',
    Id: 'MCF-2018-9111114',
    PostDate: formatPostedDate(getLocalMoment().subtract(5, 'months')),
    ExpiryDate: 'Closed 5 months ago',
    Applicants: 0,
    Unviewed: 0,
  },
  J5: {
    // 3-For 0 applicant test
    Title: '05Do not touch this job, this is for integration test - closed',
    Id: 'MCF-2018-9111115',
    PostDate: formatPostedDate(
      getLocalMoment()
        .subtract(5, 'years')
        .subtract(7, 'days'),
    ),
    ExpiryDate: 'Closed 5 years ago',
    Applicants: 0,
    Unviewed: 0,
  },
  J6: {
    // 3-For 0 applicant test
    Title: '06Do not touch this job, this is for integration test - closed',
    Id: 'MCF-2018-9111116',
    PostDate: formatPostedDate(
      getLocalMoment()
        .subtract(5, 'months')
        .subtract(7, 'days'),
    ),
    ExpiryDate: 'Closed 5 days ago',
    Applicants: 0,
    Unviewed: 0,
  },
  jobClose1: {
    // 3-For 0 applicant test
    Title: '01Do not touch this job, this is for integration test for close job - open',
    Id: 'MCF-2900-9000001',
    PostDate: formatPostedDate(
      getLocalMoment()
        .add(3, 'months')
        .subtract(7, 'days'),
    ),
    ExpiryDate: 'Auto-closes in 3 months',
    Applicants: 0,
    Unviewed: 0,
  },
  jobClose2: {
    // 3-For 0 applicant test
    Title: '01Do not touch this job, this is for integration test for close job - open',
    Id: 'MCF-2900-9000002',
    PostDate: formatPostedDate(
      getLocalMoment()
        .add(3, 'months')
        .subtract(7, 'days'),
    ),
    ExpiryDate: 'Auto-closes in 3 months',
    Applicants: 0,
    Unviewed: 0,
  },
  jobApplicantStatusUpdate1: {
    Title: '01Do not touch this job, this is for integration test to verify applicant status- open',
    Id: 'MCF-2900-9000014',
    PostDate: formatPostedDate(getLocalMoment()),
    ExpiryDate: 'Auto-closes in 29 days',
    Applicants: 1,
    Unviewed: 1,
  },
  jobApplicantStatusUpdate2: {
    Title: '01Do not touch this job, this is for integration test to update applicant status and verify - open',
    Id: 'MCF-2900-9000016',
    PostDate: formatPostedDate(getLocalMoment()),
    ExpiryDate: 'Auto-closes in 29 days',
    Applicants: 4,
    Unviewed: 4,
  },
  jobPostViewEditCheck: {
    Id: 'MCF-2900-9000015',
    uuid: 'donottouchcypressvieweditjobpost(1)',
    jobTitle: '01Do not touch this job, this is for integration test to verify veiw edit job post - open',
    jobOccupationSelection: 'Adjunct Lecturer',
    jobDescriptionValueFill:
      'jobDescription Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id enim justo. Vestibulum sed vestibulum justo. Donec posuere arcu a ligula tincidunt maximus. Praesent iaculis lacus eget erat pellentesque, hendrerit tincidunt diam feugiat. Nam tincidunt dictum quam id posuere. Mauris vitae nisl et ligula dignissim accumsan sit amet at diam.',
    jobDescriptionMaxLimitText:
      'quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc consequat interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus sit amet dictum sit amet justo donec enim diam vulputate ut pharetra sit amet aliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare aenean euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus mattis rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim sed faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu cursus euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras ornare arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse ultrices gravida dictum fusce ut placerat orci nulla pellentesque dignissim enim sit amet venenatis urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt augue interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis tincidunt id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus vestibulum sed arcu non odio euismod lacinia at quis risus sed vulputate odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id interdum velit laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nisl purus in mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac auctor augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odio eu feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis massa sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse in est ante in nibh mauris cursus mattis molestie a iaculis at erat pellentesque adipiscing commodo elit at imperdiet dui accumsan sit amet nulla facilisi morbi tempus iaculis urna id volutpat lacus laoreet non curabitur gravida arcu ac tortor dignissim convallis aenean et tortor at risus viverra adipiscing at in tellus integer feugiat scelerisque varius morbi enim nunc faucibus a pellentesque sit amet porttitor eget dolor morbi non arcu risus quis varius quam quisque id diam vel quam elementum pulvinar etiam non quam lacus suspendisse faucibus interdum posuere lorem ipsum dolor sit amet consectetur adipiscing elit duis tristique sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula ipsum a arcu cursus vitae congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas maecenas pharetra convallis posuere morbi leo urna molestie at elementum eu facilisis sed odio morbi quis commodo odio aenean sed adipiscing diam donec adipiscing tristique risus nec quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc consequat interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus sit amet dictum sit amet justo donec enim diam vulputate ut pharetra sit amet aliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare aenean euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus mattis rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim sed faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu cursus euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras ornare arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse ultrices gravida dictum fusce ut placerat orci nulla pellentesque dignissim enim sit amet venenatis urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt augue interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis tincidunt id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus vestibulum sed arcu non odio euismod lacinia at quis risus sed vulputate odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id interdum velit laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nisl purus in mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac auctor augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odio eu feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis massa sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse in est ante in nibh mauris cursus mattis molestie a iaculis at erat pellentesque adipiscing commodo elit at imperdiet dui accumsan sit amet nulla facilisi morbi tempus iaculis urna id volutpat lacus laoreet non curabitur gravida arcu ac tortor dignissim convallis aenean et tortor at risus viverra adipiscing at in tellus integer feugiat scelerisque varius morbi enim nunc faucibus a pellentesque sit amet porttitor eget dolor morbi non arcu risus quis varius quam quisque id diam vel quam elementum pulvinar etiam non quam lacus suspendisse faucibus interdum posuere lorem ipsum dolor sit amet consectetur adipiscing elit duis tristique sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula ipsum a arcu cursus vitae congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas maecenas pharetra convallis posuere morbi leo urna molestie at elementum eu facilisis sed odio morbi quis commodo odio aenean sed adipiscing diam donec adipiscing tristique risus nec quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc consequat interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus sit amet dictum sit amet justo donec enim diam vulputate ut pharetra sit amet aliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare aenean euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus mattis rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim sed faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu cursus euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras ornare arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse ultrices gravida dictum fusce ut placerat orci nulla pellentesque dignissim enim sit amet venenatis urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt augue interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis tincidunt id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus vestibulum sed arcu non odio euismod lacinia at quis risus sed vulputate odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id interdum velit laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nisl purus in mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac auctor augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odio eu feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis massa sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse in est ante in nibh mauris cursus mattis molestie a iaculis at erat pellentesque adipiscing commodo elit at imperdiet dui accumsan sit amet nulla facilisi morbi tempus iaculis urna id volutpat lacus laoreet non curabitur gravida arcu ac tortor dignissim convallis aenean et tortor at risus viverra adipiscing at in tellus integer feugiat scelerisque varius morbi enim nunc faucibus a pellentesque sit amet porttitor eget dolor morbi non arcu risus quis varius quam quisque id diam vel quam elementum pulvinar etiam non quam lacus suspendisse faucibus interdum posuere lorem ipsum dolor sit amet consectetur adipiscing elit duis tristique sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula ipsum a arcu cursus vitae congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas maecenas pharetra convallis posuere morbi leo urna molestie at elementum eu facilisis sed odio morbi quis commodo odio aenean sed adipiscing diam donec adipiscing tristique risus nec quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc consequat interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus sit amet dictum sit amet justo donec enim diam vulputate ut pharetra sit amet aliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare aenean euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus mattis rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim sed faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu cursus euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras ornare arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse ultrices gravida dictum fusce ut placerat orci nulla pellentesque dignissim enim sit amet venenatis urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt augue interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis tincidunt id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus vestibulum sed arcu non odio euismod lacinia at quis risus sed vulputate odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id interdum velit laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nisl purus in mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac auctor augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odio eu feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis massa sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse in est ante in nibh mauris cursus mattis molestie a iaculis at erat pellentesque adipiscing commodo elit at imperdiet dui accumsan sit amet nulla facilisi morbi tempus iaculis urna id volutpat lacus laoreet non curabitur gravida arcu ac tortor dignissim convallis aenean et tortor at risus viverra adipiscing at in tellus integer feugiat scelerisque varius morbi enim nunc faucibus a pellentesque sit amet porttitor eget dolor morbi non arcu risus quis varius quam quisque id diam vel quam elementum pulvinar etiam non quam lacus suspendisse faucibus interdum posuere lorem ipsum dolor sit amet consectetur adipiscing elit duis tristique sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula ipsum a arcu cursus vitae congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas maecenas pharetra convallis posuere morbi leo urna molestie at elementum eu facilisis sed odio morbi quis commodo odio aenean sed adipiscing diam donec adipiscing tristique risus nec quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc consequat interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus sit amet dictum sit amet justo donec enim diam vulputate ut pharetra sit amet aliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare aenean euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus mattis rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim sed faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu cursus euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras ornare arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse ultrices gravida dictum fusce ut placerat orci nulla pellentesque dignissim enim sit amet venenatis urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt augue interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis tincidunt id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus vestibulum sed arcu non odio euismod lacinia at quis risus sed vulputate odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id interdum velit laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nisl purus in mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac auctor augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odio eu feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis massa sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse in est ante in nibh mauris cursus mattis molestie a iaculis at erat pellentesque adipiscing commodo elit at imperdiet dui accumsan sit amet nulla facilisi morbi tempus iaculis urna id volutpat lacus laoreet non curabitur gravida arcu ac tortor dignissim convallis aenean et tortor at risus viverra adipiscing at in tellus integer feugiat scelerisque varius morbi enim nunc faucibus a pellentesque sit amet porttitor eget dolor morbi non arcu risus quis varius quam quisque id diam vel quam elementum pulvinar etiam non quam lacus suspendisse faucibus interdum posuere lorem ipsum dolor sit amet consectetur adipiscing elit duis tristique sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula ipsum a arcu cursus vitae congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas maecenas pharetra convallis posuere morbi leo urna molestie at elementum eu facilisis sed odio morbi quis commodo odio aenean sed adipiscing diam donec adipiscing tristique risus nec quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc consequat interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus sit amet dictum sit amet justo donec enim diam vulputate ut pharetra sit amet aliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare aenean euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus mattis rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim sed faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu cursus euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras ornare arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse ultrices gravida dictum fusce ut placerat orci nulla pellentesque dignissim enim sit amet venenatis urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt augue interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis tincidunt id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus vestibulum sed arcu non odio euismod lacinia at quis risus sed vulputate odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id interdum velit laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nisl purus in mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac auctor augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odio eu feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis massa sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse in est ante in nibh mauris cursus mattis molestie a iaculis at erat pellentesque adipiscing commodo elit at imperdiet dui accumsan sit amet nulla facilisi morbi tempus iaculis urna id volutpat lacus laoreet non curabitur gravida arcu ac tortor dignissim convallis aenean et tortor at risus viverra adipiscing at in tellus integer feugiat scelerisque varius morbi enim nunc faucibus a pellentesque sit amet porttitor eget dolor morbi non arcu risus quis varius quam quisque id diam vel quam elementum pulvinar etiam non quam lacus suspendisse faucibus interdum posuere lorem ipsum dolor sit amet consectetur adipiscing elit duis tristique sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula ipsum a arcu cursus vitae congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus et malesuada f v dolor sit amet consectetur adipiscing elit duis tristique sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula ipsum a arcu cursus vitae congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus et malesuada et malesuada f v dolor sit amet consectetur adipiscing elit duis tristique sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula ipsum a arcu cursus vitae congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus et malesuada',
    jobDescriptionLabel: {
      jobTitleLabel: 'Job Title',
      jobOccupationLabel: 'Occupation',
      jobDescriptionLabel: 'Job Description & Requirements',
      thirdPartyEmployerLabel: "You're posting on behalf of another company",
    },
    thirdPartyEmployerEnabledFieldLabel: {
      uenOrCompanyNameLabel: 'Company Name / UEN',
      typeOfRegistrationLabel: 'Type of Registration',
      industryLabel: 'Industry',
    },
    previewUenOrCompanyName: 'CYPRESS07',
    previewTypeOfRegistration: 'Registry of Company',
    previewIndustry: 'Assembly and testing of semiconductors',
    recommendedUnSelectSkillsSection: ['SQL', 'MySQL', 'Software Development', 'jQuery', 'HTML', 'XML'],
    keyInformation: {
      jobPostDurationLabelCheck: 'Job Post Duration',
      jobPostDurationValueFill: '21 days',
      previewJobPostDurationValue: '21 Calendar Days',
      jobPostNoOfVacancyLabelCheck: 'Number of Vacancies',
      jobPostNoOfVacancyValueFill: '1',
      jobPostCategoryLabelCheck: 'Job Category (Max 5 categories)',
      jobPostCategoryLabelPreview: 'Job Category',
      jobPostCategoryValueSelect: ['Accounting / Auditing / Taxation', 'Customer Service'],
      jobPostCategoryPreviewValue: 'Accounting / Auditing / Taxation, Customer Service',
      jobPostPositionLevelLabelCheck: 'Position Level',
      jobPostPositionLevelValueFill: 'Senior Management',
      jobPostMinNoOfExpLabelCheck: 'Minimum Years of Experience',
      jobPostMinNoOfExpValueFill: '3',
      jobPostEmploymentTypeLabelCheck: 'Employment Type',
      jobPostEmploymentTypeValueFill: 'Part Time, Permanent',
      jobPostEmploymentTypeValueSelect: ['Part Time', 'Permanent'],
      jobPostMinOfQualificationLevelLabelCheck: 'Minimum Qualification Level',
      jobPostMinOfQualificationValueFill: 'Doctorate or equivalent',
      jobPostFieldOfStudyLabelCheck: 'Field of Study',
      jobPostFieldOfStudyValueFill: 'Accountancy',
      jobPostSalaryRangeLabelCheck: 'Gross Monthly Salary Range (SGD)',
      jobPostSalaryMaxMinPreviewValue: '$3000 - 5000',
      jobPostSalaryMinValueFill: '3000',
      jobPostSalaryMaxValueFill: '5000',
      jobPostGovSupportOptionalCheck: 'Government support (Optional)',
      jobPostGovSchemeSelected: 'Career Support Programme',
    },
    jobWorkplaceDetail: {
      jobPostWorkLocationLabel: 'Workplace Address',
      jobPostWorkLocationValue: 'Local',
      jobPostWorkLocationSameLocationLabel: 'The workplace address is the same as your company address',
      jobPostWorkLocationMultipleLocationLabel: 'This position involves multiple workplace locations in Singapore',
      jobPostWorkLocation: ['Local', 'Overseas'],
      jobPostPostalCodeLabel: 'Postal Code',
      jobPostPostalCodeFillValue: '520858',
      jobPostBlockOrHouseNoLabel: 'Block/House No.',
      jobPostBlockOrHouseNoFillValue: '858',
      jobPostStreetNameLabel: 'Street Name',
      jobPostStreetNameFillValue: 'Test view edit job post street',
      jobPostBuildingNameLabel: 'Building Name (optional)',
      jobPostBuildingNameFileValue: 'View Edit Job Post Test Building',
    },
    skillFromDropDown: {
      searchSkill: 'Testing',
      searchSkillSelect: 'Testing',
      selectSkillList: [
        'A/B Testing',
        'Agile Testing',
        'Backtesting',
        'Black Box Testing',
        'Drug Testing',
        'Game Testing',
        'Load Testing',
        'Manual Testing',
        'Mobile Testing',
      ],
    },
    previewSkills: [
      'Aerospace',
      'Java',
      'Java Application Development',
      'Java Enterprise Edition',
      'Java Software Development',
      'Java Web Services',
      'JavaBeans',
      'JavaFX',
      'JavaScript',
      'JavaScript Frameworks',
      'JavaScript Libraries',
      'JavaSE',
    ],
  },
  jobPostMaxEditCount: {
    Id: 'MCF-2900-9000017',
    jobTitle: '01Do not touch this job, this is for integration test to verify view edit job post - open',
  },
  jobPostEditWithExpiryScheme: {
    Id: 'MCF-2900-9000018',
    ...editJobPostUpdateData,
  },
  jobPostMSFOpenState: {
    Id: 'JOB-2900-9000003',
    jobTitle: '01Do not touch this job, this is for integration test for open job - open',
    jobDescriptionValueFill:
      'Testing Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id enim justo. Vestibulum sed vestibulum justo. Donec posuere arcu a ligula tincidunt maximus. Praesent iaculis lacus eget erat pellentesque, hendrerit tincidunt diam feugiat. Nam tincidunt dictum quam id posuere. Mauris vitae nisl et ligula dignissim accumsan sit amet at diam. Other Requirements:  1. Manual Testing 2. Automation Testing',
    ...sharedJobPostData,
  },
  jobPostMSFCloseState: {
    Id: 'JOB-2900-9000019',
    jobTitle: '01Do not touch this job, this is for integration test for close job - close',
    jobDescriptionValueFill:
      'Testing Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id enim justo. Vestibulum sed vestibulum justo. Donec posuere arcu a ligula tincidunt maximus. Praesent iaculis lacus eget erat pellentesque, hendrerit tincidunt diam feugiat. Nam tincidunt dictum quam id posuere. Mauris vitae nisl et ligula dignissim accumsan sit amet at diam. Other Requirements: Java, c++, Javascript',
    ...sharedJobPostData,
  },
  jobPostMCFCloseState: {
    Id: 'MCF-2900-9001000',
    jobTitle: 'CLOSED Job to test Repost Counter with RepostCount equal 0',
    jobDescriptionValueFill:
      'Testing Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id enim justo. Vestibulum sed vestibulum justo. Donec posuere arcu a ligula tincidunt maximus. Praesent iaculis lacus eget erat pellentesque, hendrerit tincidunt diam feugiat. Nam tincidunt dictum quam id posuere. Mauris vitae nisl et ligula dignissim accumsan sit amet at diam.',
    ...sharedJobPostData,
    keyInformation: {
      ...sharedJobPostData.keyInformation,
      previewJobPostDurationValue: '30 Calendar Days',
    },
    previewSkills: ['Aerospace'],
  },
};
