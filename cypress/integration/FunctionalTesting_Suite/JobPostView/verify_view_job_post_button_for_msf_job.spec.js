import {
  verifyMSFViewJobPostButton,
  verifyMSFPreviewSkillsSection,
  viewJobPostingClickAndLoaded,
  viewJobPostButtonSelector,
  editJobPostButtonSelector,
  editRemainCounterSelector,
} from '../../../support/shared/job_details_page.functions';
import jobs from '../../../fixtures/JobList';
import {
  previewJobDescriptionDetails,
  previewKeyInformationDetails,
  previewWorkplaceDetails,
} from '../../../support/shared/job_post_preview_page.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Job Post View Button for MSF Jobs - UI', function() {
  before(() => {
    cy.home();
    cy.login('Job Post Close Account', 'Job Admin');
  });
  beforeEach(() => {
    cy.home();
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('UI checking - verify view job post button and not exists edit count and edit job post button for MSF job post open', function() {
    cy.searchForJobs('JOB-2900-9000003');
    verifyMSFViewJobPostButton(jobs.jobPostMSFOpenState.Id);
    viewJobPostingClickAndLoaded(viewJobPostButtonSelector);
    cy.get(editJobPostButtonSelector).should('not.exist');
    cy.get(editRemainCounterSelector).should('not.exist');
    previewJobDescriptionDetails(jobs.jobPostMSFOpenState);
    verifyMSFPreviewSkillsSection();
    previewKeyInformationDetails(jobs.jobPostMSFOpenState.keyInformation);
    previewWorkplaceDetails(jobs.jobPostMSFOpenState.jobWorkplaceDetail);
  });
  it('UI checking - verify view job post button and not exists edit count and edit job post button for MSF job post close', function() {
    cy.clickOnJobTab('Closed');
    verifyMSFViewJobPostButton(jobs.jobPostMSFCloseState.Id);
    viewJobPostingClickAndLoaded(viewJobPostButtonSelector);
    cy.get(editJobPostButtonSelector).should('not.exist');
    cy.get(editRemainCounterSelector).should('not.exist');
    previewJobDescriptionDetails(jobs.jobPostMSFCloseState);
    verifyMSFPreviewSkillsSection();
    previewKeyInformationDetails(jobs.jobPostMSFCloseState.keyInformation);
    previewWorkplaceDetails(jobs.jobPostMSFCloseState.jobWorkplaceDetail);
  });
});
