import * as JobsPageNavigate from '../../../support/shared/job_page.functions';
import jobs from '../../../fixtures/JobList';

describe('Job Post View/Edit Button for MCF Jobs - UI', function() {
  beforeEach(() => {
    cy.home();
    cy.login('Manage Applicant Status', 'Job Admin');
  });
  afterEach(() => {
    cy.logout();
  });

  it('UI checking - verify view/edit job post dropdown button', function() {
    cy.clickOnJobTab('Open');
    cy.searchForJobs(jobs.jobApplicantStatusUpdate1.Id);
    JobsPageNavigate.shouldHaveCardLoaderLoaded();
    JobsPageNavigate.clickJobByJobPostID(jobs.jobApplicantStatusUpdate1.Id);
    cy.skipApplicantsOnboarding();
    JobsPageNavigate.shouldHaveCardLoaderLoaded();
    JobsPageNavigate.mouseoverOnManageMenuButton();
    JobsPageNavigate.verifyManageJobPostViewEdit();
  });
});
