import faker from 'faker';
import {step} from 'mocha-steps';
import path from 'path';
import {
  COMPANY_PROFILE_URL,
  EDIT_COMPANY_PROFILE_LINK_SELECTOR,
  COMPANY_PROFILE_LOGO_SELECTOR,
  COMPANY_PROFILE_LOGO_LABEL_SELECTOR,
  COMPANY_PROFILE_LOGO_UPLOADER_SELECTOR,
  COMPANY_PROFILE_LOGO_IMG_SELECTOR,
  COMPANY_PROFILE_DESCRIPTION_SELECTOR,
  COMPANY_PROFILE_DESCRIPTION_LABEL_SELECTOR,
  COMPANY_PROFILE_DESCRIPTION_FIELD_SELECTOR,
  COMPANY_PROFILE_DESCRIPTION_CHARACTER_COUNT_SELECTOR,
  COMPANY_PROFILE_NUM_EMPLOYEE_SELECTOR,
  COMPANY_PROFILE_NUM_EMPLOYEE_LABEL_SELECTOR,
  COMPANY_PROFILE_NUM_EMPLOYEE_FIELD_SELECTOR,
  COMPANY_PROFILE_WEBSITE_SELECTOR,
  COMPANY_PROFILE_WEBSITE_LABEL_SELECTOR,
  COMPANY_PROFILE_WEBSITE_FIELD_SELECTOR,
  COMPANY_PROFILE_SUBMIT_BUTTON_SELECTOR,
  COMPANY_PROFILE_UPDATE_SUCCESS_MODAL_SELECTOR,
  COMPANY_PROFILE_UPDATE_SUCCESS_MODAL_OK_BUTTON_SELECTOR,
} from '../../../support/shared/company_profile_page';
import {verifyStaticNudgeContent} from '../../../support/shared/common.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {COMPANY_PROFILE_ALIAS, VIRUS_SCANNER_ALIAS} from '../../../support/commands/common.commands.js';
import {updateCompanyInfo} from '../../../../cypress/support/manageData/company.seed';

describe('Company Profile Page', () => {
  before(() => {
    cy.home();
    cy.captureNetworkRequest();
    cy.login('Test may update data', 'Job Admin');
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  step('click Edit company profile link', () => {
    const EDIT_COMPANY_PROFILE_LINK_TEXT = 'Edit company profile';
    cy.get(EDIT_COMPANY_PROFILE_LINK_SELECTOR)
      .should('have.text', EDIT_COMPANY_PROFILE_LINK_TEXT)
      .click();
    cy.url().should('include', COMPANY_PROFILE_URL);
  });

  step('should contain staticNudge on the side', () => {
    verifyStaticNudgeContent();
  });

  describe('Fields', () => {
    step('logo uploader', () => {
      cy.get(COMPANY_PROFILE_LOGO_SELECTOR).should('exist');
      cy.get(COMPANY_PROFILE_LOGO_LABEL_SELECTOR).should('have.text', 'Company Logo');
      cy.get(COMPANY_PROFILE_LOGO_UPLOADER_SELECTOR).should('exist');
    });

    step('description', () => {
      cy.get(COMPANY_PROFILE_DESCRIPTION_SELECTOR).should('exist');
      cy.get(COMPANY_PROFILE_DESCRIPTION_LABEL_SELECTOR).should('have.text', 'Company Description');
      cy.get(COMPANY_PROFILE_DESCRIPTION_FIELD_SELECTOR).should('exist');
      cy.get(COMPANY_PROFILE_DESCRIPTION_CHARACTER_COUNT_SELECTOR)
        .invoke('text')
        .should('match', /^[0-9,]+ \/ 4,000 characters$/);
    });

    step('number of employees', () => {
      cy.get(COMPANY_PROFILE_NUM_EMPLOYEE_SELECTOR).should('exist');
      cy.get(COMPANY_PROFILE_NUM_EMPLOYEE_LABEL_SELECTOR).should('have.text', 'Number of Employees (optional)');
      cy.get(COMPANY_PROFILE_NUM_EMPLOYEE_FIELD_SELECTOR).should('exist');
    });

    step('website', () => {
      cy.get(COMPANY_PROFILE_WEBSITE_SELECTOR).should('exist');
      cy.get(COMPANY_PROFILE_WEBSITE_LABEL_SELECTOR).should('have.text', 'Company Website (optional)');
      cy.get(COMPANY_PROFILE_WEBSITE_FIELD_SELECTOR).should('exist');
    });
  });

  describe('Empty Company Profile', () => {
    before(() => {
      cy.clearCookie('access-token');
      cy.captureCompanyProfile().home();
      cy.login('Empty Company Profile', 'Job Admin');
      cy.visit(COMPANY_PROFILE_URL).wait(`@${COMPANY_PROFILE_ALIAS}`);
    });

    step('all fields should be empty', () => {
      cy.get(COMPANY_PROFILE_LOGO_IMG_SELECTOR).should('not.exist');

      cy.get(COMPANY_PROFILE_DESCRIPTION_FIELD_SELECTOR).should('have.text', '');

      cy.get(COMPANY_PROFILE_NUM_EMPLOYEE_FIELD_SELECTOR).should('have.text', '');

      cy.get(COMPANY_PROFILE_WEBSITE_FIELD_SELECTOR).should('have.text', '');
    });
  });

  describe('Company Profile with data', () => {
    before(() => {
      cy.clearCookie('access-token');
      cy.home();
      cy.login('Empty Job Test Account', 'Job Admin');
    });
    afterEach(() => {
      cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
      Cypress.Cookies.preserveOnce('access-token');
    });
    after(() => {
      cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
      cy.clearCookie('access-token');
    });
    describe('should display existing company profile details', () => {
      const uen = '100000004C';
      const saveCompanyDetailsInDB = {
        description: faker.lorem
          .sentences(25)
          .slice(0, 150)
          .trim(),
        file: 'https://s3-ap-southeast-1.amazonaws.com/ojmp-data/5d826b09c62e88ed3f7b92ccec5c4dfc.jpg',
        employeeCount: 5,
        companyUrl: 'http://www.test.com',
        updatedSrc: '5d826b09c62e88ed3f7b92ccec5c4dfc.jpg',
      };
      step('update company profile details in DB', () => {
        updateCompanyInfo(saveCompanyDetailsInDB, uen);
      });
      step('open company profile page and validate existing details', () => {
        cy.visit(COMPANY_PROFILE_URL);
        validateCompanyProfileDetails(saveCompanyDetailsInDB);
      });
    });
    describe('should update new company profile data and validate company profile data are updated', () => {
      const saveCompanyDetailsInPage = {
        description: faker.lorem
          .sentences(25)
          .slice(0, 150)
          .trim(),
        file: 'companyProfile/logo/SampleJPGImage_50kbmb.jpg',
        employeeCount: '3',
        companyUrl: 'http://www.testupdate.com',
        updatedSrc: 'CYPRESS04.jpg',
        fillOption: {paste: true},
      };

      step('should enter new company details in company profile page and save', () => {
        cy.captureVirusScannerRequest()
          .fixture(saveCompanyDetailsInPage.file, 'base64')
          .then((validFile) => {
            cy.get(COMPANY_PROFILE_LOGO_UPLOADER_SELECTOR).uploadFile(
              validFile,
              path.basename(saveCompanyDetailsInPage.file),
            );
          })
          .wait(`@${VIRUS_SCANNER_ALIAS}`); // wait till virus scanner completes
        cy.get(COMPANY_PROFILE_DESCRIPTION_FIELD_SELECTOR).fill(
          saveCompanyDetailsInPage.description,
          saveCompanyDetailsInPage.fillOption,
        );
        cy.get(COMPANY_PROFILE_NUM_EMPLOYEE_FIELD_SELECTOR).fill(saveCompanyDetailsInPage.employeeCount);
        cy.get(COMPANY_PROFILE_WEBSITE_FIELD_SELECTOR).fill(
          saveCompanyDetailsInPage.companyUrl,
          saveCompanyDetailsInPage.fillOption,
        );
        cy.get(COMPANY_PROFILE_SUBMIT_BUTTON_SELECTOR).click();
        cy.get(COMPANY_PROFILE_UPDATE_SUCCESS_MODAL_SELECTOR)
          .should('contain', 'Company Profile Saved')
          .and('contain', 'You have successfully saved your company profile');
        cy.get(COMPANY_PROFILE_UPDATE_SUCCESS_MODAL_OK_BUTTON_SELECTOR).click();
      });
      step('should open company profile page and validate updated company profile details', () => {
        cy.visit(COMPANY_PROFILE_URL);
        validateCompanyProfileDetails(saveCompanyDetailsInPage);
      });
    });
  });

  describe('Buttons', () => {
    step(
      'has Cancel button and a confirmation will show up when clicked. Once declined should remain on Company Profile page',
      () => {
        cy.on('window:confirm', (_) => {
          return false;
        });

        cy.get('[data-cy=company-profile-cancel]').click();
        cy.location('href').should('include', '/company-profile');
      },
    );

    step(
      'has Cancel button and a confirmation will show up when clicked. Once accepted should navigate to Jobs page',
      () => {
        cy.on('window:confirm', (_) => {
          return true;
        });

        cy.get('[data-cy=company-profile-cancel]').click();
        cy.location('href').should('not.include', '/company-profile');
      },
    );
  });

  describe('Saving Company Profile', () => {
    before(() => {
      cy.captureNetworkRequest();
      cy.clearCookie('access-token');
      cy.captureCompanyProfile().home();
      cy.login('Account with a lot of jobs', 'Job Admin');
      cy.visit(COMPANY_PROFILE_URL).wait(`@${COMPANY_PROFILE_ALIAS}`);
    });

    step('saving company profile fail with error message', () => {
      const UEN = '100000003C';
      cy.get(COMPANY_PROFILE_DESCRIPTION_FIELD_SELECTOR)
        .fill(faker.lorem.paragraphs(150).substring(0, 4000), {paste: true})
        .blur();
      cy.server();
      cy.route({
        method: 'PATCH',
        url: `**/companies/${UEN}`,
        status: 504,
        response: {},
      });
      cy.get(COMPANY_PROFILE_SUBMIT_BUTTON_SELECTOR).click();
      cy.verifyNotificationGrowl('Temporarily unable to save company profile. Please try again.');
    });
  });
});

const validateCompanyProfileDetails = ({updatedSrc, description, employeeCount, companyUrl}) => {
  updatedSrc &&
    cy
      .get(COMPANY_PROFILE_LOGO_IMG_SELECTOR)
      .should('have.attr', 'src')
      .then((srctext) => {
        expect(srctext).to.contain(updatedSrc);
      });
  description && cy.get(COMPANY_PROFILE_DESCRIPTION_FIELD_SELECTOR).should('have.text', description);
  employeeCount && cy.get(COMPANY_PROFILE_NUM_EMPLOYEE_FIELD_SELECTOR).should('have.value', `${employeeCount}`);
  companyUrl && cy.get(COMPANY_PROFILE_WEBSITE_FIELD_SELECTOR).should('have.value', `${companyUrl}`);
};
