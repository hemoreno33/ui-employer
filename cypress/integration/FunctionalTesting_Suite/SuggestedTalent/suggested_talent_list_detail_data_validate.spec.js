import * as ManageJobsNavigate from '../../../support/shared/manage_applicants_page.functions';
import * as JobsPageNavigate from '../../../support/shared/job_page.functions';
import {getPagination, isCurrentActive, shouldHasPaginationButtonCount} from '../../../support/shared/common.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('SuggestedTalent - List/Detail view data validation', function() {
  before(() => {
    cy.captureNetworkRequest();
    cy.fixture('Candidate.json').as('Candidate');
    cy.home();
    cy.login('Manage Applicant Test Account', 'Job Admin');
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('Validate the list data and the detail data for suggested talent', function() {
    cy.log('Check 2nd job in all jobs page should have suggested talent');
    JobsPageNavigate.getJobListSuggestedTalentCountWithIndex(1).should('have.text', '60');
    cy.clickOnJobItem(1);
    cy.skipApplicantsOnboarding();
    cy.clickOnSuggestedTalentTab();
    JobsPageNavigate.shouldHaveCardLoaderLoading();
    JobsPageNavigate.cardLoaderShouldNotExist();
    cy.skipApplicantsOnboarding();
    // ManageJobsNavigate.getSuggestedTalentTabCount().should('have.text', '60');
    cy.log('No talent selected when click on suggested talent tab by default');
    ManageJobsNavigate.shouldHaveEmptyTalentRightPanel();
    ManageJobsNavigate.verifyCandidateTypeLabelLeftPanel('Talent');
    cy.clickOnCandidate(0);
    ManageJobsNavigate.getCandidateFromList(0).then(
      ManageJobsNavigate.shouldHaveCandidateListItemInfo(this.Candidate.SuggestedTalent.Talent1),
    );
    ManageJobsNavigate.verifyCandidateTypeLabelRightPanel('Talent');
    ManageJobsNavigate.shouldHaveCandidateRightTopPanelInfo(this.Candidate.SuggestedTalent.Talent1);
    ManageJobsNavigate.shouldHaveCandidateRightBottomPanelInfo(this.Candidate.SuggestedTalent.Talent1);

    cy.clickOnCandidate(1);
    ManageJobsNavigate.getCandidateFromList(1).then(
      ManageJobsNavigate.shouldHaveCandidateListItemInfo(this.Candidate.SuggestedTalent.Talent2),
    );
    ManageJobsNavigate.verifyCandidateTypeLabelRightPanel('Talent');
    ManageJobsNavigate.shouldHaveCandidateRightTopPanelInfo(this.Candidate.SuggestedTalent.Talent2);
    ManageJobsNavigate.shouldHaveCandidateRightBottomPanelInfo(this.Candidate.SuggestedTalent.Talent2);
  });

  it('Validate suggested talent pagination to be a maximum of 3 pages', function() {
    cy.home();
    cy.clickOnJobItemSuggestedTalent(1);
    JobsPageNavigate.shouldHaveCardLoaderLoading();
    JobsPageNavigate.cardLoaderShouldNotExist();
    cy.skipApplicantsOnboarding();

    getPagination(0)
      .should('have.text', '1')
      .then(isCurrentActive);
    getPagination(1).should('have.text', '2');
    getPagination(2).should('have.text', '3');
    getPagination(3).should('have.text', '❯');

    cy.captureGraphqlRequest();
    cy.clickOnPaginationIndex(2); //click on page 3
    cy.waitGraphqlRequest('getSuggestedTalents');

    getPagination(0).should('have.text', '❮');
    getPagination(1).should('have.text', '1');
    getPagination(2).should('have.text', '2');
    getPagination(3)
      .should('have.text', '3')
      .then(isCurrentActive);

    shouldHasPaginationButtonCount(4);
  });
});
