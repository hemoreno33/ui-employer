import * as ApplicantNavigate from '../../../support/shared/manage_applicants_page.functions';
import {checkLogoutPage} from '../../../support/shared/common.functions';
import * as JobsPageNavigate from '../../../support/shared/job_page.functions';

describe('Talent - on boarder test', function() {
  beforeEach(() => {
    cy.home();
    cy.login('Manage Applicant Test Account', 'Job Admin');
  });
  afterEach(() => {
    cy.logout();
  });
  describe('Talent - on boarder test 1: check the onboarder content ', function() {
    it('Should have on boarder when go to Suggested Talent view 1st time', function() {
      cy.clickOnJobItem(1);
      cy.skipApplicantsOnboarding();
      cy.clickOnSuggestedTalentTab();
      JobsPageNavigate.shouldHaveCardLoaderLoading();
      JobsPageNavigate.cardLoaderShouldNotExist();
      ApplicantNavigate.shouldHaveSuggestedTalentOnboarderContent();
      cy.clickOnAllJobs('side');
      cy.clickOnJobItem(1);
      cy.clickOnSuggestedTalentTab();
      JobsPageNavigate.shouldHaveCardLoaderLoading();
      JobsPageNavigate.cardLoaderShouldNotExist();
      ApplicantNavigate.shouldNotHaveOnboarderModule();
      cy.log('Click on the tutorial link should be able to show the on boarder');
      cy.get('[data-cy="onboarding-link"]')
        .contains('How are suggested talents identified for my job?')
        .click(); //click on the suggested talent onboarder link;
      ApplicantNavigate.shouldHaveSuggestedTalentOnboarderContent();
    });
  });

  describe('Talent - on boarder test 2: onboarder should not appear after skip button pressed', function() {
    it('Should have on boarder when go to Suggested Talent view', function() {
      cy.clickOnJobItem(1);
      cy.skipApplicantsOnboarding();
      cy.clickOnSuggestedTalentTab();
      JobsPageNavigate.shouldHaveCardLoaderLoading();
      JobsPageNavigate.cardLoaderShouldNotExist();
      cy.skipApplicantsOnboarding();
      cy.logout();
      checkLogoutPage();
      cy.home();
      cy.login('Manage Applicant Test Account', 'Job Admin');
      cy.clickOnJobItem(1);
      cy.clickOnSuggestedTalentTab();
      JobsPageNavigate.shouldHaveCardLoaderLoading();
      JobsPageNavigate.cardLoaderShouldNotExist();
      ApplicantNavigate.shouldNotHaveOnboarderModule();
    });
  });
});
