describe('Unauthorised Page', function() {
  before(() => {
    const options = Cypress.env('username')
      ? {
          auth: {
            username: Cypress.env('username'),
            password: Cypress.env('password'),
          },
        }
      : {};
    cy.visit('/jobs', options);
  });

  it('redirects to unauthorised page', function() {
    cy.url().should('eq', Cypress.config('baseUrl') + '/unauthorised');
    cy.get('[data-cy="login-with-corppass-link"]').click();
    cy.url().should('include', Cypress.env('corppassUrl'));
  });
});
