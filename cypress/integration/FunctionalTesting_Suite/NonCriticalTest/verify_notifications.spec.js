import {
  closeFeatureBanner,
  verifyFeatureBanner,
  verifyFeatureBannerShouldNotExist,
  verifyMaintenanceBanner,
  verifyMaintenancePage,
} from '../../../support/shared/notifications.functions';
import {step} from 'mocha-steps';

// The test cases have their date adjusted within the date range of their corresponding test data in the QA notification spreadsheet.
describe('Validate - Feature notification', () => {
  step('should display feature notification on home page', () => {
    cy.clock(new Date('2019-01-07').valueOf()).then(() => {
      cy.home();
      verifyFeatureBanner('[Cypress Test] Should display this if there is an active employer feature notification');
    });
  });

  step('should display feature notification after login and after close should not display', () => {
    cy.clock(new Date('2019-01-07').valueOf()).then(() => {
      cy.login('Manage Applicant Status', 'Job Admin');
      verifyFeatureBanner('[Cypress Test] Should display this if there is an active employer feature notification');
      closeFeatureBanner();
      verifyFeatureBannerShouldNotExist();
      cy.logout();
    });
  });

  step('should display feature notification on next day in home page and also should display after login', () => {
    cy.clock(new Date('2019-01-09').valueOf()).then(() => {
      cy.home();
      verifyFeatureBanner('[Cypress Test] Should display this if there is an active employer feature notification');
      cy.login('Manage Applicant Status', 'Job Admin');
      verifyFeatureBanner('[Cypress Test] Should display this if there is an active employer feature notification');
      cy.logout();
    });
  });
});

describe('Validate - Maintenance notification', () => {
  step('should display maintenance notification if it is within the date range', () => {
    cy.clock(new Date('2019-01-22').valueOf()).then(() => {
      cy.home();
      verifyMaintenanceBanner('[Cypress Test] Should display this if there is an active employer maintenance');
    });
  });

  step('should display maintenance notification after login', () => {
    cy.clock(new Date('2019-01-22').valueOf()).then(() => {
      cy.login('Manage Applicant Status', 'Job Admin');
      verifyMaintenanceBanner('[Cypress Test] Should display this if there is an active employer maintenance');
      cy.logout();
    });
  });

  step('should not display maintenance notification after the end date after re-login', () => {
    cy.clock(new Date('2019-02-02').valueOf()).then(() => {
      cy.home().login('Manage Applicant Status', 'Job Admin');
      verifyFeatureBannerShouldNotExist();
      cy.logout();
    });
  });
});

describe('Validate - Maintenance page', () => {
  step('should display maintenance page it is within the downtime date range', () => {
    cy.clock(new Date('2019-01-31').valueOf()).then(() => {
      cy.home();
      verifyMaintenancePage('[Cypress Test] Should display this if there is an active employer maintenance');
    });
  });
  step('should not display maintenance page after the end date and should be able to login', () => {
    cy.clock(new Date('2019-02-02').valueOf()).then(() => {
      cy.home()
        .login('Manage Applicant Status', 'Job Admin')
        .logout();
    });
  });
});
