import {verifyFooterLinks} from '../../../support/shared/footer.functions';
import {verifyHeaderLinks} from '../../../support/shared/header.functions';

describe('Landing Page', function() {
  before(() => {
    cy.home();
  });

  it('has MCF logo, once clicked should stay on same page', () => {
    cy.get('[data-cy=mcf-logo]').click();
    cy.url().should('include', Cypress.config('baseUrl'));
  });

  it('has Adapt and Grow logo, and should link to wsg.gov.sg website', () => {
    cy.get('[data-cy=aag-logo]').should('have.attr', 'href', 'http://www.wsg.gov.sg/adapt-and-grow.html');
  });

  it('has employer carousel', () => {
    cy.get('[data-cy="employer-carousel"]').should('exist');
    cy.get('[data-cy="employer-carousel"]')
      .find('[data-cy="banner-0-title"]')
      .contains('Discover suggested talents');
    cy.get('[data-cy="employer-carousel"]')
      .find('[data-cy="banner-0-text"]')
      .contains(
        'Not enough applicants? Find a list of potential candidates who are open to being contacted for opportunities.',
      );
    cy.get('[data-cy="employer-carousel"] :nth-child(2) > button').click();
    cy.get('[data-cy="employer-carousel"]')
      .find('[data-cy="banner-1-title"]')
      .contains('Identify suitable applicants');
    cy.get('[data-cy="employer-carousel"]')
      .find('[data-cy="banner-1-text"]')
      .contains(
        'Too many applicants? Find out which applicants are among top matches and work your way down the list.',
      );
  });

  it('has Login with CorpPass', () => {
    cy.get('[data-cy="banner-corppass-login"]')
      .should('have.attr', 'class', 'tc bg-black-30 pa4 z-1 banner__banner-login-container___13VRR')
      .find('h3.f3.fw3.white.pb2')
      .invoke('text')
      .should('equal', 'Try it out now');
    cy.get('[data-cy="banner-corppass-login"]')
      .find('[data-cy="login-with-corppass"]')
      .invoke('text')
      .should('equal', 'Log in with CorpPass');
    cy.get('[data-cy="banner-corppass-login"]')
      .find('p.f6.white-80')
      .invoke('text')
      .should('equal', 'Don’t have a CorpPass account?Get started.');
    cy.get('[data-cy="banner-corppass-login-no-account"]')
      .should('have.attr', 'href', 'https://www.corppass.gov.sg/')
      .and('have.attr', 'target', '_blank');
  });

  it('has Employer Content Pieces', () => {
    cy.get('[data-cy="employer-content"]').should(($content) => {
      expect($content).to.have.length(3);
    });
  });

  it('has Footer & Header with various links', () => {
    verifyFooterLinks();
    verifyHeaderLinks();
    cy.loginFromNavbar('Job Post Testing Account');
    verifyFooterLinks();
    verifyHeaderLinks();
    cy.logout();
  });

  it('has login link, and after login should not have login and switch to jobseeker link', () => {
    cy.loginFromNavbar('Job Post Testing Account');
    cy.getCookie('access-token').should('exist');
    cy.get('[data-cy=navbar-login-with-corppass]').should('not.exist');
    cy.logout();
  });
});
