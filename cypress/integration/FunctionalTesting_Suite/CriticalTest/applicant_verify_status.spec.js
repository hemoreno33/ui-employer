import * as ApplicantNavigate from '../../../support/shared/manage_applicants_page.functions';
import {searchAndOpenJobOnTab, shouldHaveCardLoaderLoaded} from '../../../support/shared/job_page.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {applicantStatus} from '../../../support/shared/manage_applicants_page.functions';
import {createAJobAndApplyFor} from '../../../support/manageData/applicant.seed';
import {apiJobs} from '../../../support/api/jobsApi';

const UEN = '100000008C';
const jobPosts = ['Open', 'Closed'];
const applicants = [
  {nric: 'S1643276H', bookmarkIndex: 0},
  {nric: 'S5704830A', bookmarkIndex: 1},
  {nric: 'S7566272F', bookmarkIndex: 2},
  {nric: 'S1210455C', bookmarkIndex: 3},
];
jobPosts.forEach((tab) => {
  describe(`Manage Applicants Status - ${tab} job`, function() {
    let jobPostId;
    before(() => {
      cy.home();
      cy.login('Manage Applicant Status', 'Job Admin');
      createAJobAndApplyFor(
        UEN,
        applicants.map((applicant) => applicant.nric),
      ).then((response) => {
        jobPostId = response.job.metadata.jobPostId;
        if (tab === 'Closed') {
          apiJobs.closeJob(response.job.uuid, false);
        }
        cy.visit('/');
      });
    });
    beforeEach(() => {
      cy.home();
      openJobpost(tab, jobPostId);
    });
    afterEach(() => {
      cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
      Cypress.Cookies.preserveOnce('access-token');
    });
    after(() => {
      cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
      cy.clearCookie('access-token');
    });

    it('UI checking - verify list of applicants status', function() {
      cy.clickOnCandidate(0);
      ApplicantNavigate.verifyApplicantStatusList();
    });

    it('UI checking - update job applicants status as "Shortlisted" and re-login page and verify its updated right and left panel', function() {
      cy.clickOnCandidate(0);
      applicantStatus.selectApplicantStatus('Shortlisted');
      cy.reLogin('Manage Applicant Status', 'Job Admin');
      openJobpost(tab, jobPostId);
      cy.clickOnCandidate(0);
      applicantStatus.verifyApplicantStatusRightPaneUpdated('Shortlisted');
      applicantStatus.verifyApplicantStatusLeftPanelUpdated('Shortlisted', 0);
    });

    it('UI checking - update job applicants status as "Unsuccessful" and re-login page and verify its updated right and left panel', function() {
      cy.clickOnCandidate(1);
      applicantStatus.selectApplicantStatus('Unsuccessful');
      cy.reLogin('Manage Applicant Status', 'Job Admin');
      openJobpost(tab, jobPostId);
      cy.clickOnCandidate(1);
      applicantStatus.verifyApplicantStatusRightPaneUpdated('Unsuccessful');
      applicantStatus.verifyApplicantStatusLeftPanelUpdated('Unsuccessful', 1);
    });

    it('UI checking - update job applicants status as "Hired" and re-login page and verify its updated right and left panel', function() {
      cy.clickOnCandidate(2);
      applicantStatus.selectApplicantStatus('Hired');
      cy.reLogin('Manage Applicant Status', 'Job Admin');
      openJobpost(tab, jobPostId);
      cy.clickOnCandidate(2);
      applicantStatus.verifyApplicantStatusRightPaneUpdated('Hired');
      applicantStatus.verifyApplicantStatusLeftPanelUpdated('Hired', 2);
    });

    it('UI checking - job applicants status right and left label as empty when applicant status are not updated', function() {
      cy.clickOnCandidate(3);
      applicantStatus.verifyApplicantStatusNotExist(3);
      applicantStatus.verifyApplicantStatusRightPaneUpdated('Application status');
    });
  });
});

const openJobpost = (tabJobPost, jobPostId) => {
  searchAndOpenJobOnTab(tabJobPost, jobPostId);
  cy.skipApplicantsOnboardingIfExist();
  shouldHaveCardLoaderLoaded();
};
