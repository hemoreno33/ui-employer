import {validateLoginFirstTimeScreenFields} from '../../../support/shared/common.functions';
import {step} from 'mocha-steps';
import faker from 'faker';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('first time login', function() {
  const ALL_JOBS_PAGE_URL = '/jobs';
  before(() => {
    cy.home();
    cy.login('First Time Login Account', 'Job Admin');
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  step('should land on terms and conditions page', () => {
    cy.contains('First time here ?').should('be.visible');
  });

  describe('terms and condition page', () => {
    const EMAIL_INPUT_SELECTOR = 'input#email';
    const CONTACT_NUMBER_INPUT_SELECTOR = 'input#contact-number';
    const TERMS_AND_CONDITIONS_CHECKBOX_SELECTOR = 'label[for=terms-and-conditions]';
    const TRIPARTITE_CHECKBOX_SELECTOR = 'label[for=checkbox-tripartite]';
    const JOB_LIST_SELECTOR = '[data-cy=job-list-items]';

    step('should have all the fields along with t&c', () => {
      validateLoginFirstTimeScreenFields();
    });

    step('go to all jobs page after submit', () => {
      cy.get(EMAIL_INPUT_SELECTOR).type(faker.internet.email());
      cy.get(CONTACT_NUMBER_INPUT_SELECTOR).type(faker.phone.phoneNumber('6#######'));
      cy.get(TERMS_AND_CONDITIONS_CHECKBOX_SELECTOR).click();
      cy.get(TRIPARTITE_CHECKBOX_SELECTOR).click();

      cy.get('button[type=submit]').click();

      cy.url().should('include', ALL_JOBS_PAGE_URL);
      cy.get(JOB_LIST_SELECTOR).should('exist');
    });
  });
});
