import * as JobsPageNavigate from '../../../support/shared/job_page.functions';
import jobs from '../../../fixtures/JobList';
import {verifyFooterLinks} from '../../../support/shared/footer.functions';

describe('Jobs Page - Overall UI', function() {
  before(() => {
    cy.home();
    cy.login('Manage Applicant Test Account', 'Job Admin');
  });
  after(() => {
    cy.logout();
  });

  it('Should have all UI, context showing correctly in job list page and footer', function() {
    verifyFooterLinks();
    JobsPageNavigate.cardLoaderShouldNotExist();
    JobsPageNavigate.checkHasUserName('Republic Poly Lecturer');
    // JobsPageNavigate.checkHasCompanyName('CYPRESS00'); //CYPRESS01 TEST
    // Need to add check for company logo
    JobsPageNavigate.checkHasSideBarAllJobs();
    JobsPageNavigate.checkHasSortByOption();
    JobsPageNavigate.checkHasSortBySelected('Auto-close Date');
    JobsPageNavigate.checkHasSearchBarAndSearchButton();
    JobsPageNavigate.validateJobListExistence('found'); //default in open tab
    JobsPageNavigate.checkHasOpenTabAndCountSameAsJobListing();

    JobsPageNavigate.verifyJobListField(jobs.J1, 0);
    JobsPageNavigate.verifyJobListField(jobs.J2, 1);
    JobsPageNavigate.verifyJobListField(jobs.J3, 2);
  });
});
