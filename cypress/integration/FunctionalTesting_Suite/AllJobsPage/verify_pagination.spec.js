import * as JobsPageNavigate from '../../../support/shared/job_page.functions';
import {getPagination, isCurrentActive} from '../../../support/shared/common.functions';

function checkPaginationFuc() {
  JobsPageNavigate.cardLoaderShouldNotExist();
  JobsPageNavigate.validateJobListExistence('found');
  getPagination(0)
    .should('have.text', '1')
    .then(isCurrentActive);
  getPagination(1).should('have.text', '2');
  getPagination(2).should('have.text', '3');
  getPagination(3).should('have.text', '4');
  getPagination(4).should('have.text', '5');
  getPagination(5).should('have.text', '❯');
  cy.clickOnPaginationIndex(1); //click on page 2
  JobsPageNavigate.cardLoaderShouldNotExist();
  JobsPageNavigate.validateJobListExistence('found');
  getPagination(0).should('have.text', '❮');
  getPagination(1).should('have.text', '1');
  getPagination(2)
    .should('have.text', '2')
    .then(isCurrentActive);
  getPagination(3).should('have.text', '3');
  getPagination(4).should('have.text', '4');
  getPagination(5).should('have.text', '5');
  getPagination(6).should('have.text', '6');
  getPagination(7).should('have.text', '❯');
  cy.clickOnPaginationIndex(7); //click on page 3 via > botton
  JobsPageNavigate.cardLoaderShouldNotExist();
  JobsPageNavigate.validateJobListExistence('found');
  getPagination(0).should('have.text', '❮');
  getPagination(1).should('have.text', '1');
  getPagination(2).should('have.text', '2');
  getPagination(3)
    .should('have.text', '3')
    .then(isCurrentActive);
  getPagination(4).should('have.text', '4');
  getPagination(5).should('have.text', '5');
  getPagination(6).should('have.text', '6');
  getPagination(7).should('have.text', '7');
  getPagination(8).should('have.text', '❯');
  cy.clickOnPaginationIndex(4); //click on page 4
  JobsPageNavigate.cardLoaderShouldNotExist();
  JobsPageNavigate.validateJobListExistence('found');
  getPagination(0).should('have.text', '❮');
  getPagination(1).should('have.text', '1');
  getPagination(2).should('have.text', '2');
  getPagination(3).should('have.text', '3');
  getPagination(4)
    .should('have.text', '4')
    .then(isCurrentActive);
  getPagination(5).should('have.text', '5');
  getPagination(6).should('have.text', '6');
  getPagination(7).should('have.text', '7');
  getPagination(8).should('have.text', '8');
  getPagination(9).should('have.text', '❯');
  cy.clickOnPaginationIndex(5); //click on page 5
  JobsPageNavigate.cardLoaderShouldNotExist();
  JobsPageNavigate.validateJobListExistence('found');
  getPagination(0).should('have.text', '❮');
  getPagination(1).should('have.text', '1');
  getPagination(2).should('have.text', '2');
  getPagination(3).should('have.text', '3');
  getPagination(4).should('have.text', '4');
  getPagination(5)
    .should('have.text', '5')
    .then(isCurrentActive);
  getPagination(6).should('have.text', '6');
  getPagination(7).should('have.text', '7');
  getPagination(8).should('have.text', '8');
  getPagination(9).should('have.text', '9');
  getPagination(10).should('have.text', '❯');
  cy.clickOnPaginationIndex(6); //click on page 6
  JobsPageNavigate.cardLoaderShouldNotExist();
  JobsPageNavigate.validateJobListExistence('found');
  getPagination(0).should('have.text', '❮');
  getPagination(1).should('have.text', '2');
  getPagination(2).should('have.text', '3');
  getPagination(3).should('have.text', '4');
  getPagination(4).should('have.text', '5');
  getPagination(5)
    .should('have.text', '6')
    .then(isCurrentActive);
  getPagination(6).should('have.text', '7');
  getPagination(7).should('have.text', '8');
  getPagination(8).should('have.text', '9');
  getPagination(9).should('have.text', '10');
  getPagination(10).should('have.text', '❯');
  cy.clickOnPaginationIndex(7); //click on page 8
  JobsPageNavigate.cardLoaderShouldNotExist();
  JobsPageNavigate.validateJobListExistence('found');
  getPagination(0).should('have.text', '❮');
  getPagination(1).should('have.text', '4');
  getPagination(2).should('have.text', '5');
  getPagination(3).should('have.text', '6');
  getPagination(4).should('have.text', '7');
  getPagination(5)
    .should('have.text', '8')
    .then(isCurrentActive);
  getPagination(6).should('have.text', '9');
  getPagination(7).should('have.text', '10');
  getPagination(8).should('have.text', '11');
  getPagination(9).should('have.text', '❯');
  cy.clickOnPaginationIndex(8); //click on page 11
  JobsPageNavigate.cardLoaderShouldNotExist();
  JobsPageNavigate.validateJobListExistence('found');
  getPagination(0).should('have.text', '❮');
  getPagination(1).should('have.text', '7');
  getPagination(2).should('have.text', '8');
  getPagination(3).should('have.text', '9');
  getPagination(4).should('have.text', '10');
  getPagination(5)
    .should('have.text', '11')
    .then(isCurrentActive);
  cy.clickOnPaginationIndex(0); //click on < button should go back to previous page
  JobsPageNavigate.cardLoaderShouldNotExist();
  JobsPageNavigate.validateJobListExistence('found');
  getPagination(0).should('have.text', '❮');
  getPagination(1).should('have.text', '6');
  getPagination(2).should('have.text', '7');
  getPagination(3).should('have.text', '8');
  getPagination(4).should('have.text', '9');
  getPagination(5)
    .should('have.text', '10')
    .then(isCurrentActive);
  getPagination(6).should('have.text', '11');
  getPagination(7).should('have.text', '❯');
}

describe('Jobs Page - Pagination', function() {
  beforeEach(() => {
    cy.home();
    cy.login('Account with a lot of jobs', 'Job Admin');
  });
  afterEach(() => {
    cy.logout();
  });

  it('Should have pagination in open jobs tab', function() {
    checkPaginationFuc();
  });

  it('Should have pagination in closed jobs tab', function() {
    cy.clickOnJobTab('Closed');
    checkPaginationFuc();
  });
});
