import * as JobsPageNavigate from '../../../support/shared/job_page.functions';
import jobs from '../../../fixtures/JobList';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Jobs Page - Sorting logic check', function() {
  before(() => {
    cy.home();
    cy.login('Manage Applicant Test Account', 'Job Admin');
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('Should be able to see default close date sorting option selected and verify list of sort options', function() {
    cy.clickOnJobTab('Open');
    JobsPageNavigate.checkHasSortBySelected('Auto-close Date');
    JobsPageNavigate.verifyJobListField(jobs.J1, 0);
    JobsPageNavigate.verifyJobListField(jobs.J2, 1);
    JobsPageNavigate.verifyJobListField(jobs.J3, 2);
    JobsPageNavigate.clickAndCheckDefaultSortOptions(jobs.sortingOptionsOpenJobs);
    cy.clickOnJobTab('Closed');
    JobsPageNavigate.checkHasSortBySelected('Closed Date');
    JobsPageNavigate.verifyJobListField(jobs.J6, 0);
    JobsPageNavigate.verifyJobListField(jobs.J4, 1);
    JobsPageNavigate.verifyJobListField(jobs.J5, 2);
    JobsPageNavigate.clickAndCheckDefaultSortOptions(jobs.sortingOptionsClosedJobs);
  });

  it('Should be able to sort correctly by posted date and auto-close date on open jobs tab', function() {
    cy.clickOnJobTab('Open');
    cy.clickOnSortJobsBy('Posted Date');
    JobsPageNavigate.shouldHaveCardLoaderLoaded();
    JobsPageNavigate.verifyJobListField(jobs.J3, 0);
    JobsPageNavigate.verifyJobListField(jobs.J2, 1);
    JobsPageNavigate.verifyJobListField(jobs.J1, 2);

    cy.clickOnSortJobsBy('Auto-close Date');
    JobsPageNavigate.shouldHaveCardLoaderLoaded();
    JobsPageNavigate.verifyJobListField(jobs.J1, 0);
    JobsPageNavigate.verifyJobListField(jobs.J2, 1);
    JobsPageNavigate.verifyJobListField(jobs.J3, 2);
  });

  it('Should be able to sort correctly by posted date and closed date on closed jobs tab', function() {
    cy.clickOnJobTab('Closed');
    cy.clickOnSortJobsBy('Posted Date');
    JobsPageNavigate.shouldHaveCardLoaderLoaded();
    JobsPageNavigate.verifyJobListField(jobs.J4, 0);
    JobsPageNavigate.verifyJobListField(jobs.J6, 1);
    JobsPageNavigate.verifyJobListField(jobs.J5, 2);

    cy.clickOnSortJobsBy('Closed Date');
    JobsPageNavigate.shouldHaveCardLoaderLoaded();
    JobsPageNavigate.verifyJobListField(jobs.J6, 0);
    JobsPageNavigate.verifyJobListField(jobs.J4, 1);
    JobsPageNavigate.verifyJobListField(jobs.J5, 2);
  });
});
