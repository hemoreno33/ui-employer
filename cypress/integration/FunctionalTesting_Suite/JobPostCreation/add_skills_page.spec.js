import {fillJobDescriptionStep} from '../../../support/shared/job_post_description_page.functions';
import {
  shouldHaveAddMoreSkillsValueExists,
  searchAndSelectAddMoreSkillSection,
  shouldCountMatchRecommendedSkills,
  verifyAutoPopulateRecommendedSkillsValue,
} from '../../../support/shared/job_post_skills_page.functions';
import {onClickNext, onClickBack, suppressBeforeUnloadEvent} from '../../../support/shared/common.functions';
import {step} from 'mocha-steps';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Add more skills tab in Job Posting Page', function() {
  before(() => {
    suppressBeforeUnloadEvent();
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
    cy.clickNewJobPosting();
    cy.shouldHaveFormLoaderLoaded();
  });
  beforeEach(() => {
    cy.fixture('JobPosting.json').as('jobPost');
    cy.clickNewJobPosting();
    cy.shouldHaveFormLoaderLoaded();
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  step(
    'should verify max allowed 16 recommended skills based on job title and job description and verify duplicate skills removed and also verify dedup skills from bottom section',
    function() {
      fillJobDescriptionStep(this.jobPost.jobPostTitleAndDescriptionRecommendedSkill);
      onClickNext();
      shouldCountMatchRecommendedSkills(this.jobPost.jobPostTitleAndDescriptionRecommendedSkill.recommendedSkillsCount);
      verifyAutoPopulateRecommendedSkillsValue(
        this.jobPost.jobPostTitleAndDescriptionRecommendedSkill.autoRecommendedSkills,
        {exist: true},
      );
      onClickBack();
      fillJobDescriptionStep(this.jobPost.jobPostValidateUniqueRecommendedSkills);
      onClickNext();
      shouldCountMatchRecommendedSkills(this.jobPost.jobPostValidateUniqueRecommendedSkills.recommendedSkillsCount);
      verifyAutoPopulateRecommendedSkillsValue(
        this.jobPost.jobPostValidateUniqueRecommendedSkills.autoRecommendedSkills,
        {exist: true},
      );
      searchAndSelectAddMoreSkillSection(this.jobPost.jobPostValidateUniqueRecommendedSkills.skillFromDropDown);
      verifyAutoPopulateRecommendedSkillsValue(
        this.jobPost.jobPostValidateUniqueRecommendedSkills.dedupRecommendedSkills,
        {exist: true},
      );
      shouldHaveAddMoreSkillsValueExists(
        this.jobPost.jobPostValidateUniqueRecommendedSkills.skillFromDropDown.selectSkillList,
      );
    },
  );

  step(
    'should option to search and add more skills and verify skills are moved to add more skills section if skills are exist in recommended section',
    function() {
      fillJobDescriptionStep(this.jobPost.jobPost1);
      onClickNext();
      verifyAutoPopulateRecommendedSkillsValue(this.jobPost.jobPost1.autoRecommendedSkills, {
        exist: true,
      });
      searchAndSelectAddMoreSkillSection(this.jobPost.jobPost1.skillNotFromDropDown);
      searchAndSelectAddMoreSkillSection(this.jobPost.jobPost1.skillFromDropDown);
      shouldHaveAddMoreSkillsValueExists([
        ...this.jobPost.jobPost1.skillNotFromDropDown.selectSkillList,
        ...this.jobPost.jobPost1.skillFromDropDown.selectSkillList,
      ]);
    },
  );

  step('should recommended skills auto-populate based on jobtitle and max allowed 8 skills', function() {
    fillJobDescriptionStep(this.jobPost.jobPostTitleRecommendedSkill);
    onClickNext();
    verifyAutoPopulateRecommendedSkillsValue(this.jobPost.jobPostTitleRecommendedSkill.autoRecommendedSkills, {
      exist: true,
    });
    shouldCountMatchRecommendedSkills(this.jobPost.jobPostTitleRecommendedSkill.recommendedSkillsCount);
  });

  step(
    'should recommend skills auto-populate based on job descr, max allowed 8 skills And recommend skills re-populate when job title and job descr change',
    function() {
      fillJobDescriptionStep(this.jobPost.jobPostDescriptionRecommendedSkill);
      onClickNext();
      verifyAutoPopulateRecommendedSkillsValue(this.jobPost.jobPostDescriptionRecommendedSkill.autoRecommendedSkills, {
        exist: true,
      });
      shouldCountMatchRecommendedSkills(this.jobPost.jobPostDescriptionRecommendedSkill.recommendedSkillsCount);
      onClickBack();
      fillJobDescriptionStep(this.jobPost.jobPostTitleAndDescriptionReset);
      onClickNext();
      verifyAutoPopulateRecommendedSkillsValue(this.jobPost.jobPostDescriptionRecommendedSkill.autoRecommendedSkills, {
        exist: false,
      });
    },
  );
});
