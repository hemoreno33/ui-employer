import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Create Job Posting to prompt confirm dialog if User leaving page', () => {
  before(() => {
    cy.home();
    cy.loginFromNavbar('Job Post Testing Account');
    cy.clickNewJobPosting();
    cy.shouldHaveFormLoaderLoaded();
  });
  beforeEach(() => {
    cy.location('href').should('include', '/jobs/new');
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('has Post Jobs link and by clicking on it, confirm will show up. Once declined should remain on Create Job Posting page', () => {
    cy.on('window:confirm', (_) => {
      return false;
    });

    cy.get('[data-cy=post-jobs-link]').click();
    cy.location('href').should('include', '/jobs/new');
  });

  it('has Post Jobs link and by clicking on it, confirm will show up. Once accepted should navigate to Jobs page', () => {
    cy.on('window:confirm', (_) => {
      return true;
    });

    cy.get('[data-cy=post-jobs-link]').click();
    cy.location('href').should('not.include', '/jobs/new');
  });
});
