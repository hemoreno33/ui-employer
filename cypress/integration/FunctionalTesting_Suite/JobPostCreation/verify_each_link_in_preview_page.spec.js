import {
  shouldHaveJobDescriptionValueExists,
  fillJobDescriptionStep,
} from '../../../support/shared/job_post_description_page.functions';
import {
  shouldHaveAddMoreSkillsValueExists,
  searchAndSelectAddMoreSkillSection,
} from '../../../support/shared/job_post_skills_page.functions';
import {
  fillKeyInformationOnJobPosting,
  verifyKeyInformationValueExists,
} from '../../../support/shared/job_post_key_information_page.functions';
import {
  verifyLocalWorkplaceDetailValueExists,
  selectSameLocationFromWorkplaceDetails,
  fillLocalWorkplaceDetails,
} from '../../../support/shared/job_post_workplace_details_page.functions';
import {
  clickOnLinkIn,
  verifyLinkAndColorCodeEnable,
  verifyLinkAndColorCodeDisable,
} from '../../../support/shared/job_post_preview_page.functions';
import {onClickNext, suppressBeforeUnloadEvent} from '../../../support/shared/common.functions';
import jobPostFormStructure from '../../../fixtures/JobPostFormStructure';
import {step} from 'mocha-steps';
describe('Verify every tab links from Preview tab in Job Posting Page', function() {
  before(() => {
    suppressBeforeUnloadEvent();
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
    cy.fixture('JobPosting.json')
      .as('jobPost')
      .then((jobPost) => {
        cy.clickNewJobPosting();
        cy.shouldHaveFormLoaderLoaded();
        fillJobDescriptionStep(jobPost.jobPost1);
      });
  });
  after(() => {
    cy.logout();
  });

  step('should have color coded for each and every tab and link were enabled', function() {
    // On Job Description step to verify colorcode and link disable for "job description, skills, key information, workplace details, preview job post"
    verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.jobDescriptionTab);
    verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.skillsTab);
    verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.keyInformationTab);
    verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.workplaceDetailsTab);
    verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewJobPostTab);
    onClickNext();
    // On Skills step to verify colorcode and link enable for "job description" and verify colorcode and link disable for "skills, key information, workplace details, preview job post"
    verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.jobDescriptionTab);
    verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.skillsTab);
    verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.keyInformationTab);
    verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.workplaceDetailsTab);
    verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewJobPostTab);
    searchAndSelectAddMoreSkillSection(this.jobPost.jobPost1.skillNotFromDropDown);
    searchAndSelectAddMoreSkillSection(this.jobPost.jobPost1.skillFromDropDown);
    onClickNext();
    // On Key Information step to verify colorcode and link enable for "job description, skills" and verify colorcode and link disable for "key information, workplace details, preview job post"
    verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.jobDescriptionTab);
    verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.skillsTab);
    verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.keyInformationTab);
    verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.workplaceDetailsTab);
    verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewJobPostTab);
    fillKeyInformationOnJobPosting(this.jobPost.jobPost1);
    onClickNext();
    // On Skills step to verify colorcode and link enable for "job description, skills, key information" and verify colorcode and link disable for "workplace details, preview job post"
    verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.jobDescriptionTab);
    verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.skillsTab);
    verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.keyInformationTab);
    verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.workplaceDetailsTab);
    verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewJobPostTab);
    selectSameLocationFromWorkplaceDetails();
    fillLocalWorkplaceDetails(this.jobPost.jobPost1.jobWorkplaceDetail);
    onClickNext();
    // On Skills step to verify colorcode and link enable for "job description, skills, key information, workplace details" and verify colorcode and link disable for "preview job post"
    verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.jobDescriptionTab);
    verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.skillsTab);
    verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.keyInformationTab);
    verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.workplaceDetailsTab);
    verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewJobPostTab);
    // Perform navigate job-description tab
    clickOnLinkIn('job-description');
    cy.shouldHaveFormLoaderLoaded();
    shouldHaveJobDescriptionValueExists(this.jobPost.jobPost1);
    onClickNext();
    onClickNext();
    onClickNext();
    onClickNext();
    // Perform navigate skills tab
    clickOnLinkIn('skills');
    cy.shouldHaveFormLoaderLoaded();
    shouldHaveAddMoreSkillsValueExists([
      ...this.jobPost.jobPost1.skillNotFromDropDown.selectSkillList,
      ...this.jobPost.jobPost1.skillFromDropDown.selectSkillList,
    ]);
    onClickNext();
    onClickNext();
    onClickNext();
    // Perform navigate Key-Information tab
    clickOnLinkIn('key-information');
    cy.shouldHaveFormLoaderLoaded();
    verifyKeyInformationValueExists(this.jobPost.jobPost1);
    onClickNext();
    onClickNext();
    // Perform navigate Workplace-Details tab
    clickOnLinkIn('workplace-details');
    cy.shouldHaveFormLoaderLoaded();
    verifyLocalWorkplaceDetailValueExists(this.jobPost.jobPost1.jobWorkplaceDetail);
  });
});
