import {fillJobDescriptionStep} from '../../../support/shared/job_post_description_page.functions';
import {
  searchAndSelectAddMoreSkillSection,
  verifyAutoPopulateRecommendedSkillsValue,
  shouldHaveAddMoreSkillsValueExists,
  unSelectSkillsFromRecommendedSection,
  verifyUnSelectedSkillsFromRecommendedSection,
  verifySelectedSkillsFromRecommendedSection,
  removeDuplicationFrom,
  unSelectSkillsFromAddMoreSection,
  verifyUnSelectedSkillsFromAddMoreSection,
  verifySelectedSkillsFromAddMoreSection,
  clearAllSkillsFromRecommendedSection,
  clearAllSkillsFromAddMoreSection,
} from '../../../support/shared/job_post_skills_page.functions';
import {onClickNext, onClickBack, suppressBeforeUnloadEvent} from '../../../support/shared/common.functions';
import {step} from 'mocha-steps';
describe('Select and unselect skills from recommended & add more skills tab in Job Posting Page', function() {
  before(() => {
    suppressBeforeUnloadEvent();
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
    cy.fixture('JobPosting.json').as('jobPost');
    cy.clickNewJobPosting();
    cy.shouldHaveFormLoaderLoaded();
  });
  after(() => {
    cy.logout();
  });

  step(
    'should option to unselect skills, clear all skills and skills including deselect skills will be retain when move different tab',
    function() {
      fillJobDescriptionStep(this.jobPost.jobPostVerifySelectOrUnselectSkills);
      onClickNext();
      verifyAutoPopulateRecommendedSkillsValue(this.jobPost.jobPostVerifySelectOrUnselectSkills.autoRecommendedSkills, {
        exist: true,
      });
      searchAndSelectAddMoreSkillSection(this.jobPost.jobPostVerifySelectOrUnselectSkills.skillFromDropDown);
      shouldHaveAddMoreSkillsValueExists(
        this.jobPost.jobPostVerifySelectOrUnselectSkills.skillFromDropDown.selectSkillList,
      );

      unSelectSkillsFromRecommendedSection(
        this.jobPost.jobPostVerifySelectOrUnselectSkills.recommendedUnSelectSkillsSection,
      );
      verifyUnSelectedSkillsFromRecommendedSection(
        this.jobPost.jobPostVerifySelectOrUnselectSkills.recommendedUnSelectSkillsSection,
      );
      const recommendedSkillsValueArray = [
        ...this.jobPost.jobPostVerifySelectOrUnselectSkills.recommendedUnSelectSkillsSection,
        ...this.jobPost.jobPostVerifySelectOrUnselectSkills.autoRecommendedSkills,
      ];
      verifySelectedSkillsFromRecommendedSection(removeDuplicationFrom(recommendedSkillsValueArray));

      unSelectSkillsFromAddMoreSection(this.jobPost.jobPostVerifySelectOrUnselectSkills.addMoreUnSelectSkillsSection);
      verifyUnSelectedSkillsFromAddMoreSection(
        this.jobPost.jobPostVerifySelectOrUnselectSkills.addMoreUnSelectSkillsSection,
      );
      const addMoreSkillsValueArray = [
        ...this.jobPost.jobPostVerifySelectOrUnselectSkills.addMoreUnSelectSkillsSection,
        ...this.jobPost.jobPostVerifySelectOrUnselectSkills.skillFromDropDown.selectSkillList,
      ];
      verifySelectedSkillsFromAddMoreSection(removeDuplicationFrom(addMoreSkillsValueArray));

      onClickNext();
      onClickBack();
      verifyUnSelectedSkillsFromRecommendedSection(
        this.jobPost.jobPostVerifySelectOrUnselectSkills.recommendedUnSelectSkillsSection,
      );
      verifySelectedSkillsFromRecommendedSection(removeDuplicationFrom(recommendedSkillsValueArray));
      verifyUnSelectedSkillsFromAddMoreSection(
        this.jobPost.jobPostVerifySelectOrUnselectSkills.addMoreUnSelectSkillsSection,
      );
      verifySelectedSkillsFromAddMoreSection(removeDuplicationFrom(addMoreSkillsValueArray));

      clearAllSkillsFromRecommendedSection();
      verifyUnSelectedSkillsFromRecommendedSection(
        this.jobPost.jobPostVerifySelectOrUnselectSkills.autoRecommendedSkills,
      );
      clearAllSkillsFromAddMoreSection();
      verifyUnSelectedSkillsFromAddMoreSection(
        this.jobPost.jobPostVerifySelectOrUnselectSkills.skillFromDropDown.selectSkillList,
      );
    },
  );
});
