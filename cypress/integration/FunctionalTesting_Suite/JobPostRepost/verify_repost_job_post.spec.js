import {searchAndOpenJobOnTab, closeJobWithOption} from '../../../support/shared/job_page.functions';
import {suppressBeforeUnloadEvent, verifyStaticNudgeContent} from '../../../support/shared/common.functions';
import {repostJob} from '../../../support/shared/job_repost_page_functions';
import {editJobPostButtonSelector, editRemainCounterSelector} from '../../../support/shared/job_details_page.functions';
import {
  previewJobDescriptionDetails,
  previewKeyInformationDetails,
  previewWorkplaceDetails,
  previewSkillsDetails,
} from '../../../support/shared/job_post_preview_page.functions';
import jobs from '../../../fixtures/JobList';
import {apiJobData} from '../../../fixtures/seedData/api_job_post_seed_data';
import {sixMonthsFrom, daysFrom} from '../../../support/util/date';
import {seedXCloseJobsForUEN} from '../../../support/manageData/job.seed';
import {step} from 'mocha-steps';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Verify repost job post for all pages', () => {
  before(() => {
    suppressBeforeUnloadEvent();
    cy.home();
    cy.login('Job Repost Account', 'Job Admin');
  });
  beforeEach(() => {
    cy.visit('/');
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('Repost Job Counter', () => {
    step('should verify Repost Button not disabled and both repost counter and date counter are valid', () => {
      searchAndOpenJobOnTab('Close', 'MCF-2900-9001000');
      cy.getJobPostedDate().then((postedDateAttr) => {
        repostJob.clickViewRepostJobPost();
        repostJob.validateRepostButtonStatus('enabled');
        repostJob.validateRepostDetailsInAsideBar('2 Reposts left', sixMonthsFrom(postedDateAttr));
      });
      verifyStaticNudgeContent();
    });

    step('should not exists edit job post button or edit job post count label for MCF close jobs', () => {
      searchAndOpenJobOnTab('Close', 'MCF-2900-9001000');
      repostJob.clickViewRepostJobPost();
      cy.shouldHaveFormLoaderLoaded();
      cy.get(editJobPostButtonSelector).should('not.exist');
      cy.get(editRemainCounterSelector).should('not.exist');
    });

    step('should verify job post details are display correctly for MCF close jobs', () => {
      searchAndOpenJobOnTab('Close', 'MCF-2900-9001000');
      repostJob.clickViewRepostJobPost();
      cy.shouldHaveFormLoaderLoaded();
      previewJobDescriptionDetails(jobs.jobPostMCFCloseState);
      previewSkillsDetails(jobs.jobPostMCFCloseState.previewSkills);
      previewKeyInformationDetails(jobs.jobPostMCFCloseState.keyInformation);
      previewWorkplaceDetails(jobs.jobPostMCFCloseState.jobWorkplaceDetail);
    });

    step('should verify Repost Job Modal Pop-up for MCF close jobs', function() {
      searchAndOpenJobOnTab('Close', 'MCF-2900-9001000');
      repostJob.clickViewRepostJobPost();
      cy.shouldHaveFormLoaderLoaded();
      repostJob.clickRepostJobButton();
      repostJob.validateRepostJobDetailsInModal('30 days', '');
      repostJob.validateRepostJobDetailsInModal('Repost Job', 'Repost this job for the duration of:');
      repostJob.validateRepostJobDetailsInModal('After submitting this repost, you will have', '1 repost left');
      repostJob.validateRepostJobDetailsInModal(daysFrom(30), '');
      repostJob.clickRepostModalDropDown();
      repostJob.validateRepostModalDropdownListValue();
      repostJob.clickRepostModalDropDown();
      repostJob.clickButtonInRepostJob('Cancel');
    });

    step('should verify Repost Button is disabled and repost counter is invalid', () => {
      searchAndOpenJobOnTab('Close', 'MCF-2900-9001001');
      repostJob.clickViewRepostJobPost();
      repostJob.validateRepostButtonStatus('disabled');
      repostJob.validateRepostDetailsInAsideBar('0 Reposts left');
    });

    step('should verify Repost Button is disabled and repost duration is invalid', () => {
      searchAndOpenJobOnTab('Close', 'MCF-2900-9001002');
      cy.getJobPostedDate().then((postedDateAttr) => {
        repostJob.clickViewRepostJobPost();
        repostJob.validateRepostButtonStatus('disabled');
        repostJob.validateRepostDetailsInAsideBar('', `Repost unavailable after  ${sixMonthsFrom(postedDateAttr)}`);
      });
    });

    step('should verify Repost Button is disabled; and both repost count and repost duration is invalid', () => {
      searchAndOpenJobOnTab('Close', 'MCF-2900-9001003');
      cy.getJobPostedDate().then((postedDateAttr) => {
        repostJob.clickViewRepostJobPost();
        repostJob.validateRepostButtonStatus('disabled');
        repostJob.validateRepostDetailsInAsideBar(
          '0 Reposts left',
          `Repost unavailable after  ${sixMonthsFrom(postedDateAttr)}`,
        );
      });
    });
  });

  describe('Repost end to end flow', () => {
    const UEN = '100000011C';
    let jobPostID = '';
    const firstRepostDays = 7;
    const secondRepostDays = 21;
    const payload = apiJobData.defaultJobPost;
    payload.postedCompany.uen = UEN;
    payload.metadata.expiryDate = daysFrom(14);
    before(() => {
      cy.log('seed Data - Create and close a Job').then(() => {
        seedXCloseJobsForUEN(1, payload, UEN, true).then((response) => {
          jobPostID = response[0].metadata.jobPostId;
        });
      });
    });
    step(`Should be able to search closed job, validate repost details and repost for ${firstRepostDays} days`, () => {
      searchAndOpenJobOnTab('Close', jobPostID);
      cy.getJobPostedDate().then((postedDateAttr) => {
        repostJob.clickViewRepostJobPost();
        repostJob.validateRepostButtonStatus('enabled');
        repostJob.validateRepostDetailsInAsideBar('2 Reposts left', sixMonthsFrom(postedDateAttr));
        repostJob.repostJobWithDays(firstRepostDays);
        repostJob.validateRepostAcknowledgementAndClickOK(daysFrom(firstRepostDays));
        cy.validateJobStatus('REOPENED').validateJobClosingDate(daysFrom(firstRepostDays));
      });
    });
    step('Should be able to search repost job in Open tab and close it with Filled option', () => {
      searchAndOpenJobOnTab('Open', jobPostID);
      closeJobWithOption();
    });
    step(`Should be able to search closed job, validate repost details and repost for ${secondRepostDays} days`, () => {
      searchAndOpenJobOnTab('Close', jobPostID);
      repostJob.clickViewRepostJobPost();
      repostJob.validateRepostButtonStatus('enabled');
      repostJob.validateRepostDetailsInAsideBar('1 Repost left');
      repostJob.repostJobWithDays(secondRepostDays);
      repostJob.validateRepostAcknowledgementAndClickOK(daysFrom(secondRepostDays));
      cy.validateJobStatus('REOPENED').validateJobClosingDate(daysFrom(secondRepostDays));
    });
    step('Should be able to search repost job, close it and validate repost details', () => {
      searchAndOpenJobOnTab('Open', jobPostID);
      closeJobWithOption();
      searchAndOpenJobOnTab('Close', jobPostID);
      repostJob.clickViewRepostJobPost();
      repostJob.validateRepostButtonStatus('disabled');
      repostJob.validateRepostDetailsInAsideBar('0 Reposts left');
    });
  });
});
