import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {createAJobAndApplyFor} from '../../../support/manageData/applicant.seed';
import {step} from 'mocha-steps';
import {searchAndOpenJobOnTab, shouldHaveCardLoaderLoaded} from '../../../support/shared/job_page.functions';
import {bookmarkApi} from '../../../support/api/bookmarkApi.js';
import {validateBookmarkSortingOrder} from '../../../support/shared/manage_saved_tab.functions';
import {getPagination} from '../../../support/shared/common.functions';
import {cardLoaderShouldNotExist} from '../../../support/shared/job_page.functions';
import moment from 'moment';

describe('Validate Saved tab - Sorting and Pagination', () => {
  const UEN = '100000003C';
  let jobPostID, jobUuid;
  // For the purpose of testing, after applications created for below candidates NRIC,
  // some of the applications will be used as the talent to bookmark using API.
  const applicantsTalentsSorting = [
    {nric: 'S4854022H', type: 'applicant', bookmarkIndex: 3},
    {nric: 'T1167450F', type: 'talent', bookmarkIndex: 2},
    {nric: 'S8758251E', type: 'applicant', bookmarkIndex: 1},
    {nric: 'T0884683E', type: 'talent', bookmarkIndex: 0},
  ];
  before(() => {
    cy.home();
    cy.login('Account with a lot of jobs', 'Job Admin');
    cy.log('seed data - create a job and apply');
    createAJobAndApplyFor(
      UEN,
      applicantsTalentsSorting.map((applicantTalent) => applicantTalent.nric),
    )
      .then((response) => {
        jobPostID = response.job.metadata.jobPostId;
        jobUuid = response.job.uuid;
        cy.log('seed data - bookmarking applicant and talent');
        applicantsTalentsSorting.forEach(({nric, type}) => {
          bookmarkApi.bookmarkApplicantOrTalent(jobUuid, nric, type);
        });
      })
      .then(() => {
        cy.log('Search and open the job and click Saved tab');
        searchAndOpenJobOnTab('Open', jobPostID);
        cy.clickOnJobTab('Saved');
        shouldHaveCardLoaderLoaded();
      });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('validate applicant and talents sorting order', () => {
    step('should show applicant and suggested talent in descending order (latest first)', () => {
      applicantsTalentsSorting.forEach(({nric, type, bookmarkIndex}) => {
        validateBookmarkSortingOrder(nric, type, bookmarkIndex);
      });
    });
  });

  describe('validate pagination', () => {
    const testDataPagination = [
      {
        testDescription:
          'should able to click page 2 and should show candidates and able to click first candidate in the page',
        pageIndexToClick: 1,
        pageText: '2',
      },
      {
        testDescription:
          'should able to click page ❮ and should show candidates and able to click first candidate in the page',
        pageIndexToClick: 0,
        pageText: '❮',
      },
      {
        testDescription:
          'should able to click page ❯ and should show candidates and able to click first candidate in the page',
        pageIndexToClick: 2,
        pageText: '❯',
      },
      {
        testDescription:
          'should able to click page 1 and should show candidates and able to click first candidate in the page',
        pageIndexToClick: 1,
        pageText: '1',
      },
    ];

    step('should show page 1 only and not show page 2', () => {
      getPagination(0).should('exist'); // page 1 should exists
      getPagination(1).should('not.exist'); // page 2 should not exists
    });

    step('bookmark talents via DB to get pagination, reload the page and open saved tab', () => {
      cy.log(
        'seed data - getting 30 individual id from jobseekers table and inserting in job_bookmarked_talents table to get pagination',
      );
      cy.task('profileDB', 'SELECT individual_id FROM jobseekers LIMIT 30;')
        .then((results) => {
          results.forEach((result) => {
            cy.log(`${result.individual_id}, ${jobUuid}, ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
            cy.task(
              'profileDB',
              `INSERT INTO
            job_bookmarked_talents (individual_id, job_uuid, created_on)
            VALUES
            ('${result.individual_id}', '${jobUuid}', '${moment().format('YYYY-MM-DD HH:mm:ss')}');`,
            );
          });
        })
        .then(() => {
          cy.reload().clickOnJobTab('Saved');
          shouldHaveCardLoaderLoaded();
        });
    });

    step('should show pagination with 2 and next page symbol', () => {
      getPagination(0).should('have.text', '1');
      getPagination(1).should('have.text', '2');
      getPagination(2).should('have.text', '❯');
    });

    testDataPagination.forEach(({testDescription, pageIndexToClick, pageText}) => {
      step(testDescription, () => {
        getPagination(pageIndexToClick).should('have.text', pageText);
        cy.clickOnPaginationIndex(pageIndexToClick);
        cardLoaderShouldNotExist();
        cy.clickOnCandidate(0);
      });
    });
  });
});
