/* eslint-disable @typescript-eslint/camelcase */
import {step} from 'mocha-steps';
import {searchAndOpenJobOnTab, shouldHaveCardLoaderLoaded} from '../../../support/shared/job_page.functions';
import {clickBookmarkIconIfTrue, verifyBookmarkCount} from '../../../support/shared/manage_saved_tab.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {createAJobAndApplyFor} from '../../../support/manageData/applicant.seed';
import {apiJobs} from '../../../support/api/jobsApi';
import {applicantStatus} from '../../../support/shared/manage_applicants_page.functions';

const UEN = '100000008C';
const applicantsBookmark = [
  {nric: 'S1210455C', nricName: 'Maximo', bookmarkIndex: 2},
  {nric: 'S3466230D', nricName: 'S3466230D SkillsPassport Guy', bookmarkIndex: 1},
  {nric: 'S5165745D', nricName: 'Rhett', bookmarkIndex: 0},
];
const jobPosts = ['Open', 'Closed'];

jobPosts.forEach((tab) => {
  describe(`Verify Applicant saved bookmark details in ${tab} job`, () => {
    let jobPostId;
    before(() => {
      cy.home().login('Manage Applicant Status', 'Job Admin');
      cy.log('Post a Jobs and mock applicant and talent response');
      createAJobAndApplyFor(
        UEN,
        applicantsBookmark.map((applicant) => applicant.nric),
      ).then((response) => {
        jobPostId = response.job.metadata.jobPostId;
        if (tab === 'Closed') {
          apiJobs.closeJob(response.job.uuid, false);
        }
        cy.visit('/');
      });
    });

    afterEach(() => {
      cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
      Cypress.Cookies.preserveOnce('access-token');
    });

    after(() => {
      cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
      cy.clearCookie('access-token');
    });
    step('should save applicants bookmark from applicant tab', () => {
      const applicantSaveBookmark = {
        candidateIndexes: applicantsBookmark.map((applicant) => applicant.bookmarkIndex),
      };
      searchAndOpenJobOnTab(tab, jobPostId);
      shouldHaveCardLoaderLoaded();
      cy.skipApplicantsOnboardingIfExist();
      applicantSaveBookmark.candidateIndexes.forEach((index) => {
        cy.clickOnCandidate(index);
        clickBookmarkIconIfTrue(true);
      });
    });

    step('Verify bookmark counts and applicants record exists in saved tab', () => {
      const applicantSaveBookmark = {
        bookmarkCount: applicantsBookmark.length,
      };
      reloadAndClickOnTab('Saved');
      verifyBookmarkCount(applicantSaveBookmark.bookmarkCount);
      cy.get('[data-cy=bookmarked-application-item]')
        .its('length')
        .should('be.eq', applicantSaveBookmark.bookmarkCount);
      applicantsBookmark.forEach((data) => {
        cy.get('[data-cy=candidates-name]')
          .eq(data.bookmarkIndex)
          .should('have.text', data.nricName);
      });
    });

    step('should unsave tool tip and revist the tab, applicant should disappear in saved tab', () => {
      const applicantSaveBookmark = {
        nricName: 'Rhett',
        candidateIndex: 0,
        unBookmark: true,
      };
      cy.clickOnCandidate(applicantSaveBookmark.candidateIndex);
      clickBookmarkIconIfTrue(applicantSaveBookmark.unBookmark);
      reloadAndClickOnTab('Saved');
      cy.get('[data-cy=candidates-name]')
        .contains(applicantSaveBookmark.nricName)
        .should('not.exist');
    });

    step('should bookmark applicant and applicant should appear and count increased in saved tab', () => {
      const applicantSaveBookmark = {
        nricName: 'Rhett',
        candidateIndex: 0,
        bookmark: true,
      };
      cy.clickOnApplicantTab();
      shouldHaveCardLoaderLoaded();
      cy.skipApplicantsOnboardingIfExist();
      cy.clickOnCandidate(applicantSaveBookmark.candidateIndex);
      clickBookmarkIconIfTrue(applicantSaveBookmark.bookmark);
      reloadAndClickOnTab('Saved');
      verifyBookmarkCount(applicantsBookmark.length);
      cy.get('[data-cy=candidates-name]')
        .contains(applicantSaveBookmark.nricName)
        .should('exist');
    });

    step('Verify tool tip validation for unsave and save from saved tab', () => {
      const applicantSaveBookmark = {
        nricName: 'Rhett',
        candidateIndex: 0,
        unBookmark: true,
        bookmark: true,
      };
      cy.clickOnCandidate(applicantSaveBookmark.candidateIndex);
      clickBookmarkIconIfTrue(applicantSaveBookmark.unBookmark);
      clickBookmarkIconIfTrue(applicantSaveBookmark.bookmark);
      reloadAndClickOnTab('Saved');
      verifyBookmarkCount(applicantsBookmark.length);
      cy.get('[data-cy=candidates-name]')
        .contains(applicantSaveBookmark.nricName)
        .should('exist');
    });

    step('should reflect applicant status update both place saved tab and applicant tab', () => {
      const applicantSaveBookmark = {
        nricName: 'Rhett',
        candidateIndex: 0,
        statusUpdate: 'Hired',
      };
      cy.clickOnCandidate(applicantSaveBookmark.candidateIndex);
      applicantStatus.selectApplicantStatus(applicantSaveBookmark.statusUpdate);
      applicantStatus.verifyApplicantStatusRightPaneUpdated(applicantSaveBookmark.statusUpdate);
      applicantStatus.verifyApplicantStatusLeftPanelUpdated(
        applicantSaveBookmark.statusUpdate,
        applicantSaveBookmark.candidateIndex,
      );
      cy.clickOnApplicantTab();
      shouldHaveCardLoaderLoaded();
      cy.skipApplicantsOnboardingIfExist();
      cy.clickOnCandidate(applicantSaveBookmark.candidateIndex);
      applicantStatus.verifyApplicantStatusRightPaneUpdated(applicantSaveBookmark.statusUpdate);
      applicantStatus.verifyApplicantStatusLeftPanelUpdated(
        applicantSaveBookmark.statusUpdate,
        applicantSaveBookmark.candidateIndex,
      );
    });
  });
});

const reloadAndClickOnTab = (tabName) => {
  cy.reload()
    .then(() => {
      shouldHaveCardLoaderLoaded();
      cy.skipApplicantsOnboardingIfExist();
    })
    .clickOnJobTab(tabName);
};
