/* eslint-disable @typescript-eslint/camelcase */
import {step} from 'mocha-steps';
import {searchAndOpenJobOnTab, shouldHaveCardLoaderLoaded} from '../../../support/shared/job_page.functions';
import {
  clickBookmarkIconIfTrue,
  verifyBookmarkIconTooltipText,
  verifyBookmarkIconFromCandidateList,
  verifyBookmarkSaveText,
  verifyBookmarkCount,
} from '../../../support/shared/manage_saved_tab.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {couchDBApi} from '../../../support/api/couchDBApi';

const openJobPostId = 'MCF-2018-9111130';
const closeJobPostId = 'MCF-2018-9111133';

describe('Verify bookmark actions for open job and close job', () => {
  const saveBookmarkTestData = [
    {
      testDescription: 'should verify bookmark save tooltip from all candidates',
      candidateIndexes: [0, 1, 2],
      bookmark: false,
      tooltip: 'Click to save',
      bookmarkIconAssertion: 'not.exist',
      savedBookMarkText: false,
    },
    {
      testDescription: 'click bookmark save tooltip and verify bookmark save success',
      candidateIndexes: [1, 2],
      bookmark: true,
      tooltip: 'Click to unsave',
      bookmarkIconAssertion: 'exist',
      savedBookMarkText: true,
    },
  ];
  const unsaveBookmarkTestData = [
    {
      testDescription: 'click bookmark unsave tooltip and verify bookmark unsave success',
      candidateIndexes: [1, 2],
      bookmark: true,
      tooltip: 'Click to save',
      bookmarkIconAssertion: 'not.exist',
      savedBookMarkText: false,
    },
  ];
  const executeTestCases = (data) => {
    it(`${data.testDescription}`, () => {
      data.candidateIndexes.forEach((index) => {
        cy.skipApplicantsOnboardingIfExist();
        cy.clickOnCandidate(index);
        clickBookmarkIconIfTrue(data.bookmark);
        verifyBookmarkIconTooltipText(data.tooltip);
        verifyBookmarkIconFromCandidateList(index, data.bookmarkIconAssertion);
        verifyBookmarkSaveText(data.savedBookMarkText);
      });
    });
  };
  before(() => {
    cy.home().login('Test may update data', 'Job Admin');
    cy.log('Remove bookmark details for the applicants and talents');
    resetBookmarkDetailForApplicantsAndTalents(openJobPostId);
    resetBookmarkDetailForApplicantsAndTalents(closeJobPostId);
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.logout();
  });
  describe('Open Job - Verify applicants and suggested talents bookmark save and total counts in saved tab', () => {
    before(() => {
      searchAndOpenJobOnTab('Open', openJobPostId);
      shouldHaveCardLoaderLoaded();
    });
    describe('Applicants - verify bookmark save tooltip', () => {
      saveBookmarkTestData.forEach((data) => {
        executeTestCases(data);
      });
    });
    describe('Suggested talents - verify bookmark save tooltip', () => {
      before(() => {
        cy.clickOnSuggestedTalentTab();
        shouldHaveCardLoaderLoaded();
      });
      saveBookmarkTestData.forEach((data) => {
        executeTestCases(data);
      });
    });
    describe('Verify save bookmark count for both applicants and suggested talents', () => {
      step('Verify save bookmark total counts in saved tab', () => {
        verifyBookmarkCount(4);
      });
    });
  });
  describe('Open Job - Verify applicants and suggested talents unbookmark save and total counts in saved tab', () => {
    before(() => {
      searchAndOpenJobOnTab('Open', openJobPostId);
      shouldHaveCardLoaderLoaded();
    });
    describe('Applicants - verify bookmark unsave tooltip', () => {
      unsaveBookmarkTestData.forEach((data) => {
        executeTestCases(data);
      });
    });
    describe('Suggested talents - verify bookmark unsave tooltip', () => {
      before(() => {
        cy.clickOnSuggestedTalentTab();
        shouldHaveCardLoaderLoaded();
      });
      unsaveBookmarkTestData.forEach((data) => {
        executeTestCases(data);
      });
    });
    describe('Verify unsave bookmark count for both applicants and suggested talents', () => {
      step('Verify unsave bookmark total counts in saved tab', () => {
        verifyBookmarkCount(0);
      });
    });
  });
  describe('Close Job - Verify applicants and suggested talents bookmark save and total counts in saved tab', () => {
    before(() => {
      searchAndOpenJobOnTab('Close', closeJobPostId);
      shouldHaveCardLoaderLoaded();
    });
    describe('Applicants - verify bookmark save tooltip', () => {
      saveBookmarkTestData.forEach((data) => {
        executeTestCases(data);
      });
    });
    describe('Suggested talents - verify bookmark save tooltip', () => {
      before(() => {
        cy.clickOnSuggestedTalentTab();
        shouldHaveCardLoaderLoaded();
      });
      saveBookmarkTestData.forEach((data) => {
        executeTestCases(data);
      });
    });
    describe('Verify save bookmark count for both applicants and suggested talents', () => {
      step('Verify save bookmark total counts in saved tab', () => {
        verifyBookmarkCount(4);
      });
    });
  });
  describe('Close Job - Verify applicants and suggested talents bookmark unsave and total counts in saved tab', () => {
    before(() => {
      searchAndOpenJobOnTab('Close', closeJobPostId);
      shouldHaveCardLoaderLoaded();
    });
    describe('Applicants - verify bookmark unsave tooltip', () => {
      unsaveBookmarkTestData.forEach((data) => {
        executeTestCases(data);
      });
    });
    describe('Suggested talents - verify bookmark unsave tooltip', () => {
      before(() => {
        cy.clickOnSuggestedTalentTab();
        shouldHaveCardLoaderLoaded();
      });
      unsaveBookmarkTestData.forEach((data) => {
        executeTestCases(data);
      });
    });
    describe('Verify unsave bookmark count for both applicants and suggested talents', () => {
      step('Verify unsave bookmark total counts in saved tab', () => {
        verifyBookmarkCount(0);
      });
    });
  });
});
describe('Verify save bookmark error message for both open job and close job', () => {
  const bookmarkNegativeTestData = [
    {
      testDescription: 'click bookmark save tooltip and verify save bookmark error message',
      candidateIndexes: [1],
      bookmark: true,
      tooltip: 'Click to unsave',
      bookmarkIconAssertion: 'exist',
      savedBookMarkText: true,
      growlErrorText: 'Temporarily unable to unsave candidate. Please try again.',
    },
  ];
  const executeNegativeTestCase = () => {
    bookmarkNegativeTestData.forEach((data) => {
      it(`${data.testDescription}`, () => {
        data.candidateIndexes.forEach((index) => {
          cy.skipApplicantsOnboardingIfExist();
          cy.clickOnCandidate(index);
          clickBookmarkIconIfTrue(data.bookmark);
          cy.verifyNotificationGrowl(data.growlErrorText);
          verifyBookmarkIconTooltipText(data.tooltip);
          verifyBookmarkIconFromCandidateList(index, data.bookmarkIconAssertion);
          verifyBookmarkSaveText(data.savedBookMarkText);
        });
      });
    });
  };
  before(() => {
    cy.home().login('Test may update data', 'Job Admin');
    openJobAndBookmarkCandidateIndex('Open', openJobPostId, 1);
    openJobAndBookmarkCandidateIndex('Close', closeJobPostId, 1);
    cy.mockGraphQL('profile', {
      setApplicationBookmark: {
        response: {
          errors: [{message: 'mock applicant error response'}],
          data: {setApplicationBookmarkedOn: null},
        },
      },
      setSuggestedTalentBookmark: {
        response: {
          errors: [{message: 'mock applicant error response'}],
          data: {setApplicationBookmarkedOn: null},
        },
      },
    }).visit('/');
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.logout();
  });
  describe('Open Job - Verify applicants and suggested talents bookmark error message', () => {
    before(() => {
      searchAndOpenJobOnTab('Open', openJobPostId);
      shouldHaveCardLoaderLoaded();
    });
    describe('Applicants - verify bookmark save error message', () => {
      executeNegativeTestCase();
    });
    describe('Suggested talents - verify bookmark save error message', () => {
      before(() => {
        cy.clickOnSuggestedTalentTab();
        shouldHaveCardLoaderLoaded();
      });
      executeNegativeTestCase();
    });
    describe('Saved tab - verify bookmark save error message', () => {
      before(() => {
        cy.clickOnJobTab('Saved');
        shouldHaveCardLoaderLoaded();
      });
      executeNegativeTestCase();
    });
  });
  describe('Close Job - Verify applicants and suggested talents bookmark error message', () => {
    before(() => {
      searchAndOpenJobOnTab('Close', closeJobPostId);
      shouldHaveCardLoaderLoaded();
    });
    describe('Applicants - verify bookmark save error message', () => {
      executeNegativeTestCase();
    });
    describe('Suggested talents - verify bookmark save error message', () => {
      before(() => {
        cy.clickOnSuggestedTalentTab();
        shouldHaveCardLoaderLoaded();
      });
      executeNegativeTestCase();
    });
    describe('Saved tab - verify bookmark save error message', () => {
      before(() => {
        cy.clickOnJobTab('Saved');
        shouldHaveCardLoaderLoaded();
      });
      executeNegativeTestCase();
    });
  });
});

const resetBookmarkDetailForApplicantsAndTalents = (jobPostId) => {
  cy.log(`Remove details for ${jobPostId}`);
  cy.task('jobDB', `SELECT uuid FROM jobs WHERE job_post_id = '${jobPostId}';`).then((results) => {
    const uuid = results[0].uuid;
    cy.log('Remove applicants bookmark detail for the job in couchDB');
    couchDBApi.find({selector: {jobId: uuid}}).then((responseBody) => {
      const applicantsDocs = responseBody.docs;
      cy.wrap(applicantsDocs).each((doc) => {
        delete doc.bookmarkedOn; // remove bookmark detail for the applicant
        couchDBApi.update(doc);
      });
    });

    cy.log('Remove talents bookmark detail for the job in profile DB');
    cy.task('profileDB', `DELETE FROM job_bookmarked_talents WHERE job_uuid = '${uuid}';`);
  });
};

const openJobAndBookmarkCandidateIndex = (tab, jobPostId, bookmarkCandidateIndex) => {
  searchAndOpenJobOnTab(tab, jobPostId);
  shouldHaveCardLoaderLoaded();
  cy.clickOnCandidate(bookmarkCandidateIndex);
  clickBookmarkIconIfTrue(true);
  verifyBookmarkIconTooltipText('Click to unsave');
  cy.clickOnSuggestedTalentTab();
  shouldHaveCardLoaderLoaded();
  cy.skipApplicantsOnboardingIfExist();
  cy.clickOnCandidate(bookmarkCandidateIndex);
  clickBookmarkIconIfTrue(true);
  verifyBookmarkIconTooltipText('Click to unsave');
};
