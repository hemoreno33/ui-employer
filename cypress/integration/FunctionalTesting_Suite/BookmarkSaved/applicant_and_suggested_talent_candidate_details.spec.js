/* eslint-disable @typescript-eslint/camelcase */
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {createAJobAndApplyFor} from '../../../support/manageData/applicant.seed';
import {step} from 'mocha-steps';
import {searchAndOpenJobOnTab, shouldHaveCardLoaderLoaded} from '../../../support/shared/job_page.functions';
import {bookmarkApi} from '../../../support/api/bookmarkApi';
import {apiJobseeker} from '../../../support/api/jobseekerApi';
import {
  getCandidateFromList,
  shouldHaveCandidateListItemInfo,
  shouldHaveCandidateRightTopPanelInfo,
  shouldHaveCandidateRightBottomPanelInfo,
} from '../../../support/shared/manage_applicants_page.functions';
import {
  selectUnavailableTalentByName,
  validateUnavailableTalentLeftPanelInfo,
  validateUnavailableTalentRightPanelInfo,
  clickBookmarkIconIfTrue,
  validateBookmarkRibbonNotExistFor,
  validateUnavailableTalentNotExist,
} from '../../../support/shared/manage_saved_tab.functions';

describe('Validate Saved tab - applicant and suggested talent details', () => {
  const UEN = '100000003C';
  let jobPostID, jobUuid;
  // For the purpose of testing, after applications created for below candidates NRIC,
  // some of the applications will be used as the talent to bookmark using API.
  const applicantsTalentsSorting = [
    {nric: 'S1040617Z', type: 'applicant', bookmarkIndex: 1},
    {nric: 'T7807887H', type: 'talent', bookmarkIndex: 0}, // when candidate is bookmarked latest will be first
  ];
  const nrics = applicantsTalentsSorting.map((applicantTalent) => applicantTalent.nric);
  before(() => {
    cy.home();
    cy.login('Account with a lot of jobs', 'Job Admin');
    cy.log('seed data - create a job and apply');
    createAJobAndApplyFor(UEN, nrics)
      .then((response) => {
        jobPostID = response.job.metadata.jobPostId;
        jobUuid = response.job.uuid;
        cy.log('set talent profile to visible');
        cy.task(
          'profileDB',
          `UPDATE jobseekers SET is_show_profile = '1' WHERE nric = '${applicantsTalentsSorting[1].nric}';`,
        );
        cy.log('seed data - bookmarking applicant and talent');
        applicantsTalentsSorting.forEach(({nric, type}) => {
          bookmarkApi.bookmarkApplicantOrTalent(jobUuid, nric, type);
        });
      })
      .then(() => {
        cy.log('Search and open the job and click Saved tab');
        searchAndOpenJobOnTab('Open', jobPostID);
        cy.clickOnJobTab('Saved');
        shouldHaveCardLoaderLoaded();
      });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  applicantsTalentsSorting.forEach(({nric, type, bookmarkIndex}) => {
    describe(`validate ${type} - LHS and RHS details`, () => {
      step(`should show ${type} details in LHS`, () => {
        cy.clickOnCandidate(bookmarkIndex);
        apiJobseeker.getCandidateProfileDetails(nric, type).then((candidateDetails) => {
          getCandidateFromList(bookmarkIndex).then(shouldHaveCandidateListItemInfo(candidateDetails));
        });
      });

      step(`should show ${type} details in RHS top panel`, () => {
        apiJobseeker.getCandidateProfileDetails(nric, type).then((candidateDetails) => {
          shouldHaveCandidateRightTopPanelInfo(candidateDetails);
        });
      });

      step(`should show ${type} details in RHS bottom panel - all tabs`, () => {
        apiJobseeker.getCandidateProfileDetails(nric, type).then((candidateDetails) => {
          shouldHaveCandidateRightBottomPanelInfo(candidateDetails);
        });
      });
    });
  });

  describe('validate no longer available talent preview', () => {
    let talentDetails;
    step('set profile preferences as Not seeking opportunities via DB, reload the page and get talent details', () => {
      cy.task(
        'profileDB',
        `UPDATE jobseekers SET is_show_profile = '0' WHERE nric = '${applicantsTalentsSorting[1].nric}';`,
      ).reload();
      apiJobseeker
        .getCandidateProfileDetails(applicantsTalentsSorting[1].nric, applicantsTalentsSorting[1].type)
        .then((candidateDetails) => {
          talentDetails = candidateDetails;
        });
    });

    step('should show "This talent no longer available" along with name in LHS', () => {
      selectUnavailableTalentByName(talentDetails.name);
      validateUnavailableTalentLeftPanelInfo(talentDetails.name);
    });

    step('should show "This talent no longer available" along with name in RHS panel', () => {
      validateUnavailableTalentRightPanelInfo(talentDetails.name);
      validateUnavailableTalentRightPanelInfo('This talent is no longer available');
      validateUnavailableTalentRightPanelInfo('You may want to unsave this talent');
    });

    step('should not show ribbon and unsave static text when unbookmarked', () => {
      clickBookmarkIconIfTrue(true);
      validateBookmarkRibbonNotExistFor(talentDetails.name);
      validateUnavailableTalentRightPanelInfo('You may want to unsave this talent', 'not.contain');
    });

    step('should not show talent after page is reloaded', () => {
      cy.reload().then(() => {
        validateUnavailableTalentNotExist(talentDetails.name);
      });
    });
  });
});
