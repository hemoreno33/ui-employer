import * as ApplicantNavigate from '../../../support/shared/manage_applicants_page.functions';
import {checkLogoutPage} from '../../../support/shared/common.functions';

describe('Applicant - on boarder test', function() {
  beforeEach(() => {
    cy.home();
    cy.login('Manage Applicant Test Account', 'Job Admin');
  });
  afterEach(() => {
    cy.logout();
  });
  describe('Applicant - on boarder test 1: check the onboarder content ', function() {
    it('Should have on boarder when go to applicant view 1st time', function() {
      cy.clickOnJobItem(1);
      ApplicantNavigate.shouldHaveApplicantOnboarderContent();
      cy.clickOnAllJobs('side');
      cy.clickOnJobItem(1);
      ApplicantNavigate.shouldNotHaveOnboarderModule();
      cy.get('[data-cy="onboarding-link"]')
        .contains('How do I sort applicants by suitability for my job?')
        .click(); //click on applicant onboarder link
      ApplicantNavigate.shouldHaveApplicantOnboarderContent();
    });
  });

  describe('Applicant - on boarder test 2: onboarder should not appear after skip button pressed', function() {
    it('Should have on boarder when go to applicant view', function() {
      cy.clickOnJobItem(1);
      cy.skipApplicantsOnboarding();
      cy.logout();
      checkLogoutPage();
      cy.home();
      cy.login('Manage Applicant Test Account', 'Job Admin');
      cy.clickOnJobItem(1);
      ApplicantNavigate.shouldNotHaveOnboarderModule();
    });
  });
});
