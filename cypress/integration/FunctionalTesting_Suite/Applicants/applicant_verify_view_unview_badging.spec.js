import * as JobsNavigate from '../../../support/shared/job_page.functions';
import {checkLogoutPage} from '../../../support/shared/common.functions';

describe('Applicant - View/unviewed badging test', function() {
  beforeEach(() => {
    cy.home();
    cy.captureGraphqlRequest().login('Account For WriteDB', 'Job Admin');
  });
  afterEach(() => {
    cy.logout();
  });

  it('Validate the view and unview badging', function() {
    cy.waitGraphqlRequest('getApplicationsCount')
      .getUnviewedBadgeCountByIndex(1)
      .then((count) => {
        cy.clickOnJobItem(1);
        cy.skipApplicantsOnboardingIfExist();
        cy.clickUnviewedCandidateByIndex(0);
        JobsNavigate.shouldHaveCardLoaderLoaded();
        //TODO: data validation
        cy.clickOnAllJobs('left');
        //verify the unview has reduced 1
        JobsNavigate.checkUnviewedBadge(1, count - 1);
        //log out and login again check the unviewed has been remained.
        cy.logout();
        checkLogoutPage();

        cy.home();
        cy.login('Account For WriteDB', 'Job Admin');
        JobsNavigate.shouldHaveApplicantsCountGet();
        JobsNavigate.checkUnviewedBadge(1, count - 1);
      });
  });
});
