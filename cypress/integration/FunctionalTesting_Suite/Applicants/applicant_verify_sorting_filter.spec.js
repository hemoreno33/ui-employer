import * as ManageJobsNavigate from '../../../support/shared/manage_applicants_page.functions';
import {cardLoaderShouldNotExist} from '../../../support/shared/job_page.functions';
import {step} from 'mocha-steps';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Applicant - Sort applicants test', function() {
  before(() => {
    cy.fixture('Candidate.json').as('Candidate');
    cy.home();
    cy.login('Manage Applicant Test Account', 'Job Admin')
      .clickOnJobItem(1)
      .skipApplicantsOnboarding();
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('Test to sort applicants by WCC/Jobkred/DateApplied', () => {
    step('Sort by WCC', function() {
      cy.clickOnSortBy('Job match by WCC');
      cardLoaderShouldNotExist();
      cy.clickOnCandidate(0); // By clicking first candidate, making cypress to wait indirectly till candidates load.
      ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(0);
      ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(1);
      ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(2);
      ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(3);
      ManageJobsNavigate.shouldNotHaveTopMatcherInApplicantList(4);
      ManageJobsNavigate.shouldNotHaveTopMatcherInApplicantList(5);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant6, 0);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant5, 1);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant4, 2);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant2, 3);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant1, 4);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant3, 5);
    });

    step('Sort by Jobkred', function() {
      cy.clickOnSortBy('Job match by JobKred');
      cardLoaderShouldNotExist();
      cy.clickOnCandidate(0); // By clicking first candidate, making cypress to wait indirectly till candidates load.
      ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(0);
      ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(1);
      ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(2);
      ManageJobsNavigate.shouldNotHaveTopMatcherInApplicantList(3);
      ManageJobsNavigate.shouldNotHaveTopMatcherInApplicantList(4);
      ManageJobsNavigate.shouldNotHaveTopMatcherInApplicantList(5);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant1, 0);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant5, 1);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant6, 2);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant2, 3);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant4, 4);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant3, 5);
    });

    step('Sort by Date applied', function() {
      cy.clickOnSortBy('Date applied');
      cardLoaderShouldNotExist();
      cy.clickOnCandidate(0); // By clicking first candidate, making cypress to wait indirectly till candidates load.
      ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(0);
      ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(1);
      ManageJobsNavigate.shouldNotHaveTopMatcherInApplicantList(2);
      ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(3);
      ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(4);
      ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(5);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant1, 0);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant2, 1);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant3, 2);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant4, 3);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant5, 4);
      ManageJobsNavigate.applicantLeftPanelNameValidation(this.Candidate.Applicant.Applicant6, 5);
    });
  });
});
