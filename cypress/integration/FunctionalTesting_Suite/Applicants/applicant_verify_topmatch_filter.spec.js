import * as ManageJobsNavigate from '../../../support/shared/manage_applicants_page.functions';
import * as JobsPageNavigate from '../../../support/shared/job_page.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Applicant - Top Matches applicants test', function() {
  const verifyTopMatcherInApplicationList = (numOfApplicants) => {
    cy.wrap(Cypress._.range(0, numOfApplicants))
      .wait(500) // wait till page render after loader disappear
      .each(($el, index) => {
        ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(index);
      });
  };
  before(() => {
    cy.home();
    cy.login('Manage Applicant Test Account', 'Job Admin');
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('it filters top matching applicants only when sort applicants by WCC/Jobkred/DateApplied and checkbox is checked', function() {
    cy.clickOnJobItem(1);
    cy.skipApplicantsOnboardingIfExist();
    cy.log('Sort by WCC');
    cy.clickOnSortBy('Job match by WCC');
    cy.clickOnTopMatchCheckbox();
    JobsPageNavigate.shouldHaveCardLoaderLoading();
    JobsPageNavigate.cardLoaderShouldNotExist();
    verifyTopMatcherInApplicationList(4);
    ManageJobsNavigate.shouldHaveTopMatchTotalShown(4);
    cy.log('Sort by Jobkred');
    cy.clickOnSortBy('Job match by JobKred');
    cy.clickOnTopMatchCheckbox();
    JobsPageNavigate.shouldHaveCardLoaderLoading();
    JobsPageNavigate.cardLoaderShouldNotExist();
    verifyTopMatcherInApplicationList(3);
    ManageJobsNavigate.shouldHaveTopMatchTotalShown(3);
    cy.log('Sort by Date applied');
    cy.clickOnSortBy('Date applied');
    cy.clickOnTopMatchCheckbox();
    JobsPageNavigate.shouldHaveCardLoaderLoading();
    JobsPageNavigate.cardLoaderShouldNotExist();
    verifyTopMatcherInApplicationList(5);
    ManageJobsNavigate.shouldHaveTopMatchTotalShown(5);
  });
  it('it should disable top match checkbox when there are no top matches found', function() {
    cy.home();
    cy.clickOnJobItem(2);
    cy.skipApplicantsOnboardingIfExist();
    ManageJobsNavigate.shouldHaveTopMatchTotalShown(0);
    cy.get('[data-cy="top-match-checkbox"]').should('be.disabled');
  });
});
