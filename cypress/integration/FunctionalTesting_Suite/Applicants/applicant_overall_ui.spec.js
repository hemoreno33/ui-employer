import * as ManageJobsNavigate from '../../../support/shared/manage_applicants_page.functions';
import * as JobsPageNavigate from '../../../support/shared/job_page.functions';
import jobs from '../../../fixtures/JobList';

describe('Manage Applicants Page - UI', function() {
  beforeEach(() => {
    cy.fixture('Candidate.json').as('applicant');
    cy.home();
    cy.login('Manage Applicant Test Account', 'Job Admin');
  });
  afterEach(() => {
    cy.logout();
  });

  it('UI checking - Manage Applicants View', function() {
    JobsPageNavigate.validateJobListExistence('found');
    cy.clickOnJobItem(1);
    cy.skipApplicantsOnboarding();
    ManageJobsNavigate.manageApplicantsView(jobs.J2);
  });
});
