import {
  clickJobByJobPostID,
  shouldHaveCardLoaderLoading,
  cardLoaderShouldNotExist,
} from '../../support/shared/job_page.functions';
import {step} from 'mocha-steps';

describe('Validate Job Posting successful acknowledgment page & presented posted job from all jobs in Employer page', function() {
  before(() => {
    cy.fixture('E2EJobPosting.json').as('job');
    cy.fixture('E2ECandidate.json').as('Candidate');
  });
  step('should verify all fields in job post acknowledgement page & verify job post in all jobs page', function() {
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
    cy.clickOnJobTab('Open');
    cy.clickOnSortJobsBy('Posted Date');
    cy.searchForJobs('MCF-2019-0000961');
    shouldHaveCardLoaderLoading();
    cardLoaderShouldNotExist();
    clickJobByJobPostID('MCF-2019-0000961');
    cy.skipApplicantsOnboarding();
    shouldHaveCardLoaderLoading();
    cardLoaderShouldNotExist();
    // Need to implement for verify applicant details
  });
});
