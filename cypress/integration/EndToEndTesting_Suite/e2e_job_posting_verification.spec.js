import {fillJobDescriptionStep} from '../../../support/shared/job_post_description_page.functions';
import {searchAndSelectAddMoreSkillSection} from '../../../support/shared/job_post_skills_page.functions';
import {fillKeyInformationOnJobPosting} from '../../../support/shared/job_post_key_information_page.functions';
import {
  selectSameLocationFromWorkplaceDetails,
  fillLocalWorkplaceDetails,
} from '../../../support/shared/job_post_workplace_details_page.functions';
import {
  previewJobDescriptionDetails,
  previewSkillsDetails,
  previewKeyInformationDetails,
  previewWorkplaceDetails,
} from '../../../support/shared/job_post_preview_page.functions';
import {
  verifyFieldsInAcknowledgementPage,
  verifyJobPostPresentInAllJobsList,
  getJobPostId,
} from '../../../support/shared/job_post_success_page.functions';
import {onClickNext, onClickSubmitJobPost, suppressBeforeUnloadEvent} from '../../../support/shared/common.functions';
import {step} from 'mocha-steps';

describe('Validate Job Posting successful acknowledgment page & presented posted job from all jobs in Employer page', function() {
  before(() => {
    suppressBeforeUnloadEvent();
    cy.fixture('E2EJobPosting.json').as('job');
  });
  step('should verify all fields in job post acknowledgement page & verify job post in all jobs page', function() {
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
    cy.clickNewJobPosting();
    cy.shouldHaveFormLoaderLoaded();
    fillJobDescriptionStep(this.job.jobPost);
    onClickNext();
    searchAndSelectAddMoreSkillSection(this.job.jobPost.skillNotFromDropDown);
    searchAndSelectAddMoreSkillSection(this.job.jobPost.skillFromDropDown);
    onClickNext();
    fillKeyInformationOnJobPosting(this.job.jobPost);
    onClickNext();
    selectSameLocationFromWorkplaceDetails();
    fillLocalWorkplaceDetails(this.job.jobPost.jobWorkplaceDetail);
    onClickNext();
    previewJobDescriptionDetails(this.job.jobPost);
    previewSkillsDetails(Object.values(this.job.jobPost.previewSkills));
    previewKeyInformationDetails(this.job.jobPost.keyInformation);
    previewWorkplaceDetails(this.job.jobPost.jobWorkplaceDetail);
    onClickSubmitJobPost();
    verifyFieldsInAcknowledgementPage(this.job.jobPost);
    getJobPostId().then((jobPostId) => {
      verifyJobPostPresentInAllJobsList(jobPostId, this.job.jobPost.jobTitle);
    });
  });
});
