#!/bin/sh
set -eux
echo "start running entrypoint.sh"

rm -fr mochawesome-report/*
rm -fr cypress/report/*
cypress run --spec cypress/integration/FunctionalTesting_Suite/**/*.spec.js || true
mkdir -p mochawesome-report/screenshots 
npx mochawesome-merge --reportDir cypress/report > mochawesome-report/mochawesome.json
cp cypress/screenshots/**/**/**/*.png mochawesome-report/screenshots/ 2>/dev/null || true
npx mochawesome-report-generator mochawesome-report/mochawesome.json
