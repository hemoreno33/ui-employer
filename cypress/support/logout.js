Cypress.Commands.add('logout', () => {
  cy.get('[data-cy=user-fullname]').trigger('mouseover');
  cy.get('#logout-button').click({force: true});
});
