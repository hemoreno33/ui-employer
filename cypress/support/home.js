// auth options code make it outside since it's used for authenticate while routing a page
function getOptions() {
  return Cypress.env('username')
    ? {
        auth: {
          username: Cypress.env('username'),
          password: Cypress.env('password'),
        },
      }
    : {};
}
Cypress.Commands.add('home', () => {
  cy.visit('/', getOptions());
});
Cypress.Commands.add('visitPage', (routePath) => {
  cy.visit(routePath, getOptions());
});
