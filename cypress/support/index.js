// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './login';
import './logout';
import './home';
import './commands/job_page.commands';
import './commands/job_post_creation.commands';
import './commands/manage_applicants.commands';
import './commands/common.commands';

// Alternatively you can use CommonJS syntax:
// require('./commands')

Cypress.on('test:after:run', (test, runnable) => {
  const addContext = require('mochawesome/addContext');
  if (test.state == 'failed') {
    addContext(
      {test},
      `./screenshots/${runnable.parent.title.replace(/\//g, '')} -- ${test.title.replace(/\//g, '')} (failed).png`,
    );
  }
});
