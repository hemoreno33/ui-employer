export const OPEN_JOB_REQUEST_ALIAS = 'openJobRequest';
export const CLOSE_JOB_REQUEST_ALIAS = 'closeJobRequest';
export const GRAPHQL_REQUEST_ALIAS = 'graphqlRequest';
export const COMPANY_PROFILE_ALIAS = 'companyProfileRequest';
export const VIRUS_SCANNER_ALIAS = 'virusScannerRequest';

// After login, store "mcf-employer-account-expiry" value in localStorage since navigate page without refresh & re-authenticate.
export const ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY = 'mcf-employer-account-expiry';

Cypress.Commands.add('captureOpenJobRequest', () => {
  cy.captureNetworkRequest();
  cy.server();
  cy.route({
    method: 'GET',
    url: '**/jobs?jobStatuses=[102,103*',
  }).as(OPEN_JOB_REQUEST_ALIAS);
});

Cypress.Commands.add('captureClosedJobRequest', () => {
  cy.captureNetworkRequest();
  cy.server();
  cy.route({
    method: 'GET',
    url: '**/jobs?jobStatuses=[9*',
  }).as(CLOSE_JOB_REQUEST_ALIAS);
});

Cypress.Commands.add('captureGraphqlRequest', () => {
  cy.captureNetworkRequest();
  cy.server();
  cy.route({
    method: 'POST',
    url: '**/profile',
  }).as(GRAPHQL_REQUEST_ALIAS);
});

Cypress.Commands.add('captureCompanyProfile', () => {
  cy.captureNetworkRequest();
  cy.server();
  cy.route({
    method: 'GET',
    url: '**/companies/**',
  }).as(COMPANY_PROFILE_ALIAS);
});

Cypress.Commands.add('captureVirusScannerRequest', () => {
  cy.captureNetworkRequest();
  cy.server();
  cy.route({
    method: 'POST',
    url: Cypress.config('apiVirusScanner') + '/',
  }).as(VIRUS_SCANNER_ALIAS);
});

let perserveLocalStorageKeys = [];
Cypress.Commands.add('preserveLocalStorageOnce', (localStorageKey) => {
  perserveLocalStorageKeys.push(localStorageKey);
});

const clear = Cypress.LocalStorage.clear;
Cypress.LocalStorage.clear = (keys = []) => {
  if (perserveLocalStorageKeys.length > 0 && keys.length == 0) {
    const localStorageKeys = Object.keys(localStorage);
    const filteredKeys = localStorageKeys.filter((key) => perserveLocalStorageKeys.indexOf(key) < 0);
    perserveLocalStorageKeys = [];
    if (filteredKeys.length > 0) {
      return clear.apply(Cypress.LocalStorage, [filteredKeys]);
    }
  } else {
    return clear.apply(Cypress.LocalStorage, [keys]);
  }
};

Cypress.Commands.add('verifyAllSelectedValue', (selectedValueSelector, selectedValueFromFixture) => {
  const selectedValueInDropDown = [];
  cy.get(selectedValueSelector, {
    timeout: 20000,
  })
    .each(($el) => {
      selectedValueInDropDown.push($el.text());
    })
    .then(() => {
      expect(selectedValueFromFixture).to.have.members(selectedValueInDropDown);
    });
});

Cypress.Commands.add('verifyAllSelectedValueNotExist', (selectedValueSelector, selectedValueFromFixture) => {
  const selectedValueInDropDown = [];
  cy.get(selectedValueSelector, {
    timeout: 20000,
  })
    .each(($el) => {
      selectedValueInDropDown.push($el.text());
    })
    .then(() => {
      expect(selectedValueInDropDown).not.have.members(selectedValueFromFixture);
    });
});

Cypress.Commands.add('verifyPreviewValue', (selector, label, value) => {
  cy.get(selector)
    .contains(label)
    .next()
    .should('have.text', value);
});

Cypress.Commands.add('waitGraphqlRequest', (operationName) => {
  const isAliases = Array.isArray(operationName) && operationName.length > 1;
  const waitAlias = isAliases ? operationName.map(() => `@${GRAPHQL_REQUEST_ALIAS}`) : `@${GRAPHQL_REQUEST_ALIAS}`;
  cy.wait(waitAlias).then((xhr) => {
    if (isAliases) {
      const requestOperationName = xhr.map(({request}) => request.body.operationName);
      const differences = operationName.filter((opName) => {
        const i = requestOperationName.indexOf(opName);
        if (i >= 0) {
          requestOperationName.splice(i, 1);
          return false;
        } else {
          return true;
        }
      });
      if (differences.length > 0) {
        return cy.waitGraphqlRequest(differences);
      }
    } else {
      if (xhr.request.body.operationName !== String(operationName)) {
        return cy.waitGraphqlRequest(operationName);
      }
    }
  });
});

Cypress.Commands.add('shouldHaveValueAndReadOnlyAttr', (selector, value) => {
  cy.get(selector)
    .should('have.value', value)
    .should('have.attr', 'readonly');
});

Cypress.Commands.add('waitForGrowlToDisappear', () => {
  cy.get('.Toastify__toast-body', {timeout: 20000}).should('not.exist');
});

Cypress.Commands.add('verifyNotificationGrowl', (message) => {
  cy.get('[role=alert]')
    .should('have.text', message)
    .should('exist');
  cy.waitForGrowlToDisappear();
});

Cypress.Commands.add('clickButton', (selector) => {
  cy.get(selector).click();
});

Cypress.Commands.add(
  'paste',
  {
    prevSubject: 'element',
  },
  (subject, pastePayload) => {
    Cypress.log({name: 'paste', message: pastePayload});

    // if element type=input, do the below hack. Somehow input paste event doesn't trigger propagate to trigger change event
    if (subject.is('input')) {
      subject[0].value = pastePayload;
      // type a space and then removing it to trigger the onChange event
      return cy.wrap(subject).type(' {backspace}');
    }

    const clipboardData = new DataTransfer();
    clipboardData.setData('text/plain', pastePayload);
    const pasteEvent = new ClipboardEvent('paste', {bubbles: true, cancelable: true, clipboardData});
    subject[0].dispatchEvent(pasteEvent);

    return subject;
  },
);

Cypress.Commands.add(
  'uploadFile',
  {
    prevSubject: 'element',
  },
  (subject, base64Content, filename) => {
    Cypress.log({name: 'uploadFile', message: filename});
    Cypress.Blob.base64StringToBlob(base64Content).then((blob) => {
      const file = new File([blob], filename);
      const dataTransfer = new DataTransfer();
      dataTransfer.items.add(file);
      subject[0].files = dataTransfer.files;
    });
    return cy.wrap(subject).trigger('change');
  },
);

Cypress.Commands.add(
  'fill',
  {
    prevSubject: 'element',
  },
  (subject, text, {paste = false} = {}) => {
    Cypress.log({name: 'fill', message: text});
    if (paste) {
      return cy
        .wrap(subject)
        .clear()
        .paste(text);
    }
    return cy
      .wrap(subject)
      .clear()
      .type(text);
  },
);

Cypress.Commands.add('captureNetworkRequest', () => {
  // there is an issue with cypress not capturing Fetch API request
  // https://github.com/cypress-io/cypress/issues/1619#issuecomment-383587597
  Cypress.on('window:before:load', (win) => {
    win.fetch = null;
  });
});

Cypress.Commands.add('mockGraphQL', (endPoint, stubResponses) => {
  cy.on('window:before:load', (win) => {
    cy.stub(win, 'fetch', (...args) => {
      const [url, request] = args;
      if (url.indexOf('api') !== -1 && url.indexOf(endPoint) !== -1 && request.method === 'POST') {
        const postBody = JSON.parse(request.body);
        const stub = stubResponses[postBody.operationName];
        if (stub) {
          const stubVariables =
            stub.variables === undefined ? true : Cypress._.isMatch(postBody.variables, stub.variables);
          if (stubVariables) {
            return Promise.resolve({
              ok: stub.ok === undefined ? true : stub.ok,
              text() {
                return Promise.resolve(JSON.stringify(stub.response));
              },
              json() {
                return Promise.resolve(stub.response);
              },
            });
          }
        }
      }
      return fetch(...args); // Allowing to make real API call
    }).as('MOCK_GRAPHQL');
  });
});
