/// <reference types="Cypress" />

// This section contains collections of method related to applying jobs via API.
// These methods can be used directly for applying jobs by passing Uuid. However, if the
// employer portal is logged in before calling these methods or if jobs are created using apiJobs,
// then the existing employer session will be logged out, need to re-login
// into the employer portal.
// TODO: application status update for applied job.

import faker from 'faker';
import moment from 'moment';

export const apiJobseeker = {
  getAccessToken(nric) {
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'GET',
        url: `${Cypress.config('jobseekerAccount')}/sp/cb?RelayState=${Cypress.config(
          'jobseekerUrl',
        )}/&userName=${nric}`,
      }).then((response) => {
        resolve(response.body.match('value="(.*)" />')[1]);
      });
    });
  },

  getIndividualId(nric, accessToken) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName": "profileByNric",
        "variables": {
            "nric": "${nric}"
        },
        "query":"query profileByNric($nric: String!) {  profile: profileByNric(nric: $nric) {    id    }  }"
    }`;
      this.profileTemplate(accessToken, body).then((response) => {
        if (JSON.stringify(response.body).includes('errors')) {
          this.createProfile(accessToken).then((response) => {
            resolve(response.body.data.profile.create.id);
          });
        } else {
          resolve(response.body.data.profile.id);
        }
      });
    });
  },

  createProfile(accessToken) {
    return new Cypress.Promise((resolve) => {
      const body = `{
          "operationName":"createPersonalParticular",
          "variables":{
              "personalParticular":{
                  "name":"${faker.name.findName()}",
                  "preferredName":"${faker.name.firstName()}",
                  "email":"${faker.internet.email()}",
                  "employmentStatus":{"id":"1"},
                  "contactNumber":{
                      "areaCode":null,
                      "countryCode":"65",
                      "telephoneNumber":"91234567"
                    },"unemploymentSince":null
                }
            },
            "query":"mutation createPersonalParticular($personalParticular: ProfileInput!) {  profile {    create(input: $personalParticular) {      id    }  }}"}`;
      this.profileTemplate(accessToken, body).then((response) => {
        resolve(response);
      });
    });
  },

  createEductionIfNot(nric, accessToken) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName": "profileByNric",
        "variables": {
            "nric": "${nric}"
        },
        "query":"query profileByNric($nric: String!) {  profile: profileByNric(nric: $nric) {    academicQualifications {      id         }  }}"
    }`;
      this.profileTemplate(accessToken, body).then((response) => {
        if (response.body.data.profile.academicQualifications.length < 1) {
          this.createEducation(accessToken).then((response) => {
            resolve(response);
          });
        }
        resolve(response);
      });
    });
  },

  createEducation(accessToken) {
    return new Cypress.Promise((resolve) => {
      const body = `{
          "operationName":"createEducation",
          "variables":{
              "education":{
                  "qualification":{
                      "qualificationAttainedCode":"70",
                      "fieldOfStudyCode":"1152"
                    },
                    "institution":"MIT",
                    "yearAttained":"2001-01-01",
                    "isHighest":true
                }
            },
            "query":"mutation createEducation($education: EducationInput!) {  education {    create(input: $education) {      id      qualification {        qualificationAttained        qualificationAttainedCode        fieldOfStudy        fieldOfStudyCode        name      }      institution      yearAttained      isHighest    }  }}"}`;
      this.profileTemplate(accessToken, body).then((response) => {
        resolve(response);
      });
    });
  },

  getDocumentID(nric, accessToken, index = 0) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName": "profileByNric",
        "variables": {
            "nric": "${nric}"
        },
        "query":"query profileByNric($nric: String!) {  profile: profileByNric(nric: $nric) {    documents {      id    }  }}"
    }`;

      this.profileTemplate(accessToken, body).then((response) => {
        if (response.body.data.profile.documents.length < 1) {
          this.uploadDocument(accessToken).then((response) => {
            resolve(response.body.data.document.create.id);
          });
        } else {
          resolve(response.body.data.profile.documents[index].id);
        }
      });
    });
  },

  uploadDocument(accessToken) {
    return new Cypress.Promise((resolve) => {
      const documentName = 'functionalSample.pdf';
      cy.fixture(`resume/${documentName}`, 'base64').then((resumeBase64) => {
        const body = `{
            "operationName":"uploadDocument",
            "variables":{
                "base64File":"${resumeBase64}",
                "documentName":"${documentName}"
            },
            "query":"mutation uploadDocument($base64File: String!, $documentName: String!) {  document {    create(base64File: $base64File, documentName: $documentName) {      id      fileName      fileSize      isDefault      url      createdOn    }  }}"}`;
        this.profileTemplate(accessToken, body).then((response) => {
          resolve(response);
        });
      });
    });
  },

  submitApplication(jobUuid, individualId, documentID, accessToken) {
    return new Cypress.Promise((resolve) => {
      const source = 'MCF';
      const body = `{
          "operationName":"submitApplication",
          "variables":{
              "applicationData":{
                  "individualId":"${individualId}",
                  "jobUuid":"${jobUuid}",
                  "documentId":"${documentID}",
                  "source":{"name":"${source}"
                }
            }
        },
        "query":"mutation submitApplication($applicationData: JobApplicationInput!) {jobApplication {   create(input: $applicationData) {     applicationId      jobId    }  }}"}`;
      this.profileTemplate(accessToken, body).then((response) => {
        resolve(response);
      });
    });
  },

  postWorkExperienceIfNot(nric, accessToken) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName": "profileByNric",
        "variables": {
            "nric": "${nric}"
        },
        "query":"query profileByNric($nric: String!) {  profile: profileByNric(nric: $nric) {    employmentHistories {      id         }  }}"
    }`;
      this.profileTemplate(accessToken, body).then((response) => {
        if (response.body.data.profile.employmentHistories.length < 1) {
          this.postWorkExperience(accessToken).then((response) => {
            resolve(response);
          });
        }
        resolve(response);
      });
    });
  },

  postWorkExperience(accessToken) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName":"postWorkExperience",
        "variables": {
          "workExperience": {
            "isCurrentJob": true,
            "organizationName": "MCF",
            "jobTitle": "Senior Software Architect",
            "occupations": [{"ssocCode": 10237}],
            "startDate": "2001-01-01",
            "industry": {
              "code": "62014"
            },
            "employmentTypes": [{"id": "07"}],
            "endDate": null
          }
        },
      "query":"mutation postWorkExperience($workExperience: EmploymentHistoryInput!) {  employmentHistory {create(input: $workExperience) { id      organizationName jobTitle jobDescription startDate endDate isCurrentJob occupations { ssocCode } industry { code } employmentTypes { id } } }}"}`;
      this.profileTemplate(accessToken, body).then((response) => {
        resolve(response);
      });
    });
  },

  getSkills(nric, accessToken) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName":"getSkills",
        "variables":{
          "nric":"${nric}"
        },
        "query":"query getSkills($nric: String!) {  profile: profileByNric(nric: $nric) { skills { id skill }  }}"
      }`;
      this.profileTemplate(accessToken, body).then((response) => {
        resolve(response);
      });
    });
  },

  deleteSkills(accessToken, skills = []) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName":"deleteSkills",
        "variables":{
          "input":{
            "skills":[${skills.toString()}]
            }
          },
        "query":"mutation deleteSkills($input: SkillInput!) {  skills { delete(input: $input)  }}"
      }`;
      this.profileTemplate(accessToken, body).then((response) => {
        resolve(response);
      });
    });
  },

  createSkills(accessToken, skills = []) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName":"createSkills",
        "variables":{
          "input":{
            "skills":[${skills.toString()}]
          }
        },
        "query":"mutation createSkills($input: SkillInput!) { skills { create(input: $input)  }}"
      }`;
      this.profileTemplate(accessToken, body).then((response) => {
        resolve(response);
      });
    });
  },

  deleteAllAndCreateSkills(nric, accessToken) {
    return new Cypress.Promise((resolve) => {
      const defaultSkills = [25, 48, 189, 191, 192, 217, 580, 2653, 3487, 2412, 100084, 100085, 8747, 7935];
      this.getSkills(nric, accessToken).then((skillsResponse) => {
        const skillsId = skillsResponse.body.data.profile.skills.map((skill) => skill.id);
        this.deleteSkills(accessToken, skillsId).then(() => {
          this.createSkills(accessToken, defaultSkills).then(() => {
            resolve();
          });
        });
      });
    });
  },

  profileByNric(nric, accessToken) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName":"profileByNric",
        "variables":{
          "nric":"${nric}",
          "isFetchContactInfoFromMyInfo":false,
          "isSyncWithMSF":false
        },
        "query":"query profileByNric($nric: String!, $isFetchContactInfoFromMyInfo: Boolean, $isSyncWithMSF: Boolean) {  profile: profileByNric(nric: $nric, isFetchContactInfoFromMyInfo: $isFetchContactInfoFromMyInfo, isSyncWithMSF: $isSyncWithMSF) {    id    name    preferredName    email                contactNumber { telephoneNumber }    employmentHistories {      id      organizationName      jobTitle      jobDescription      startDate      endDate      isCurrentJob      occupations {        ssocCode      }      industry {        code      }      employmentTypes {        id      }      hasValidationError    }    academicQualifications {      qualification {        qualificationAttained        qualificationAttainedCode        fieldOfStudy        fieldOfStudyCode        name      }      institution      yearAttained      isHighest      hasValidationError    }    documents {       fileName      fileSize      isDefault      url    }    skills {      id      skill    }  }}"
      }`;
      this.profileTemplate(accessToken, body).then((response) => {
        resolve(response);
      });
    });
  },

  profileTemplate(accessToken, body) {
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'POST',
        url: `${Cypress.config('api')}/profile`,
        headers: {
          'Content-Type': 'application/json',
          Cookie: `access-token=${accessToken}`,
        },
        body: body,
        timeout: 25000,
      }).then((response) => {
        resolve(response);
      });
    });
  },

  applyJob(jobUuid, nrics = []) {
    const applyJobResponses = [];
    return new Cypress.Promise((resolve) => {
      cy.wrap(nrics)
        .each((nric) => {
          this.getAccessToken(nric).then((accessToken) => {
            this.getIndividualId(nric, accessToken).then((individualId) => {
              this.postWorkExperienceIfNot(nric, accessToken);
              this.deleteAllAndCreateSkills(nric, accessToken);
              this.createEductionIfNot(nric, accessToken).then(() => {
                this.getDocumentID(nric, accessToken).then((documentID) => {
                  this.submitApplication(jobUuid, individualId, documentID, accessToken).then((response) => {
                    applyJobResponses.push({
                      individualId: individualId,
                      applicaionId: response.body,
                    });
                  });
                });
              });
            });
          });
        })
        .then(() => {
          resolve(applyJobResponses);
        });
    });
  },

  // this method to create candidate details by calling profileByNric API in specific format for
  // below methods in manage_applicants_page.functions
  // shouldHaveCandidateListItemInfo,
  // shouldHaveCandidateRightTopPanelInfo,
  // shouldHaveCandidateRightBottomPanelInfo
  getCandidateProfileDetails(nric, type = 'applicant') {
    return new Cypress.Promise((resolve) => {
      this.getAccessToken(nric).then((candidateAccessToken) => {
        this.profileByNric(nric, candidateAccessToken).then((response) => {
          const candidateDetails = response.body.data.profile;
          const candidateDataToValidate = {
            name: type === 'applicant' ? candidateDetails.preferredName : candidateDetails.name,
            jobtitle: candidateDetails.employmentHistories[0].jobTitle,
            ...(type === 'applicant'
              ? {applyDate: `Applied on ${moment().format('D MMM YYYY')}`}
              : {lastLoginDate: 'Active'}),
            hasResume: true,
            jobcompany: candidateDetails.employmentHistories[0].organizationName,
            email: candidateDetails.email,
            mobileNumber:
              type === 'applicant'
                ? '+65' + candidateDetails.contactNumber.telephoneNumber
                : candidateDetails.contactNumber.telephoneNumber,
            skills: candidateDetails.skills.map((s) => s.skill),
            jobs: [
              {
                title: candidateDetails.employmentHistories[0].jobTitle,
                company: candidateDetails.employmentHistories[0].organizationName,
                year: candidateDetails.employmentHistories[0].startDate.substring(0, 4),
              },
            ],
            educations: [
              {
                year: candidateDetails.academicQualifications[0].yearAttained.substring(0, 4),
                // eslint-disable-next-line @typescript-eslint/camelcase
                ssec_foc: candidateDetails.academicQualifications[0].qualification.fieldOfStudy,
                institution: candidateDetails.academicQualifications[0].institution,
              },
            ],
          };
          cy.log(candidateDataToValidate);
          resolve(candidateDataToValidate);
        });
      });
    });
  },
};
