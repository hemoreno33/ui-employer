/* eslint-disable @typescript-eslint/camelcase */
// This section contains collections of method to manage applications for the jobs via CouchDB API's

export const couchDBApi = {
  find(query) {
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'POST',
        url: `${Cypress.config('couchDbUrl')}/_find`,
        headers: {
          'Content-Type': 'application/json',
        },
        body: query,
        timeout: 25000,
      }).then((response) => {
        resolve(response.body);
      });
    });
  },

  update(updatePayLoad) {
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'PUT',
        url: `${Cypress.config('couchDbUrl')}/${updatePayLoad._id}?conflicts=true`,
        headers: {
          'Content-Type': 'application/json',
        },
        body: updatePayLoad,
        timeout: 25000,
      }).then((response) => {
        resolve(response.body);
      });
    });
  },
};
