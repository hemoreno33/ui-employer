/// <reference types="Cypress" />

// This section contains collections of method to manage Job for employer via API's
// All below method should be used after login into employer portal.
// TODO: Login employer via API

export const apiJobs = {
  createJob(jobDataPayLoad) {
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'POST',
        url: `${Cypress.config('api')}/${Cypress.config('apiVersion')}/jobs`,
        headers: {
          'Content-Type': 'application/json',
        },
        body: jobDataPayLoad,
      }).then((response) => {
        resolve(response.body);
      });
    });
  },

  editJob(uuid, jobUpdateDataPayLoad) {
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'PUT',
        url: `${Cypress.config('api')}/${Cypress.config('apiVersion')}/jobs/${uuid}`,
        headers: {
          'Content-Type': 'application/json',
        },
        body: jobUpdateDataPayLoad,
      }).then((response) => {
        resolve(response.body);
      });
    });
  },

  extendJobDuration(uuid, durationDate) {
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'PATCH',
        url: `${Cypress.config('api')}/${Cypress.config('apiVersion')}/jobs/${uuid}`,
        headers: {
          'Content-Type': 'application/json',
        },
        body: `{"expiryDate":"${durationDate}"}`,
      }).then((response) => {
        resolve(response.body);
      });
    });
  },

  closeJob(uuid, isVacant = false) {
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'PUT',
        url: `${Cypress.config('api')}/${Cypress.config('apiVersion')}/jobs/${uuid}/close`,
        headers: {
          'Content-Type': 'application/json',
        },
        body: `{"isVacant":${isVacant}}`,
      }).then((response) => {
        resolve(response);
      });
    });
  },
};
