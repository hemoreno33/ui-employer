// This section contains collections of method to bookmark applicant and talents

import {couchDBApi} from '../api/couchDBApi.js';

export const bookmarkApi = {
  setApplicationBookmark(applicationId, isBookmark = true) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName":"setApplicationBookmark",
        "variables":{
            "applicationId":"${applicationId}",
            "isBookmark":${isBookmark}
        },
        "query":"mutation setApplicationBookmark($applicationId: String!, $isBookmark: Boolean!) {  setApplicationBookmarkedOn(applicationId: $applicationId, isBookmark: $isBookmark) {    id    bookmarkedOn    __typename  }}"
    }`;
      // eslint-disable-next-line cypress/no-unnecessary-waiting
      cy.wait(2000) // Waiting 2 second, so that insertion wont happen at same  time for all applications which will to validate descending sort
        .request({
          method: 'POST',
          url: `${Cypress.config('api')}/profile`,
          headers: {
            'Content-Type': 'application/json',
          },
          body: body,
        })
        .then((response) => {
          resolve(response.body);
        });
    });
  },

  setSuggestedTalentBookmark(individualId, jobId, isBookmark = true) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName":"setSuggestedTalentBookmark",
        "variables":{
            "individualId":"${individualId}",
            "jobId":"${jobId}",
            "isBookmark":${isBookmark}
        },
        "query":"mutation setSuggestedTalentBookmark($individualId: String!, $jobId: String!, $isBookmark: Boolean!) {  setSuggestedTalentBookmark(individualId: $individualId, jobId: $jobId, isBookmark: $isBookmark)}"
    }`;
      // eslint-disable-next-line cypress/no-unnecessary-waiting
      cy.wait(2000) // Waiting 2 second, so that insertion wont happen at same time for all talent which will to validate descending sort
        .request({
          method: 'POST',
          url: `${Cypress.config('api')}/profile`,
          headers: {
            'Content-Type': 'application/json',
          },
          body: body,
        })
        .then((response) => {
          resolve(response.body);
        });
    });
  },

  bookmarkApplicantOrTalent(jobUuid, nric, type, isBookmark = true) {
    cy.log(`Bookmark ${type} - ${nric} for job: ${jobUuid}`);
    return new Cypress.Promise((resolve) => {
      if (type === 'applicant') {
        this.getApplicationId(jobUuid, nric).then((applicationId) => {
          this.setApplicationBookmark(applicationId, isBookmark).then((responseBody) => {
            resolve(responseBody);
          });
        });
      } else {
        cy.task('profileDB', `SELECT individual_id FROM jobseekers WHERE nric = '${nric}';`).then((result) => {
          this.setSuggestedTalentBookmark(result[0].individual_id, jobUuid, isBookmark).then((responseBody) => {
            resolve(responseBody);
          });
        });
      }
    });
  },

  getApplicationId(jobUuid, nric) {
    return new Cypress.Promise((resolve) => {
      cy.task('profileDB', `SELECT individual_id FROM jobseekers WHERE nric = '${nric}';`).then((result) => {
        couchDBApi
          .find({
            selector: {
              jobId: jobUuid,
              applicant: {
                id: result[0].individual_id,
              },
            },
          })
          .then((responseBody) => {
            resolve(responseBody.docs[0]._id);
          });
      });
    });
  },
};
