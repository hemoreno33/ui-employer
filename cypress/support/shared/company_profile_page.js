export const COMPANY_PROFILE_URL = '/company-profile';
export const EDIT_COMPANY_PROFILE_LINK_SELECTOR = '[data-cy="to-company-profile"]';

export const COMPANY_PROFILE_LOGO_SELECTOR = '[data-cy="company-profile-logotype"]';
export const COMPANY_PROFILE_LOGO_LABEL_SELECTOR = `${COMPANY_PROFILE_LOGO_SELECTOR} > label`;
export const COMPANY_PROFILE_LOGO_UPLOADER_SELECTOR = `${COMPANY_PROFILE_LOGO_SELECTOR} input[type=file]`;
export const COMPANY_PROFILE_LOGO_UPLOADER_ERROR_SELECTOR = `${COMPANY_PROFILE_LOGO_SELECTOR} [data-cy="logouploader-error"]`;
export const COMPANY_PROFILE_LOGO_IMG_SELECTOR = `${COMPANY_PROFILE_LOGO_SELECTOR} img`;

export const COMPANY_PROFILE_DESCRIPTION_SELECTOR = '[data-cy="company-profile-description"]';
export const COMPANY_PROFILE_DESCRIPTION_LABEL_SELECTOR = `${COMPANY_PROFILE_DESCRIPTION_SELECTOR} label`;
export const COMPANY_PROFILE_DESCRIPTION_FIELD_SELECTOR = `${COMPANY_PROFILE_DESCRIPTION_SELECTOR} div[class="DraftEditor-root"] div[contenteditable="true"]`;
export const COMPANY_PROFILE_DESCRIPTION_ERROR_SELECTOR = `${COMPANY_PROFILE_DESCRIPTION_SELECTOR} [data-cy="text-editor-error"]`;
export const COMPANY_PROFILE_DESCRIPTION_CHARACTER_COUNT_SELECTOR = `${COMPANY_PROFILE_DESCRIPTION_SELECTOR} [data-cy="text-editor-character-count"]`;

export const COMPANY_PROFILE_NUM_EMPLOYEE_SELECTOR = '[data-cy="company-profile-number-of-employees"]';
export const COMPANY_PROFILE_NUM_EMPLOYEE_LABEL_SELECTOR = `${COMPANY_PROFILE_NUM_EMPLOYEE_SELECTOR} label`;
export const COMPANY_PROFILE_NUM_EMPLOYEE_FIELD_SELECTOR = `${COMPANY_PROFILE_NUM_EMPLOYEE_SELECTOR} input[type=number]`;
export const COMPANY_PROFILE_NUM_EMPLOYEE_ERROR_SELECTOR = `${COMPANY_PROFILE_NUM_EMPLOYEE_SELECTOR} #number-of-employees-error`;

export const COMPANY_PROFILE_WEBSITE_SELECTOR = '[data-cy="company-profile-website"]';
export const COMPANY_PROFILE_WEBSITE_LABEL_SELECTOR = `${COMPANY_PROFILE_WEBSITE_SELECTOR} label`;
export const COMPANY_PROFILE_WEBSITE_FIELD_SELECTOR = `${COMPANY_PROFILE_WEBSITE_SELECTOR} input[type=text]`;
export const COMPANY_PROFILE_WEBSITE_ERROR_SELECTOR = `${COMPANY_PROFILE_WEBSITE_SELECTOR} #company-website-error`;

export const COMPANY_PROFILE_SUBMIT_BUTTON_SELECTOR = '[data-cy="company-profile-save"]';
export const COMPANY_PROFILE_UPDATE_SUCCESS_MODAL_SELECTOR = '[data-cy="update-company-profile-modal"]';
export const COMPANY_PROFILE_UPDATE_SUCCESS_MODAL_OK_BUTTON_SELECTOR = '[data-cy="ok-button-redirect-company-profile"]';
