const previewJobDescriptionSelector = '[data-cy=job-description_preview]';
const previewSkillsSelector = '[data-cy=skills_preview] [data-cy=skill-icon] span';
const previewKeyInformationSelector = '[data-cy=key-information_preview]';
const workplaceDetailsPreviewSelector = '[data-cy=workplace-details_preview]';
export function previewUENCompanyDetails(previewUENCompanyDetails) {
  cy.verifyPreviewValue(
    previewJobDescriptionSelector,
    previewUENCompanyDetails.thirdPartyEmployerEnabledFieldLabel.uenOrCompanyNameLabel,
    previewUENCompanyDetails.previewUenOrCompanyName,
  );
  cy.verifyPreviewValue(
    previewJobDescriptionSelector,
    previewUENCompanyDetails.thirdPartyEmployerEnabledFieldLabel.typeOfRegistrationLabel,
    previewUENCompanyDetails.previewTypeOfRegistration,
  );
  cy.verifyPreviewValue(
    previewJobDescriptionSelector,
    previewUENCompanyDetails.thirdPartyEmployerEnabledFieldLabel.industryLabel,
    previewUENCompanyDetails.previewIndustry,
  );
}
export function previewJobDescriptionDetails(previewJobDescriptionValue) {
  cy.verifyPreviewValue(
    previewJobDescriptionSelector,
    previewJobDescriptionValue.jobDescriptionLabel.jobTitleLabel,
    previewJobDescriptionValue.jobTitle,
  );
  cy.verifyPreviewValue(
    previewJobDescriptionSelector,
    previewJobDescriptionValue.jobDescriptionLabel.jobOccupationLabel,
    previewJobDescriptionValue.jobOccupationSelection,
  );
  cy.verifyPreviewValue(
    previewJobDescriptionSelector,
    previewJobDescriptionValue.jobDescriptionLabel.jobDescriptionLabel,
    previewJobDescriptionValue.jobDescriptionValueFill,
  );
}
export function previewSkillsDetails(previewSkillsValue) {
  cy.verifyAllSelectedValue(previewSkillsSelector, previewSkillsValue);
}
export function previewKeyInformationDetails(previewKeyInformationValue) {
  cy.verifyPreviewValue(
    previewKeyInformationSelector,
    previewKeyInformationValue.jobPostDurationLabelCheck,
    previewKeyInformationValue.previewJobPostDurationValue,
  );
  cy.verifyPreviewValue(
    previewKeyInformationSelector,
    previewKeyInformationValue.jobPostNoOfVacancyLabelCheck,
    previewKeyInformationValue.jobPostNoOfVacancyValueFill,
  );
  cy.verifyPreviewValue(
    previewKeyInformationSelector,
    previewKeyInformationValue.jobPostCategoryLabelPreview,
    previewKeyInformationValue.jobPostCategoryPreviewValue,
  );
  cy.verifyPreviewValue(
    previewKeyInformationSelector,
    previewKeyInformationValue.jobPostPositionLevelLabelCheck,
    previewKeyInformationValue.jobPostPositionLevelValueFill,
  );
  cy.verifyPreviewValue(
    previewKeyInformationSelector,
    previewKeyInformationValue.jobPostMinNoOfExpLabelCheck,
    previewKeyInformationValue.jobPostMinNoOfExpValueFill,
  );
  cy.verifyPreviewValue(
    previewKeyInformationSelector,
    previewKeyInformationValue.jobPostEmploymentTypeLabelCheck,
    previewKeyInformationValue.jobPostEmploymentTypeValueFill,
  );
  cy.verifyPreviewValue(
    previewKeyInformationSelector,
    previewKeyInformationValue.jobPostMinOfQualificationLevelLabelCheck,
    previewKeyInformationValue.jobPostMinOfQualificationValueFill,
  );

  if (previewKeyInformationValue.jobPostFieldOfStudyValueFill.includes('Blank')) {
    cy.get(previewKeyInformationSelector)
      .contains(previewKeyInformationValue.jobPostFieldOfStudyLabelCheck)
      .should('not.exist');
  } else {
    cy.verifyPreviewValue(
      previewKeyInformationSelector,
      previewKeyInformationValue.jobPostFieldOfStudyLabelCheck,
      previewKeyInformationValue.jobPostFieldOfStudyValueFill,
    );
  }
  cy.verifyPreviewValue(
    previewKeyInformationSelector,
    previewKeyInformationValue.jobPostSalaryRangeLabelCheck,
    previewKeyInformationValue.jobPostSalaryMaxMinPreviewValue,
  );
}

export function previewWorkplaceDetails(previewWorkplaceValue) {
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    previewWorkplaceValue.jobPostWorkLocationLabel,
    previewWorkplaceValue.jobPostWorkLocationValue,
  );
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    previewWorkplaceValue.jobPostPostalCodeLabel,
    previewWorkplaceValue.jobPostPostalCodeFillValue,
  );
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    previewWorkplaceValue.jobPostBlockOrHouseNoLabel,
    previewWorkplaceValue.jobPostBlockOrHouseNoFillValue,
  );
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    previewWorkplaceValue.jobPostStreetNameLabel,
    previewWorkplaceValue.jobPostStreetNameFillValue,
  );
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    previewWorkplaceValue.jobPostBuildingNameLabel,
    previewWorkplaceValue.jobPostBuildingNameFileValue,
  );
}
export function previewOverseasWorkPlaceDetails(previewWorkplaceValue) {
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    previewWorkplaceValue.jobPostWorkplaceLabel,
    previewWorkplaceValue.jobPostWorkLocationValue,
  );
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    previewWorkplaceValue.jobPostCountryLabel,
    previewWorkplaceValue.jobPostCountryFillValue,
  );
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    previewWorkplaceValue.jobPostOverseasAddress1Label,
    previewWorkplaceValue.jobPostOverseasAddress1FillValue,
  );
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    previewWorkplaceValue.jobPostOverseasAddress2Label,
    previewWorkplaceValue.jobPostOverseasAddress2FillValue,
  );
}
export function verifyLinkAndColorCodeEnable(jobPostTabDetails) {
  const label = jobPostTabDetails['label'].replace(/\s+/g, '-');
  cy.get(`[data-cy=${label}-link]`)
    .should('have.class', 'blue')
    .should('have.attr', 'href', jobPostTabDetails['link'])
    .find('div')
    .contains(jobPostTabDetails['label']);
}

export function verifyLinkAndColorCodeDisable(jobPostTabDetails) {
  const label = jobPostTabDetails['label'].replace(/\s+/g, '-');
  cy.get(`[data-cy=${label}-link]`)
    .should('have.class', 'disabled-link')
    .should('have.attr', 'href', jobPostTabDetails['link'])
    .find('div')
    .contains(jobPostTabDetails['label']);
}

export function reviewPagesOnJobPosting(jobReviewDetails) {
  cy.get('[data-cy="job-description"]')
    .find('h4')
    .should('contain', jobReviewDetails.reviewLabelJobDescription);
  cy.get('[data-cy="skills"]')
    .find('h4')
    .should('contain', jobReviewDetails.reviewLabelSkills);
  cy.get('[data-cy="key-information"]')
    .find('h4')
    .should('contain', jobReviewDetails.reviewKeyInformation);
  cy.get('[data-cy="workplace-details"]')
    .find('h4')
    .should('contain', jobReviewDetails.reviewWorkplaceDetails);
}

export function verifyEditLinkOptionOnJobPosting() {
  cy.get('[data-cy="job-description"]')
    .find('a')
    .should('have.attr', 'href', '/#job-description')
    .should('have.text', 'Edit');
  cy.get('[data-cy="skills"]')
    .find('a')
    .should('have.attr', 'href', '/#skills')
    .should('have.text', 'Edit');
  cy.get('[data-cy="key-information"]')
    .find('a')
    .should('have.attr', 'href', '/#key-information')
    .should('have.text', 'Edit');
  cy.get('[data-cy="workplace-details"]')
    .find('a')
    .should('have.attr', 'href', '/#workplace-details')
    .should('have.text', 'Edit');
}

export const clickOnLinkIn = (selector) => {
  cy.log('click on link by' + selector);
  cy.get(`[data-cy="${selector}"]`)
    .find('a')
    .click();
};

export const verifyGovernmentSupportScheme = (scheme, assertion = 'contain') => {
  cy.contains('div', 'Government Support').should(assertion, scheme);
};

export const clickSubmitJobPost = () => {
  cy.get('[data-cy=new-post-next]').click();
};
