const verifyMCFLogoLink = () => {
  cy.get('[data-cy=mcf-logo]').should('have.attr', 'href', '/');
};

const verifySwitchToJobseekerLink = () => {
  cy.get('[data-cy=switch-jobseeker]')
    .should('have.text', 'Switch to Jobseeker')
    .should('have.attr', 'href', 'https://www.mycareersfuture.sg');
};

const verifyPostJobsLink = () => {
  cy.get('[data-cy=post-jobs-link] span').should('have.text', 'Post Jobs');
  cy.get('[data-cy=post-jobs-link]').should('have.attr', 'href', '/jobs');
};

const verifyEmployerToolkitLink = () => {
  cy.get('[data-cy=employers-toolkit-link]')
    .should('have.text', 'Employers Toolkit')
    .should('have.attr', 'target', '_blank')
    .should('have.attr', 'href', 'https://content.mycareersfuture.sg/category/employers-toolkit');
};

export const verifyHeaderLinks = () => {
  verifyMCFLogoLink();
  verifySwitchToJobseekerLink();
  verifyPostJobsLink();
  verifyEmployerToolkitLink();
};
