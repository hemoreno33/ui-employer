import {shouldHaveCardLoaderLoading, cardLoaderShouldNotExist} from './job_page.functions';

export function verifyFieldsInAcknowledgementPage(jobPostSuccessfulFields) {
  cy.contains('div', jobPostSuccessfulFields.acknowledgementSuccessLabel);
  cy.get('[data-cy="success-job-title"]').should('have.text', jobPostSuccessfulFields.jobTitle);
  cy.get('[data-cy="success-job-expiry-date"]').contains(jobPostSuccessfulFields.acknowlegementExpiryDateLabel);
  cy.get('[data-cy="success-job-post-id"]').should(($div) => {
    const text = $div.text();
    expect(text).to.match(/^MCF-(\d){4}-(\d){7}/gi);
    expect(text).not.to.include('TEST');
  });
  cy.get('[data-cy="success-redirect-jobseeker"]').should(
    'have.text',
    jobPostSuccessfulFields.acknowlegementViewJobPostLabel,
  );
  cy.get('[data-cy="success-redirect-employer"]').should(
    'have.text',
    jobPostSuccessfulFields.acknowlegementReturnAllJobsLabel,
  );
}

export function getJobPostId() {
  return cy.get('[data-cy="success-job-post-id"]').invoke('text');
}

export function readLinkFromSuccessfulJobPostForJobSeekerPage() {
  return cy.get('a[data-cy="success-redirect-jobseeker"]').invoke('attr', 'href');
}

export function verifyJobPostPresentInAllJobsList(jobPostId, jobTitle) {
  cy.get('[data-cy="success-redirect-employer"]').click();
  cy.clickOnSortJobsBy('Posted Date');
  shouldHaveCardLoaderLoading();
  cardLoaderShouldNotExist();
  cy.get('[data-cy="job-list-item_job-id"]')
    .eq(0)
    .should('have.text', jobPostId);
  cy.get('[data-cy="job-list-item_job-title"]')
    .eq(0)
    .should('have.text', jobTitle);
}
