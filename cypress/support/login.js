import {userInfos, authAccess} from './users';

Cypress.Commands.add('login', (companyName, role) => {
  cy.get('[data-cy=login-with-corppass]').click();
  login(companyName, role);
});
Cypress.Commands.add('loginFromNavbar', (companyName, role) => {
  cy.get('[data-cy=navbar-login-with-corppass]').click();
  login(companyName, role);
});
Cypress.Commands.add('reLogin', (companyName, role) => {
  cy.logout();
  cy.reload();
  cy.clearCookies();
  cy.home();
  cy.login(companyName, role);
});
const login = (companyName, role) => {
  cy.url().should('include', Cypress.env('corppassUrl'));

  if (role) {
    const auth = authAccess.find((auth) => {
      return auth.role === 'Job Admin';
    });

    cy.get('#role').type(auth.role);
    cy.get('#startDate').type(auth.startDate);
    cy.get('button[name="add"').click();
  }

  const user = userInfos.find((user) => {
    return user.companyName === companyName;
  });

  const {accountType, singpassHolder, userId, userName, userFullName, entityType, entityId} = user;

  cy.get('#accountType').select(accountType);
  if (singpassHolder) {
    cy.get('#singpassHolder').check();
  }
  cy.get('#entityId').type(entityId);
  cy.get('#userName').type(userName);
  cy.get('#userId').type(userId);
  cy.get('#userFullName').type(userFullName);
  if (entityType === 'UEN') {
    cy.get('#uen').check();
  } else {
    cy.get('#nonuen').check();
  }
  cy.get('form:first').submit();

  cy.url().should('contain', Cypress.config('baseUrl'));
};
