// Check if value for inserting into a varchar type column in a insert SQL query statement is null or string
// and return appropriate value for inserting into DB
export const nullCheck = (value) => (value !== null ? value : 'NULL');
