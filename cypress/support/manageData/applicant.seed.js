import {seedXNewJobsForUEN} from '../manageData/job.seed';
import {apiJobseeker} from '../api/jobseekerApi';
export const createAJobAndApplyFor = (uen, nrics = [], payload = '') => {
  return new Cypress.Promise((resolve) => {
    seedXNewJobsForUEN(1, payload, uen).then((jobResponse) => {
      cy.log(jobResponse);
      applyAJobFor(jobResponse[0].uuid, nrics).then((applyResponses) => {
        resolve({job: jobResponse[0], applicants: applyResponses});
      });
    });
  });
};

export const applyAJobFor = (jobUuid, nrics = []) => {
  return new Cypress.Promise((resolve) => {
    apiJobseeker.applyJob(jobUuid, nrics).then((response) => {
      resolve(response);
    });
  });
};
