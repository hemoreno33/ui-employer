import {nullCheck} from './../util/db';

const defaultCompanyInfo = {
  uen: 'default_uen',
  description: null,
  logoFileName: null,
  logoUploadPath: null,
  name: 'default_name',
  ssicCode: null,
  registrationTypeId: null,
  employeeCount: null,
  companyUrl: null,
};

const defaultCompanyAddressInfo = {
  uen: 'default_uen',
  block: null,
  street: null,
  floor: '11',
  unit: '639',
  building: null,
  postalCode: '460427',
  purposeId: 1,
};

export const seedCompanyInfo = (amendCompanyInfo) => {
  const companyToSeed = {
    ...defaultCompanyInfo,
    ...amendCompanyInfo,
  };

  cy.task(
    'jobDB',
    `INSERT INTO company_info(uen,description,name,ssic_code) 
      VALUES (
        ${companyToSeed.uen},
        ${nullCheck(companyToSeed.description)},
        ${nullCheck(companyToSeed.name)},
        ${nullCheck(companyToSeed.ssicCode)});`,
  );

  cy.task(
    'jobDB',
    `INSERT INTO company_logos (uen, file_name, upload_path, file_hash, virus_scan_result)
      VALUES (
        ${companyToSeed.uen},
        ${nullCheck(companyToSeed.logoFileName)},
        ${nullCheck(companyToSeed.logoUploadPath)},
        null,
        null);`,
  );
};

export const seedCompanyAddressInfo = (amendCompanyAddressInfo) => {
  const companyAddressToSeed = {
    ...defaultCompanyAddressInfo,
    ...amendCompanyAddressInfo,
  };

  cy.task(
    'jobDB',
    `INSERT INTO company_addresses(uen,block,street,floor,unit,building,postal_code,purpose_id) 
        VALUES (
          ${companyAddressToSeed.uen},
          ${nullCheck(companyAddressToSeed.block)},
          ${nullCheck(companyAddressToSeed.street)},
          ${nullCheck(companyAddressToSeed.floor)},
          ${nullCheck(companyAddressToSeed.unit)},
          ${nullCheck(companyAddressToSeed.building)},
          ${nullCheck(companyAddressToSeed.postalCode)},
          ${nullCheck(companyAddressToSeed.purposeId)});`,
  );
};

export const updateCompanyInfo = (companyToSeed, uen) => {
  cy.task(
    'jobDB',
    `UPDATE company_info SET description = '${nullCheck(companyToSeed.description)}',
     employee_count = ${nullCheck(companyToSeed.employeeCount)},
     company_url = '${nullCheck(companyToSeed.companyUrl)}'
     WHERE uen = '${uen}';`,
  );

  cy.task(
    'jobDB',
    `UPDATE company_logos SET file_name = '${nullCheck(companyToSeed.updatedSrc)}',
     upload_path = '${nullCheck(companyToSeed.file)}',
     file_hash = null,
     virus_scan_result = null
     WHERE uen = '${uen}';`,
  );
};

export const deleteCompany = (uen) => {
  cy.task('jobDB', `DELETE FROM company_logos WHERE uen='${uen}';`);
  cy.task('jobDB', `DELETE FROM company_info WHERE uen='${uen}';`);
  cy.task('jobDB', `DELETE FROM company_addresses WHERE uen='${uen}';`);
};
