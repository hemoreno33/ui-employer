# Cypress Integration Test

# Usage

### Run cypress against employer-mcf-qa

```bash

# In ui-employer/cypress
docker-compose up

```

#### Settings

- **CYPRESS_baseUrl**: Sets url for the cypress runner to run test against defaults to https://employer-qa.ci.mcf.sh
- **CYPRESS_corppassUrl**: Sets Url for authentication defaults to https://account-qa.ci.mcf.sh/cp/login

##### When accessing from public internet, the following are required

- **CYPRESS_usename**: Basic auth username
- **Cypress_password**: Basic auth password

### Some wiki pages for cypress test data

https://gitlab.com/mycf.sg/ui-employer/wikis/How-to-get-your-candidate-appear-in-the-suggested-talent-list%3F

https://gitlab.com/mycf.sg/ui-employer/wikis/Test-Data-used-for-Cypress-Integration-in-UI-Employer

https://gitlab.com/mycf.sg/ui-employer/wikis/Where-to-update-the-Test-Data-(For-UI-Employer)

### How to run test in local

`npx cypress run --spec "{file path to run}"` Use this command to verify some of the test fail when push to remote

`yarn cypress:open-dev` Use this command to open the suite with localhost - ref. cypress/config/dev.json

`yarn cypress:open-qa` Use this command to open the suite with qa - ref. cypress/config/qa.json

`yarn cypress:open --env username=<username>,password=<password>` Use this command to open the suite with QA external URL, as same like in pipeline - ref. cypress/config/qa-external.json.
<username>, <password> need to passed as same as in https://vault.gds-gov.tech/#/secrets?secret/gds-ace/mcf/basic-auth/ OJMP_EMP_QA_USERNAME & OJMP_EMP_QA_PASSWORD

### How to run test in local with QA environment
Almost all spec could be executed locally, however some spec has DB check/validation so below setup is required for those spec.

create .env.[enviroment to run] (example `.env.qa`) in cypress/config/dbConfig

Input required value based on vault: https://vault.gds-gov.tech/#/secrets?path=secret%2Fgds-ace%2Fmcf%2Fdatabase

### Seed test Data
Any of the following can seed the test data for the test:
1. api-jobs and api-profile
2. APIs
3. DB
#### api-jobs and api-profile
Other repos control data, and these data cause issue when multiple pipeline run as the data are shared.
The data are seeded in
api-jobs repo - src/seeds/manual/qa/jobs.ts
api-profile - src/database/couchdb/seeds/qa/applications.ts

#### APIs
Can use to create non-shared dynamic data which cant be affected by other tests and can be controlled by the test it self.
APIs are recommended way to seed test data.

#### DB
Data inserted via backend without any business validation from the application can use to create non-shared dynamic data which cant be affected by other tests and controlled by test itself.

Available methods for both APIs and DB - cypress/support/manageData/applicant.seed.js, job.seed.js and test data - cypress/fixtures/seedData/api_job_post_seed_data.js
