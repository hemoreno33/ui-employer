import {isNil} from 'lodash/fp';
import React from 'react';
import {NumberLoader} from './NumberLoader';

export interface ITotalCountProps {
  count?: number | null;
  title: string;
  loading: boolean;
}

export const TotalCount: React.FunctionComponent<ITotalCountProps> = ({count, title, loading}) => {
  return (
    <>
      <h4 data-cy="total-count" className="f3 fw3 ma0 lh-solid w-100">
        {loading ? <NumberLoader /> : isNil(count) ? '-' : count}
      </h4>
      <span data-cy="total-count-title" className="f6 fw4 o-60 w-100">
        {title}
      </span>
    </>
  );
};
