import {mount} from 'enzyme';
import React from 'react';
import {ITotalCountProps, TotalCount} from '~/components/JobList/TotalCount';

describe('Jobs/TotalCount', () => {
  it('renders correctly', () => {
    const totalCountProps: ITotalCountProps = {
      loading: false,
      title: 'Applications',
    };

    let wrapper = mount(<TotalCount {...totalCountProps} />);
    expect(wrapper).toMatchSnapshot();

    totalCountProps.count = 0;
    wrapper = mount(<TotalCount {...totalCountProps} />);
    expect(wrapper).toMatchSnapshot();

    totalCountProps.count = 1;
    wrapper = mount(<TotalCount {...totalCountProps} />);
    expect(wrapper).toMatchSnapshot();
  });
});
