import React from 'react';
import styles from './JobList.scss';

export const NumberLoader: React.FunctionComponent = () => (
  <div className={`flex justify-center items-center ${styles.spinnerContainer}`}>
    <div className={styles.bounce1} />
    <div className={styles.bounce2} />
    <div className={styles.bounce3} />
  </div>
);
