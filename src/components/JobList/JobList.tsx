import React from 'react';
import {JobListItem} from '~/components/JobList/JobListItem';
import {CreateJobPostingLink} from '~/components/Navigation/NewJobPostButton/CreateJobPostingLink';
import {IJobPost} from '~/services/employer/jobs.types';

export interface IJobListProps {
  jobs: IJobPost[];
  total: number;
  hasSearchResultMessage?: boolean;
}

const searchResultMessage = (total: number) => {
  if (total) {
    return total > 1 ? `${total.toString()} jobs found` : '1 job found';
  }
  return 'No jobs found. Please try a different search term.';
};

export const JobList: React.FunctionComponent<IJobListProps> = ({jobs, total, hasSearchResultMessage = false}) => {
  const defaultEmptyMessage = (
    <>
      There are currently no jobs to display.
      <CreateJobPostingLink />
    </>
  );
  const jobListItems = jobs.map((job) => <JobListItem key={job.uuid} job={job} />);
  const showResultMessage = hasSearchResultMessage ? (
    <h1 data-cy="search-result" className="f4 lh-copy black-50 tc pa3 pt5">
      {searchResultMessage(total)}
    </h1>
  ) : null;
  const showDefaultMessage =
    hasSearchResultMessage || jobListItems.length ? null : (
      <h1 className="f4 lh-copy black-50 tc pa3 pt5">{defaultEmptyMessage}</h1>
    );

  return (
    <div className="center ph3-ns mv3" data-cy="job-list">
      {showResultMessage}
      {showDefaultMessage}
      <ul data-cy="job-list-items" className="center ph3-ns pv3 mv3">
        {jobListItems}
      </ul>
    </div>
  );
};
