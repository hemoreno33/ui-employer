import {Interaction, Matchers} from '@pact-foundation/pact';
import {act} from 'react-dom/test-utils';
import {mount} from 'enzyme';
import {noop} from 'lodash/fp';
import {addDays, format} from 'date-fns';
import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {RepostJobPostModal} from '../RepostJobPostModal';
import {PactBuilder, transformArrayToEachLikeMatcher} from '~/__mocks__/pact';

const v2ApiMock = jest.fn();
jest.mock('~/config', () => ({
  config: {
    url: {
      apiJob: {
        get v2() {
          return v2ApiMock();
        },
      },
    },
  },
}));

describe('RepostJobPostModal', () => {
  let pactBuilder: PactBuilder;
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-job');
    await pactBuilder.setup();
    v2ApiMock.mockImplementation(() => `http://${pactBuilder.host}:${pactBuilder.port}/v2`);
  });

  afterAll(async () => pactBuilder.provider.finalize());

  it('should call the repost job api and display the correct auto-close date based on selection', async () => {
    const jobUuid = '77e25f585fba0c8c212e8b9f1779755f';
    const jobPosting = {
      ...jobPostMock,
      hiringCompany: undefined,
      uuid: jobUuid,
    };
    const interaction = new Interaction()
      .given(`job ${jobUuid} is closed and has 0 repost count`)
      .uponReceiving('repostJobPosting')
      .withRequest({
        body: {
          expiryDate: format(addDays(new Date(), 30), 'yyyy-MM-dd'),
        },
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'PATCH',
        path: `/v2/jobs/77e25f585fba0c8c212e8b9f1779755f/repost`,
      })
      .willRespondWith({
        body: Matchers.like(transformArrayToEachLikeMatcher(jobPosting)),
        status: 200,
      });
    await pactBuilder.provider.addInteraction(interaction);

    const wrapper = mount(
      <MemoryRouter>
        <RepostJobPostModal onCancel={noop} job={jobPosting} />
      </MemoryRouter>,
    );

    const expectedExpiryDate = format(addDays(new Date(), 30), 'd MMM yyyy');
    expect(wrapper.find('[data-cy="repost-close-date"]').text()).toEqual(expectedExpiryDate);
    expect(wrapper.find('[data-cy="repost-modal-repost-count"]').text()).toEqual('1 repost');

    await act(async () => {
      wrapper.find('[data-cy="submit-button-repost-jobpost-modal"]').simulate('click');
    });

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });

    wrapper.update();

    expect(wrapper.find('[data-cy="repost-acknowledgement"]')).toHaveLength(1);
    expect(wrapper.find('[data-cy="repost-close-date-acknowledgement"]').text()).toEqual(expectedExpiryDate);
  });
});
