import {mount} from 'enzyme';
import React from 'react';
import {JobPostMenuItem} from '../JobPostMenuItem';

describe('JobPostMenuItem', () => {
  it('should render', () => {
    const props = {
      id: 'menu-item-id',
      title: 'title',
    };
    const wrapper = mount(<JobPostMenuItem {...props} />);
    expect(wrapper.find(`[data-cy="${props.id}"]`)).toHaveLength(1);
  });
});
