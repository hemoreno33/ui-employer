import React from 'react';
import styles from './ViewJobPost.scss';

interface IViewJobPostButtonProps {
  onClick: (event: React.MouseEvent<HTMLElement>) => void;
}

export const ViewJobPostButton: React.FunctionComponent<IViewJobPostButtonProps> = ({onClick}) => {
  return (
    <button
      data-cy="view-job-post-button"
      className={`relative f3 white pa3 bg-washed-blue pointer ${styles.viewJobPostButton}`}
      onClick={onClick}
    >
      <div className="flex flex-column items-center justify-around">
        <i className={`w-30 db relative ${styles.iconViewJobMenu}`} />
        <span className="f6 mt2">View Job Posting</span>
      </div>
    </button>
  );
};
