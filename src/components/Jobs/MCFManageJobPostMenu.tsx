import Tooltip from 'rc-tooltip';
import React from 'react';
import {IJobPostMenuItem, JobPostMenuItem} from './JobPostMenuItem';
import styles from './ManageJobPostMenu.scss';

export interface IMCFManageJobPostMenuProps {
  menuItems: IJobPostMenuItem[];
}

export const MCFManageJobPostMenu: React.FunctionComponent<IMCFManageJobPostMenuProps> = ({menuItems}) => {
  return (
    <Tooltip
      id="manage-job-tooltip"
      placement="bottom"
      trigger={['hover']}
      overlayClassName="absolute z-5 bg-white black-60 shadow-1 tl"
      destroyTooltipOnHide={true}
      overlay={menuItems.map((menuItem, index) => (
        <div key={menuItem.id} className={index > 0 ? 'bt b--black-30' : ''}>
          <JobPostMenuItem {...menuItem} />
        </div>
      ))}
    >
      <button
        data-cy="manage-job-post-button"
        className={`relative f3 white pa3 bg-washed-blue pointer ${styles.manageJobPostButton}`}
      >
        <div className="flex flex-column items-center justify-around">
          <i className={`db relative ${styles.iconManageJobMenu}`} />
          <span className="f6 lh-solid mt2">Manage Job</span>
          <i className={`db relative ${styles.iconDownArrow}`} />
        </div>
      </button>
    </Tooltip>
  );
};
