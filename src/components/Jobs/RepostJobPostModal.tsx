import {addDays, format} from 'date-fns';
import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import {useAsyncFn} from 'react-use';
import {Dropdown} from '~/components/Core/Dropdown';
import {Modal} from '~/components/Core/Modal';
import {growlNotification, growlType} from '~/components/GrowlNotification/GrowlNotification';
import {NumberLoader} from '~/components/JobList/NumberLoader';
import {CandidatesTabs} from '~/pages/Candidates/Candidates.constants';
import {durationOptions} from '~/pages/JobPosting/JobPosting.constants';
import {IJobPost} from '~/services/employer/jobs.types';
import {repostJobPost} from '~/services/employer';
import {jobToPath} from '~/util/url';
import styles from './JobPostModal.scss';

export interface IRepostJobPostModalProps {
  onCancel: () => void;
  job: IJobPost;
}

const MAX_REPOST_COUNT = 2;
const MAX_REPOST_DURATION = durationOptions[3].value;

export const RepostJobPostModal: React.FunctionComponent<IRepostJobPostModalProps> = ({job, onCancel}) => {
  const repostCount = job.metadata.repostCount;
  const remainingRepostsAfterSubmission = MAX_REPOST_COUNT - repostCount - 1;

  const [repostDuration, setRepostDuration] = useState(MAX_REPOST_DURATION);
  const [isSubmitted, setIsSubmitted] = useState(false);
  const newCloseDate = addDays(new Date(Date.now()), repostDuration);
  const newCloseDateFormatted = format(newCloseDate, 'd MMM yyyy');

  const jobPath = jobToPath({
    company: job.hiringCompany ? job.hiringCompany.name : job.postedCompany.name,
    jobTitle: job.title,
    uuid: job.uuid,
  });

  const [state, fetch] = useAsyncFn(async () => {
    try {
      await repostJobPost(job.uuid, format(newCloseDate, 'yyyy-MM-dd'));
      setIsSubmitted(true);
    } catch (error) {
      growlNotification(<div>Temporarily unable to repost job.</div>, growlType.ERROR, {
        toastId: 'repostError',
      });
      onCancel();
    }
  }, [job.uuid, newCloseDate]);

  const loadingContent = (
    <div data-cy="repost-jobpost-loader" className="bg-white black-60">
      <div className="tc ph5 pv4">
        <div className={styles.loadingContainer}>
          <NumberLoader />
        </div>
        <p className="primary ma0 mt2 f4-5">Please wait while job reopens</p>
      </div>
    </div>
  );

  const submittedContent = (
    <div data-cy="repost-acknowledgement">
      <div className="pa4 h5">
        <h3 className="f4 ma0 pb3 fw6 mb2">Repost successful</h3>
        <p className="f6 mb0">
          {job.title} ({job.metadata.jobPostId}) has been reposted successfully.
        </p>
        <p className="f6 mb0">
          Auto-close date:{' '}
          <span data-cy="repost-close-date-acknowledgement" className="primary b">
            {newCloseDateFormatted}
          </span>
        </p>
      </div>
      <div className="pa2 tr bg-light-gray">
        <Link
          to={`${jobPath}/${CandidatesTabs.Applications}`}
          className="ma2 pa3 tc w-50 bg-primary link white pointer dib"
          onClick={onCancel}
        >
          Ok
        </Link>
      </div>
    </div>
  );

  const defaultContent = (
    <>
      <div className="pa4">
        <h3 className="f4 ma0 pb3 fw6">Repost Job</h3>
        <div className="w-70 mb4">
          <Dropdown
            id="repost-jobpost-modal-duration-dropdown"
            label="Repost this job for the duration of:"
            data={durationOptions}
            input={{
              onChange: setRepostDuration,
              value: repostDuration,
            }}
          />
        </div>
        <p className="f6 mb0">
          The auto-close date will be{' '}
          <span data-cy="repost-close-date" className="primary b">
            {newCloseDateFormatted}
          </span>
          .
        </p>
        <p className="f6 mb0">
          After submitting this repost, you will have{' '}
          <span data-cy="repost-modal-repost-count" className="primary b">
            {remainingRepostsAfterSubmission} repost{remainingRepostsAfterSubmission === 1 ? '' : 's'}
          </span>{' '}
          left.
        </p>
      </div>
      <div className="pa2 tc bg-light-gray">
        <button
          data-cy="cancel-button-repost-jobpost-modal"
          className={`ma2 pa3 primary ${styles.cancelButton}`}
          onClick={onCancel}
        >
          Cancel
        </button>
        <button
          data-cy="submit-button-repost-jobpost-modal"
          className={`ma2 pa3 bg-primary ${styles.submitButton}`}
          onClick={fetch}
        >
          Repost
        </button>
      </div>
    </>
  );

  return (
    <Modal>
      <div data-cy="repost-jobpost-modal" className="bg-white black-60">
        {state.loading ? loadingContent : isSubmitted ? submittedContent : defaultContent}
      </div>
    </Modal>
  );
};
