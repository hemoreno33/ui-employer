import {RouteComponentProps, withRouter} from 'react-router';
import {differenceInDays, parseISO} from 'date-fns';
import {compact, gte} from 'lodash/fp';
import React, {useState} from 'react';
import {MCFManageJobPostMenu} from '~/components/Jobs/MCFManageJobPostMenu';
import {MCFRepostJobPostButton} from '~/components/Jobs/MCFRepostJobPostButton';
import {ViewJobPostButton} from '~/components/Jobs/ViewJobPostButton';
import {JobStatusCodes} from '~/services/employer/jobs.types';
import {isMCFJob} from '~/util/isMCFJob';
import {jobToPath} from '~/util/url';
import {CloseJobPostModalContainer} from './CloseJobPostModalContainer';
import {ExtendJobPostModalContainer} from './ExtendJobPostModalContainer';
import {IJobPostMenuItem} from './JobPostMenuItem';
import {IJobPost} from '~/services/employer/jobs.types';

const MAX_DURATION = 30;

export interface IJobPostMenuProps extends RouteComponentProps {
  job: IJobPost;
}

export enum JobPostModal {
  None,
  CloseJobPostModal,
  ExtendJobPostModal,
}

export const JobPostMenu: React.FunctionComponent<IJobPostMenuProps> = ({job, history}) => {
  const [jobPostModal, setJobPostModal] = useState(JobPostModal.None);
  const {jobPostId, expiryDate, newPostingDate} = job.metadata;
  const jobDuration = differenceInDays(parseISO(expiryDate), parseISO(newPostingDate));
  const isMaxDuration = gte(jobDuration, MAX_DURATION);
  const companyName = job.hiringCompany ? job.hiringCompany.name : job.postedCompany.name;
  const jobLinkUrl = jobToPath({
    company: companyName,
    jobTitle: job.title,
    uuid: job.uuid,
  });
  const onViewJobPostClick = () => {
    history.push({
      pathname: `${jobLinkUrl}/view`,
    });
  };
  const menuItems: IJobPostMenuItem[] = compact([
    {
      id: 'manage-job-edit-item',
      onClick: onViewJobPostClick,
      title: 'View/Edit Job Details',
    },
    isMaxDuration
      ? {
          disabled: true,
          hoverMessage: 'The maximum duration has already been selected for this job',
          id: 'manage-job-extend-item-disabled',
          title: 'Extend Posting Duration',
        }
      : {
          id: 'manage-job-extend-item',
          onClick: () => setJobPostModal(JobPostModal.ExtendJobPostModal),
          title: 'Extend Posting Duration',
        },
    {
      id: 'manage-job-close-job-item',
      onClick: () => setJobPostModal(JobPostModal.CloseJobPostModal),
      title: 'Close Job',
    },
  ]);

  const showMcfMenu = () =>
    job.status.id === JobStatusCodes.Closed ? (
      <MCFRepostJobPostButton onClick={onViewJobPostClick} />
    ) : (
      <MCFManageJobPostMenu menuItems={menuItems} />
    );
  const showMsfMenu = () => <ViewJobPostButton onClick={onViewJobPostClick} />;

  const renderJobPostModal = (modal: JobPostModal) => {
    switch (modal) {
      case JobPostModal.ExtendJobPostModal:
        return <ExtendJobPostModalContainer job={job} onCancel={() => setJobPostModal(JobPostModal.None)} />;
      case JobPostModal.CloseJobPostModal:
        return <CloseJobPostModalContainer job={job} onCancel={() => setJobPostModal(JobPostModal.None)} />;
      default:
        return null;
    }
  };

  return (
    <>
      {isMCFJob(jobPostId) ? showMcfMenu() : showMsfMenu()}
      {renderJobPostModal(jobPostModal)}
    </>
  );
};

export default withRouter(JobPostMenu);
