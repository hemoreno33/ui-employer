import {action, ActionType} from 'typesafe-actions';

export interface IUpdateCloseJobState {
  isLoading: boolean;
  submitted: boolean;
  employerSelection: string;
  employerSelectionPrompt: boolean;
}

export const PENDING = 'PENDING';
export const SUCCESS = 'SUCCESS';
export const ERROR = 'ERROR';
export const SELECT_JOB_IS_VACANT = 'SELECT_JOB_IS_VACANT';
export const SELECT_JOB_IS_VACANT_PROMPT = 'SELECT_JOB_IS_VACANT_PROMPT';

const pendingSubmit = () => action(PENDING);
const successSubmit = () => action(SUCCESS);
const errorSubmit = () => action(ERROR);
const isJobVacantSelection = (payload: string) => action(SELECT_JOB_IS_VACANT, payload);
const isJobVacantSelectionPrompt = () => action(SELECT_JOB_IS_VACANT_PROMPT);

export type IUpdateCloseJobActions = ActionType<
  | typeof pendingSubmit
  | typeof successSubmit
  | typeof errorSubmit
  | typeof isJobVacantSelection
  | typeof isJobVacantSelectionPrompt
>;

export const closeJobReducer = (state: IUpdateCloseJobState, actions: IUpdateCloseJobActions): IUpdateCloseJobState => {
  switch (actions.type) {
    case SELECT_JOB_IS_VACANT_PROMPT:
      return {
        ...state,
        employerSelectionPrompt: true,
      };
    case SELECT_JOB_IS_VACANT:
      return {
        ...state,
        employerSelection: actions.payload,
      };
    case PENDING:
      return {
        ...state,
        isLoading: true,
        submitted: false,
      };
    case SUCCESS:
      return {
        ...state,
        employerSelection: '',
        employerSelectionPrompt: false,
        isLoading: false,
        submitted: true,
      };
    case ERROR:
      return {
        ...state,
        employerSelection: '',
        employerSelectionPrompt: false,
        isLoading: false,
        submitted: false,
      };
    default: {
      return state;
    }
  }
};
