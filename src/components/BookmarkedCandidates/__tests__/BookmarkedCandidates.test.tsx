import {MockedProvider, MockedProviderProps} from '@apollo/react-testing';
import {InMemoryCache, IntrospectionFragmentMatcher} from 'apollo-cache-inmemory';
import {mount, ReactWrapper} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {
  bookmarkedApplicationMock,
  bookmarkedTalentMock,
} from '~/__mocks__/bookmarkedCandidates/bookmarkedCandidates.mock';
import {CardLoader} from '~/components/Layouts/CardLoader';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import {CandidatesListItem} from '~/components/Candidates/CandidatesListItem';
import {TemporarilyUnavailable} from '~/components/Layouts/TemporarilyUnavailable';
import {MissingCandidate} from '~/components/Candidates/MissingCandidate';
import {Pagination} from '~/components/Navigation/Pagination';
import {cleanSnapshot, nextTick} from '~/testUtil/enzyme';
import {GET_BOOKMARKED_CANDIDATES} from '~/graphql/candidates/bookmarkedCandidates.query';
import introspectionResult, {GetBookmarkedCandidatesQuery} from '~/graphql/__generated__/types';
import * as dateUtil from '~/util/date';
import {BookmarkedCandidates, BOOKMARKED_CANDIDATES_LIMIT} from '../BookmarkedCandidates';
import {BookmarkedCandidatesInfo} from '../BookmarkedCandidatesInfo';
import {ApplicationStatus} from '~/components/CandidateInfo/ApplicationStatus';
import {EmptyBookmarkedCandidatesList} from '~/components/BookmarkedCandidates/EmptyBookmarkedCandidatesList';

jest.spyOn(dateUtil, 'getElapsedDays').mockImplementation(() => '3 days ago');

describe('BookmarkedCandidates', () => {
  it('should render loader then display both bookmarked applications and talents', async () => {
    const wrapper = mountBookmarkedCandidates();

    expect(wrapper.find(CardLoader)).toHaveLength(1);

    await nextTick(wrapper);

    const candidateListItems = wrapper.find(CandidatesListItem);
    expect(candidateListItems).toHaveLength(3);
    // @ts-ignore toJson typing issue when using .find
    expect(toJson(candidateListItems, cleanSnapshot())).toMatchSnapshot();

    const bookmarkedApplications = wrapper.find('[data-cy="bookmarked-application-item"]');
    expect(bookmarkedApplications).toHaveLength(2);
    bookmarkedApplications
      .first()
      .find('[data-cy="candidate-info"]')
      .simulate('click');
    const bookmarkedApplicationInfo = wrapper.find(BookmarkedCandidatesInfo);
    // @ts-ignore toJson typing issue when using .find
    expect(toJson(bookmarkedApplicationInfo, cleanSnapshot())).toMatchSnapshot();
    expect(bookmarkedApplicationInfo.find(ApplicationStatus)).toHaveLength(1);

    const bookmarkedTalents = wrapper.find('[data-cy="bookmarked-talent-item"]');
    expect(bookmarkedTalents).toHaveLength(1);
    bookmarkedTalents
      .first()
      .find('[data-cy="candidate-info"]')
      .simulate('click');
    const bookmarkedTalentInfo = wrapper.find(BookmarkedCandidatesInfo);
    // @ts-ignore toJson typing issue when using .find
    expect(toJson(bookmarkedTalentInfo, cleanSnapshot())).toMatchSnapshot();
    expect(bookmarkedTalentInfo.find(ApplicationStatus)).toHaveLength(0);
  });

  it('should render CandidatesFetchError when a network error is received', async () => {
    const queryMocks = [
      {
        request: {
          query: GET_BOOKMARKED_CANDIDATES,
          variables: baseVariables,
        },
        result: new Error(),
      },
    ];
    const wrapper = mountBookmarkedCandidates(queryMocks);
    await nextTick(wrapper);
    expect(wrapper.find(TemporarilyUnavailable)).toHaveLength(1);
  });

  it('should render EmptyBookmarkedCandidatesList when there are zero candidates', async () => {
    const queryMocks = [
      {
        request: {
          query: GET_BOOKMARKED_CANDIDATES,
          variables: baseVariables,
        },
        result: {
          data: {
            __typename: 'BookmarkedCandidates',
            bookmarkedCandidatesForJob: {
              __typename: 'BookmarkedCandidates',
              bookmarkedCandidates: [],
              total: 0,
              bookmark: 'pageOneBookmark',
            },
          },
        },
      },
    ];
    const wrapper = mountBookmarkedCandidates(queryMocks);
    await nextTick(wrapper);
    expect(wrapper.find(EmptyBookmarkedCandidatesList)).toHaveLength(1);
  });

  it('should fetch candidates and navigate to another page when pagination is clicked', async () => {
    const queryMocks = [
      ...defaultQueryMocks,
      {
        request: {
          query: GET_BOOKMARKED_CANDIDATES,
          variables: {
            jobId: jobIdMock,
            limit: BOOKMARKED_CANDIDATES_LIMIT,
            skip: BOOKMARKED_CANDIDATES_LIMIT,
            bookmark: 'pageOneBookmark',
          },
        },
        result: {
          data: {
            bookmarkedCandidatesForJob: {
              __typename: 'BookmarkedCandidates',
              bookmarkedCandidates: [bookmarkedApplicationMock, bookmarkedTalentMock],
              total: BOOKMARKED_CANDIDATES_LIMIT * 2,
              bookmark: 'pageTwoBookmark',
            },
          },
        },
      },
    ];
    const wrapper = mountBookmarkedCandidates(queryMocks);
    await nextTick(wrapper);

    expect(wrapper.find(CandidatesListItem)).toHaveLength(3);

    const pagination = wrapper.find(Pagination);
    expect(pagination.find('.bg-secondary').text()).toEqual('1');
    const secondPage = pagination.findWhere((node) => node.type() === 'span' && node.text() === '2');
    secondPage.simulate('click');

    await nextTick(wrapper);

    const pageChange = wrapper.find(Pagination);
    expect(pageChange.find('.bg-secondary').text()).toEqual('2');

    wrapper.update();
    expect(wrapper.find(CandidatesListItem)).toHaveLength(2);
  });

  it('should render errors for null values and display the rest', async () => {
    const queryMocks = [
      {
        request: {
          query: GET_BOOKMARKED_CANDIDATES,
          variables: baseVariables,
        },
        result: {
          data: {
            bookmarkedCandidatesForJob: {
              __typename: 'BookmarkedCandidates',
              bookmarkedCandidates: [bookmarkedApplicationMock, bookmarkedTalentMock, null, null],
              total: BOOKMARKED_CANDIDATES_LIMIT * 2,
              bookmark: 'pageOneBookmark',
            },
          },
        },
      },
    ];
    const wrapper = mountBookmarkedCandidates(queryMocks);
    await nextTick(wrapper);

    const missingCandidates = wrapper.find(MissingCandidate).find('[candidateType="candidate"]');
    expect(missingCandidates).toHaveLength(2);
  });
});

const jobIdMock = 'job123';
const baseVariables = {
  jobId: jobIdMock,
  limit: BOOKMARKED_CANDIDATES_LIMIT,
  skip: 0,
};

const defaultQueryMockResult: {data: GetBookmarkedCandidatesQuery} = {
  data: {
    bookmarkedCandidatesForJob: {
      __typename: 'BookmarkedCandidates',
      bookmarkedCandidates: [
        bookmarkedApplicationMock,
        bookmarkedTalentMock,
        {...bookmarkedApplicationMock, id: 'BMA-2', applicant: {...bookmarkedApplicationMock.applicant, id: 'A-2'}},
      ],
      total: BOOKMARKED_CANDIDATES_LIMIT * 2,
      bookmark: 'pageOneBookmark',
    },
  },
};

const defaultQueryMocks = [
  {
    request: {
      query: GET_BOOKMARKED_CANDIDATES,
      variables: baseVariables,
    },
    result: defaultQueryMockResult,
  },
];

const reduxState = {
  features: {bookmarkTab: true},
};
const store = configureStore()(reduxState);
const dispatchProps = {
  onBookmarkedCandidateClicked: jest.fn(),
  onBookmarkedCandidatesPageClicked: jest.fn(),
  onBookmarkedCandidateResumeClicked: jest.fn(),
};

const mountBookmarkedCandidates = (queryMocks: MockedProviderProps['mocks'] = defaultQueryMocks): ReactWrapper => {
  const fragmentMatcher = new IntrospectionFragmentMatcher({introspectionQueryResultData: introspectionResult});
  const cache = new InMemoryCache({fragmentMatcher});
  return mount(
    <MockedProvider mocks={queryMocks} cache={cache} addTypename={false}>
      <Provider store={store}>
        <BookmarkedCandidates {...dispatchProps} jobUuid={jobIdMock} />
      </Provider>
    </MockedProvider>,
  );
};
