import {isEmpty} from 'lodash/fp';
import React, {useState, useEffect} from 'react';
import {useQuery, useMutation} from 'react-apollo';
import {mcf} from '@mcf/constants';
import {GET_BOOKMARKED_CANDIDATES} from '~/graphql/candidates/bookmarkedCandidates.query';
import {Pagination} from '~/components/Navigation/Pagination';
import {
  GetBookmarkedCandidatesQuery,
  GetBookmarkedCandidatesQueryVariables,
  GetBookmarkedCandidatesBookmarkedCandidates,
  GetBookmarkedCandidatesBookmarkedCandidatesForJob,
  Mutation,
  MutationSetResumeLastDownloadedDateArgs,
  MutationSetApplicationStatusArgs,
} from '~/graphql/__generated__/types';
import {CardLoader} from '~/components/Layouts/CardLoader';
import {TemporarilyUnavailable, temporaryUnavailableMsg} from '~/components/Layouts/TemporarilyUnavailable';
import {CandidatesPanes} from '~/components/Candidates/CandidatesPanes';
import {SET_RESUME_LAST_DOWNLOADED_DATE, SET_RESUME_DOWNLOAD_AND_STATUS_UPDATE} from '~/graphql/applications';
import {EmptyBookmarkedCandidatesList} from '~/components/BookmarkedCandidates/EmptyBookmarkedCandidatesList';
import {BookmarkedCandidateList} from '~/components/BookmarkedCandidates/BookmarkedCandidatesList';
import {BookmarkedCandidatesInfoContainer} from '~/components/BookmarkedCandidates/BookmarkedCandidatesInfoContainer';
import {isApplication} from '~/components/BookmarkedCandidates/bookmarkedCandidatesUtil';
import {IBookmarkedCandidatesContainerDispatchProps} from './BookmarkedCandidatesContainer';
import {DOWNLOAD_RESUME_GA_LABEL} from '~/util/analytics';

export const BOOKMARKED_CANDIDATES_LIMIT = 20;

interface IBookmarkedCandidatesProps extends IBookmarkedCandidatesContainerDispatchProps {
  jobUuid: string;
  refetchBookmarkedCandidatesTotal?: () => Promise<any>;
}

export const BookmarkedCandidates: React.FunctionComponent<IBookmarkedCandidatesProps> = ({
  jobUuid,
  refetchBookmarkedCandidatesTotal,
  onBookmarkedCandidateClicked,
  onBookmarkedCandidatesPageClicked,
  onBookmarkedCandidateResumeClicked,
}) => {
  const [bookmarkedCandidates, setBookmarkedCandidates] = useState<
    GetBookmarkedCandidatesBookmarkedCandidatesForJob['bookmarkedCandidates']
  >([]);
  const [selectedCandidate, setSelectedCandidate] = useState<GetBookmarkedCandidatesBookmarkedCandidates>();
  const [bookmark, setBookmark] = useState<string>();
  const [page, setPage] = useState(0);

  const {loading, error, data} = useQuery<GetBookmarkedCandidatesQuery, GetBookmarkedCandidatesQueryVariables>(
    GET_BOOKMARKED_CANDIDATES,
    {
      variables: {
        jobId: jobUuid,
        limit: BOOKMARKED_CANDIDATES_LIMIT,
        skip: page * BOOKMARKED_CANDIDATES_LIMIT,
        bookmark,
      },
      fetchPolicy: 'network-only',
      errorPolicy: 'all',
      notifyOnNetworkStatusChange: true,
      skip: !jobUuid,
    },
  );

  useEffect(() => {
    setBookmarkedCandidates(data?.bookmarkedCandidatesForJob?.bookmarkedCandidates ?? []);
  }, [JSON.stringify(data)]);
  useEffect(() => {
    setBookmarkedCandidates(
      bookmarkedCandidates.map((bookmarkedCandidate) =>
        bookmarkedCandidate?.id === selectedCandidate?.id ? selectedCandidate : bookmarkedCandidate,
      ),
    );
  }, [JSON.stringify(selectedCandidate)]);

  const [setResumeLastDownloadedDate] = useMutation<
    {
      setResumeLastDownloadedDate: Mutation['setResumeLastDownloadedDate'];
    },
    MutationSetResumeLastDownloadedDateArgs
  >(SET_RESUME_LAST_DOWNLOADED_DATE);
  const [setResumeDownloadAndStatusUpdate] = useMutation<
    {
      setResumeLastDownloadedDate: Mutation['setResumeLastDownloadedDate'];
      setApplicationStatus: Mutation['setApplicationStatus'];
    },
    MutationSetApplicationStatusArgs
  >(SET_RESUME_DOWNLOAD_AND_STATUS_UPDATE);

  const onResumeClick = async (candidate: GetBookmarkedCandidatesBookmarkedCandidates) => {
    if (isApplication(candidate)) {
      if (candidate.statusId === mcf.JOB_APPLICATION_STATUS.RECEIVED) {
        await setResumeDownloadAndStatusUpdate({
          variables: {
            applicationId: candidate.id!,
            statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
          },
        });
      } else {
        await setResumeLastDownloadedDate({
          variables: {
            applicationId: candidate.id!,
          },
        });
      }
    }
  };

  const bookmarkedCandidatesPanesProps = {
    left: (resetRightScroll: () => void) => (
      <>
        <BookmarkedCandidateList
          selectedCandidate={selectedCandidate}
          bookmarkedCandidates={bookmarkedCandidates}
          onCandidateClick={(candidate, positionIndex) => {
            setSelectedCandidate(candidate);
            onBookmarkedCandidateClicked(candidate, page * BOOKMARKED_CANDIDATES_LIMIT + positionIndex + 1, jobUuid);
            resetRightScroll();
          }}
          onResumeClick={(candidate) => {
            onResumeClick(candidate);
            onBookmarkedCandidateResumeClicked(DOWNLOAD_RESUME_GA_LABEL.LIST);
          }}
        />
        <Pagination
          currentPage={page}
          totalItemsLength={data?.bookmarkedCandidatesForJob?.total ?? 0}
          itemsPerPage={BOOKMARKED_CANDIDATES_LIMIT}
          maxVisiblePageButtons={5}
          onPageChange={(newPage) => {
            setPage(newPage);
            // set bookmark along with page to prevent extra fetching due to bookmark change
            setBookmark(data?.bookmarkedCandidatesForJob?.bookmark);
            onBookmarkedCandidatesPageClicked(jobUuid, newPage + 1);
          }}
        />
      </>
    ),
    right: (
      <BookmarkedCandidatesInfoContainer
        selectedCandidate={selectedCandidate}
        setSelectedCandidate={setSelectedCandidate}
        bookmarkedCandidates={bookmarkedCandidates}
        setBookmarkedCandidates={setBookmarkedCandidates}
        refetchBookmarkedCandidatesTotal={refetchBookmarkedCandidatesTotal}
        onResumeClick={(candidate) => {
          onResumeClick(candidate);
          onBookmarkedCandidateResumeClicked(DOWNLOAD_RESUME_GA_LABEL.DETAIL);
        }}
        jobId={jobUuid}
      />
    ),
  };

  if (loading) {
    return <CardLoader className="pa3 w-50 o-70" cardNumber={8} boxNumber={1} />;
  } else if (error?.networkError) {
    return <TemporarilyUnavailable message={temporaryUnavailableMsg} />;
  } else if (isEmpty(bookmarkedCandidates)) {
    return <EmptyBookmarkedCandidatesList />;
  } else {
    return <CandidatesPanes {...bookmarkedCandidatesPanesProps} />;
  }
};
