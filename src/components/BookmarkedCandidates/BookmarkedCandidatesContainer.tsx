import {connect} from 'react-redux';
import {BookmarkedCandidates} from './BookmarkedCandidates';
import {
  onBookmarkedCandidateClicked,
  onBookmarkedCandidatesPageClicked,
  onBookmarkedCandidateResumeClicked,
} from '~/flux/analytics';

export interface IBookmarkedCandidatesContainerDispatchProps {
  onBookmarkedCandidateClicked: typeof onBookmarkedCandidateClicked;
  onBookmarkedCandidatesPageClicked: typeof onBookmarkedCandidatesPageClicked;
  onBookmarkedCandidateResumeClicked: typeof onBookmarkedCandidateResumeClicked;
}

export const BookmarkedCandidatesContainer = connect<{}, IBookmarkedCandidatesContainerDispatchProps>(null, {
  onBookmarkedCandidateClicked,
  onBookmarkedCandidatesPageClicked,
  onBookmarkedCandidateResumeClicked,
})(BookmarkedCandidates);
