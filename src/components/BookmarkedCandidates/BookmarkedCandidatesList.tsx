import React from 'react';
import {format, parseISO} from 'date-fns';
import {getElapsedDays} from '~/util/date';
import {HiddenCandidateListItem} from '~/components/Candidates/HiddenCandidateListItem';
import {MissingCandidate} from '~/components/Candidates/MissingCandidate';
import {CandidatesListItem} from '~/components/Candidates/CandidatesListItem';
import {ErrorBoundary} from '~/components/Core/ErrorBoundary';
import {CandidateType as CandidateLabelType} from '~/components/CandidateLabel/CandidateLabel.tsx';
import {getApplicationStatusWithShortlisted} from '~/util/getApplicationStatusWithShortlisted';
import {
  GetBookmarkedCandidatesBookmarkedCandidates,
  GetBookmarkedCandidatesBookmarkedCandidatesForJob,
} from '~/graphql/__generated__/types';
import {isApplication, isTalent, isHiddenTalent} from '~/components/BookmarkedCandidates/bookmarkedCandidatesUtil';

interface IBookmarkedCandidateListProps {
  bookmarkedCandidates: GetBookmarkedCandidatesBookmarkedCandidatesForJob['bookmarkedCandidates'];
  selectedCandidate?: GetBookmarkedCandidatesBookmarkedCandidates;
  onCandidateClick: (candidate: GetBookmarkedCandidatesBookmarkedCandidates, positionIndex: number) => void;
  onResumeClick: (candidate: GetBookmarkedCandidatesBookmarkedCandidates) => void;
}

export const BookmarkedCandidateList: React.FunctionComponent<IBookmarkedCandidateListProps> = ({
  bookmarkedCandidates,
  selectedCandidate,
  onCandidateClick,
  onResumeClick,
}) => {
  return (
    <ul className="ma0 pt1 pl3 pr1 list" data-cy="bookmarked-candidate-list">
      {bookmarkedCandidates?.map((bookmarkedCandidate, index) => {
        const key = bookmarkedCandidate?.id ?? index;
        if (!bookmarkedCandidate) {
          return <MissingCandidate key={key} candidateType={'candidate'} />;
        }
        const isSelected = selectedCandidate ? bookmarkedCandidate.id === selectedCandidate.id : false;
        if (isApplication(bookmarkedCandidate)) {
          const {applicant, bookmarkedOn, statusId, isShortlisted, createdOn} = bookmarkedCandidate;
          const status = getApplicationStatusWithShortlisted(statusId, isShortlisted);
          return (
            <div data-cy="bookmarked-application-item" key={key}>
              <ErrorBoundary fallback={<MissingCandidate candidateType={'application'} />}>
                <li>
                  <CandidatesListItem
                    selected={isSelected}
                    isViewed={false}
                    isBookmarked={!!bookmarkedOn}
                    candidate={applicant}
                    candidateType={CandidateLabelType.applicant}
                    date={`Applied on ${format(parseISO(createdOn), 'd MMM yyyy')}`}
                    onClick={() => onCandidateClick(bookmarkedCandidate, index)}
                    onResumeClick={() => onResumeClick(bookmarkedCandidate)}
                    applicationStatus={status}
                  />
                </li>
              </ErrorBoundary>
            </div>
          );
        } else if (isTalent(bookmarkedCandidate)) {
          const {talent, bookmarkedOn} = bookmarkedCandidate;
          return (
            <div data-cy="bookmarked-talent-item" key={key}>
              <ErrorBoundary fallback={<MissingCandidate candidateType={'suggested talent'} />}>
                <li>
                  <CandidatesListItem
                    selected={isSelected}
                    isViewed={false}
                    isBookmarked={!!bookmarkedOn}
                    candidate={talent}
                    candidateType={CandidateLabelType.suggestedTalent}
                    date={`Active ${getElapsedDays(talent.lastLogin)}`}
                    onClick={() => onCandidateClick(bookmarkedCandidate, index)}
                    onResumeClick={() => onResumeClick(bookmarkedCandidate)}
                  />
                </li>
              </ErrorBoundary>
            </div>
          );
        } else if (isHiddenTalent(bookmarkedCandidate)) {
          const {talent, bookmarkedOn} = bookmarkedCandidate;
          return (
            <li>
              <HiddenCandidateListItem
                selected={isSelected}
                isViewed={false}
                isBookmarked={!!bookmarkedOn}
                candidate={talent}
                candidateType={CandidateLabelType.suggestedTalent}
                onClick={() => onCandidateClick(bookmarkedCandidate, index)}
              />
            </li>
          );
        } else {
          return <MissingCandidate key={key} candidateType={'candidate'} />;
        }
      })}
    </ul>
  );
};
