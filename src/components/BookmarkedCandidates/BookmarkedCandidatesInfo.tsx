import {format, parseISO, formatISO} from 'date-fns';
import {getElapsedDays} from '~/util/date';
import React, {useEffect, useState} from 'react';
import {useMutation} from 'react-apollo';
import {EmptyBookmarkedCandidate} from '~/components/BookmarkedCandidates/EmptyBookmarkedCandidate';
import {CandidateInfo} from '~/components/CandidateInfo/CandidateInfo';
import {HiddenCandidateInfo} from '~/components/CandidateInfo/HiddenCandidateInfo';
import {ErrorBoundary} from '~/components/Core/ErrorBoundary';
import {ApplicationStatus} from '~/components/CandidateInfo/ApplicationStatus';
import {growlNotification, growlType} from '~/components/GrowlNotification/GrowlNotification';
import {SET_APPLICATION_BOOKMARK} from '~/graphql/applications';
import {CandidateType as CandidateLabelType} from '~/components/CandidateLabel/CandidateLabel.tsx';
import {SET_SUGGESTED_TALENT_BOOKMARK} from '~/graphql/suggestedTalents';
import {
  GetBookmarkedCandidatesBookmarkedCandidates,
  GetBookmarkedCandidatesBookmarkedCandidatesForJob,
  Mutation,
  MutationSetApplicationBookmarkedOnArgs,
  MutationSetSuggestedTalentBookmarkArgs,
} from '~/graphql/__generated__/types';
import {isApplication, isTalent, isHiddenTalent} from '~/components/BookmarkedCandidates/bookmarkedCandidatesUtil';
import {IBookmarkedCandidatesInfoContainerDispatchProps} from './BookmarkedCandidatesInfoContainer';

interface BookmarkedCandidatesInfoProps extends IBookmarkedCandidatesInfoContainerDispatchProps {
  selectedCandidate?: GetBookmarkedCandidatesBookmarkedCandidates;
  bookmarkedCandidates: GetBookmarkedCandidatesBookmarkedCandidatesForJob['bookmarkedCandidates'];
  setSelectedCandidate: (candidate?: GetBookmarkedCandidatesBookmarkedCandidates) => void;
  setBookmarkedCandidates: (
    candidates: GetBookmarkedCandidatesBookmarkedCandidatesForJob['bookmarkedCandidates'],
  ) => void;
  refetchBookmarkedCandidatesTotal?: () => Promise<any>;
  onResumeClick: (candidate: GetBookmarkedCandidatesBookmarkedCandidates) => void;
  jobId: string;
}

export const BookmarkedCandidatesInfo: React.FunctionComponent<BookmarkedCandidatesInfoProps> = ({
  selectedCandidate,
  bookmarkedCandidates,
  setSelectedCandidate,
  setBookmarkedCandidates,
  refetchBookmarkedCandidatesTotal,
  onResumeClick,
  jobId,
  onBookmarkedCandidateTabClicked,
}) => {
  const [candidateForRevert, setCandidateForRevert] = useState<GetBookmarkedCandidatesBookmarkedCandidates>();

  const [setApplicationBookmark] = useMutation<
    {setApplicationBookmark: Mutation['setApplicationBookmarkedOn']},
    MutationSetApplicationBookmarkedOnArgs
  >(SET_APPLICATION_BOOKMARK);
  const [setSuggestedTalentBookmark] = useMutation<
    {setSuggestedTalentBookmark: Mutation['setSuggestedTalentBookmark']},
    MutationSetSuggestedTalentBookmarkArgs
  >(SET_SUGGESTED_TALENT_BOOKMARK);

  useEffect(() => {
    setBookmarkedCandidates(
      bookmarkedCandidates.map((bookmarkedCandidate) =>
        bookmarkedCandidate?.id === candidateForRevert?.id ? candidateForRevert : bookmarkedCandidate,
      ),
    );
    if (selectedCandidate?.id === candidateForRevert?.id) {
      setSelectedCandidate(candidateForRevert);
    }
  }, [JSON.stringify(candidateForRevert)]);

  const onCandidateBookmarkClick = async (checked: boolean) => {
    if (selectedCandidate) {
      try {
        setSelectedCandidate({
          ...selectedCandidate,
          bookmarkedOn: checked ? formatISO(Date.now()) : null,
        });
        if (selectedCandidate.__typename === 'Application') {
          await setApplicationBookmark({
            variables: {
              applicationId: selectedCandidate.id,
              isBookmark: checked,
            },
          });
        } else if (selectedCandidate.__typename === 'Talent' || selectedCandidate.__typename === 'HiddenTalent') {
          await setSuggestedTalentBookmark({
            variables: {individualId: selectedCandidate.id, jobId, isBookmark: checked},
          });
        }
        if (refetchBookmarkedCandidatesTotal) {
          await refetchBookmarkedCandidatesTotal();
        }
      } catch (_) {
        setCandidateForRevert(selectedCandidate);
        growlNotification(
          `Temporarily unable to ${checked ? 'save' : 'unsave'} candidate. Please try again.`,
          growlType.ERROR,
          {
            toastId: `setCandidateBookmark${selectedCandidate?.id}`,
          },
        );
      }
    }
  };
  if (selectedCandidate) {
    if (isApplication(selectedCandidate)) {
      const {id, bookmarkedOn, applicant, createdOn, statusId, isShortlisted} = selectedCandidate;
      return (
        <ErrorBoundary key={selectedCandidate.id} fallback={<EmptyBookmarkedCandidate />}>
          <CandidateInfo
            key={selectedCandidate.id}
            candidate={applicant}
            formattedDate={`Applied on ${format(parseISO(createdOn), 'd MMM yyyy')}`}
            isBookmarked={!!bookmarkedOn}
            candidateType={CandidateLabelType.applicant}
            statusSelector={<ApplicationStatus statusId={statusId} isShortlisted={isShortlisted} applicationId={id} />}
            onCandidateBookmarkClick={onCandidateBookmarkClick}
            onResumeClick={() => onResumeClick(selectedCandidate)}
            candidateTabClicked={onBookmarkedCandidateTabClicked}
          />
        </ErrorBoundary>
      );
    } else if (isTalent(selectedCandidate)) {
      const {id, talent, bookmarkedOn} = selectedCandidate;
      return (
        <ErrorBoundary key={selectedCandidate.id} fallback={<EmptyBookmarkedCandidate />}>
          <CandidateInfo
            key={id}
            candidate={talent}
            formattedDate={`Active ${getElapsedDays(talent.lastLogin)}`}
            isBookmarked={!!bookmarkedOn}
            candidateType={CandidateLabelType.suggestedTalent}
            onCandidateBookmarkClick={onCandidateBookmarkClick}
            onResumeClick={() => onResumeClick(selectedCandidate)}
            candidateTabClicked={onBookmarkedCandidateTabClicked}
          />
        </ErrorBoundary>
      );
    } else if (isHiddenTalent(selectedCandidate)) {
      const {talent, bookmarkedOn} = selectedCandidate;
      return (
        <HiddenCandidateInfo
          candidate={talent}
          candidateType={CandidateLabelType.suggestedTalent}
          isBookmarked={!!bookmarkedOn}
          onCandidateBookmarkClick={onCandidateBookmarkClick}
        />
      );
    } else {
      return <EmptyBookmarkedCandidate />;
    }
  } else {
    return <EmptyBookmarkedCandidate />;
  }
};
