import React from 'react';
import {RouteComponentProps} from 'react-router';
import {withRouter} from 'react-router';

import styles from './CancelButtonWithConfirmation.scss';

const CancelButtonWithConfirmation: React.FunctionComponent<RouteComponentProps> = ({history}) => (
  <a
    href="/jobs"
    className={`${styles.button} pa3 db ba b--primary primary bg-white no-underline pointer dib v-mid tc`}
    data-cy="company-profile-cancel"
    onClick={(event) => {
      event.preventDefault();

      const confirmMessage =
        'If you leave this page, your changes will not be saved.\nClick OK to leave this page, or Cancel to stay on this page.';

      if (window.confirm(confirmMessage)) {
        history.push('/');
      }
    }}
  >
    Cancel
  </a>
);

export default withRouter(CancelButtonWithConfirmation);
