import React from 'react';
import styles from './CandidateErrorBar.scss';

export const CandidateErrorBar = () => (
  <div id="error-banner" className={`bg-red pa2 f6 white mh4 ${styles.errorBar}`}>
    <span className="pl2 lh-copy">
      Looks like some applicant details cannot be displayed.{' '}
      <a
        id="error-banner-feedback-link"
        className="white fw6"
        href="https://portal.ssg-wsg.gov.sg"
        target="_blank"
        rel="noopener"
      >
        Let us know
      </a>{' '}
      your JOB ID so we can look into it.
    </span>
  </div>
);
