import {mount} from 'enzyme';
import React from 'react';
import {CandidateErrorBar} from '../CandidateErrorBar';

describe('CandidateErrorBar', () => {
  it('should render CandidateErrorBar span correctly', () => {
    expect(mount(<CandidateErrorBar />)).toMatchSnapshot();
  });
});
