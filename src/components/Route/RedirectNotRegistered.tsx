import React from 'react';
import {Redirect} from 'react-router-dom';
import {FormLoader} from '~/components/Layouts/FormLoader';

interface IRedirectUnauthorisedProps {
  hasRegistered: boolean;
  path: any;
  privateElement: any;
  loaded: boolean;
}

export const RedirectNotRegistered: React.FunctionComponent<IRedirectUnauthorisedProps> = ({
  hasRegistered,
  privateElement,
  path,
  loaded,
}) => {
  // add loader
  if (!loaded) {
    return (
      <div className={`w-100 pa4 bg-page-gray absolute db flex flex-wrap justify-center`}>
        <FormLoader className="pa3 w-60 relative h5 o-50" cardNumber={1} hasTextAreaLoader={false} />
        <FormLoader className="pa3 w-60 relative h5 o-50" cardNumber={1} />
      </div>
    );
  }
  if (hasRegistered && path === '/terms-and-conditions') {
    return <Redirect to="/jobs" />;
  }
  if (!hasRegistered && path !== '/terms-and-conditions') {
    return <Redirect to="/terms-and-conditions" />;
  }
  return privateElement;
};
