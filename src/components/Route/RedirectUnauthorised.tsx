import React from 'react';
import {Redirect} from 'react-router-dom';

interface IRedirectUnauthorisedProps {
  hasRedirectCondition: boolean;
  children: any;
}

export const RedirectUnauthorised: React.FunctionComponent<IRedirectUnauthorisedProps> = ({
  hasRedirectCondition,
  children,
}) => {
  if (hasRedirectCondition) {
    return <Redirect to="/unauthorised" />;
  }
  return children;
};
