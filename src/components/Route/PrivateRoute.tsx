import React from 'react';
import {Route, RouteProps} from 'react-router-dom';
import {RedirectNotRegisteredContainer} from '~/components/Route/RedirectNotRegisteredContainer';
import {RedirectUnauthorisedContainer} from './RedirectUnauthorisedContainer';

export const PrivateRoute = ({component: BaseComponent, render, path, ...rest}: RouteProps) => (
  <Route
    {...rest}
    path={path}
    render={(props) => {
      const toRender =
        BaseComponent !== null
          ? React.createElement(BaseComponent || '', {
              ...props,
              key: props.location.state && props.location.state.key, // trigger a remount on BaseComponent if key is set
            })
          : render
          ? render(props)
          : undefined;
      return (
        <RedirectUnauthorisedContainer>
          <RedirectNotRegisteredContainer path={path} privateElement={toRender} />
        </RedirectUnauthorisedContainer>
      );
    }}
  />
);
