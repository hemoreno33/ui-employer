import {connect} from 'react-redux';
import {getAccountUser} from '~/flux/account';
import {IAppState} from '~/flux/index';
import {App} from './App';

export const mapStateToProps = (state: IAppState) => ({user: getAccountUser(state)});

export const AppContainer = connect(mapStateToProps)(App);
