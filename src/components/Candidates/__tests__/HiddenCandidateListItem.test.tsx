import {mount} from 'enzyme';
import {HiddenCandidateListItem} from '../HiddenCandidateListItem';
import React from 'react';
import configureStore from 'redux-mock-store';
import {IAppState} from '~/flux';
import {Provider} from 'react-redux';
import toJson from 'enzyme-to-json';
import {cleanSnapshot} from '~/testUtil/enzyme';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';

describe('Candidates/HiddenCandidatesListItem', () => {
  const state: Pick<IAppState, 'features'> = {
    features: {
      bookmarkTab: true,
    },
  };
  const store = configureStore()(state);
  const hiddenCandidate = {
    name: 'dragon',
  };
  it('should render HiddenCandidateListItem', () => {
    const wrapper = mount(
      <Provider store={store}>
        <HiddenCandidateListItem
          candidate={hiddenCandidate}
          selected={true}
          candidateType={CandidateType.suggestedTalent}
          onClick={jest.fn()}
          isViewed={true}
          isBookmarked={true}
        />
      </Provider>,
    );
    // @ts-ignore toJson typing issue when using .find
    expect(toJson(wrapper.find(HiddenCandidateListItem), cleanSnapshot())).toMatchSnapshot();
  });

  it.each`
    type                             | result
    ${CandidateType.applicant}       | ${'applicant'}
    ${CandidateType.suggestedTalent} | ${'talent'}
  `('should display message with relevant candidate type: $type', ({type, result}) => {
    const wrapper = mount(
      <Provider store={store}>
        <HiddenCandidateListItem
          candidate={hiddenCandidate}
          selected={true}
          candidateType={type}
          onClick={jest.fn()}
          isViewed={true}
          isBookmarked={true}
        />
      </Provider>,
    );

    const hiddenCandidateListItem = wrapper.find(HiddenCandidateListItem);
    expect(hiddenCandidateListItem.text()).toContain(`This ${result} is no longer available`);
  });
});
