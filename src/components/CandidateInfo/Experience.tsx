import React from 'react';
import {useHover} from 'react-use';
import styles from './Experience.scss';

interface IExperienceProps {
  title: string;
  location: string;
  time: string;
  description?: string;
  isVerified?: boolean;
}

export const Experience: React.FunctionComponent<IExperienceProps> = ({
  time,
  title,
  location,
  description,
  isVerified,
}) => {
  const verifiedLabel = (hovered: boolean) => (
    <span data-cy="verified-label" className={`green relative pointer f6 ph2 dib br-pill ${styles.verifiedContainer}`}>
      <i data-cy="verified-check-icon" className={`dib mr2 relative ${styles.verifiedCheckIcon}`} />
      VERIFIED
      <i data-cy="verified-info-icon" className={`dib ml2 relative ${styles.verifiedInfoIcon}`} />
      {hovered && (
        <span
          className={`absolute bg-mid-gray white f6 fw4 lh-copy pa3 shadow-1 db z-1 ${styles.verifiedTooltip}`}
          data-cy="verified-label-tooltip"
        >
          This certification has been verified. It has been awarded by the educational institution in the stated year.
        </span>
      )}
    </span>
  );
  const [verifiedLabelHoverable] = useHover(verifiedLabel);

  return (
    <div className="mb4 lh-copy f5">
      <div data-cy="experience-time" className="black-40 f6">
        {time}
      </div>
      <div>
        <div data-cy="experience-title" className="fw6 black-60 f4-5 mr3 dib">
          {title} {'  '}
          {isVerified ? verifiedLabelHoverable : null}
        </div>
      </div>
      <div data-cy="experience-location" className="black-50 f5-5">
        {location}
      </div>
      {description ? (
        <p data-cy="experience-description" className="ma0 mt2 black-60">
          {description}
        </p>
      ) : null}
    </div>
  );
};
