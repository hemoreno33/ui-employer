import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {suggestedTalentsMock} from '~/__mocks__/suggestedTalents/suggestedTalents.mocks';
import {CandidateInfoPanel} from '~/components/CandidateInfo/CandidateInfoPanel';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {IAppState} from '~/flux';
import {cleanSnapshot} from '~/testUtil/enzyme';

describe('Candidates/CandidateInfoPanel', () => {
  const state: Pick<IAppState, 'features'> = {
    features: {
      bookmarkTab: true,
    },
  };
  const store = configureStore()(state);
  describe('applicant', () => {
    const candidateInfoPanelProps = {
      candidate: applicationMock.applicant,
      candidateType: CandidateType.applicant,
      formattedDate: 'Applied on 10 Jan 2018',
      onResumeClick: jest.fn(),
      onCandidateBookmarkClick: jest.fn(),
      position: '',
      isBookmarked: false,
    };
    it('should render CandidateInfo', () => {
      const wrapper = mount(
        <Provider store={store}>
          <CandidateInfoPanel {...candidateInfoPanelProps} />
        </Provider>,
      );
      expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
    });
  });

  describe('suggested talent', () => {
    const candidateInfoPanelProps = {
      candidate: suggestedTalentsMock[0].talent,
      candidateType: CandidateType.suggestedTalent,
      formattedDate: 'Active 10 days ago',
      onResumeClick: jest.fn(),
      onCandidateBookmarkClick: jest.fn(),
      position: '',
      isBookmarked: false,
    };
    it('should render CandidateInfo', () => {
      const wrapper = mount(
        <Provider store={store}>
          <CandidateInfoPanel {...candidateInfoPanelProps} />
        </Provider>,
      );
      expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
    });

    it('should not render email and mobile when its not present', () => {
      const wrapper = mount(
        <Provider store={store}>
          <CandidateInfoPanel
            {...candidateInfoPanelProps}
            candidate={{...suggestedTalentsMock[0].talent, email: undefined, mobileNumber: undefined}}
          />
        </Provider>,
      );

      const emailDisplay = wrapper.find('[data-cy="candidate-right-panel-1-email"]');
      expect(emailDisplay).toHaveLength(0);
      const mobileDisplay = wrapper.find('[data-cy="candidate-right-panel-1-mobile"]');
      expect(mobileDisplay).toHaveLength(0);
    });
  });

  describe('bookmark candidate toggle', () => {
    const candidateInfoPanelProps = {
      candidate: applicationMock.applicant,
      candidateType: CandidateType.applicant,
      formattedDate: 'Applied on 10 Jan 2018',
      onResumeClick: jest.fn(),
      onCandidateBookmarkClick: jest.fn(),
      position: '',
    };
    it('should render bookmark toggle as toggled on when isBookmarked is true', () => {
      const wrapper = mount(
        <Provider store={store}>
          <CandidateInfoPanel {...candidateInfoPanelProps} isBookmarked={true} />
        </Provider>,
      );
      const bookmarkToggle = wrapper.find('[data-cy="bookmark-candidate"]');
      expect(toJson(bookmarkToggle, cleanSnapshot())).toMatchSnapshot();
    });
    it('should not render bookmark toggle as toggled off when isBookmarked is false', () => {
      const wrapper = mount(
        <Provider store={store}>
          <CandidateInfoPanel {...candidateInfoPanelProps} isBookmarked={false} />
        </Provider>,
      );
      const bookmarkToggle = wrapper.find('[data-cy="bookmark-candidate"]');
      expect(toJson(bookmarkToggle, cleanSnapshot())).toMatchSnapshot();
    });
  });
});
