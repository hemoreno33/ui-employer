import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import {range} from 'lodash/fp';
import React from 'react';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {CandidateErrorBar} from '~/components/CandidateErrorBar/CandidateErrorBar';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {IAppState} from '~/flux';
import {cleanSnapshot} from '~/testUtil/enzyme';
import {CandidateInfo, ICandidateInfoProps} from '../CandidateInfo';

describe('CandidateInfo/CandidateInfo', () => {
  const state: Pick<IAppState, 'features'> = {
    features: {
      jobPost: false,
    },
  };
  const store = configureStore()(state);
  it('should render candidate info with tab contents', () => {
    const candidateInfoProps = {
      candidate: applicationMock.applicant,
      candidateType: CandidateType.applicant,
      candidateTabClicked: jest.fn(),
      onCandidateBookmarkClick: jest.fn(),
      formattedDate: 'Applied on 10 Jan 2018',
      scores: {
        jobkred: 2.0,
        wcc: 5.0,
      },
      isBookmarked: false,
    };
    const wrapper = mount(
      <Provider store={store}>
        <CandidateInfo {...candidateInfoProps} />
      </Provider>,
    );
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();

    const skillsTabTitle = wrapper.find('[title="Skills"]');
    skillsTabTitle.simulate('click');
    expect(wrapper.find('[data-cy="skills-tab"]')).toMatchSnapshot();

    const workExperiencesTabTitle = wrapper.find('[title="Work Experience"]');
    workExperiencesTabTitle.simulate('click');
    expect(toJson(wrapper.find('[data-cy="work-experiences-tab"]'), cleanSnapshot())).toMatchSnapshot();

    const educationHistoryTabTitle = wrapper.find('[title="Education History"]');
    educationHistoryTabTitle.simulate('click');
    expect(toJson(wrapper.find('[data-cy="education-history-tab"]'), cleanSnapshot())).toMatchSnapshot();
  });

  it('should render skills tab when there are more than 10 skills and viewSkillsButton is clicked', () => {
    const candidateInfoProps = {
      candidate: {
        ...applicationMock.applicant,
        skills: range(0)(11).map((id) => ({id, skill: `${id}`})),
      },
      candidateTabClicked: jest.fn(),
      onCandidateBookmarkClick: jest.fn(),
      candidateType: CandidateType.applicant,
      formattedDate: 'Applied on 10 Jan 2018',
      scores: {
        jobkred: 2.0,
        wcc: 5.0,
      },
      isBookmarked: false,
    };
    const wrapper = mount(
      <Provider store={store}>
        <CandidateInfo {...candidateInfoProps} />
      </Provider>,
    );
    const viewSkillsButton = wrapper.find('ViewSkillsButton a');
    viewSkillsButton.simulate('click');
    expect(toJson(wrapper.find('[data-cy="skills-tab"]'), cleanSnapshot())).toMatchSnapshot();
  });

  it('should render candidate info tab contents for empty state', () => {
    const candidateInfoProps = {
      candidate: {
        ...applicationMock.applicant,
        education: [],
        skills: [],
        workExperiences: [],
      },
      candidateType: CandidateType.applicant,
      candidateTabClicked: jest.fn(),
      onCandidateBookmarkClick: jest.fn(),
      formattedDate: 'Applied on 10 Jan 2018',
      scores: {
        jobkred: 2.0,
        wcc: 5.0,
      },
      isBookmarked: false,
    };
    const wrapper = mount(
      <Provider store={store}>
        <CandidateInfo {...candidateInfoProps} />
      </Provider>,
    );
    expect(wrapper.find('[data-cy="overview-tab"]')).toMatchSnapshot();

    const skillsTabTitle = wrapper.find('[title="Skills"]');
    skillsTabTitle.simulate('click');
    expect(wrapper.find('[data-cy="skills-tab"]')).toMatchSnapshot();

    const workExperiencesTabTitle = wrapper.find('[title="Work Experience"]');
    workExperiencesTabTitle.simulate('click');
    expect(wrapper.find('[data-cy="work-experiences-tab"]')).toMatchSnapshot();

    const educationHistoryTabTitle = wrapper.find('[title="Education History"]');
    educationHistoryTabTitle.simulate('click');
    expect(wrapper.find('[data-cy="education-history-tab"]')).toMatchSnapshot();
  });

  it('should reduce the opacity and remove point from download button when there is no resume', () => {
    const candidateInfoProps: ICandidateInfoProps = {
      candidate: {
        ...applicationMock.applicant,
        education: [],
        resume: null,
        skills: [],
        workExperiences: [],
      },
      candidateTabClicked: jest.fn(),
      onCandidateBookmarkClick: jest.fn(),
      candidateType: CandidateType.applicant,
      formattedDate: 'Applied on 10 Jan 2018',
      isBookmarked: false,
    };
    const wrapper = mount(
      <Provider store={store}>
        <CandidateInfo {...candidateInfoProps} />
      </Provider>,
    );
    const downloadButton = wrapper.find('[data-cy="applicant-resume-download-button"]');
    expect(downloadButton).toMatchSnapshot();
  });

  describe('CandidateErrorBar', () => {
    it('should render CandidateErrorBar if candidate info is corrupted', () => {
      const candidateInfoProps: ICandidateInfoProps = {
        candidate: {
          ...applicationMock.applicant,
          education: [],
          skills: [],
          workExperiences: [],
        },
        candidateType: CandidateType.applicant,
        candidateTabClicked: jest.fn(),
        onCandidateBookmarkClick: jest.fn(),
        formattedDate: 'Applied on 10 Jan 2018',
        isBookmarked: false,
      };
      const wrapper = mount(
        <Provider store={store}>
          <CandidateInfo {...candidateInfoProps} />
        </Provider>,
      );
      const errorBar = wrapper.find(CandidateErrorBar);
      expect(errorBar).toHaveLength(1);
    });

    it('should not render CandidateErrorBar if candidate info is valid', () => {
      const candidateInfoProps: ICandidateInfoProps = {
        candidate: applicationMock.applicant,
        candidateType: CandidateType.applicant,
        candidateTabClicked: jest.fn(),
        onCandidateBookmarkClick: jest.fn(),
        formattedDate: 'Applied on 10 Jan 2018',
        isBookmarked: false,
      };
      const wrapper = mount(
        <Provider store={store}>
          <CandidateInfo {...candidateInfoProps} />
        </Provider>,
      );
      const errorBar = wrapper.find(CandidateErrorBar);
      expect(errorBar).toHaveLength(0);
    });
  });
});
