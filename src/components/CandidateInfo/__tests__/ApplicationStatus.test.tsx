import {MockedProvider} from '@apollo/react-testing';
import {mcf} from '@mcf/constants';
import {ApolloGraphQLInteraction, Matchers} from '@pact-foundation/pact';
import {mount} from 'enzyme';
import React from 'react';
import {ApolloProvider} from 'react-apollo';
import {act} from 'react-dom/test-utils';
import {PactBuilder} from '~/__mocks__/pact';
import {Dropdown} from '~/components/Core/Dropdown';
import {SET_APPLICATION_SHORTLIST_AND_STATUS_UPDATE} from '~/graphql/applications';
import {nextTick} from '~/testUtil/enzyme';
import {ApplicationStatus} from '../ApplicationStatus';
// temporarily suppress the following error, which will be fixed when upgrading Graphql package 14.5.8
// error TS7016: Could not find a declaration file for module 'graphql'.
// @ts-ignore
import {print} from 'graphql';

const applicationIdMock = 'R90SS0001A-90';

describe('ApplicationStatus', () => {
  let pactBuilder: PactBuilder;
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-profile');
    await pactBuilder.setup();
  });

  afterAll(async () => pactBuilder.provider.finalize());

  it('should display the correct statusId in the dropdown selection', () => {
    const props = {
      applicationId: applicationIdMock,
      isShortlisted: false,
      statusId: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL,
    };
    const component = mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <ApplicationStatus {...props} />
      </ApolloProvider>,
    );
    const dropdown = component.find(Dropdown);
    expect(dropdown.prop('input').value).toEqual(mcf.JOB_APPLICATION_STATUS.SUCCESSFUL);
  });

  it('should display no selection if the application has under review status but isShortlisted is false', () => {
    const props = {
      applicationId: applicationIdMock,
      isShortlisted: false,
      statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
    };
    const component = mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <ApplicationStatus {...props} />
      </ApolloProvider>,
    );
    const dropdown = component.find(Dropdown);
    expect(dropdown.prop('input').value).toEqual(undefined);
  });

  it('should update the application statusId and set isShortlisted to true if under review status is selected', async () => {
    const variables = {
      applicationId: applicationIdMock,
      isShortlisted: true,
      statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
    };
    const mockResult = {
      data: {
        setApplicationShortlistState: {id: applicationIdMock, isShortlisted: true},
        setApplicationStatus: {id: applicationIdMock, statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW},
      },
    };
    const graphqlInteraction = new ApolloGraphQLInteraction()
      .uponReceiving('getApplicationShortlistAndStatusUpdateUnderReview')
      .withQuery(print(SET_APPLICATION_SHORTLIST_AND_STATUS_UPDATE))
      .withOperation('setApplicationShortlistAndStatusUpdate')
      .withRequest({
        method: 'POST',
        path: '/profile',
      })
      .withVariables(variables)
      .willRespondWith({
        body: Matchers.like(mockResult),
        status: 200,
      });
    await pactBuilder.provider.addInteraction(graphqlInteraction);

    const props = {
      applicationId: applicationIdMock,
      isShortlisted: false,
      statusId: mcf.JOB_APPLICATION_STATUS.RECEIVED,
    };
    const component = mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <ApplicationStatus {...props} />
      </ApolloProvider>,
    );

    act(() => {
      component
        .find(Dropdown)
        .prop('input')
        .onChange(mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW);
    });
    component.update();
    expect(component.find('.spinnerContainer')).toHaveLength(1);

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
    component.update();

    expect(component.find(Dropdown).prop('input').value).toEqual(mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW);
  });

  it('should update the application statusId and set isShortlisted to false if hired status is selected', async () => {
    const variables = {
      applicationId: applicationIdMock,
      isShortlisted: false,
      statusId: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL,
    };
    const mockResult = {
      data: {
        setApplicationShortlistState: {id: applicationIdMock, isShortlisted: false},
        setApplicationStatus: {id: applicationIdMock, statusId: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL},
      },
    };
    const graphqlInteraction = new ApolloGraphQLInteraction()
      .uponReceiving('getApplicationShortlistAndStatusUpdateSuccessful')
      .withQuery(print(SET_APPLICATION_SHORTLIST_AND_STATUS_UPDATE))
      .withOperation('setApplicationShortlistAndStatusUpdate')
      .withRequest({
        method: 'POST',
        path: '/profile',
      })
      .withVariables(variables)
      .willRespondWith({
        body: Matchers.like(mockResult),
        status: 200,
      });
    await pactBuilder.provider.addInteraction(graphqlInteraction);

    const props = {
      applicationId: applicationIdMock,
      isShortlisted: false,
      statusId: mcf.JOB_APPLICATION_STATUS.RECEIVED,
    };
    const component = mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <ApplicationStatus {...props} />
      </ApolloProvider>,
    );

    act(() => {
      component
        .find(Dropdown)
        .prop('input')
        .onChange(mcf.JOB_APPLICATION_STATUS.SUCCESSFUL);
    });
    component.update();
    expect(component.find('.spinnerContainer')).toHaveLength(1);

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
    component.update();

    expect(component.find(Dropdown).prop('input').value).toEqual(mcf.JOB_APPLICATION_STATUS.SUCCESSFUL);
  });

  it('should revert to previous selection if updating the status failed', async () => {
    const mutationMockError = [
      {
        error: new Error('Error'),
        request: {
          query: SET_APPLICATION_SHORTLIST_AND_STATUS_UPDATE,
          variables: {
            applicationId: applicationIdMock,
            isShortlisted: true,
            statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
          },
        },
      },
    ];
    const props = {
      applicationId: applicationIdMock,
      isShortlisted: false,
      statusId: mcf.JOB_APPLICATION_STATUS.RECEIVED,
    };
    const component = mount(
      <MockedProvider mocks={mutationMockError} addTypename={false}>
        <ApplicationStatus {...props} />
      </MockedProvider>,
    );

    act(() => {
      component
        .find(Dropdown)
        .prop('input')
        .onChange(mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW);
    });
    await nextTick(component);
    await nextTick(component);
    expect(component.find(Dropdown).prop('input').value).toEqual(mcf.JOB_APPLICATION_STATUS.RECEIVED);
  });
});
