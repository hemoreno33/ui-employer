import {mount} from 'enzyme';
import React from 'react';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {ISkill} from '~/graphql/candidates';
import {isNotNil} from '~/util/isNotNil';
import {Overview} from '../Overview';

describe('CandidateInfo/Overview', () => {
  it('should not render the candidate info partially when some are missing', () => {
    const skills: ISkill[] = applicationMock.applicant.skills.filter(isNotNil);
    const wrapper = mount(
      <Overview skills={skills} educationList={[]} workExperiences={[]} onViewSkills={jest.fn()} />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
