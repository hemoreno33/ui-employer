import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {EducationHistory} from '~/components/CandidateInfo/EducationHistory';
import {IEducation} from '~/graphql/candidates';
import {cleanSnapshot} from '~/testUtil/enzyme';

describe('CandidateInfo/EducationHistory', () => {
  const educationMock: IEducation = applicationMock.applicant.education[0]!;

  it('should render Education not more than the specified limit', () => {
    const educationListMock: IEducation[] = [educationMock, educationMock];
    const wrapper = mount(<EducationHistory educationList={educationListMock} limit={1} />);
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });

  it('should render null placeholder if ssecFosDescription is undefined', () => {
    const educationListMock: IEducation[] = [
      {
        ...educationMock,
        ssecFosDescription: null,
      },
    ];
    const wrapper = mount(<EducationHistory educationList={educationListMock} />);
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });

  it('should render when optional values are not defined', () => {
    const educationListMock: IEducation[] = [
      {
        institution: 'School Name',
        isHighest: true,
        name: null,
        ssecEqaCode: null,
        ssecEqaDescription: null,
        ssecFosDescription: null,
        yearAttained: null,
      },
    ];
    const wrapper = mount(<EducationHistory educationList={educationListMock} />);
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });
});
