import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {HiddenCandidateInfo} from '../HiddenCandidateInfo';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {IAppState} from '~/flux';
import {cleanSnapshot} from '~/testUtil/enzyme';
import {BookmarkToggle} from '~/components/BookmarkToggle/BookmarkToggle';

describe('Candidates/HiddenCandidateInfo', () => {
  const state: Pick<IAppState, 'features'> = {
    features: {
      bookmarkTab: true,
    },
  };
  const hiddenCandidate = {name: 'dragon'};
  const onCandidateBookmarkClickMock = jest.fn();
  const store = configureStore()(state);

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should render HiddenCandidateInfo', () => {
    const wrapper = mount(
      <Provider store={store}>
        <HiddenCandidateInfo
          candidate={hiddenCandidate}
          candidateType={CandidateType.suggestedTalent}
          isBookmarked={true}
          onCandidateBookmarkClick={onCandidateBookmarkClickMock}
        />
      </Provider>,
    );

    // @ts-ignore toJson typing issue when using .find
    expect(toJson(wrapper.find(HiddenCandidateInfo), cleanSnapshot())).toMatchSnapshot();
  });

  it.each`
    type                             | result
    ${CandidateType.applicant}       | ${'applicant'}
    ${CandidateType.suggestedTalent} | ${'talent'}
  `('should display message with relevant candidate type: $type', ({type, result}) => {
    const wrapper = mount(
      <Provider store={store}>
        <HiddenCandidateInfo
          candidate={hiddenCandidate}
          candidateType={type}
          isBookmarked={true}
          onCandidateBookmarkClick={onCandidateBookmarkClickMock}
        />
      </Provider>,
    );

    const hiddenCandidateInfo = wrapper.find(HiddenCandidateInfo);
    expect(hiddenCandidateInfo.text()).toContain(`This ${result} is no longer available`);
  });

  it('should not show the unsave text and bookmark toggle if isBookmarked is false', () => {
    const wrapper = mount(
      <Provider store={store}>
        <HiddenCandidateInfo
          candidate={hiddenCandidate}
          candidateType={CandidateType.suggestedTalent}
          isBookmarked={false}
          onCandidateBookmarkClick={onCandidateBookmarkClickMock}
        />
      </Provider>,
    );
    const unsaveText = wrapper.find('[data-cy="hidden-candidate-unsave-text"]');
    expect(unsaveText).toHaveLength(0);
    const bookmarkToggle = wrapper.find(BookmarkToggle);
    expect(bookmarkToggle).toHaveLength(0);
  });
});
