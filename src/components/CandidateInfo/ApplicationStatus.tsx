import {mcf} from '@mcf/constants';
import React, {useState} from 'react';
import {Mutation, MutationFunction, MutationResult} from 'react-apollo';
import {Dropdown} from '~/components/Core/Dropdown';
import {growlNotification, growlType} from '~/components/GrowlNotification/GrowlNotification';
import {NumberLoader} from '~/components/JobList/NumberLoader';
import {Maybe} from '~/graphql/__generated__/types';
import {SET_APPLICATION_SHORTLIST_AND_STATUS_UPDATE} from '~/graphql/applications';
import styles from './CandidateInfo.scss';

export const updateApplicationStatusOptions = [
  {
    label: 'Shortlisted',
    value: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
  },
  {
    label: 'Unsuccessful',
    value: mcf.JOB_APPLICATION_STATUS.UNSUCCESSFUL,
  },
  {
    label: 'Hired',
    value: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL,
  },
];

interface IApplicationStatusProps {
  statusId?: number;
  isShortlisted?: Maybe<boolean>;
  applicationId: string;
}

export const ApplicationStatus: React.FunctionComponent<IApplicationStatusProps> = ({
  statusId,
  isShortlisted,
  applicationId,
}) => {
  /*
    If the status id is UNDER_REVIEW and isShortlisted is false, there would be no selection showed in the dropdown. This scenario
    would happen if the user downloaded the resume but has not yet selected to shortlist it from this dropdown.
  */
  const isStatusUnderReview = statusId === mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW;
  const initialStatus = !isShortlisted && isStatusUnderReview ? undefined : statusId;

  const [selectedStatus, setSelectedStatus] = useState<mcf.JOB_APPLICATION_STATUS | undefined>(initialStatus);
  const [previousStatus, setPreviousStatus] = useState<mcf.JOB_APPLICATION_STATUS | undefined>();
  const onError = () => {
    growlNotification('Temporarily unable to update application status. Please try again.', growlType.ERROR, {
      toastId: 'updateStatusError',
    });
    setSelectedStatus(previousStatus);
  };

  return (
    <div data-cy="applicant-update-status" className={styles.applicantStatus}>
      <Mutation mutation={SET_APPLICATION_SHORTLIST_AND_STATUS_UPDATE} onError={onError}>
        {(setApplicationShortlistAndStatusUpdate: MutationFunction, {loading}: MutationResult) => {
          if (loading) {
            return (
              <div className={`flex flex-column justify-center items-center ${styles.applicantStatus}`}>
                <NumberLoader />
              </div>
            );
          }
          return (
            <Dropdown
              id="update-application-status-dropdown"
              key={selectedStatus}
              placeholder="Application status"
              data={updateApplicationStatusOptions}
              input={{
                onChange: (value) => {
                  const isValueUnderReview = value === mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW;
                  setApplicationShortlistAndStatusUpdate({
                    variables: {
                      applicationId,
                      isShortlisted: isValueUnderReview,
                      statusId: value,
                    },
                  });
                  setPreviousStatus(selectedStatus);
                  setSelectedStatus(value);
                },
                value: selectedStatus,
              }}
            />
          );
        }}
      </Mutation>
    </div>
  );
};
