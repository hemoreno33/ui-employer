import React, {ReactElement} from 'react';
import {CandidateErrorBar} from '~/components/CandidateErrorBar/CandidateErrorBar';
import {EducationHistory, Overview, Skills, WorkExperiences} from '~/components/CandidateInfo';
import styles from '~/components/CandidateInfo/CandidateInfo.scss';
import {CandidateInfoPanel} from '~/components/CandidateInfo/CandidateInfoPanel';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {Tab, Tabs, TabsUse} from '~/components/Navigation/Tabs';
import {
  onApplicationCandidateTabClicked,
  onSuggestedTalentCandidateTabClicked,
  onBookmarkedCandidateTabClicked,
} from '~/flux/analytics';
import {getFullPosition, sortEducationByLatest, sortWorkExperienceByLatest} from '~/flux/applications';
import {ICandidate} from '~/graphql/candidates';
import {isCorrupted} from '~/util/candidate';
import {isNotNil} from '~/util/isNotNil';

export enum CandidateTabs {
  OVERVIEW,
  EDUCATION_HISTORY,
  SKILLS,
  WORK_EXPERIENCES,
}

export interface ICandidateInfoState {
  selectedTab: CandidateTabs;
}

export interface ICandidateInfoProps {
  candidate: ICandidate;
  candidateType: CandidateType;
  showTopMatch?: boolean;
  isBookmarked: boolean;
  formattedDate: string;
  candidateTabClicked:
    | typeof onApplicationCandidateTabClicked
    | typeof onSuggestedTalentCandidateTabClicked
    | typeof onBookmarkedCandidateTabClicked;
  onResumeClick?: () => void;
  statusSelector?: ReactElement;
  onCandidateBookmarkClick: (checked: boolean) => void;
}

export class CandidateInfo extends React.Component<ICandidateInfoProps, ICandidateInfoState> {
  constructor(props: ICandidateInfoProps) {
    super(props);
    this.state = {
      selectedTab: CandidateTabs.OVERVIEW,
    };
  }

  public handleTabSelect = (index: CandidateTabs) => {
    this.setState({selectedTab: index});
    if (this.props.candidateTabClicked) {
      this.props.candidateTabClicked(index);
    }
  };

  public render() {
    const {
      candidate,
      candidateType,
      formattedDate,
      onResumeClick,
      showTopMatch,
      isBookmarked,
      statusSelector,
      onCandidateBookmarkClick,
    } = this.props;
    const {education, skills, workExperiences} = this.props.candidate;
    const sortedEducation = sortEducationByLatest(education.filter(isNotNil));
    const sortedWorkExperiences = sortWorkExperienceByLatest(workExperiences.filter(isNotNil));
    const position = getFullPosition(candidate);

    return (
      <div className="flex flex-column bg-white w-100">
        {isCorrupted(candidate) ? <CandidateErrorBar /> : ''}
        <CandidateInfoPanel
          candidate={candidate}
          candidateType={candidateType}
          position={position}
          formattedDate={formattedDate}
          showTopMatch={showTopMatch}
          isBookmarked={isBookmarked}
          onResumeClick={onResumeClick}
          statusSelector={statusSelector}
          onCandidateBookmarkClick={onCandidateBookmarkClick}
        />
        <section className={`${styles.infoContainer} bg-white flex-shrink-0`}>
          <Tabs use={TabsUse.PANEL} selectedTab={this.state.selectedTab} onTabSelect={this.handleTabSelect}>
            <Tab tabId={CandidateTabs.OVERVIEW} title={'Overview'}>
              <section data-cy="overview-tab" className="pa4">
                <Overview
                  educationList={sortedEducation}
                  skills={skills.filter(isNotNil)}
                  workExperiences={sortedWorkExperiences}
                  onViewSkills={() => this.handleTabSelect(CandidateTabs.SKILLS)}
                />
              </section>
            </Tab>
            <Tab tabId={CandidateTabs.SKILLS} title={'Skills'}>
              <section data-cy="skills-tab" className="pa4">
                <Skills skills={skills.filter(isNotNil)} />
              </section>
            </Tab>
            <Tab tabId={CandidateTabs.WORK_EXPERIENCES} title={'Work Experience'}>
              <section data-cy="work-experiences-tab" className="pa4">
                <WorkExperiences workExperiences={sortedWorkExperiences} />
              </section>
            </Tab>
            <Tab tabId={CandidateTabs.EDUCATION_HISTORY} title={'Education History'}>
              <section data-cy="education-history-tab" className="pa4">
                <EducationHistory educationList={sortedEducation} />
              </section>
            </Tab>
          </Tabs>
        </section>
      </div>
    );
  }
}
