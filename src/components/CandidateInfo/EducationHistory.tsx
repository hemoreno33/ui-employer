import {isNil} from 'lodash/fp';
import React from 'react';
import {Experience} from '~/components/CandidateInfo/Experience';
import {formatEducationTitle} from '~/flux/applications';
import {IEducation} from '~/graphql/candidates';
import {NULL_PLACEHOLDER} from '~/util/constants';

interface IEducationHistoryProps {
  educationList: IEducation[];
  limit?: number;
}

export const EducationHistory: React.FunctionComponent<IEducationHistoryProps> = ({educationList, limit}) =>
  educationList.length ? (
    <div>
      <ul className="list pl0 ma0">
        {educationList
          .map((education, index) => {
            const title = formatEducationTitle(education);
            return (
              <li data-cy="education-history-field" key={index}>
                <Experience
                  title={title}
                  location={education.institution || NULL_PLACEHOLDER}
                  time={!isNil(education.yearAttained) ? `${education.yearAttained}` : NULL_PLACEHOLDER}
                  description={education.name || undefined}
                  isVerified={isNil(education.isVerified) ? false : education.isVerified}
                />
              </li>
            );
          })
          .slice(0, limit)}
      </ul>
    </div>
  ) : (
    <div className="f5 black-50 lh-copy tc">
      This candidate has not added any education history. <br />
      You may want to view the resume instead.
    </div>
  );
