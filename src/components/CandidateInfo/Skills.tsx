import React from 'react';
import styles from '~/components/CandidateInfo/CandidateInfo.scss';
import {ISkill} from '~/graphql/candidates';

interface ISkillsProps {
  skills: ISkill[];
  limit?: number;
}

export const Skills: React.FunctionComponent<ISkillsProps> = ({skills, limit}) =>
  skills.length ? (
    <ul className="flex list pl0 ma0 flex-wrap">
      {skills
        .map((skill) => (
          <li data-cy="skill-icon" key={skill.id} className={`flex f5-5 br-pill bg-primary white mr2 pv1 pl2 pr3 mb1`}>
            <i className={`${styles.skillIcon}`} />
            <span>{skill.skill}</span>
          </li>
        ))
        .slice(0, limit)}
    </ul>
  ) : (
    <div className="f5 black-50 lh-copy tc">
      This candidate has not added any skills. <br />
      You may want to view the resume instead.
    </div>
  );

export default Skills;
