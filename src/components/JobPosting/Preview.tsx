import History from 'history';
import React from 'react';
import {Link} from 'react-router-dom';
import {IJobPostingStep, jobPostingSteps} from '~/components/JobPosting/JobPostingForm';
import {JobDescriptionPreview} from '~/components/JobPosting/PreviewInfo/JobDescriptionPreview';
import {SkillsPreview} from '~/components/JobPosting/PreviewInfo/SkillsPreview';
import {WorkplaceDetailsPreview} from '~/components/JobPosting/PreviewInfo/WorkplaceDetailsPreview';
import {KeyInformationPreview} from './PreviewInfo/KeyInformationPreview';

interface IPreview {
  locationState: History.LocationState;
}

const renderPreviewComponent = (step: IJobPostingStep) => {
  switch (step.id) {
    case '#job-description':
      return <JobDescriptionPreview />;
    case '#skills':
      return <SkillsPreview />;
    case '#key-information':
      return <KeyInformationPreview />;
    case '#workplace-details':
      return <WorkplaceDetailsPreview />;
    default:
      return <>Blank</>;
  }
};

export const Preview: React.FunctionComponent<IPreview> = (props) => {
  const previewSteps = jobPostingSteps.slice(0, -1);
  return (
    <>
      {previewSteps.map((step, index) => (
        <section className="pa3" key={step.id} data-cy={`${step.id.slice(1)}_preview`}>
          <div className="flex justify-between" data-cy={step.id.slice(1)}>
            <h4 className="ma0 mb3 secondary">
              {index + 1}. {step.description.toUpperCase()}
            </h4>
            <Link
              id={step.id}
              className="icon-edit f5-5 blue"
              to={{
                hash: step.id,
                state: props.locationState,
              }}
            >
              Edit
            </Link>
          </div>
          {renderPreviewComponent(step)}
          {previewSteps.length === index + 1 ? null : <div className="b--black-20 bt mt3 mb4 w-100" />}
        </section>
      ))}
    </>
  );
};
