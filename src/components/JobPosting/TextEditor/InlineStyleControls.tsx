import {DraftInlineStyleType, EditorState} from 'draft-js';
import React from 'react';
import {TextEditorButton} from './TextEditorButton';

const INLINE_STYLES: Array<{label: string; style: DraftInlineStyleType}> = [
  {label: 'B', style: 'BOLD'},
  {label: '', style: 'ITALIC'},
  {label: 'U', style: 'UNDERLINE'},
];

interface IInlineStyleControlsProps {
  editorState: EditorState;
  onClick: (type: DraftInlineStyleType) => void;
}
export const InlineStyleControls: React.FunctionComponent<IInlineStyleControlsProps> = ({editorState, onClick}) => {
  const currentStyle = editorState.getCurrentInlineStyle();
  return (
    <>
      {INLINE_STYLES.map((type) => (
        <TextEditorButton
          key={type.style}
          active={currentStyle.has(type.style)}
          label={type.label}
          onClick={() => onClick(type.style)}
          className={type.style.toLowerCase()}
        />
      ))}
    </>
  );
};
