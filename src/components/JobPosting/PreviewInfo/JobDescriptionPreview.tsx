import {find} from 'lodash';
import React from 'react';
import {Query} from 'react-apollo';
import {FormSpy} from 'react-final-form';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {FormLoader} from '~/components/Layouts/FormLoader';
import {
  GetCommonQuery,
  GetCommonQueryVariables,
  GetCommonSsicQuery,
  GetCommonSsicQueryVariables,
} from '~/graphql/__generated__/types';
import {GET_SSIC_LIST, GET_SSOC_LIST} from '~/graphql/jobs/jobs.query';
import {GetCompanyInfoQuery} from '~/services/employer';
import {NULL_PLACEHOLDER} from '~/util/constants';
import {sanitize} from 'dompurify';

import {PreviewField} from './PreviewField';

export interface ISsocProps {
  ssoc: number;
  ssocTitle: string;
}

export const JobDescriptionPreview: React.FunctionComponent = () => {
  return (
    <FormSpy
      render={({values}) => {
        const {
          title,
          occupation,
          description,
          otherRequirements,
          thirdPartyEmployer,
          thirdPartyEmployerEntityId,
          industry,
          registrationType,
        }: IJobPostingFormState['jobDescription'] = values.jobDescription;
        const combinedDescription = `${description}${otherRequirements ?? ''}`;
        return (
          <>
            {thirdPartyEmployer && thirdPartyEmployerEntityId ? (
              <div className="flex flex-wrap pa3">
                <h5 className="black-60 f4-5 fw6 lh-copy mt2 mb4 normal">Posting on behalf of another company</h5>
                <GetCompanyInfoQuery uen={thirdPartyEmployerEntityId!}>
                  {({data}) => (
                    <PreviewField containerClass=" w-100 pb2 mb3" label="Company Name / UEN">
                      {data ? data.name : thirdPartyEmployerEntityId}
                    </PreviewField>
                  )}
                </GetCompanyInfoQuery>
                <PreviewField containerClass="pr2 w-50 pb2 mb3" label="Type of Registration">
                  {registrationType || 'Not Applicable'}
                </PreviewField>
                <Query<GetCommonSsicQuery, GetCommonSsicQueryVariables>
                  query={GET_SSIC_LIST}
                  variables={{code: industry}}
                  skip={!industry}
                >
                  {({data: ssicList, loading: ssicListLoading}) => {
                    if (ssicListLoading) {
                      return <FormLoader className="o-50" />;
                    }
                    const ssic = ssicList && ssicList.common.ssicList.find(({code}) => code === industry);
                    return (
                      <PreviewField containerClass="pl2 w-50 pb2 mb3" label="Industry">
                        {(ssic ? ssic.description : industry) || 'Not Applicable'}
                      </PreviewField>
                    );
                  }}
                </Query>
              </div>
            ) : null}
            <Query<GetCommonQuery, GetCommonQueryVariables> query={GET_SSOC_LIST}>
              {({loading, error, data}) => {
                if (loading || error || !data) {
                  return <FormLoader className="o-50" />;
                }
                return (
                  <div className="flex flex-wrap pa3">
                    <PreviewField containerClass="pr2 w-50 pb2 mb3" label="Job Title">
                      {title}
                    </PreviewField>
                    <PreviewField containerClass="pl2 w-50 pb2 mb3" label="Occupation">
                      {(
                        find(data.common.ssocList, (item) => item.ssoc === occupation) || {
                          ssocTitle: NULL_PLACEHOLDER,
                        }
                      ).ssocTitle || NULL_PLACEHOLDER}
                    </PreviewField>
                    <div className="w-100">
                      <h5 className="ma0 black-50 f6 lh-title normal">Job Description & Requirements</h5>
                      <div
                        className="mv2 break-word"
                        dangerouslySetInnerHTML={{__html: sanitize(combinedDescription) || NULL_PLACEHOLDER}}
                      />
                    </div>
                  </div>
                );
              }}
            </Query>
          </>
        );
      }}
    />
  );
};
