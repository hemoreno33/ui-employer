import React from 'react';
import {FormSpy} from 'react-final-form';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import styles from './SkillsPreview.scss';

export const SkillsPreview: React.FunctionComponent = () => {
  return (
    <FormSpy
      render={({values}) => {
        const {addedSkills, recommendedSkills}: IJobPostingFormState['jobPostingSkills'] = values.jobPostingSkills;
        const skills = [...recommendedSkills, ...addedSkills];

        return skills.length ? (
          skills
            .filter((skill) => skill.selected)
            .map((item) => {
              return (
                <div
                  key={item.id}
                  data-cy="skill-icon"
                  className={`dib f5 br-pill mh1 mv1 pv1 ph3 ba b--primary ${styles.color} ${styles.skillButton}`}
                >
                  <span>{item.skill}</span>
                </div>
              );
            })
        ) : (
          <span>There are no selected skills</span>
        );
      }}
    />
  );
};
