import {MockedProvider} from '@apollo/react-testing';
import {mcf} from '@mcf/constants';
import {ApolloGraphQLInteraction, Interaction, Matchers} from '@pact-foundation/pact';
import {mount} from 'enzyme';
import {noop} from 'lodash';
import React from 'react';
import {ApolloProvider} from 'react-apollo';
import {act} from 'react-dom/test-utils';
import {Form} from 'react-final-form';
import {PactBuilder} from '~/__mocks__/pact';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {PreviewField} from '~/components/JobPosting/PreviewInfo/PreviewField';
import {GET_EDUCATION_LIST} from '~/graphql/jobs/jobs.query';
import {ISubScheme} from '~/services/schemes/getCompanySchemes';
import {nextTick} from '~/testUtil/enzyme';
import {KeyInformationPreview} from '../KeyInformationPreview';
import {JobStatusCodes} from '~/services/employer/jobs.types';
import {AutoCloseDateSubtitle} from '../AutoCloseDateSubtitle';
// temporarily suppress the following error, which will be fixed when upgrading Graphql package 14.5.8
// error TS7016: Could not find a declaration file for module 'graphql'.
// @ts-ignore
import {print} from 'graphql';

const v2ApiMock = jest.fn();
jest.mock('~/config', () => ({
  config: {
    url: {
      apiJob: {
        get v2() {
          return v2ApiMock();
        },
      },
    },
  },
}));

describe('KeyInformationPreview', () => {
  const subScheme: ISubScheme = {
    id: 1,
    programme: 'WSQ Advanced Certificate in Industrial Design (PnT)',
    schemeId: mcf.SCHEME_ID.PCP,
  };

  let jobPactBuilder: PactBuilder;
  let profilePactBuilder: PactBuilder;
  beforeAll(async () => {
    jobPactBuilder = new PactBuilder('api-job');
    v2ApiMock.mockImplementation(() => `http://${jobPactBuilder.host}:${jobPactBuilder.port}/v2`);
    await jobPactBuilder.setup();

    profilePactBuilder = new PactBuilder('api-profile');
    await profilePactBuilder.setup();
  });
  afterAll(async () => {
    await jobPactBuilder.provider.finalize();
    await profilePactBuilder.provider.finalize();
  });

  beforeEach(async () => {
    const getSubSchemeByIdQuery = new Interaction()
      .uponReceiving('getSubSchemeById')
      .withRequest({
        method: 'GET',
        path: `/v2/schemes/${mcf.SCHEME_ID.PCP}/subSchemes/1`,
      })
      .willRespondWith({
        body: Matchers.like(subScheme),
        headers: {
          'access-control-allow-credentials': 'true',
        },
        status: 200,
      });
    await jobPactBuilder.provider.addInteraction(getSubSchemeByIdQuery);

    const graphqlQuery = new ApolloGraphQLInteraction()
      .uponReceiving('getCommonEducation')
      .withQuery(print(GET_EDUCATION_LIST))
      .withOperation('getCommonEducation')
      .withRequest({
        method: 'POST',
        path: '/profile',
      })
      .willRespondWith({
        body: {
          data: {
            common: {
              ssecEqaList: Matchers.eachLike({actualCode: '1', description: 'JOHN DOE'}),
              ssecFosList: Matchers.eachLike({actualCode: '1', description: 'JANE DOE'}),
            },
          },
        },
        status: 200,
      });
    await profilePactBuilder.provider.addInteraction(graphqlQuery);
  });

  it('should render all selected schemes', async () => {
    const initialValues: Partial<IJobPostingFormState> = {
      keyInformation: {
        jobCategories: [],
        schemes: [
          {id: mcf.SCHEME_ID.CAREER_SUPPORT, selected: true},
          {id: mcf.SCHEME_ID.P_MAX, selected: false},
          {id: mcf.SCHEME_ID.PCP, subSchemeId: 1, selected: true},
        ],
      },
    };
    const wrapper = mount(
      <ApolloProvider client={profilePactBuilder.getApolloClient()}>
        <Form
          onSubmit={noop}
          initialValues={initialValues}
          render={() => <KeyInformationPreview jobPostingDate={undefined} />}
        />
      </ApolloProvider>,
    );

    await act(async () => {
      await jobPactBuilder.verifyInteractions();
    });
    await profilePactBuilder.verifyInteractions();

    const schemePreviewList = wrapper.find('ul > li');
    expect(schemePreviewList).toHaveLength(2);
    expect(schemePreviewList.at(0).text()).toEqual(
      mcf.SCHEMES.find((scheme) => scheme.id === mcf.SCHEME_ID.CAREER_SUPPORT)!.scheme,
    );
    expect(schemePreviewList.at(1).text()).toEqual(
      `${mcf.SCHEMES.find((scheme) => scheme.id === mcf.SCHEME_ID.PCP)!.scheme} (${subScheme.programme})`,
    );
  });
  describe('ssecFOS display', () => {
    const getSsecMock = [
      {
        request: {
          query: GET_EDUCATION_LIST,
          variables: {},
        },
        result: {
          data: {
            common: {
              ssecEqaList: [
                {actualCode: '41', description: 'A levels'},
                {actualCode: '70', description: 'Bachelors'},
              ],
              ssecFosList: [{actualCode: '1', description: 'Awesomeness'}],
            },
          },
        },
      },
    ];
    const keyInformationPreviewComponent = (initialValues: any) => {
      return mount(
        <MockedProvider mocks={getSsecMock} addTypename={false}>
          <Form onSubmit={noop} initialValues={initialValues} render={() => <KeyInformationPreview />} />
        </MockedProvider>,
      );
    };

    it('should not render field of study if education level is GCE A levels or below', async () => {
      const initialValues: Partial<IJobPostingFormState> = {
        keyInformation: {
          fieldOfStudy: '1',
          jobCategories: [],
          qualification: '41',
          schemes: [],
        },
      };

      const wrapper = keyInformationPreviewComponent(initialValues);
      await nextTick(wrapper);
      const fos = wrapper.find(PreviewField).findWhere((component) => component.prop('label') === 'Field of Study');
      expect(fos).toHaveLength(0);
    });

    it('should render field of study if education level is above GCE A levels', async () => {
      const initialValues: Partial<IJobPostingFormState> = {
        keyInformation: {
          fieldOfStudy: '1',
          jobCategories: [],
          qualification: '70',
          schemes: [],
        },
      };

      const wrapper = keyInformationPreviewComponent(initialValues);
      await nextTick(wrapper);

      const fos = wrapper.find(PreviewField).findWhere((component) => component.prop('label') === 'Field of Study');
      expect(fos).toHaveLength(1);
      expect(fos.find('p').text()).toEqual('Awesomeness');
    });
  });
  describe('AutoCloseDateSubtitle', () => {
    it.each`
      status                   | length
      ${JobStatusCodes.Open}   | ${1}
      ${JobStatusCodes.Closed} | ${0}
    `('should render $length AutoCloseDateSubtitle if status is $status', async ({status, length}) => {
      const initialValues: Partial<IJobPostingFormState> = {
        keyInformation: {
          jobCategories: [],
          jobPostDuration: 7,
          schemes: [],
        },
      };
      const wrapper = mount(
        <ApolloProvider client={profilePactBuilder.getApolloClient()}>
          <Form
            onSubmit={noop}
            initialValues={initialValues}
            render={() => <KeyInformationPreview jobPostingDate={undefined} jobStatusId={status} />}
          />
        </ApolloProvider>,
      );
      expect(wrapper.find(AutoCloseDateSubtitle)).toHaveLength(length);
    });
  });
});
