import React from 'react';

export const MSFSkillsPreview: React.FunctionComponent = () => (
  <div className="pa3 black-20 tc i">Only applicable for jobs created on this portal</div>
);
