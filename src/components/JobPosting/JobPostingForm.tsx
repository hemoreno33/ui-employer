import {isEqual, noop} from 'lodash/fp';
import React, {useEffect, useState} from 'react';
import {Form} from 'react-final-form';
// @ts-ignore react-final-form-listeners does not contain typescript file.
import {OnChange} from 'react-final-form-listeners';
import {RouteComponentProps} from 'react-router';
import {Link} from 'react-router-dom';
import {JobDescriptionFields} from '~/components/JobPosting/JobDescriptionFields';
import {JobPostingContext} from '~/components/JobPosting/JobPostingContext';
import {CreateKeyInformationFields} from '~/components/JobPosting/KeyInformation/CreateKeyInformationFields';
import {Preview} from '~/components/JobPosting/Preview';
import {CreateJobSkillsFields} from '~/components/JobPosting/Skills/CreateJobSkillsFields';
import {IPillSkill} from '~/components/JobPosting/Skills/skills.types';
import {CreateWorkplaceDetailsFields} from '~/components/JobPosting/WorkplaceDetails/CreateWorkplaceDetailsFields';
import {Location, LocationType} from '~/components/JobPosting/WorkplaceDetails/workplaceDetails.types';
import {ErrorCard} from '~/components/Layouts/ErrorCard';

import styles from './JobPostingForm.scss';

type Step = '#job-description' | '#skills' | '#key-information' | '#workplace-details' | '#preview';

export interface IJobPostingStep {
  id: Step;
  description: string;
  next?: Step;
  previous?: Step;
}

export const jobPostingSteps: IJobPostingStep[] = [
  {
    description: 'Job Description',
    id: '#job-description',
    next: '#skills',
  },
  {
    description: 'Skills',
    id: '#skills',
    next: '#key-information',
    previous: '#job-description',
  },
  {
    description: 'Key Information',
    id: '#key-information',
    next: '#workplace-details',
    previous: '#skills',
  },
  {
    description: 'Workplace Details',
    id: '#workplace-details',
    next: '#preview',
    previous: '#key-information',
  },
  {
    description: 'Review',
    id: '#preview',
    previous: '#workplace-details',
  },
];

let isFormPristine: boolean;

const askBeforeUnload = (event: BeforeUnloadEvent) => {
  if (!isFormPristine) {
    event.returnValue = 'You have unfinished changes!';
  }
};

export interface IJobPostingFormState {
  jobDescription: {
    description?: string;
    industry?: string;
    registrationType?: string;
    title?: string;
    occupation?: number;
    otherRequirements?: string;
    thirdPartyEmployerEntityId?: string;
    thirdPartyEmployer: boolean;
  };
  jobPostingSkills: {
    addedSkills: IPillSkill[];
    recommendedSkills: IPillSkill[];
  };
  keyInformation: {
    jobPostDuration?: number;
    numberOfVacancies?: number;
    jobCategories: number[];
    positionLevel?: number;
    minimumYearsExperience?: number;
    employmentType?: number[];
    qualification?: string;
    fieldOfStudy?: string;
    minimumSalary?: number;
    maximumSalary?: number;
    schemes: Array<{id: number; subSchemeId?: number; selected: boolean}>;
  };
  workplaceDetails: {
    location: Location;
    locationType: LocationType;
    postalCode?: string;
    block?: string;
    street?: string;
    building?: string;
    overseasCountry?: string;
    foreignAddress1?: string;
    foreignAddress2?: string;
  };
}

export const formInitialState: IJobPostingFormState = {
  jobDescription: {
    thirdPartyEmployer: false,
  },
  jobPostingSkills: {
    addedSkills: [],
    recommendedSkills: [],
  },
  keyInformation: {
    jobCategories: [],
    schemes: [],
  },
  workplaceDetails: {
    location: Location.Local,
    locationType: LocationType.SameAsCompany,
  },
};

interface IJobPostingFormProps extends RouteComponentProps {
  initialValues?: IJobPostingFormState;
  jobDescription?: React.ReactElement;
  jobSkills?: React.ReactElement;
  keyInformation?: React.ReactElement;
  workplaceDetails?: React.ReactElement;
  onTitleChange?: (values: IJobPostingFormState['jobDescription']['title']) => void;
  onSubmit: (values: IJobPostingFormState) => void;
  startJobPosting: () => void;
}

export const JobPostingForm: React.FunctionComponent<IJobPostingFormProps> = ({
  initialValues,
  jobDescription,
  jobSkills,
  keyInformation,
  workplaceDetails,
  history,
  location,
  onTitleChange = noop,
  onSubmit,
  startJobPosting,
}) => {
  const initialState = initialValues ? initialValues : formInitialState;
  const [jobDescriptionChanged, setJobDescriptionChanged] = useState(true);
  const [showErrorCard, setShowErrorCard] = useState(false);
  const [formLoading, setFormLoading] = useState(false);

  // only gets updated everytime next is clicked
  const [formState, setFormState] = useState(formInitialState);

  useEffect(() => {
    window.addEventListener('beforeunload', askBeforeUnload);
    return () => {
      window.removeEventListener('beforeunload', askBeforeUnload);
    };
  }, []);

  useEffect(() => {
    startJobPosting();
  }, []);

  let currentStepIndex = jobPostingSteps.findIndex((step) => step.id === location.hash);
  currentStepIndex = currentStepIndex === -1 ? 0 : currentStepIndex;
  const currentStep = jobPostingSteps[currentStepIndex];

  const stepIndicator = jobPostingSteps.map((step, index) => (
    <Link
      to={{
        hash: step.id,
        state: location.state,
      }}
      key={step.id}
      className={`no-underline pa3 f6 ${
        step.id === currentStep.id
          ? `${styles.selected} fw7 black-80 disabled-link`
          : `${index < currentStepIndex ? 'blue fw6' : 'black-30 fw6 disabled-link'}`
      }`}
      data-cy={`${step.description.replace(/\s+/g, '-')}-link`}
    >
      <div className="tc ml1">{index + 1}.</div>
      <div className="tc">{step.description}</div>
    </Link>
  ));

  const onNext = (values: IJobPostingFormState, nextStep: string) => {
    setJobDescriptionChanged(
      !isEqual(formState.jobDescription.title)(values.jobDescription.title) ||
        !isEqual(formState.jobDescription.description)(values.jobDescription.description),
    );
    setFormState(values);

    history.push(nextStep, location.state);
  };

  const onPrevious = (previousStep?: string) => {
    if (previousStep) {
      setShowErrorCard(false);
      history.push(previousStep, location.state);
    }
  };

  const renderFormForStep = (step: IJobPostingStep) => {
    switch (step.id) {
      case '#job-description':
        return jobDescription || <JobDescriptionFields />;
      case '#skills':
        return jobSkills || <CreateJobSkillsFields />;
      case '#key-information':
        return keyInformation || <CreateKeyInformationFields />;
      case '#workplace-details':
        return workplaceDetails || <CreateWorkplaceDetailsFields />;
      case '#preview':
        return <Preview locationState={location.state} />;

      default:
        return <>Blank</>;
    }
  };

  return (
    <Form<IJobPostingFormState>
      onSubmit={(values) => {
        if (currentStep.next) {
          return onNext(values, currentStep.next);
        }
        return onSubmit(values);
      }}
      initialValues={initialState}
      initialValuesEqual={isEqual}
      render={({
        handleSubmit,
        hasValidationErrors,
        submitting,
        pristine,
        values,
        submitError,
        submitFailed,
        dirtySinceLastSubmit,
      }) => {
        // NOTE: have to calc ourselves since final-form calc `pristine` only for visible fields
        isFormPristine = pristine && isEqual(values, initialState);
        return (
          <section className="flex-auto db">
            <div className="flex justify-around bg-white">{stepIndicator}</div>

            <form onSubmit={handleSubmit}>
              <OnChange name={'jobDescription.title'}>
                {(title: IJobPostingFormState['jobDescription']['title']) => onTitleChange(title)}
              </OnChange>
              <JobPostingContext.Provider
                value={{
                  jobDescriptionChanged,
                  setFormLoading,
                  setShowErrorCard,
                  showErrorCard,
                  values: values as IJobPostingFormState,
                }}
              >
                <div className={`bg-black-05 mb3 pa4 pt4 ${styles.formBase}`}>{renderFormForStep(currentStep)}</div>
              </JobPostingContext.Provider>
              {submitFailed && hasValidationErrors && showErrorCard ? (
                <div className="pb3">
                  <ErrorCard message="Please amend the highlighted fields to continue" />
                </div>
              ) : null}
              {!dirtySinceLastSubmit && submitError && <div className="bg-red white pa3 mb3">{submitError}</div>}
              <div className="flex justify-end">
                {currentStep.previous && (
                  <button
                    data-cy="new-post-previous"
                    className={`${styles.button} pa3 db ba b--primary primary bg-white`}
                    onClick={() => onPrevious(currentStep.previous)}
                    type="button"
                  >
                    Back
                  </button>
                )}
                <button
                  data-cy="new-post-next"
                  className={`${styles.button} pa3 db ml3 b--primary white bg-primary`}
                  onClick={() => setShowErrorCard(true)}
                  disabled={submitting || formLoading}
                  type="submit"
                >
                  {currentStep.next ? 'Next' : 'Submit Job Post'}
                </button>
              </div>
            </form>
          </section>
        );
      }}
    />
  );
};
