import {CheckBox, TextInput} from '@govtechsg/mcf-mcfui';
import {mcf} from '@mcf/constants';
import React, {useContext, useEffect} from 'react';
import {Query} from 'react-apollo';
import {Field, useField} from 'react-final-form';
import {Condition} from '~/components/Core/Condition';
import {GetCommonSsicQuery, GetCommonSsicQueryVariables} from '~/graphql/__generated__/types';
import {GET_SSIC_LIST} from '~/graphql/jobs/jobs.query';
import {GetCompanyInfoQuery} from '~/services/employer';
import {composeValidators, defaultRequired} from '~/util/fieldValidations';
import {CompaniesLookup} from './CompaniesLookup';
import {JobPostingContext} from './JobPostingContext';
import styles from './ThirdPartyEmployerFields.scss';

export const ThirdPartyEmployerFields: React.FunctionComponent = () => {
  const {setFormLoading} = useContext(JobPostingContext);
  const thirdPartyEmployerEntityId = useField('jobDescription.thirdPartyEmployerEntityId', {
    subscription: {value: true},
  });
  return (
    <>
      <div className="flex pa2 w-100">
        <Field name="jobDescription.thirdPartyEmployer">
          {({input}) => {
            return (
              <CheckBox
                id="third-party-employer-checkbox"
                label="You're posting on behalf of another company"
                input={input}
              />
            );
          }}
        </Field>
      </div>
      <Condition when="jobDescription.thirdPartyEmployer" is={true}>
        <GetCompanyInfoQuery uen={thirdPartyEmployerEntityId.input.value}>
          {({data, error, loading}) => {
            if (loading) {
              setFormLoading(true);
            } else {
              setFormLoading(false);
            }
            return (
              <div className={`flex-column mb2 ${styles.expandAnimate}`}>
                <div className="pa2 w-100 pt4" data-cy="company-uen">
                  <Field
                    name="jobDescription.thirdPartyEmployerEntityId"
                    validate={composeValidators(defaultRequired, () => error && 'Please check the UEN')}
                    /**
                     * In order to rerender my Field with a new validation function when the `error` props is ready.
                     * We will need to update the Field key prop and force a rerender. For more info, refer to below link
                     * https://github.com/final-form/react-final-form#validate-value-any-allvalues-object-meta-fieldstate--any
                     */
                    key={`third-party-employer-entity-id${loading ? '-loading' : ''}`}
                  >
                    {({input, meta}) => {
                      return (
                        <CompaniesLookup
                          id="entity-id"
                          input={input}
                          meta={meta}
                          placeholder="Search..."
                          subtitle="Type to search for company name or enter full UEN"
                          label="Company Name / UEN"
                          inputLabel={data && `${data.name} (${data.uen})`}
                        />
                      );
                    }}
                  </Field>
                </div>
                <div className="flex">
                  <div className={`pa2 w-50 ${styles.readonly}`}>
                    <Field name="jobDescription.registrationType">
                      {({input, meta}) => {
                        useEffect(() => {
                          const registrationType = mcf.COMPANY_REGISTRATION_TYPES.find(
                            ({id}) => id === (data && data.registrationTypeId),
                          );
                          input.onChange(
                            registrationType ? registrationType.registrationType : data && data.registrationTypeId,
                          );
                        }, [data && data.registrationTypeId, error]);
                        return (
                          <TextInput
                            id="registration-type"
                            label="Type of Registration"
                            placeholder=""
                            input={input}
                            meta={meta}
                            readOnly
                          />
                        );
                      }}
                    </Field>
                  </div>
                  <div className={`pa2 w-50 ${styles.readonly}`}>
                    <Query<GetCommonSsicQuery, GetCommonSsicQueryVariables> query={GET_SSIC_LIST}>
                      {({data: ssicList, loading: ssicListLoading}) => (
                        <Field name="jobDescription.industry">
                          {({input, meta}) => {
                            useEffect(() => {
                              if (!ssicListLoading) {
                                const ssic =
                                  ssicList &&
                                  ssicList.common.ssicList.find(({code}) => code === (data && data.ssicCode));
                                input.onChange(ssic ? ssic.description : data && data.ssicCode);
                              }
                            }, [data && data.ssicCode, error, ssicListLoading]);
                            return (
                              <TextInput
                                id="industry"
                                label="Industry"
                                placeholder=""
                                input={input}
                                meta={meta}
                                readOnly
                              />
                            );
                          }}
                        </Field>
                      )}
                    </Query>
                  </div>
                </div>
                <hr className="b--black-10 bb mv4" />
              </div>
            );
          }}
        </GetCompanyInfoQuery>
      </Condition>
    </>
  );
};
