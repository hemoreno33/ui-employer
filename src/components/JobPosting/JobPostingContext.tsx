import {noop} from 'lodash';
import {createContext, Dispatch, SetStateAction} from 'react';
import {formInitialState, IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';

interface IJobPostingContext {
  jobDescriptionChanged: boolean;
  setFormLoading: Dispatch<SetStateAction<boolean>>;
  setShowErrorCard: Dispatch<SetStateAction<boolean>>;
  showErrorCard: boolean;
  values: IJobPostingFormState;
}

const defaultValue: IJobPostingContext = {
  jobDescriptionChanged: false,
  setFormLoading: noop,
  setShowErrorCard: noop,
  showErrorCard: false,
  values: formInitialState,
};

export const JobPostingContext = createContext<IJobPostingContext>(defaultValue);
