import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {JobPostingForm} from './JobPostingForm';
import {startJobPosting} from '~/flux/jobPosting/jobPosting.actions';

const mapDispatchToProps = (dispatch: any) => {
  return {
    startJobPosting: () => {
      dispatch(startJobPosting());
    },
  };
};

export const JobPostingFormContainer = connect(null, mapDispatchToProps)(withRouter(JobPostingForm));
