export enum Location {
  Local = 'local',
  Overseas = 'overseas',
}

export enum LocationType {
  None,
  SameAsCompany,
  Multiple,
}
