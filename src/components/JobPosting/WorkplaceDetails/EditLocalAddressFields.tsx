import {CheckBox, TextInput} from '@govtechsg/mcf-mcfui';
import React, {useContext} from 'react';
import {Field} from 'react-final-form';
import {WhenFieldChanges} from '~/components/Core/WhenFieldChanges';
import {JobPostingContext} from '~/components/JobPosting/JobPostingContext';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {validPostalCodeMemo} from '~/components/JobPosting/WorkplaceDetails/workplaceDetailsValidations';
import {FormLoader} from '~/components/Layouts/FormLoader';
import {composeValidators, defaultRequired, exactLength, validateIf} from '~/util/fieldValidations';
import {ILocalAddressFieldsProps} from './CreateLocalAddressFields';
import styles from './LocalAddressFields.scss';
import {LocationType} from './workplaceDetails.types';

export const EditLocalAddressFields: React.FunctionComponent<ILocalAddressFieldsProps> = ({
  companyAddress,
  isLoading,
}) => {
  const {
    setFormLoading,
    setShowErrorCard,
    values: {workplaceDetails},
  } = useContext(JobPostingContext);
  const readOnly = workplaceDetails.locationType !== LocationType.None;

  if (isLoading) {
    setFormLoading(true);
    return <FormLoader className="o-50" cardNumber={2} />;
  }
  setFormLoading(false);

  const sameAsCompanyAddressState: IJobPostingFormState['workplaceDetails'] = {
    ...workplaceDetails,
    block: (companyAddress && companyAddress.block) || '',
    building: (companyAddress && companyAddress.building) || '',
    postalCode: (companyAddress && companyAddress.postalCode) || '',
    street: (companyAddress && companyAddress.street) || '',
  };

  const multipleLocationState: IJobPostingFormState['workplaceDetails'] = {
    ...workplaceDetails,
    block: '',
    building: '',
    postalCode: '',
    street: '',
  };

  /**
   * don't have to set default value for address
   * 'same as company address' checkbox is not checked by default even if it was previously checked during submission
   * if the address values are blank then 'multiple locations' checkbox should be checked
   */

  return (
    <div data-cy="local-address-fields" className="flex-column pt3">
      <div className="mb3">
        <WhenFieldChanges
          field="workplaceDetails.locationType"
          becomes={LocationType.SameAsCompany}
          set="workplaceDetails"
          to={sameAsCompanyAddressState}
        />
        <Field name="workplaceDetails.locationType">
          {({input}) => {
            return (
              <CheckBox
                label={<span>The workplace address is the same as your company address</span>}
                input={{
                  onChange: () => {
                    input.onChange(
                      input.value === LocationType.SameAsCompany ? LocationType.None : LocationType.SameAsCompany,
                    );
                    setShowErrorCard(false);
                  },
                  value: input.value === LocationType.SameAsCompany,
                }}
                id="checkbox-sameLocation"
                disabled={companyAddress ? false : true}
              />
            );
          }}
        </Field>
      </div>
      <div>
        <WhenFieldChanges
          field="workplaceDetails.locationType"
          becomes={LocationType.Multiple}
          set="workplaceDetails"
          to={multipleLocationState}
        />
        <Field name="workplaceDetails.locationType">
          {({input}) => {
            return (
              <CheckBox
                label={<span>This position involves multiple workplace locations in Singapore</span>}
                input={{
                  onChange: () => {
                    input.onChange(input.value === LocationType.Multiple ? LocationType.None : LocationType.Multiple);
                    setShowErrorCard(false);
                  },
                  value: input.value === LocationType.Multiple,
                }}
                id="checkbox-multipleLocation"
              />
            );
          }}
        </Field>
      </div>
      <hr className="b--black-10 bb mv4" />
      <div className={`flex-column w-50 pa2 ${styles.readonly}`}>
        <Field
          name="workplaceDetails.postalCode"
          validate={validateIf(
            'workplaceDetails.locationType',
            (value) => value !== LocationType.Multiple,
            composeValidators(defaultRequired, exactLength(6, 'This should be a 6-digit number'), validPostalCodeMemo),
          )}
        >
          {({input, meta}) => {
            return (
              <TextInput
                id="postal-code"
                label="Postal Code"
                placeholder={readOnly ? '' : 'Enter postal code'}
                input={input}
                type="number"
                meta={meta}
                readOnly={readOnly}
              />
            );
          }}
        </Field>
      </div>
      <div className={`flex ${styles.readonly}`}>
        <div className="w-50 pa2">
          <Field
            name="workplaceDetails.block"
            validate={validateIf(
              'workplaceDetails.locationType',
              (value) => value !== LocationType.Multiple,
              defaultRequired,
            )}
          >
            {({input, meta}) => {
              return (
                <TextInput
                  id="block-house-num"
                  label="Block/House No."
                  placeholder={readOnly ? '' : 'Enter block/house number'}
                  input={input}
                  meta={meta}
                  readOnly={readOnly}
                />
              );
            }}
          </Field>
        </div>
        <div className="w-50 pa2">
          <Field
            name="workplaceDetails.street"
            validate={validateIf(
              'workplaceDetails.locationType',
              (value) => value !== LocationType.Multiple,
              defaultRequired,
            )}
          >
            {({input, meta}) => {
              return (
                <TextInput
                  id="street-name"
                  label="Street Name"
                  placeholder={readOnly ? '' : 'Enter street name'}
                  input={input}
                  meta={meta}
                  readOnly={readOnly}
                />
              );
            }}
          </Field>
        </div>
      </div>
      <div className={`flex-column w-50 pa2 ${styles.readonly}`}>
        <Field name="workplaceDetails.building">
          {({input, meta}) => {
            return (
              <TextInput
                id="building-name"
                label="Building Name (optional)"
                placeholder={readOnly ? '' : 'Enter building name'}
                input={input}
                meta={meta}
                readOnly={readOnly}
              />
            );
          }}
        </Field>
      </div>
    </div>
  );
};
