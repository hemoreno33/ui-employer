import {mount} from 'enzyme';
import {noop} from 'lodash';
import React from 'react';
import {Form} from 'react-final-form';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {JobPostingContext} from '~/components/JobPosting/JobPostingContext';
import {formInitialState, IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import CreateLocalAddressFields from '~/components/JobPosting/WorkplaceDetails/CreateLocalAddressFieldsContainer';
import {LocationType} from '~/components/JobPosting/WorkplaceDetails/workplaceDetails.types';
import {COMPANY_REQUEST_SUCCEEDED} from '~/flux/company/company.constants';
import {CompanyAddressType, ICompanyAddress} from '~/services/employer/company';

describe('JobPosting/CreateLocalAddressFields', () => {
  it('should autopopulate address fields when locationType is SameAsCompany', async () => {
    const initialState: IJobPostingFormState = {
      ...formInitialState,
      workplaceDetails: {
        ...formInitialState.workplaceDetails,
        locationType: LocationType.SameAsCompany,
      },
    };

    const mockAddress: ICompanyAddress = {
      block: 'the block',
      building: 'the building',
      postalCode: '111222',
      purpose: CompanyAddressType.OPERATING,
      street: 'the street',
    };

    const store = configureStore()({
      company: {companyInfo: {addresses: [mockAddress], uen: 'uen123'}, fetchStatus: COMPANY_REQUEST_SUCCEEDED},
    });
    const wrapper = mount(
      <Provider store={store}>
        <JobPostingContext.Provider
          value={{
            jobDescriptionChanged: false,
            setFormLoading: noop,
            setShowErrorCard: noop,
            showErrorCard: false,
            values: initialState,
          }}
        >
          <Form onSubmit={noop} render={() => <CreateLocalAddressFields />} />
        </JobPostingContext.Provider>
      </Provider>,
    );

    const localAddressFields = wrapper.find('[data-cy="local-address-fields"]');
    expect(localAddressFields.find('input#postal-code').props().value).toEqual(mockAddress.postalCode);
    expect(localAddressFields.find('input#street-name').props().value).toEqual(mockAddress.street);
    expect(localAddressFields.find('input#block-house-num').props().value).toEqual(mockAddress.block);
    expect(localAddressFields.find('input#building-name').props().value).toEqual(mockAddress.building);
  });

  it('should disable checkbox-sameLocation if there is no address', async () => {
    const store = configureStore()({
      company: {companyInfo: {addresses: [], uen: 'uen123'}, fetchStatus: COMPANY_REQUEST_SUCCEEDED},
    });
    const wrapper = mount(
      <Provider store={store}>
        <Form initialValues={formInitialState} onSubmit={noop} render={() => <CreateLocalAddressFields />} />
      </Provider>,
    );

    expect(wrapper.find('input#checkbox-sameLocation').props().disabled).toEqual(true);
  });
});
