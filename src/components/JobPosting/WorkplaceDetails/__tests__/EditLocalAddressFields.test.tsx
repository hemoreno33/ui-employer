import {mount} from 'enzyme';
import {noop} from 'lodash';
import React from 'react';
import {Form} from 'react-final-form';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {JobPostingContext} from '~/components/JobPosting/JobPostingContext';
import {formInitialState, IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import EditLocalAddressFields from '~/components/JobPosting/WorkplaceDetails/EditLocalAddressFieldsContainer';
import {LocationType} from '~/components/JobPosting/WorkplaceDetails/workplaceDetails.types';
import {COMPANY_REQUEST_SUCCEEDED} from '~/flux/company/company.constants';

describe('JobPosting/EditLocalAddressFields', () => {
  it('should disable checkbox-sameLocation if there is no address', async () => {
    const initialState: IJobPostingFormState = {
      ...formInitialState,
      workplaceDetails: {
        ...formInitialState.workplaceDetails,
        locationType: LocationType.SameAsCompany,
      },
    };
    const store = configureStore()({
      company: {companyInfo: {addresses: [], uen: 'uen123'}, fetchStatus: COMPANY_REQUEST_SUCCEEDED},
    });
    const wrapper = mount(
      <Provider store={store}>
        <JobPostingContext.Provider
          value={{
            jobDescriptionChanged: false,
            setFormLoading: noop,
            setShowErrorCard: noop,
            showErrorCard: false,
            values: initialState,
          }}
        >
          <Form initialValues={formInitialState} onSubmit={noop} render={() => <EditLocalAddressFields />} />
        </JobPostingContext.Provider>
      </Provider>,
    );

    expect(wrapper.find('input#checkbox-sameLocation').props().disabled).toEqual(true);
  });
});
