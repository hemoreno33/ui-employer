import {connect} from 'react-redux';
import {COMPANY_FETCH_REQUESTED} from '~/flux/company/company.constants';
import {IAppState} from '~/flux/index';
import {CreateLocalAddressFields} from './CreateLocalAddressFields';
import {getCompanyOperatingAddress} from '~/flux/company';

const mapStateToProps = ({company}: IAppState) => ({
  companyAddress: getCompanyOperatingAddress(company),
  isLoading: !company || !company.fetchStatus || company.fetchStatus === COMPANY_FETCH_REQUESTED,
});

export default connect(mapStateToProps)(CreateLocalAddressFields);
