import {mcf} from '@mcf/constants';
import {memoize} from 'lodash';
import {flatten, includes} from 'lodash/fp';
import {getAddressByPostalCode} from '~/services/oneMap/oneMap';
import {setPromiseTimeout} from '~/util/timeout';

const ONE_MAP_VALIDATION_TIMEOUT = 2000;

export const validPostalCodeSector = (value: string) => {
  const validSectors = flatten(mcf.DISTRICTS.map((district) => district.sectors));
  const sector = value.substring(0, 2);
  return includes(sector)(validSectors);
};

export const validPostalCode = async (value: string) => {
  const errorMessage = 'Please check your postal code';
  try {
    const {found} = await setPromiseTimeout(getAddressByPostalCode(value), ONE_MAP_VALIDATION_TIMEOUT);
    return found > 0 ? undefined : errorMessage;
  } catch {
    return validPostalCodeSector(value) ? undefined : errorMessage;
  }
};

export const validPostalCodeMemo = memoize(validPostalCode);
