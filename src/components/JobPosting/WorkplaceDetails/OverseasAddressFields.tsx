import {TextInput, TypeAhead} from '@govtechsg/mcf-mcfui';
import React, {useContext} from 'react';
import {Query} from 'react-apollo';
import {Field, useFormState} from 'react-final-form';
import {JobPostingContext} from '~/components/JobPosting/JobPostingContext';
import {FormLoader} from '~/components/Layouts/FormLoader';
import {GetCommonCountryQuery, GetCommonCountryQueryVariables} from '~/graphql/__generated__/types';
import {GET_COUNTRY_LIST} from '~/graphql/jobs/jobs.query';
import {composeValidators, defaultRequired, maxLength} from '~/util/fieldValidations';

export const MAX_ADDRESS_LENGTH = 60;

export const OverseasAddressFields: React.FunctionComponent = () => {
  const {setFormLoading} = useContext(JobPostingContext);
  const {validating} = useFormState({subscription: {validating: true}});
  return (
    <>
      <Query<GetCommonCountryQuery, GetCommonCountryQueryVariables> query={GET_COUNTRY_LIST}>
        {({loading, error, data}) => {
          if (loading || error || !data) {
            setFormLoading(true);
            return <FormLoader className="o-50" />;
          }
          setFormLoading(false);
          /**
           * Will wait for validations to finish before mounting OverseasAddressFields since the validation results
           * from OverseasAddressFields is being overridden when the async validator in LocalAddressFields resolves.
           */
          return validating ? null : (
            <div className="flex-column">
              <hr className="b--black-10 bb mt2 mb3" />
              <div className="w-50 pa2" data-cy="workplace-country">
                <Field name="workplaceDetails.overseasCountry" validate={defaultRequired}>
                  {({input, meta}) => {
                    return (
                      <TypeAhead
                        id="country"
                        label="Country"
                        placeholder="Search..."
                        input={{...input, className: ''}}
                        meta={meta}
                        data={data!.common.countryList}
                        targetKey="description"
                        valueKey="description"
                      />
                    );
                  }}
                </Field>
              </div>
              <div className="flex">
                <div className="w-50 pa2" data-cy="workplace-foreign-address1">
                  <Field
                    name="workplaceDetails.foreignAddress1"
                    validate={composeValidators(
                      defaultRequired,
                      maxLength(
                        MAX_ADDRESS_LENGTH,
                        `Please keep within ${MAX_ADDRESS_LENGTH.toLocaleString()} characters`,
                      ),
                    )}
                  >
                    {({input, meta}) => {
                      return (
                        <TextInput
                          id="foreignAddress1"
                          label="Overseas Address 1"
                          placeholder="Enter Address 1"
                          input={input}
                          meta={meta}
                        />
                      );
                    }}
                  </Field>
                </div>
                <div className="w-50 pa2" data-cy="workplace-foreign-address2">
                  <Field
                    name="workplaceDetails.foreignAddress2"
                    validate={maxLength(
                      MAX_ADDRESS_LENGTH,
                      `Please keep within ${MAX_ADDRESS_LENGTH.toLocaleString()} characters`,
                    )}
                  >
                    {({input, meta}) => {
                      return (
                        <TextInput
                          id="foreignAddress2"
                          label="Overseas Address 2 (optional)"
                          placeholder="Enter Address 2"
                          input={input}
                          meta={meta}
                        />
                      );
                    }}
                  </Field>
                </div>
              </div>
            </div>
          );
        }}
      </Query>
    </>
  );
};
