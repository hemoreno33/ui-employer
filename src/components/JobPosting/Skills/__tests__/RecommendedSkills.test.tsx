import {mount, ReactWrapper} from 'enzyme';
import {noop} from 'lodash';
import React from 'react';
import {act} from 'react-dom/test-utils';
import {Form} from 'react-final-form';
import {JobPostingContext} from '~/components/JobPosting/JobPostingContext';
import {formInitialState, IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {JobPostingSkillsProvider} from '~/components/JobPosting/Skills/JobPostingSkillsContext';
import * as recommendedSkillsServices from '~/services/skills/fetchRecommendedSkills';
import {nextTick} from '~/testUtil/enzyme';
import {RecommendedSkills} from '../RecommendedSkills';
import {SkillList} from '../SkillList';

describe('RecommendedSkills', () => {
  const props = {
    header: 'skills header',
    input: {
      onChange: jest.fn(),
    },
    selected: true,
  };

  const jobPostingValues: IJobPostingFormState = {
    ...formInitialState,
    jobDescription: {
      ...formInitialState.jobDescription,
      description: 'Job Description',
      title: 'Job Title',
    },
  };

  const recommendedSkillsMock = [{id: 111, skill: 'recommendedSkillsMock', selected: true}];

  const skillsInitialState = {
    addedSkills: [],
    foundSkills: [],
    recommendedSkills: [],
  };

  const fetchRecommendedSkillsMock = jest
    .spyOn(recommendedSkillsServices, 'fetchRecommendedSkills')
    .mockImplementation(() => Promise.resolve(recommendedSkillsMock));

  afterEach(() => {
    props.input.onChange.mockClear();
    fetchRecommendedSkillsMock.mockClear();
  });

  it('should call fetchRecommendedSkillsRequested on mount and set recommended skills as selected if selected prop is true', async () => {
    let wrapper!: ReactWrapper;
    await act(async () => {
      wrapper = mount(
        <Form
          initialValues={jobPostingValues}
          onSubmit={noop}
          render={() => (
            <JobPostingContext.Provider
              value={{
                jobDescriptionChanged: true,
                setFormLoading: noop,
                setShowErrorCard: noop,
                showErrorCard: false,
                values: jobPostingValues,
              }}
            >
              <JobPostingSkillsProvider initialState={skillsInitialState}>
                <RecommendedSkills {...props} selected={true} />
              </JobPostingSkillsProvider>
            </JobPostingContext.Provider>
          )}
        />,
      );
    });

    expect(fetchRecommendedSkillsMock).toBeCalledWith(
      jobPostingValues.jobDescription.title,
      jobPostingValues.jobDescription.description,
    );

    wrapper.update();
    const skillList = wrapper.find(SkillList);
    expect(skillList).toHaveLength(1);
    const recommendedSkills = skillList.props().list;
    recommendedSkills.forEach((skill) => expect(skill.selected).toEqual(true));
  });

  it('should call fetchRecommendedSkillsRequested on mount and set recommended skills as unselected if selected prop is false', async () => {
    let wrapper!: ReactWrapper;
    await act(async () => {
      wrapper = mount(
        <Form
          initialValues={jobPostingValues}
          onSubmit={noop}
          render={() => (
            <JobPostingContext.Provider
              value={{
                jobDescriptionChanged: true,
                setFormLoading: noop,
                setShowErrorCard: noop,
                showErrorCard: false,
                values: jobPostingValues,
              }}
            >
              <JobPostingSkillsProvider initialState={skillsInitialState}>
                <RecommendedSkills {...props} selected={false} />
              </JobPostingSkillsProvider>
            </JobPostingContext.Provider>
          )}
        />,
      );
    });

    expect(fetchRecommendedSkillsMock).toBeCalledWith(
      jobPostingValues.jobDescription.title,
      jobPostingValues.jobDescription.description,
    );

    wrapper.update();
    const skillList = wrapper.find(SkillList);
    expect(skillList).toHaveLength(1);
    const recommendedSkills = skillList.props().list;
    recommendedSkills.forEach((skill) => expect(skill.selected).toEqual(false));
  });

  it('should not call fetchRecommendedSkillsRequested on mount if jobDescriptionChanged is false', async () => {
    await act(async () => {
      mount(
        <Form
          initialValues={jobPostingValues}
          onSubmit={noop}
          render={() => (
            <JobPostingContext.Provider
              value={{
                jobDescriptionChanged: false,
                setFormLoading: noop,
                setShowErrorCard: noop,
                showErrorCard: false,
                values: jobPostingValues,
              }}
            >
              <JobPostingSkillsProvider initialState={skillsInitialState}>
                <RecommendedSkills {...props} />
              </JobPostingSkillsProvider>
            </JobPostingContext.Provider>
          )}
        />,
      );
    });
    expect(fetchRecommendedSkillsMock).not.toHaveBeenCalled();
  });

  it('should render loader then display recommended skills', async () => {
    const wrapper = mount(
      <Form
        initialValues={jobPostingValues}
        onSubmit={noop}
        render={() => (
          <JobPostingContext.Provider
            value={{
              jobDescriptionChanged: true,
              setFormLoading: noop,
              setShowErrorCard: noop,
              showErrorCard: false,
              values: jobPostingValues,
            }}
          >
            <JobPostingSkillsProvider initialState={skillsInitialState}>
              <RecommendedSkills {...props} />
            </JobPostingSkillsProvider>
          </JobPostingContext.Provider>
        )}
      />,
    );
    wrapper.update();
    expect(wrapper.find('[data-cy="form-loader"]')).toHaveLength(1);
    await nextTick(wrapper);

    expect(wrapper.find('[data-cy="form-loader"]')).toHaveLength(0);
    expect(wrapper.find(SkillList)).toHaveLength(1);
    expect(wrapper.find('[data-cy="recommended-skills"]')).toMatchSnapshot();
  });

  it('should render placeholder when recommended skills is empty', async () => {
    let wrapper!: ReactWrapper;
    await act(async () => {
      wrapper = mount(
        <Form
          initialValues={jobPostingValues}
          onSubmit={noop}
          render={() => (
            <JobPostingContext.Provider
              value={{
                jobDescriptionChanged: false,
                setFormLoading: noop,
                setShowErrorCard: noop,
                showErrorCard: false,
                values: jobPostingValues,
              }}
            >
              <JobPostingSkillsProvider initialState={skillsInitialState}>
                <RecommendedSkills {...props} />
              </JobPostingSkillsProvider>
            </JobPostingContext.Provider>
          )}
        />,
      );
    });
    wrapper.update();

    expect(wrapper.find(SkillList)).toHaveLength(0);
    expect(wrapper.find('[data-cy="recommended-skills-empty"]')).toHaveLength(1);
  });
});
