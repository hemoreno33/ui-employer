import {mount} from 'enzyme';
import React from 'react';
import {SkillButton} from '../SkillButton';

describe('Skills/SkillButton', () => {
  it('should render SkillButton correctly', () => {
    const skillButtonProps = {
      label: 'Leadership',
      selected: false,
    };

    const component = mount(<SkillButton {...skillButtonProps} />);

    expect(component).toMatchSnapshot();
  });
});
