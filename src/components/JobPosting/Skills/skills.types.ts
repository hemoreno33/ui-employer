export interface IJobSkill {
  id: number;
  skill: string;
}

export interface IPillSkill extends IJobSkill {
  selected: boolean;
}

export interface ISkillsByJobResponse {
  jobTitle: string;
  skills: IJobSkill[];
}

export interface IWccSkillsByJobResponse {
  skills: IJobSkill[];
}
