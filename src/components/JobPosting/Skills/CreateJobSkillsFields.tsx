import React from 'react';
import {Field, useField} from 'react-final-form';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {AddedSkills} from '~/components/JobPosting/Skills/AddedSkills';
import {IJobPostingSkillsState, JobPostingSkillsProvider} from '~/components/JobPosting/Skills/JobPostingSkillsContext';
import {RecommendedSkills} from '~/components/JobPosting/Skills/RecommendedSkills';
import {SkillsErrorBorder} from '~/components/JobPosting/Skills/SkillsErrorBorder';
import {SkillsSearchSectionContainer} from '~/components/JobPosting/Skills/SkillsSearchSectionContainer';
import {
  greaterThanMaxSkillsSelected,
  lessThanMinSkillsSelected,
} from '~/components/JobPosting/Skills/skillsValidations';
import {composeValidators} from '~/util/fieldValidations';

export const CreateJobSkillsFields: React.FunctionComponent = () => {
  const {
    input: {value: jobPostingSkills},
    meta,
  } = useField<IJobPostingFormState['jobPostingSkills']>('jobPostingSkills', {
    subscription: {value: true, error: true, submitFailed: true},
    validate: composeValidators(lessThanMinSkillsSelected, greaterThanMaxSkillsSelected),
  });

  const initialState: IJobPostingSkillsState = {
    addedSkills: jobPostingSkills.addedSkills || [],
    foundSkills: [],
    recommendedSkills: jobPostingSkills.recommendedSkills || [],
  };
  return (
    <JobPostingSkillsProvider initialState={initialState}>
      <section className="mb4">
        <div className="f4-5 fw6 black-60 db lh-copy">
          List the required skills so we can match candidates to your job
        </div>
        <span className="mt2 mb3 f5-5 fw4 black-60 db lh-copy">
          Check the pre-selected skills and add other relevant skills. Include at least 10 skills and no more than 20
          skills.
        </span>
        <SkillsErrorBorder error={meta.submitFailed ? meta.error : ''}>
          <Field name="jobPostingSkills.recommendedSkills">
            {({input}) => <RecommendedSkills input={input} selected={true} header="Skills based on job description" />}
          </Field>
          <Field name="jobPostingSkills.addedSkills">
            {({input}) => <AddedSkills input={input} header="Other skills you have added" />}
          </Field>
        </SkillsErrorBorder>
        <SkillsSearchSectionContainer />
      </section>
    </JobPostingSkillsProvider>
  );
};
