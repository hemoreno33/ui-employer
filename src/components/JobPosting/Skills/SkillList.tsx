import React from 'react';
import {IPillSkill} from '~/components/JobPosting/Skills/skills.types';
import {SkillButton} from './SkillButton';
export interface ISkillListProps {
  list: IPillSkill[];
  onClick: (skill: IPillSkill) => void;
}

export const SkillList: React.FunctionComponent<ISkillListProps> = ({list, onClick}) => {
  const skills = list.map((skillItem) => {
    return (
      <SkillButton
        key={skillItem.id}
        label={skillItem.skill}
        selected={skillItem.selected}
        onClick={() => {
          onClick(skillItem);
        }}
      />
    );
  });
  return <div className="center mv3">{skills}</div>;
};
