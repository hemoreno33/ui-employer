import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';

export const MIN_SELECTED_SKILLS = 10;
export const MAX_SELECTED_SKILLS = 20;

export enum JobPostingSkillsErrors {
  lessThanMinSkillsSelected = 'lessThanMinSkillsSelected',
  greaterThanMaxSkillsSelected = 'greaterThanMaxSkillsSelected',
}

export const lessThanMinSkillsSelected = ({addedSkills, recommendedSkills}: IJobPostingFormState['jobPostingSkills']) =>
  [...addedSkills, ...recommendedSkills].filter((skill) => skill.selected).length < MIN_SELECTED_SKILLS
    ? JobPostingSkillsErrors.lessThanMinSkillsSelected
    : undefined;

export const greaterThanMaxSkillsSelected = ({
  addedSkills,
  recommendedSkills,
}: IJobPostingFormState['jobPostingSkills']) =>
  [...addedSkills, ...recommendedSkills].filter((skill) => skill.selected).length > MAX_SELECTED_SKILLS
    ? JobPostingSkillsErrors.greaterThanMaxSkillsSelected
    : undefined;
