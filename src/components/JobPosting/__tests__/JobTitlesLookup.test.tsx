import {mount} from 'enzyme';
import React from 'react';
import {jobTitlesMock} from '~/flux/jobTitles/__mocks__/jobTitles.mock';
import {JobTitlesLookup} from '../JobTitlesLookup';

describe('Autocomplete/Autocomplete', () => {
  it('should render Autocomplete correctly', () => {
    const autocompleteProps = {
      fetchJobTitlesRequested: jest.fn(),
      jobTitles: jobTitlesMock.results,
      onChange: jest.fn(),
      onInputChange: jest.fn(),
    };
    const component = mount(<JobTitlesLookup {...autocompleteProps} />);

    expect(component).toMatchSnapshot();
  });
});
