import {MockedProvider} from '@apollo/react-testing';
import {mcf} from '@mcf/constants';
import {mount} from 'enzyme';
import {noop} from 'lodash';
import React from 'react';
import {Form} from 'react-final-form';
import {Provider} from 'react-redux';
import {Link, MemoryRouter as Router} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {IJobPostingFormState, jobPostingSteps} from '~/components/JobPosting/JobPostingForm';
import {Location, LocationType} from '~/components/JobPosting/WorkplaceDetails/workplaceDetails.types';
import {GET_SSOC_LIST} from '~/graphql/jobs/jobs.query';
import {nextTick} from '~/testUtil/enzyme';
import {Preview} from '../Preview';

describe('JobPosting/Preview', () => {
  it('should render all edit links', async () => {
    const getSsocQueryMock = [
      {
        request: {
          query: GET_SSOC_LIST,
        },
        result: {
          data: {
            common: {
              ssocList: [
                {
                  ssoc: 1,
                  ssocTitle: 'some',
                },
              ],
            },
          },
        },
      },
    ];
    const initialValues: IJobPostingFormState = {
      jobDescription: {
        description: '',
        industry: '',
        occupation: 123,
        registrationType: '',
        thirdPartyEmployer: false,
        thirdPartyEmployerEntityId: '',
        title: '',
      },
      jobPostingSkills: {
        addedSkills: [],
        recommendedSkills: [],
      },
      keyInformation: {
        employmentType: [7],
        fieldOfStudy: '0521',
        jobCategories: [1, 4, 7, 8],
        jobPostDuration: 14,
        maximumSalary: 1212,
        minimumSalary: 12,
        minimumYearsExperience: 12,
        numberOfVacancies: 12,
        positionLevel: 1,
        qualification: '91',
        schemes: [
          {id: mcf.SCHEME_ID.P_MAX, selected: true},
          {id: mcf.SCHEME_ID.CAREER_TRIAL, selected: true},
        ],
      },
      workplaceDetails: {
        block: '',
        building: '',
        foreignAddress1: '',
        foreignAddress2: '',
        location: Location.Local,
        locationType: LocationType.None,
        overseasCountry: '',
        postalCode: '',
        street: '',
      },
    };

    const store = configureStore()({
      company: {
        companyInfo: {
          uen: '',
        },
      },
    });
    const component = mount(
      <MockedProvider mocks={getSsocQueryMock} addTypename={false}>
        <Provider store={store}>
          <Router>
            <Form initialValues={initialValues} onSubmit={noop} render={() => <Preview locationState={''} />} />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(component);

    const links = component.find(Link);

    expect(links).toHaveLength(jobPostingSteps.length - 1);
    links.forEach((link, index) => {
      expect(link.prop('to')).toStrictEqual({hash: jobPostingSteps[index].id, state: ''});
    });
  });
});
