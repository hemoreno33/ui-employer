import {ApolloGraphQLInteraction, Interaction, Matchers} from '@pact-foundation/pact';
import {mount} from 'enzyme';
import {noop} from 'lodash';
import React from 'react';
import {ApolloProvider} from 'react-apollo';
import {act} from 'react-dom/test-utils';
import {Form} from 'react-final-form';
import {PactBuilder, transformArrayToEachLikeMatcher} from '~/__mocks__/pact';
import {formInitialState, IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {GET_SSIC_LIST} from '~/graphql/jobs/jobs.query';
import {CompanyAddressType, ICompanyInfoWithAddresses} from '~/services/employer/company';
import {ThirdPartyEmployerFields} from '../ThirdPartyEmployerFields';
// temporarily suppress the following error, which will be fixed when upgrading Graphql package 14.5.8
// error TS7016: Could not find a declaration file for module 'graphql'.
// @ts-ignore
import {print} from 'graphql';

const v2ApiMock = jest.fn();
jest.mock('~/config', () => ({
  config: {
    url: {
      apiJob: {
        get v2() {
          return v2ApiMock();
        },
      },
    },
  },
}));

describe('JobPosting/ThirdPartyEmployerFields', () => {
  const thirdPartyEmployerEntityId = 'T08GB0046G';

  let profilePactBuilder: PactBuilder;
  let jobPactBuilder: PactBuilder;
  beforeAll(async () => {
    profilePactBuilder = new PactBuilder('api-profile');
    jobPactBuilder = new PactBuilder('api-job');
    await profilePactBuilder.setup();
    await jobPactBuilder.setup();
    v2ApiMock.mockImplementation(() => `http://${jobPactBuilder.host}:${jobPactBuilder.port}/v2`);
  });
  beforeEach(async () => {
    const queryMockResult = {
      data: {
        common: {
          ssicList: Matchers.eachLike({
            code: '85494',
            description: 'Academic tutoring services (eg tuition centres, private tutoring services)',
          }),
        },
      },
    };
    const graphqlQuery = new ApolloGraphQLInteraction()
      .uponReceiving('getSSICList')
      .withQuery(print(GET_SSIC_LIST))
      .withOperation('getCommonSsic')
      .withRequest({
        method: 'POST',
        path: '/profile',
      })
      .willRespondWith({
        body: queryMockResult,
        status: 200,
      });
    await profilePactBuilder.provider.addInteraction(graphqlQuery);
    const companyInfoMock: ICompanyInfoWithAddresses = {
      addresses: [
        {
          block: '9',
          street: 'WOODLANDS AVENUE 9',
          postalCode: '12345',
          purpose: CompanyAddressType.OPERATING,
        },
      ],
      description:
        'About Republic Polytechnic The first educational institution in Singapore to leverage the Problem-based Learning approach for all its diploma programmes, Republic Polytechnic (RP) has seven schools and one academic centre offering forty-two diplomas in Applied Science, Engineering, Management and Communication, Events and Hospitality, Infocomm, Sports, Health & Leisure, and Technology for the Arts.',
      name: 'company name',
      registrationTypeId: 1,
      ssicCode: '85494',
      uen: thirdPartyEmployerEntityId,
    };
    const getCompanyInfoQuery = new Interaction()
      .uponReceiving('getCompanyInfo')
      .withRequest({
        method: 'GET',
        path: `/v2/companies/${thirdPartyEmployerEntityId}`,
      })
      .willRespondWith({
        body: Matchers.like(transformArrayToEachLikeMatcher(companyInfoMock)),
        headers: {
          'access-control-allow-credentials': 'true',
        },
        status: 200,
      });
    await jobPactBuilder.provider.addInteraction(getCompanyInfoQuery);
  });
  afterAll(async () => {
    await profilePactBuilder.provider.finalize();
    await jobPactBuilder.provider.finalize();
  });

  it('should render ThirdPartyEmployerFields correctly', async () => {
    const initialState: IJobPostingFormState = {
      ...formInitialState,
      jobDescription: {thirdPartyEmployer: true, thirdPartyEmployerEntityId},
    };

    mount(
      <ApolloProvider client={profilePactBuilder.getApolloClient()}>
        <Form initialValues={initialState} onSubmit={noop} render={() => <ThirdPartyEmployerFields />} />
      </ApolloProvider>,
    );

    await act(async () => {
      await jobPactBuilder.verifyInteractions();
    });
    await profilePactBuilder.verifyInteractions();
  });
});
