import {get} from 'lodash/fp';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {searchableFieldRequired, validateIf} from '~/util/fieldValidations';

/*
 * https://www.singstat.gov.sg/-/media/files/standards_and_classifications/educational_classification/ssec2015-report.pdf
 * Not required if eduation level is GCE A levels and below (< 42)
 * Double-digits codes '0n', where [5, 6, 7, 8, 9] ∋ n, are not valid codes
 */
export const fosRequired = (ssecEqaCode: string) => !new RegExp('^([02][1-4]|1[1-3]|3[1-5|9]|41)$').test(ssecEqaCode);

export const requiredIfQualificationIsAboveALevels = validateIf(
  'keyInformation.qualification',
  fosRequired,
  searchableFieldRequired,
);

export const minNotMoreThanMax = (value: IJobPostingFormState['keyInformation']) => {
  const minimumSalary = get('minimumSalary', value);
  const maximumSalary = get('maximumSalary', value);
  return minimumSalary && maximumSalary && minimumSalary > maximumSalary
    ? {
        maximumSalary: ' ', // need a non-empty value here for red border
        minMaxRange: 'Please check that the Min is not greater than the Max',
        minimumSalary: undefined,
      }
    : undefined;
};
