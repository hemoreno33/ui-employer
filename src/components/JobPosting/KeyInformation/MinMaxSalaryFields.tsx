import {TextInput} from '@govtechsg/mcf-mcfui';
import {gte} from 'lodash';
import {get} from 'lodash/fp';
import React from 'react';
import {Field} from 'react-final-form';
import {minNotMoreThanMax} from '~/components/JobPosting/KeyInformation/keyInformationValidations';
import {composeValidators, defaultRequired, greaterThan, lessThan} from '~/util/fieldValidations';

const dashSize = 30; // size of dash container
const pa2 = 0.5; // padding used around all Field in this form, value comes from tachyons
const dashStyle = {flexBasis: `${dashSize}px`};
const minMaxStyle = {flexBasis: `calc(50% - ${dashSize / 2}px - ${pa2 / 2}rem)`};
const errorStyle = {
  color: 'var(--red)',
  display: 'block',
  fontSize: 'var(--font-size-5-5)',
  fontWeight: 400,
};

export const careerSupportThresholdMin = 3600;
export const careerSupportThresholdMax = 4000;

const careerSupportThresholdBelowMinMessage = (
  <>
    Since this job pays <span className="fw6">below $3600</span>, it is not eligible for the Career Support Programme
    (see below).
  </>
);
const careerSupportThresholdMinMessage = (
  <>
    If this job pays <span className="fw6">at least $3600</span> and your company is an SME, it may be eligible for the
    Career Support Programme (see below).
  </>
);
const careerSupportThresholdMaxMessage = (
  <>
    If this job pays <span className="fw6">at least $4000</span>, it may be eligible for the Career Support Programme.
  </>
);

export const MinMaxSalaryFields: React.FunctionComponent = () => {
  return (
    <>
      <div className="pt2">
        <span data-cy="job-gross-month-label" className="f5 fw6 black-60 pl1">
          Gross Monthly Salary Range (SGD)
        </span>
      </div>
      <div className="flex">
        <div className="w-50">
          <Field name="keyInformation" validate={minNotMoreThanMax}>
            {(renderProps) => (
              <>
                <div className="flex">
                  <span className="f4 black-60 db pr1 pt4">$</span>
                  <div style={minMaxStyle}>
                    <Field
                      name="keyInformation.minimumSalary"
                      parse={(value) => (value === '' ? value : Number(value))}
                      validate={composeValidators(
                        defaultRequired,
                        greaterThan(0),
                        lessThan(100000000, 'This number should not be more than 8 digits'),
                      )}
                    >
                      {({input, meta}) => {
                        return (
                          <TextInput
                            id="job-minimum-salary"
                            label=""
                            placeholder="Min"
                            input={input}
                            meta={meta}
                            type="number"
                          />
                        );
                      }}
                    </Field>
                  </div>
                  <div className="tc pt4" style={dashStyle}>
                    -
                  </div>
                  <div style={minMaxStyle}>
                    <Field
                      name="keyInformation.maximumSalary"
                      parse={(value) => (value === '' ? value : Number(value))}
                      validate={composeValidators(
                        defaultRequired,
                        greaterThan(0),
                        lessThan(100000000, 'This number should not be more than 8 digits'),
                      )}
                    >
                      {({input, meta}) => {
                        return (
                          <TextInput
                            id="job-maximum-salary"
                            label=""
                            placeholder="Max"
                            input={input}
                            meta={meta}
                            type="number"
                          />
                        );
                      }}
                    </Field>
                  </div>
                </div>
                <div id="job-salary-error" style={errorStyle}>
                  {get('meta.error.minMaxRange', renderProps)}
                </div>
              </>
            )}
          </Field>
        </div>
        <div className="pa2 pl0 w-50 flex">
          <Field name="keyInformation.maximumSalary" subscription={{value: true}}>
            {({input: {value}}) => {
              if (value) {
                const message = gte(value, careerSupportThresholdMax)
                  ? careerSupportThresholdMaxMessage
                  : gte(value, careerSupportThresholdMin)
                  ? careerSupportThresholdMinMessage
                  : careerSupportThresholdBelowMinMessage;

                return <div className="ml4 bg-light-gray pa3 align-center v-mid lh-copy black-80">{message}</div>;
              }
              return null;
            }}
          </Field>
        </div>
      </div>
    </>
  );
};
