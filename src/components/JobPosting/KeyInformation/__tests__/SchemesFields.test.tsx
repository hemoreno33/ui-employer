import {mcf} from '@mcf/constants';
import {mount} from 'enzyme';
import {noop} from 'lodash';
import React from 'react';
import {act} from 'react-dom/test-utils';
import {Form} from 'react-final-form';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import * as getCompanySchemes from '~/services/schemes/getCompanySchemes';
import {SchemesFields} from '../SchemesFields';

const getCompanySchemesMock = jest.spyOn(getCompanySchemes, 'getCompanySchemes');

const schemesUen = 'T08GB0046G';
const schemesMock: getCompanySchemes.ICompanyScheme[] = [
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.CAREER_SUPPORT,
      scheme: 'Career Support Program (CSP)',
    },
    startDate: '2019-01-01',
  },
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.P_MAX,
      scheme: 'P-Max',
    },
    startDate: '2019-01-01',
  },
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.PCP,
      scheme: 'Professional Conversion Programme (PCP)',
    },
    startDate: '2019-01-01',
    subScheme: {
      id: 1,
      programme: 'PCP for Public Transport Bus (Service Controller/ Assistant Service Controller)',
      schemeId: mcf.SCHEME_ID.PCP,
    },
  },
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.PCP,
      scheme: 'Professional Conversion Programme (PCP)',
    },
    startDate: '2019-01-01',
    subScheme: {
      id: 2,
      programme: 'PCP for Public Transport Rail Sector (Executive Engineer/ Assistant Engineer)',
      schemeId: mcf.SCHEME_ID.PCP,
    },
  },
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.PCP,
      scheme: 'Professional Conversion Programme (PCP)',
    },
    startDate: '2019-01-01',
    subScheme: {
      id: 3,
      programme: 'PCP for Public Transport Rail Sector (Station Manager/ Assistant Station Manager)',
      schemeId: mcf.SCHEME_ID.PCP,
    },
  },
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.PCP,
      scheme: 'Professional Conversion Programme (PCP)',
    },
    startDate: '2019-01-01',
    subScheme: {
      id: 4,
      programme: 'PCP for Regional Operations Manager',
      schemeId: mcf.SCHEME_ID.PCP,
    },
  },
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.CAREER_TRIAL,
      scheme: 'Career Trial',
    },
    startDate: '2019-01-01',
  },
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.SG_UNITED_TRAINEESHIPS,
      scheme: 'SGUnited Traineeships',
    },
    startDate: '2019-01-01',
  },
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.SG_UNITED_MID_CAREER_PATHWAYS,
      scheme: 'SGUnited Mid-Career Pathways Programme',
    },
    startDate: '2019-01-01',
  },
];

describe('SchemeFields', () => {
  it('should render all schemes and not disable Career Support Program if maximum salary is > 3600', async () => {
    getCompanySchemesMock.mockImplementation(() => Promise.resolve(schemesMock));
    const initialValues: Partial<IJobPostingFormState> = {
      keyInformation: {
        jobCategories: [],
        maximumSalary: 3600,
        schemes: [],
      },
    };

    let wrapper: any;
    await act(async () => {
      wrapper = mount(
        <Form
          onSubmit={noop}
          initialValues={initialValues}
          render={() => <SchemesFields isLoading={false} uen={schemesUen} />}
        />,
      );
    });

    wrapper.update();

    expect(getCompanySchemesMock).toBeCalledWith(schemesUen);
    schemesMock.forEach((schemeMock) => {
      expect(wrapper.find(`input#schemes-checkbox-${schemeMock.scheme.id}`)).toHaveLength(1);
      expect(wrapper.find(`input#schemes-checkbox-${schemeMock.scheme.id}`).props().disabled).toEqual(false);
    });
  });

  it('should render all schemes and not disable Career Support Program if maximum salary is = 3600', async () => {
    getCompanySchemesMock.mockImplementation(() => Promise.resolve(schemesMock));
    const initialValues: Partial<IJobPostingFormState> = {
      keyInformation: {
        jobCategories: [],
        maximumSalary: 3600,
        schemes: [],
      },
    };

    let wrapper: any;
    await act(async () => {
      wrapper = mount(
        <Form
          onSubmit={noop}
          initialValues={initialValues}
          render={() => <SchemesFields isLoading={false} uen={schemesUen} />}
        />,
      );
    });
    wrapper.update();
    expect(getCompanySchemesMock).toBeCalledWith(schemesUen);
    schemesMock.forEach((schemeMock) => {
      expect(wrapper.find(`input#schemes-checkbox-${schemeMock.scheme.id}`)).toHaveLength(1);
      expect(wrapper.find(`input#schemes-checkbox-${schemeMock.scheme.id}`).props().disabled).toEqual(false);
    });
  });

  it('should disable checkbox for Career Support Program if max salary is < 3600', async () => {
    getCompanySchemesMock.mockImplementation(() => Promise.resolve(schemesMock));
    const initialValues: Partial<IJobPostingFormState> = {
      keyInformation: {
        jobCategories: [],
        maximumSalary: 2000,
        schemes: [],
      },
    };

    let wrapper: any;
    await act(async () => {
      wrapper = mount(
        <Form
          onSubmit={noop}
          initialValues={initialValues}
          render={() => <SchemesFields isLoading={false} uen={schemesUen} />}
        />,
      );
    });
    wrapper.update();
    expect(getCompanySchemesMock).toBeCalledWith(schemesUen);
    schemesMock.forEach((schemeMock) => {
      expect(wrapper.find(`input#schemes-checkbox-${schemeMock.scheme.id}`)).toHaveLength(1);
      // only disable Career Support Program checkbox
      if (schemeMock.scheme.id === mcf.SCHEME_ID.CAREER_SUPPORT) {
        expect(wrapper.find(`input#schemes-checkbox-${schemeMock.scheme.id}`).props().disabled).toEqual(true);
      } else {
        expect(wrapper.find(`input#schemes-checkbox-${schemeMock.scheme.id}`).props().disabled).toEqual(false);
      }
    });
  });

  it('should render subschemes dropdown if a selected scheme has sub-schemes', async () => {
    getCompanySchemesMock.mockImplementation(() => Promise.resolve(schemesMock));
    const initialValues: Partial<IJobPostingFormState> = {
      keyInformation: {
        jobCategories: [],
        maximumSalary: 4000,
        schemes: [{id: mcf.SCHEME_ID.PCP, selected: true}],
      },
    };

    let wrapper: any;
    await act(async () => {
      wrapper = mount(
        <Form
          onSubmit={noop}
          initialValues={initialValues}
          render={() => <SchemesFields isLoading={false} uen={schemesUen} />}
        />,
      );
    });
    wrapper.update();
    expect(wrapper.find('[data-cy="subschemes-dropdown"]')).toHaveLength(1);
  });

  it('should keep previous scheme selection if it is present in the fetched company schemes', async () => {
    getCompanySchemesMock.mockImplementation(() => Promise.resolve(schemesMock));
    const initialValues: Partial<IJobPostingFormState> = {
      keyInformation: {
        jobCategories: [],
        maximumSalary: 4000,
        schemes: [
          {id: mcf.SCHEME_ID.CAREER_SUPPORT, selected: true},
          {id: mcf.SCHEME_ID.PCP, selected: true},
        ],
      },
    };

    let wrapper: any;
    await act(async () => {
      wrapper = mount(
        <Form
          onSubmit={noop}
          initialValues={initialValues}
          render={() => <SchemesFields isLoading={false} uen={schemesUen} />}
        />,
      );
    });
    wrapper.update();
    expect(getCompanySchemesMock).toBeCalledWith(schemesUen);
    schemesMock.forEach((schemeMock) => {
      const schemeCheckBox = wrapper.find(`input#schemes-checkbox-${schemeMock.scheme.id}`);
      expect(schemeCheckBox).toHaveLength(1);
      if (schemeMock.scheme.id === mcf.SCHEME_ID.CAREER_SUPPORT || schemeMock.scheme.id === mcf.SCHEME_ID.PCP) {
        expect(schemeCheckBox.find('input').props().checked).toEqual(true);
      } else {
        expect(schemeCheckBox.find('input').props().checked).toEqual(false);
      }
    });
  });

  it('should fetch schemes using third party uen if third party is selected', async () => {
    getCompanySchemesMock.mockImplementation(() => Promise.resolve(schemesMock));
    const thirdPartyUen = 'THIRDPARTYUEN';
    const initialValues: Partial<IJobPostingFormState> = {
      jobDescription: {
        thirdPartyEmployer: true,
        thirdPartyEmployerEntityId: thirdPartyUen,
      },
      keyInformation: {
        jobCategories: [],
        maximumSalary: 4000,
        schemes: [{id: mcf.SCHEME_ID.PCP, selected: true}],
      },
    };

    let wrapper: any;
    await act(async () => {
      wrapper = mount(
        <Form
          onSubmit={noop}
          initialValues={initialValues}
          render={() => <SchemesFields isLoading={false} uen={schemesUen} />}
        />,
      );
    });
    wrapper.update();
    expect(getCompanySchemesMock).toBeCalledWith(thirdPartyUen);
    schemesMock.forEach((schemeMock) => {
      expect(wrapper.find(`input#schemes-checkbox-${schemeMock.scheme.id}`)).toHaveLength(1);
      expect(wrapper.find(`input#schemes-checkbox-${schemeMock.scheme.id}`).props().disabled).toEqual(false);
    });
  });
});
