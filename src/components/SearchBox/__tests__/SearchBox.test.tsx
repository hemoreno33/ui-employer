import {mount} from 'enzyme';
import React from 'react';
import {SearchBox} from '~/components/SearchBox/SearchBox';

describe('SearchBox', () => {
  it('should render SearchBox', () => {
    const onSearchMock = jest.fn();
    const SearchBoxProps = {
      clearable: true,
      initialValue: 'foobar',
      onSearch: onSearchMock,
      placeholder: 'placeholder here',
    };
    const wrapper = mount(<SearchBox {...SearchBoxProps} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should sync state.value with props.initialValue when props.initialValue updated', () => {
    const onSearchMock = jest.fn();
    const SearchBoxProps = {
      initialValue: 'initial value on mount',
      onSearch: onSearchMock,
    };
    const wrapper = mount(<SearchBox {...SearchBoxProps} />);

    expect(wrapper.state('value')).toEqual('initial value on mount');

    wrapper.setProps({initialValue: 'component update of initialValue props'});
    expect(wrapper.state('value')).toEqual('component update of initialValue props');
  });

  it('should not sync state.value with props.initialValue when props.initialValue has no update', () => {
    const onSearchMock = jest.fn();
    const SearchBoxProps = {
      initialValue: 'no updates to initialValue',
      onSearch: onSearchMock,
    };
    const wrapper = mount(<SearchBox {...SearchBoxProps} />);

    expect(wrapper.state('value')).toEqual('no updates to initialValue');

    wrapper.setState({value: 'state updated e.g. thru input handleChange'});
    wrapper.setProps({initialValue: 'no updates to initialValue'});
    expect(wrapper.state('value')).toEqual('state updated e.g. thru input handleChange');
  });
});
