import React from 'react';
import styles from './Cross.scss';

export interface ICrossProps {
  onClick: () => any;
}

export const Cross: React.FunctionComponent<ICrossProps> = ({onClick}) => {
  return (
    <div className={`${styles.cross}`} onClick={onClick}>
      <span>×</span>
    </div>
  );
};
