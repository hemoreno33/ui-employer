import React from 'react';
import {EmptyState} from '~/components/Layouts/EmptyState';

export const EmptyApplication: React.FunctionComponent<any> = () => (
  <EmptyState className="ph5 black-30">
    <h1 data-cy="empty-application" className="f4 ph3">
      Pick an applicant from the left to view profile details.
    </h1>
  </EmptyState>
);
