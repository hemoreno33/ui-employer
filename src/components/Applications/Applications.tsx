import {mcf} from '@mcf/constants';
import {format, parseISO, formatISO} from 'date-fns';
import {growlNotification, growlType} from '~/components/GrowlNotification/GrowlNotification';
import {isEmpty} from 'lodash/fp';
import React, {useState, useEffect} from 'react';
import {useQuery, useMutation} from 'react-apollo';
import {FeatureFlag} from '~/components/Core/FeatureFlag';
import {ApplicationList, EmptyApplication, EmptyApplicationList} from '~/components/Applications';
import {ApplicationsToolBar} from '~/components/ApplicationsToolBar/ApplicationsToolBar';
import {ApplicationStatus} from '~/components/CandidateInfo/ApplicationStatus';
import {CandidateInfo} from '~/components/CandidateInfo/CandidateInfo';
import {CandidatesPanes} from '~/components/Candidates/CandidatesPanes';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {Dropdown} from '~/components/Core/Dropdown';
import {ErrorBoundary} from '~/components/Core/ErrorBoundary';
import {TopMatchCheckboxContainer} from '~/components/Filters/TopMatchCheckbox/TopMatchCheckboxContainer';
import {CardLoader} from '~/components/Layouts/CardLoader';
import {TemporarilyUnavailable, temporaryUnavailableMsg} from '~/components/Layouts/TemporarilyUnavailable';
import {Pagination} from '~/components/Navigation/Pagination';
import {ApplicationsOnboarding} from '~/components/Onboarding/ApplicationsOnboarding';
import {MassCheckboxWithUpdateStatusButton} from '~/components/MassJobPostStatusUpdate/MassJobPostStatusUpdate';
import {ApplicationSortCriteria, CANDIDATES_SORT_CRITERIA, getTopMatchFilter} from '~/flux/applications';
import {
  GetApplicationsApplications,
  GetApplicationsApplicationsForJob,
  GetApplicationsQuery,
  GetApplicationsQueryVariables,
  SortDirection,
  Mutation,
  MutationSetApplicationBookmarkedOnArgs,
  MutationSetResumeLastDownloadedDateArgs,
  MutationSetApplicationStatusArgs,
} from '~/graphql/__generated__/types';
import {
  APPLICATIONS_LIMIT,
  GET_APPLICATIONS,
  SET_RESUME_DOWNLOAD_AND_STATUS_UPDATE,
  SET_RESUME_LAST_DOWNLOADED_DATE,
  SET_APPLICATION_BOOKMARK,
} from '~/graphql/applications';
import {isTopMatch} from '~/services/scores/isTopMatch';
import {DOWNLOAD_RESUME_GA_LABEL} from '~/util/analytics';
import {IApplicationsProps} from './ApplicationsContainer';

export interface IApplicationsState {
  page: number;
  selectedApplicationId?: string;
  sortCriteria: ApplicationSortCriteria;
  isTopMatchChecked: boolean;
}

export const Applications: React.FunctionComponent<IApplicationsProps> = ({
  jobUuid,
  onApplicationClicked,
  onApplicationTopMatchLabelClicked,
  onApplicationPageClicked,
  onApplicationCandidateResumeClicked,
  onApplicationTopMatchFilter,
  onApplicationSortingTypeClicked,
  onApplicationCandidateTabClicked,
  refetchBookmarkedCandidatesTotal,
}) => {
  const [applicationsIdChecked, setApplicationsIdChecked] = useState<Array<string>>([]);
  const [isTopMatchChecked, setIsTopMatchChecked] = useState(false);
  const [page, setPage] = useState(0);
  const [sortCriteria, setSortCriteria] = useState(ApplicationSortCriteria.CREATED_ON);
  const [applications, setApplications] = useState<NonNullable<GetApplicationsApplicationsForJob['applications']>>([]);
  const [selectedApplication, setSelectedApplication] = useState<GetApplicationsApplications>();
  const [previousApplicationState, setPreviousApplicationState] = useState<GetApplicationsApplications>();
  const [setApplicationBookmark] = useMutation<
    {setApplicationBookmark: Mutation['setApplicationBookmarkedOn']},
    MutationSetApplicationBookmarkedOnArgs
  >(SET_APPLICATION_BOOKMARK);

  const [setResumeLastDownloadedDate] = useMutation<
    {
      setResumeLastDownloadedDate: Mutation['setResumeLastDownloadedDate'];
    },
    MutationSetResumeLastDownloadedDateArgs
  >(SET_RESUME_LAST_DOWNLOADED_DATE);

  const [setResumeDownloadAndStatusUpdate] = useMutation<
    {
      setResumeLastDownloadedDate: Mutation['setResumeLastDownloadedDate'];
      setApplicationStatus: Mutation['setApplicationStatus'];
    },
    MutationSetApplicationStatusArgs
  >(SET_RESUME_DOWNLOAD_AND_STATUS_UPDATE);

  const {loading, error, data} = useQuery<GetApplicationsQuery, GetApplicationsQueryVariables>(GET_APPLICATIONS, {
    variables: {
      jobId: jobUuid,
      limit: APPLICATIONS_LIMIT,
      skip: page * APPLICATIONS_LIMIT,
      sortBy: [{field: sortCriteria, direction: SortDirection.Desc}],
      ...(isTopMatchChecked ? {filterBy: getTopMatchFilter(sortCriteria)} : {}),
    },
    fetchPolicy: 'network-only',
    errorPolicy: 'all',
    notifyOnNetworkStatusChange: true,
    skip: !jobUuid,
  });

  useEffect(() => {
    setApplications(data?.applicationsForJob?.applications ?? []);
    setApplicationsIdChecked([]);
  }, [JSON.stringify(data)]);

  useEffect(() => {
    setApplications(
      applications.map((application) =>
        application?.id === selectedApplication?.id ? selectedApplication : application,
      ),
    );
  }, [JSON.stringify(selectedApplication)]);

  useEffect(() => {
    setApplications(
      applications.map((application) =>
        application?.id === previousApplicationState?.id ? previousApplicationState : application,
      ),
    );
    if (selectedApplication?.id === previousApplicationState?.id) {
      setSelectedApplication(previousApplicationState);
    }
  }, [JSON.stringify(previousApplicationState)]);

  const handleTopMatchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setApplicationsIdChecked([]);
    setIsTopMatchChecked(event.target.checked);
    setPage(0);
    onApplicationTopMatchFilter(event.target.checked);
  };

  const handleSortCriteriaChange = (sortCrieteria: ApplicationSortCriteria) => {
    setApplicationsIdChecked([]);
    setSortCriteria(sortCrieteria);
    setIsTopMatchChecked(false);
    onApplicationSortingTypeClicked(sortCriteria);
  };

  const onApplicationCheckboxChanged = (isCheckboxChecked: boolean, candidateId: string) => {
    if (isCheckboxChecked) {
      setApplicationsIdChecked([...applicationsIdChecked, candidateId]);
    } else {
      const filteredApplicantsSelectionId = applicationsIdChecked.filter((id) => id !== candidateId);
      setApplicationsIdChecked(filteredApplicantsSelectionId);
    }
  };

  const applicationsTotal = data?.applicationsForJob?.total ?? 0;

  const applicationPanesProps = () => {
    const onCandidateBookmarkClick = async (checked: boolean) => {
      if (selectedApplication) {
        try {
          setSelectedApplication({
            ...selectedApplication,
            bookmarkedOn: checked ? formatISO(Date.now()) : null,
          });
          await setApplicationBookmark({
            variables: {
              applicationId: selectedApplication.id,
              isBookmark: checked,
            },
          });
          if (refetchBookmarkedCandidatesTotal) {
            await refetchBookmarkedCandidatesTotal();
          }
        } catch (error) {
          setPreviousApplicationState(selectedApplication);
          growlNotification(
            `Temporarily unable to ${checked ? 'save' : 'unsave'} candidate. Please try again.`,
            growlType.ERROR,
            {
              toastId: `setApplicationBookmarkId${selectedApplication.id}`,
            },
          );
        }
      }
    };
    return {
      left: (resetRightScroll: () => void) => (
        <>
          <ApplicationList
            applicationsIdChecked={applicationsIdChecked}
            applications={applications}
            onSelectApplication={(application, positionIndex) => {
              setSelectedApplication(application);
              onApplicationClicked(application, page * APPLICATIONS_LIMIT + positionIndex + 1);
              resetRightScroll();
            }}
            onSelectTopMatchApplication={onApplicationTopMatchLabelClicked}
            selectedApplication={selectedApplication}
            sortCriteria={sortCriteria}
            onApplicationCandidateResumeClicked={onApplicationCandidateResumeClicked}
            onApplicationCheckboxChanged={onApplicationCheckboxChanged}
          />
          <Pagination
            currentPage={page}
            totalItemsLength={applicationsTotal}
            itemsPerPage={APPLICATIONS_LIMIT}
            maxVisiblePageButtons={5}
            onPageChange={(page) => {
              setApplicationsIdChecked([]);
              setPage(page);
              onApplicationPageClicked(jobUuid, page + 1);
            }}
          />
        </>
      ),
      right: selectedApplication ? (
        // Added key prop to reset the ApplicationInfo selected tab on a different application
        <ErrorBoundary key={selectedApplication.id} fallback={<EmptyApplication />}>
          <CandidateInfo
            key={selectedApplication.id}
            candidate={selectedApplication.applicant}
            showTopMatch={isTopMatch(selectedApplication.scores, sortCriteria)}
            formattedDate={`Applied on ${format(parseISO(selectedApplication.createdOn), 'd MMM yyyy')}`}
            isBookmarked={!!selectedApplication.bookmarkedOn}
            candidateType={CandidateType.applicant}
            candidateTabClicked={onApplicationCandidateTabClicked}
            onResumeClick={() => {
              // prevent moving back to under review on resume download
              if (selectedApplication.statusId === mcf.JOB_APPLICATION_STATUS.RECEIVED) {
                setResumeDownloadAndStatusUpdate({
                  variables: {
                    applicationId: selectedApplication.id,
                    statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
                  },
                });
              } else {
                setResumeLastDownloadedDate({
                  variables: {
                    applicationId: selectedApplication.id,
                  },
                });
              }
              onApplicationCandidateResumeClicked(DOWNLOAD_RESUME_GA_LABEL.DETAIL);
            }}
            statusSelector={
              <ApplicationStatus
                statusId={selectedApplication.statusId}
                isShortlisted={selectedApplication.isShortlisted}
                applicationId={selectedApplication.id}
              />
            }
            onCandidateBookmarkClick={onCandidateBookmarkClick}
          />
        </ErrorBoundary>
      ) : (
        <EmptyApplication />
      ),
    };
  };

  const displayApplicationsState = () => {
    if (loading) {
      return <CardLoader className="pa3 w-50 o-70" cardNumber={8} boxNumber={1} />;
    } else if (error && error.networkError) {
      return <TemporarilyUnavailable message={temporaryUnavailableMsg} />;
    } else if (isEmpty(applications)) {
      return <EmptyApplicationList />;
    } else return <CandidatesPanes {...applicationPanesProps()} />;
  };

  const massApplicationCheckboxChanged = () => {
    if (applicationsIdChecked.length === 0) {
      const applicationsIdSelected: string[] = [];
      applications.forEach((application) => {
        if (application) {
          applicationsIdSelected.push(application.id);
        }
      });
      setApplicationsIdChecked(applicationsIdSelected);
    } else {
      setApplicationsIdChecked([]);
    }
  };

  const hasApplications = applications.length > 0;

  const onMassApplicationButtonClicked = () => {
    // eslint-disable-next-line no-console
    console.log('update date button clicked');
  };

  return (
    <>
      <ApplicationsToolBar
        massSelectionSlot={
          <FeatureFlag
            name="massSelectionApplicantCheckbox"
            render={() => (
              <MassCheckboxWithUpdateStatusButton
                isCheckboxDisabled={applications.length === 0}
                totalApplicationsChecked={applicationsIdChecked.length}
                onMassApplicationCheckboxChanged={massApplicationCheckboxChanged}
                isMassApplicationCheckboxChecked={
                  hasApplications && applicationsIdChecked.length === applications.length
                }
                isMassApplicationCheckboxIndeterminate={
                  hasApplications &&
                  applicationsIdChecked.length > 0 &&
                  applicationsIdChecked.length != applications.length
                }
                onMassApplicationButtonClicked={onMassApplicationButtonClicked}
              />
            )}
          />
        }
        sortSlot={
          <Dropdown
            data={CANDIDATES_SORT_CRITERIA}
            input={{
              onChange: handleSortCriteriaChange,
              value: sortCriteria,
            }}
          />
        }
        topMatchSlot={
          <TopMatchCheckboxContainer
            checked={isTopMatchChecked}
            sortCriteria={sortCriteria}
            onTopMatchChange={handleTopMatchChange}
          />
        }
        onBoardingSlot={<ApplicationsOnboarding />}
      />
      <div data-cy="applications-view">{displayApplicationsState()}</div>
    </>
  );
};
