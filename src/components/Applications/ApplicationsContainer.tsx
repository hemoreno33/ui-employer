import {connect} from 'react-redux';
import {
  onApplicationCandidateResumeClicked,
  onApplicationCandidateTabClicked,
  onApplicationClicked,
  onApplicationPageClicked,
  onApplicationSortingTypeClicked,
  onApplicationTopMatchFilter,
  onApplicationTopMatchLabelClicked,
} from '~/flux/analytics';
import {IAppState} from '~/flux/index';
import {Applications} from './Applications';

export interface IApplicationsContainerDispatchProps {
  onApplicationClicked: typeof onApplicationClicked;
  onApplicationTopMatchLabelClicked: typeof onApplicationTopMatchLabelClicked;
  onApplicationCandidateTabClicked: typeof onApplicationCandidateTabClicked;
  onApplicationPageClicked: typeof onApplicationPageClicked;
  onApplicationCandidateResumeClicked: typeof onApplicationCandidateResumeClicked;
  onApplicationSortingTypeClicked: typeof onApplicationSortingTypeClicked;
  onApplicationTopMatchFilter: typeof onApplicationTopMatchFilter;
}

export interface IApplicationsContainerOwnProps {
  jobUuid: string;
  refetchBookmarkedCandidatesTotal?: () => Promise<any>;
}

export type IApplicationsProps = IApplicationsContainerDispatchProps & IApplicationsContainerOwnProps;

export const ApplicationsContainer = connect<
  {},
  IApplicationsContainerDispatchProps,
  IApplicationsContainerOwnProps,
  IAppState
>(null, {
  onApplicationCandidateResumeClicked,
  onApplicationCandidateTabClicked,
  onApplicationClicked,
  onApplicationPageClicked,
  onApplicationSortingTypeClicked,
  onApplicationTopMatchFilter,
  onApplicationTopMatchLabelClicked,
})(Applications);
