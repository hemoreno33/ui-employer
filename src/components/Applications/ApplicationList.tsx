import {mcf} from '@mcf/constants';
import {DataProxy} from 'apollo-cache';
import {format, parseISO} from 'date-fns';
import React from 'react';
import {adopt} from 'react-adopt';
import {Mutation, MutationFunction, MutationResult} from 'react-apollo';
import {CandidatesListItem} from '~/components/Candidates/CandidatesListItem';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {MissingCandidate} from '~/components/Candidates/MissingCandidate';
import {ErrorBoundary} from '~/components/Core/ErrorBoundary';
import {ApplicationSortCriteria} from '~/flux/applications';
import {GetApplicationsApplications, GetApplicationsCountQuery, Maybe} from '~/graphql/__generated__/types';
import {
  GET_APPLICATIONS_COUNT,
  SET_APPLICATION_AS_VIEWED,
  SET_APPLICATION_AS_VIEWED_WITH_RESUME_DOWNLOAD,
  SET_APPLICATION_AS_VIEWED_WITH_RESUME_DOWNLOAD_AND_STATUS_UPDATE,
  SET_RESUME_DOWNLOAD_AND_STATUS_UPDATE,
  SET_RESUME_LAST_DOWNLOADED_DATE,
} from '~/graphql/applications/applications.query';
import {isTopMatch} from '~/services/scores/isTopMatch';
import {DOWNLOAD_RESUME_GA_LABEL} from '~/util/analytics';
import {getApplicationStatusWithShortlisted} from '~/util/getApplicationStatusWithShortlisted';
import {ApplicantCheckbox} from '~/components/Applications/ApplicantCheckbox';

export interface IApplicationListProps {
  applicationsIdChecked: Array<string>;
  applications: Array<Maybe<GetApplicationsApplications>>;
  selectedApplication?: GetApplicationsApplications;
  onSelectApplication: (application: GetApplicationsApplications, positionIndex: number) => void;
  sortCriteria: ApplicationSortCriteria;
  onSelectTopMatchApplication: () => void;
  onApplicationCandidateResumeClicked: (gaLabel: DOWNLOAD_RESUME_GA_LABEL) => void;
  onApplicationCheckboxChanged: (checkboxChecked: boolean, candidateId: string) => void;
  isCheckboxChecked?: boolean;
}

export const ApplicationList: React.FunctionComponent<IApplicationListProps> = ({
  applicationsIdChecked,
  applications,
  selectedApplication,
  onSelectApplication,
  onSelectTopMatchApplication,
  sortCriteria,
  onApplicationCandidateResumeClicked,
  onApplicationCheckboxChanged,
}) => {
  return (
    <ul className="ma0 pt1 pl3 pr1 list" data-cy="applications-list">
      {applications.map((application, index) => {
        const key = (application && application.id) || index;
        if (!application) {
          return <MissingCandidate key={key} candidateType="application" />;
        }
        const {applicant, scores, isShortlisted, isViewed, statusId} = application;
        const status = getApplicationStatusWithShortlisted(statusId, isShortlisted);
        const isSelected = selectedApplication ? selectedApplication.id === application.id : false;
        const appliedOn = `Applied on ${format(parseISO(application.createdOn), 'd MMM yyyy')}`;
        const updateApplicationCountCache = (cache: DataProxy) => {
          const query = GET_APPLICATIONS_COUNT;
          const variables = {jobId: application.job.uuid};
          const data = cache.readQuery({
            query,
            variables,
          }) as GetApplicationsCountQuery;
          data!.applicationsForJob!.unviewedTotal -= 1;
          cache.writeQuery({
            data,
            query,
            variables,
          });
        };
        const Composed = adopt({
          setApplicationAsViewed: ({render}) => (
            <Mutation
              key={key}
              mutation={SET_APPLICATION_AS_VIEWED}
              variables={{applicationId: application.id}}
              update={updateApplicationCountCache}
            >
              {(mutation: MutationFunction<any, {applicationId: string}>, result: MutationResult<any>) =>
                render({mutation, result})
              }
            </Mutation>
          ),
          setApplicationAsViewedWithResumeDownload: ({render}) => (
            <Mutation
              mutation={SET_APPLICATION_AS_VIEWED_WITH_RESUME_DOWNLOAD}
              variables={{applicationId: application.id}}
              update={updateApplicationCountCache}
            >
              {(
                mutation: MutationFunction<any, {applicationId: string; statusId: number}>,
                result: MutationResult<any>,
              ) => render({mutation, result})}
            </Mutation>
          ),
          setApplicationAsViewedWithResumeDownloadAndStatusUpdate: ({render}) => (
            <Mutation
              mutation={SET_APPLICATION_AS_VIEWED_WITH_RESUME_DOWNLOAD_AND_STATUS_UPDATE}
              variables={{applicationId: application.id, statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW}}
              update={updateApplicationCountCache}
            >
              {(
                mutation: MutationFunction<any, {applicationId: string; statusId: number}>,
                result: MutationResult<any>,
              ) => render({mutation, result})}
            </Mutation>
          ),
          setResumeDownloadAndStatusUpdate: ({render}) => (
            <Mutation
              mutation={SET_RESUME_DOWNLOAD_AND_STATUS_UPDATE}
              variables={{applicationId: application.id, statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW}}
            >
              {(
                mutation: MutationFunction<any, {applicationId: string; statusId: number}>,
                result: MutationResult<any>,
              ) => render({mutation, result})}
            </Mutation>
          ),
          setResumeLastDownloadedDate: ({render}) => (
            <Mutation mutation={SET_RESUME_LAST_DOWNLOADED_DATE} variables={{applicationId: application.id}}>
              {(
                mutation: MutationFunction<any, {applicationId: string; statusId: number}>,
                result: MutationResult<any>,
              ) => render({mutation, result})}
            </Mutation>
          ),
        });
        return (
          <ErrorBoundary key={key} fallback={<MissingCandidate candidateType="application" />}>
            <Composed key={key}>
              {({
                setApplicationAsViewed,
                setApplicationAsViewedWithResumeDownload,
                setApplicationAsViewedWithResumeDownloadAndStatusUpdate,
                setResumeDownloadAndStatusUpdate,
                setResumeLastDownloadedDate,
              }: {
                setApplicationAsViewed: any;
                setApplicationAsViewedWithResumeDownload: any;
                setApplicationAsViewedWithResumeDownloadAndStatusUpdate: any;
                setResumeDownloadAndStatusUpdate: any;
                setResumeLastDownloadedDate: any;
              }) => (
                <li className="flex items-center">
                  <ApplicantCheckbox
                    applicantKey={key.toString()}
                    onApplicantCheckboxChanged={(event) => {
                      onApplicationCheckboxChanged(event.target.checked, application.id);
                    }}
                    isApplicantCheckboxChecked={applicationsIdChecked.includes(application.id)}
                  />
                  <CandidatesListItem
                    selected={isSelected}
                    candidate={applicant}
                    candidateType={CandidateType.applicant}
                    isViewed={isViewed}
                    key={key}
                    isBookmarked={!!application.bookmarkedOn}
                    date={appliedOn}
                    showTopMatch={isTopMatch(scores, sortCriteria)}
                    onClick={() => {
                      if (!isViewed) {
                        setApplicationAsViewed.mutation();
                      }
                      onSelectApplication(application, index);
                      if (isTopMatch(scores, sortCriteria)) {
                        onSelectTopMatchApplication();
                      }
                    }}
                    onResumeClick={() => {
                      const isStatusReceived = application.statusId === mcf.JOB_APPLICATION_STATUS.RECEIVED;
                      if (isViewed) {
                        if (isStatusReceived) {
                          setResumeDownloadAndStatusUpdate.mutation();
                        } else {
                          setResumeLastDownloadedDate.mutation();
                        }
                      } else {
                        if (isStatusReceived) {
                          setApplicationAsViewedWithResumeDownloadAndStatusUpdate.mutation();
                        } else {
                          setApplicationAsViewedWithResumeDownload.mutation();
                        }
                      }
                      onApplicationCandidateResumeClicked(DOWNLOAD_RESUME_GA_LABEL.LIST);
                      onSelectApplication(application, index);
                    }}
                    applicationStatus={status}
                  />
                </li>
              )}
            </Composed>
          </ErrorBoundary>
        );
      })}
    </ul>
  );
};
