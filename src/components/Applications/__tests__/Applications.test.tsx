import {MockedProvider, MockedProviderProps} from '@apollo/react-testing';
import {ApolloGraphQLInteraction, Matchers} from '@pact-foundation/pact';
import {mount, ReactWrapper} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {ApolloProvider} from 'react-apollo';
import {act} from 'react-dom/test-utils';
import {Provider} from 'react-redux';
import {MemoryRouter as Router} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {PactBuilder, transformArrayToEachLikeMatcher} from '~/__mocks__/pact';
import {CandidatesListItem} from '~/components/Candidates/CandidatesListItem';
import {MissingCandidate} from '~/components/Candidates/MissingCandidate';
import {Dropdown, IDefaultDropdownProps} from '~/components/Core/Dropdown';
import {CardLoader} from '~/components/Layouts/CardLoader';
import {ApplicationsToolBar} from '~/components/ApplicationsToolBar/ApplicationsToolBar';
import {TemporarilyUnavailable} from '~/components/Layouts/TemporarilyUnavailable';
import {EmptyApplicationList} from '~/components/Applications';
import {Pagination} from '~/components/Navigation/Pagination';
import {OnboardingContext} from '~/components/Onboarding/OnboardingContext';
import {ApplicationSortCriteria, CANDIDATES_SORT_CRITERIA} from '~/flux/applications';
import {SortDirection} from '~/graphql/__generated__/types';
import {APPLICATIONS_LIMIT, GET_APPLICATIONS, GET_APPLICATIONS_TOP_MATCH_TOTAL} from '~/graphql/applications';
import {cleanSnapshot, nextTick} from '~/testUtil/enzyme';
import {Applications} from '../Applications';
// temporarily suppress the following error, which will be fixed when upgrading Graphql package 14.5.8
// error TS7016: Could not find a declaration file for module 'graphql'.
// @ts-ignore
import {print} from 'graphql';

describe('Applications', () => {
  let pactBuilder: PactBuilder;
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-profile');
    await pactBuilder.setup();
  });
  afterAll(async () => pactBuilder.provider.finalize());

  it('should render a loader when loading and then Applications view after data is received', async () => {
    const queryMockResult = {
      data: {
        applicationsForJob: {
          applications: Matchers.eachLike(transformArrayToEachLikeMatcher(applicationMock)),
          total: Matchers.like(APPLICATIONS_LIMIT * 2),
        },
      },
    };

    const getApplicationsQuery = new ApolloGraphQLInteraction()
      .uponReceiving('getApplications')
      .withQuery(print(GET_APPLICATIONS))
      .withOperation('getApplications')
      .withRequest({
        method: 'POST',
        path: '/profile',
      })
      .withVariables(baseVariables)
      .willRespondWith({
        body: queryMockResult,
        status: 200,
      });
    await pactBuilder.provider.addInteraction(getApplicationsQuery);
    const getApplicationsTopMatchTotalQuery = new ApolloGraphQLInteraction()
      .uponReceiving('getApplicationsTopMatchTotal')
      .withQuery(print(GET_APPLICATIONS_TOP_MATCH_TOTAL))
      .withOperation('getApplicationsTopMatchTotal')
      .withRequest({
        method: 'POST',
        path: '/profile',
      })
      .withVariables(topMatchCountBaseVariables)
      .willRespondWith({
        body: Matchers.like(topMatchCountMockResult),
        status: 200,
      });
    await pactBuilder.provider.addInteraction(getApplicationsTopMatchTotalQuery);

    const wrapper = mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <Provider store={store}>
          <Router>
            <OnboardingContext.Provider value={onboardingContextProps}>
              <Applications {...applicationsProps} />
            </OnboardingContext.Provider>
          </Router>
        </Provider>
      </ApolloProvider>,
    );

    expect(wrapper.find(CardLoader)).toHaveLength(1);

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
    wrapper.update();

    const candidateListItems = wrapper.find(CandidatesListItem);
    expect(candidateListItems).toHaveLength(1);
    // @ts-ignore toJson typing issue when using .find
    expect(toJson(candidateListItems, cleanSnapshot())).toMatchSnapshot();
  });

  it('should render CandidatesFetchError when a network error is received', async () => {
    const queryMocks = [
      {
        error: new Error(),
        request: {
          query: GET_APPLICATIONS,
          variables: baseVariables,
        },
      },
    ];

    const wrapper = mountApplicationsComponent(queryMocks);
    await nextTick(wrapper);

    const applicationsToolbar = wrapper.find(ApplicationsToolBar);
    expect(applicationsToolbar).toHaveLength(1);

    const candidatesFetchError = wrapper.find(TemporarilyUnavailable);

    expect(candidatesFetchError).toHaveLength(1);
    expect(candidatesFetchError).toMatchSnapshot();
  });

  it('should render EmptyApplications when there are zero applications', async () => {
    const queryMockResult = {
      data: {
        applicationsForJob: {
          applications: [],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    };

    const queryMocks = [
      {
        request: {
          query: GET_APPLICATIONS,
          variables: baseVariables,
        },
        result: queryMockResult,
      },
    ];

    const wrapper = mountApplicationsComponent(queryMocks);
    await nextTick(wrapper);

    const applicationsToolbar = wrapper.find(ApplicationsToolBar);
    expect(applicationsToolbar).toHaveLength(1);

    const emptyApplicationList = wrapper.find(EmptyApplicationList);

    expect(emptyApplicationList).toHaveLength(1);
    expect(emptyApplicationList).toMatchSnapshot();
  });

  it('should render errors for null values and render the rest of applications', async () => {
    const queryMockResult = {
      data: {
        applicationsForJob: {
          applications: [...applicationsMock, null, null],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    };

    const queryMocks = [
      {
        request: {
          query: GET_APPLICATIONS,
          variables: baseVariables,
        },
        result: queryMockResult,
      },
    ];

    const wrapper = mountApplicationsComponent(queryMocks);
    await nextTick(wrapper);

    const applicationsToolbar = wrapper.find(ApplicationsToolBar);
    expect(applicationsToolbar).toHaveLength(1);

    const candidateListItems = wrapper.find(CandidatesListItem);
    expect(candidateListItems).toHaveLength(2);
    // @ts-ignore toJson typing issue when using .find
    expect(toJson(candidateListItems, cleanSnapshot())).toMatchSnapshot();

    const missingApplications = wrapper.find(MissingCandidate).find('[candidateType="application"]');
    expect(missingApplications).toHaveLength(2);
    // @ts-ignore toJson typing issue when using .find
    expect(toJson(missingApplications, cleanSnapshot())).toMatchSnapshot();
  });

  it('should fetch applications and navigate to another page when pagination is clicked', async () => {
    const wrapper = mountApplicationsComponent();
    await nextTick(wrapper);

    const pagination = wrapper.find(Pagination);
    expect(pagination.find('.bg-secondary').text()).toEqual('1');

    const secondPage = pagination.findWhere((node) => node.type() === 'span' && node.text() === '2');
    secondPage.simulate('click');

    await nextTick(wrapper);

    const pageChange = wrapper.find(Pagination);
    expect(pageChange.find('.bg-secondary').text()).toEqual('2');
  });

  it('should fetch applications and set isTopMatchChecked to false when sorting change', async () => {
    const wrapper = mountApplicationsComponent();
    await nextTick(wrapper);

    const dropdown = wrapper.find<IDefaultDropdownProps<typeof CANDIDATES_SORT_CRITERIA[number]>>(Dropdown);
    const dropdownInput = dropdown.prop('input')!;
    act(() => {
      dropdownInput.onChange!(ApplicationSortCriteria.SCORES_WCC);
    });
    await nextTick(wrapper);

    expect(wrapper.find('[data-cy="top-match-checkbox"]').prop('checked')).toEqual(false);
  });

  it('should call fetchMoreApplications with filtered applications when sorting changes and isTopMatchChecked true', async () => {
    const wrapper = mountApplicationsComponent();

    await nextTick(wrapper);

    act(() => {
      wrapper.find('[data-cy="top-match-checkbox"]').simulate('change', {target: {checked: true}});
    });

    await nextTick(wrapper, 100);

    expect(wrapper.find('[data-cy="top-match-checkbox"]').prop('checked')).toEqual(true);

    expect(wrapper.find(CandidatesListItem)).toHaveLength(4);
    expect(wrapper.find(CandidatesListItem)).toMatchSnapshot();

    act(() => {
      wrapper.find<IDefaultDropdownProps<typeof CANDIDATES_SORT_CRITERIA[number]>>(Dropdown).prop('input')!.onChange!(
        ApplicationSortCriteria.SCORES_WCC,
      );
    });

    await nextTick(wrapper);

    expect(wrapper.find(Dropdown).prop('input')!.value).toEqual(ApplicationSortCriteria.SCORES_WCC);
    expect(wrapper.find('[data-cy="top-match-checkbox"]').prop('checked')).toEqual(false);

    act(() => {
      wrapper.find('[data-cy="top-match-checkbox"]').simulate('change', {target: {checked: true}});
    });

    await nextTick(wrapper);

    expect(wrapper.find('[data-cy="top-match-checkbox"]').prop('checked')).toEqual(true);
    expect(wrapper.find(CandidatesListItem)).toHaveLength(3);
    expect(wrapper.find(CandidatesListItem)).toMatchSnapshot();

    act(() => {
      wrapper.find<IDefaultDropdownProps<typeof CANDIDATES_SORT_CRITERIA[number]>>(Dropdown).prop('input')!.onChange!(
        ApplicationSortCriteria.SCORES_JOBKRED,
      );
    });

    await nextTick(wrapper);

    expect(wrapper.find(Dropdown).prop('input')!.value).toEqual(ApplicationSortCriteria.SCORES_JOBKRED);
    expect(wrapper.find('[data-cy="top-match-checkbox"]').prop('checked')).toEqual(false);
    expect(wrapper.find(CandidatesListItem)).toHaveLength(3);
    expect(wrapper.find(CandidatesListItem)).toMatchSnapshot();
  });

  it('should set all individual application checkbox to false when sortBy is changed', async () => {
    const queryMockResult = {
      data: {
        applicationsForJob: {
          applications: [...twentyOneApplicationsMock, null, null],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    };

    const queryMocks = [
      {
        request: {
          query: GET_APPLICATIONS,
          variables: baseVariables,
        },
        result: queryMockResult,
      },
    ];

    const wrapper = mountApplicationsComponent(queryMocks);

    await nextTick(wrapper);

    act(() => {
      wrapper.find('[data-cy="candidate-checkbox-I-0"]').simulate('change', {target: {checked: true}});
      wrapper.find('[data-cy="candidate-checkbox-I-1"]').simulate('change', {target: {checked: true}});
      wrapper.find('[data-cy="candidate-checkbox-I-2"]').simulate('change', {target: {checked: true}});
    });

    await nextTick(wrapper);

    const dropdown = wrapper.find<IDefaultDropdownProps<typeof CANDIDATES_SORT_CRITERIA[number]>>(Dropdown);
    const dropdownInput = dropdown.prop('input')!;

    act(() => {
      dropdownInput.onChange!(ApplicationSortCriteria.CREATED_ON);
    });

    await nextTick(wrapper);

    expect(wrapper.find('[data-cy="candidate-checkbox-I-0"]').prop('checked')).toEqual(false);
    expect(wrapper.find('[data-cy="candidate-checkbox-I-1"]').prop('checked')).toEqual(false);
    expect(wrapper.find('[data-cy="candidate-checkbox-I-2"]').prop('checked')).toEqual(false);
    expect(wrapper.find('[data-cy="mass-application-status-select-all-text"]')).toHaveLength(1);
  });

  it('should check all applcation checkbox if the overall checkbox is checked', async () => {
    const queryMockResult = {
      data: {
        applicationsForJob: {
          applications: [...twentyOneApplicationsMock, null, null],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    };

    const queryMocks = [
      {
        request: {
          query: GET_APPLICATIONS,
          variables: baseVariables,
        },
        result: queryMockResult,
      },
    ];

    const wrapper = mountApplicationsComponent(queryMocks);

    await nextTick(wrapper);

    act(() => {
      wrapper.find('[data-cy="mass-application-selection-checkbox"]').simulate('change', {target: {checked: true}});
    });

    await nextTick(wrapper);

    Array.from(Array(20).keys()).forEach((key) =>
      expect(wrapper.find(`[data-cy="candidate-checkbox-I-${key}"]`).prop('checked')).toEqual(true),
    );
    expect(wrapper.find('[data-cy="mass-application-status-update-button"]')).toHaveLength(1);
  });

  it('should set all individual application checkbox to false after overall checkbox change to false', async () => {
    const queryMockResult = {
      data: {
        applicationsForJob: {
          applications: [...twentyOneApplicationsMock, null, null],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    };

    const queryMocks = [
      {
        request: {
          query: GET_APPLICATIONS,
          variables: baseVariables,
        },
        result: queryMockResult,
      },
    ];

    const wrapper = mountApplicationsComponent(queryMocks);

    await nextTick(wrapper);

    act(() => {
      wrapper.find('[data-cy="candidate-checkbox-I-0"]').simulate('change', {target: {checked: true}});
      wrapper.find('[data-cy="candidate-checkbox-I-1"]').simulate('change', {target: {checked: true}});
      wrapper.find('[data-cy="candidate-checkbox-I-2"]').simulate('change', {target: {checked: true}});
    });

    await nextTick(wrapper);

    act(() => {
      wrapper.find('[data-cy="mass-application-selection-checkbox"]').simulate('change', {target: {checked: false}});
    });

    await nextTick(wrapper);

    expect(wrapper.find('[data-cy="candidate-checkbox-I-0"]').prop('checked')).toEqual(false);
    expect(wrapper.find('[data-cy="candidate-checkbox-I-1"]').prop('checked')).toEqual(false);
    expect(wrapper.find('[data-cy="candidate-checkbox-I-2"]').prop('checked')).toEqual(false);
    expect(wrapper.find('[data-cy="mass-application-status-select-all-text"]')).toHaveLength(1);
  });
});

const applicationsProps = {
  jobUuid: 'R90SS0001A(4)',
  onApplicationCandidateResumeClicked: jest.fn(),
  onApplicationCandidateTabClicked: jest.fn(),
  onApplicationClicked: jest.fn(),
  onApplicationPageClicked: jest.fn(),
  onApplicationSortingTypeClicked: jest.fn(),
  onApplicationTopMatchFilter: jest.fn(),
  onApplicationTopMatchLabelClicked: jest.fn(),
  removeCandidateIdSelection: jest.fn(),
  addCandidateIdSelection: jest.fn(),
  resetCandidateIdSelection: jest.fn(),
};

const applicationsMock = [
  {
    ...applicationMock,
    id: 'I-1',
  },
  {
    ...applicationMock,
    id: 'I-2',
  },
];

const twentyOneApplicationsMock = Array.from(Array(20).keys()).map((keys) => {
  return {
    ...applicationMock,
    id: `I-${keys}`,
  };
});

const jobKredApplicationsMock = [
  {
    ...applicationMock,
    id: 'JOBKRED-1',
  },
];

const wccApplicationsMock = [
  {
    ...applicationMock,
    id: 'wcc-1',
  },
];

const baseVariables = {
  jobId: applicationsProps.jobUuid,
  limit: APPLICATIONS_LIMIT,
  skip: 0,
  sortBy: [{field: ApplicationSortCriteria.CREATED_ON, direction: SortDirection.Desc}],
};

const onboardingContextProps = {
  isApplicationViewed: true,
  setApplicationViewed: jest.fn(),
};

const defaultQueryMockResult = {
  data: {
    applicationsForJob: {
      applications: applicationsMock,
      total: APPLICATIONS_LIMIT * 2,
    },
  },
};

const topMatchCountBaseVariables = {
  filterBy: [
    {field: 'scores.jobkred', value: 0.7},
    {field: 'scores.wcc', value: 0.6},
  ],
  jobId: applicationsProps.jobUuid,
};

const topMatchCountMockResult = {
  data: {
    applicationsForJob: {
      total: APPLICATIONS_LIMIT * 2,
    },
  },
};

const defaultQueryMocks = [
  {
    request: {
      query: GET_APPLICATIONS,
      variables: baseVariables,
    },
    result: defaultQueryMockResult,
  },
  {
    request: {
      query: GET_APPLICATIONS,
      variables: {
        ...baseVariables,
        skip: APPLICATIONS_LIMIT,
      },
    },
    result: defaultQueryMockResult,
  },
  {
    request: {
      query: GET_APPLICATIONS,
      variables: {
        ...baseVariables,
        filterBy: [
          {field: 'scores.jobkred', value: 0.7},
          {field: 'scores.wcc', value: 0.6},
        ],
      },
    },
    result: {
      data: {
        applicationsForJob: {
          applications: [...applicationsMock, ...jobKredApplicationsMock, ...wccApplicationsMock],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    },
  },
  {
    request: {
      query: GET_APPLICATIONS_TOP_MATCH_TOTAL,
      variables: topMatchCountBaseVariables,
    },
    result: topMatchCountMockResult,
  },
  {
    request: {
      query: GET_APPLICATIONS,
      variables: {
        ...baseVariables,
        sortBy: [{field: ApplicationSortCriteria.SCORES_WCC, direction: SortDirection.Desc}],
      },
    },
    result: {
      data: {
        applicationsForJob: {
          applications: [...applicationsMock, ...wccApplicationsMock],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    },
  },
  {
    request: {
      query: GET_APPLICATIONS,
      variables: {
        ...baseVariables,
        filterBy: [{field: 'scores.wcc', value: 0.6}],
        sortBy: [{field: ApplicationSortCriteria.SCORES_WCC, direction: SortDirection.Desc}],
      },
    },
    result: {
      data: {
        applicationsForJob: {
          applications: [...applicationsMock, ...wccApplicationsMock],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    },
  },
  {
    request: {
      query: GET_APPLICATIONS_TOP_MATCH_TOTAL,
      variables: {
        ...topMatchCountBaseVariables,
        filterBy: [{field: 'scores.wcc', value: 0.6}],
      },
    },
    result: topMatchCountMockResult,
  },
  {
    request: {
      query: GET_APPLICATIONS,
      variables: {
        ...baseVariables,
        sortBy: [{field: ApplicationSortCriteria.SCORES_JOBKRED, direction: SortDirection.Desc}],
      },
    },
    result: {
      data: {
        applicationsForJob: {
          applications: [...applicationsMock, ...jobKredApplicationsMock],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    },
  },
  {
    request: {
      query: GET_APPLICATIONS,
      variables: {
        ...baseVariables,
        filterBy: [{field: 'scores.jobkred', value: 0.7}],
        sortBy: [{field: ApplicationSortCriteria.SCORES_JOBKRED, direction: SortDirection.Desc}],
      },
    },
    result: {
      data: {
        applicationsForJob: {
          applications: [...applicationsMock, ...jobKredApplicationsMock],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    },
  },
  {
    request: {
      query: GET_APPLICATIONS_TOP_MATCH_TOTAL,
      variables: {
        ...topMatchCountBaseVariables,
        filterBy: [{field: 'scores.jobkred', value: 0.7}],
      },
    },
    result: topMatchCountMockResult,
  },
];

const reduxState = {
  router: {
    location: {
      pathname: `${applicationsProps.jobUuid}/applications`,
    },
  },
  features: {bookmarkTab: true, massSelectionApplicantCheckbox: true},
};
const store = configureStore()(reduxState);

const mountApplicationsComponent = (queryMocks: MockedProviderProps['mocks'] = defaultQueryMocks): ReactWrapper => {
  return mount(
    <MockedProvider mocks={queryMocks} addTypename={false}>
      <Provider store={store}>
        <Router>
          <OnboardingContext.Provider value={onboardingContextProps}>
            <Applications {...applicationsProps} />
          </OnboardingContext.Provider>
        </Router>
      </Provider>
    </MockedProvider>,
  );
};
