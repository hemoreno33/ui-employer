import React from 'react';
import {FeatureFlag} from '~/components/Core/FeatureFlag';
import styles from '~/components/Candidates/CandidatesList.scss';

export interface IApplicantCheckboxProps {
  applicantKey: string;
  onApplicantCheckboxChanged: (event: React.ChangeEvent<HTMLInputElement>) => void;
  isApplicantCheckboxChecked: boolean;
}

export const ApplicantCheckbox: React.FunctionComponent<IApplicantCheckboxProps> = ({
  applicantKey,
  onApplicantCheckboxChanged,
  isApplicantCheckboxChecked = false,
}) => {
  return (
    <FeatureFlag
      name="massSelectionApplicantCheckbox"
      render={() => {
        return (
          <div className="pr1">
            <label>
              <input
                data-cy={`candidate-checkbox-${applicantKey}`}
                className={styles.inputCheckbox}
                type="checkbox"
                onChange={onApplicantCheckboxChanged}
                checked={isApplicantCheckboxChecked}
              />
              <span className="f5 black-80 pointer" />
            </label>
          </div>
        );
      }}
    />
  );
};
