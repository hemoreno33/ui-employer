import {shallow} from 'enzyme';
import React from 'react';
import {LoginButton} from '../LoginButton';

describe('LoginButton', () => {
  it('renders correctly', () => {
    const onClickMock = jest.fn();
    const wrapper = shallow(<LoginButton onClick={onClickMock} />);
    wrapper.simulate('click');

    expect(onClickMock).toHaveBeenCalledTimes(1);
    expect(wrapper).toMatchSnapshot();
  });
});
