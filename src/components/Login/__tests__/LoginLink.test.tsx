import {shallow} from 'enzyme';
import React from 'react';
import {LoginLink, NavLoginLink} from '../LoginLink';

describe('LoginLink', () => {
  it('renders correctly', () => {
    const onClickMock = jest.fn();
    const wrapper = shallow(<LoginLink onClick={onClickMock} />);
    wrapper.simulate('click');

    expect(onClickMock).toHaveBeenCalledTimes(1);
    expect(wrapper).toMatchSnapshot();
  });
});

describe('NavLoginLink', () => {
  it('renders correctly', () => {
    const onClickMock = jest.fn();
    const wrapper = shallow(<NavLoginLink onClick={onClickMock} />);
    wrapper.simulate('click');

    expect(onClickMock).toHaveBeenCalledTimes(1);
    expect(wrapper).toMatchSnapshot();
  });
});
