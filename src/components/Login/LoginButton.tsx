import React from 'react';
const styles = require('./loginButton.scss');

interface ILoginButtonProps {
  onClick: (event: React.MouseEvent<HTMLElement>) => void;
}

export const LoginButton: React.FunctionComponent<ILoginButtonProps> = ({onClick}) => {
  return (
    <button
      id="loginWithCorpPass"
      onClick={onClick}
      className={`pa3 white fw6 tc f4 bg-primary w-100 br2 generic-btn ${styles.loginWithCorppass}`}
      data-cy="login-with-corppass"
    >
      Log in with CorpPass
    </button>
  );
};
