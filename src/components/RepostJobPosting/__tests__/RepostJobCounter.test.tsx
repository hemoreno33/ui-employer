import {canRepostCount, canRepostDate} from '../RepostJobCounter';

describe('RepostJobCounter', () => {
  describe('canRepostCount()', () => {
    it('should return true if repost count less than 2', () => {
      expect(canRepostCount(0)).toBe(true);
    });
    it('should return false if repost count out of range 0..2', () => {
      expect(canRepostCount(2)).toBe(false);
      expect(canRepostCount(-1)).toBe(false);
    });
  });

  describe('canRepostDate()', () => {
    it('should return true if date within 6 months', () => {
      expect(canRepostDate(new Date(2020, 2, 1), new Date(2020, 2, 10))).toBe(true);
    });
    it("should return false if it's out of range", () => {
      expect(canRepostDate(new Date(2019, 1, 1), new Date(2020, 2, 10))).toBe(false);
      expect(canRepostDate(new Date(2021, 3, 1), new Date(2020, 2, 10))).toBe(false);
    });
  });
});
