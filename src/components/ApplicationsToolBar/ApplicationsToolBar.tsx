import React from 'react';
import styles from '~/components/ApplicationsToolBar/ApplicationsToolBar.scss';

export interface IApplicationToolBarProps {
  massSelectionSlot: JSX.Element;
  sortSlot: JSX.Element;
  topMatchSlot: JSX.Element;
  onBoardingSlot: JSX.Element;
}

export const ApplicationsToolBar: React.FunctionComponent<IApplicationToolBarProps> = ({
  sortSlot,
  topMatchSlot,
  onBoardingSlot,
  massSelectionSlot,
}: any) => {
  return (
    <>
      <div className={`flex bg-black-50 bb b--black-05 w-100 items-stretch ${styles.applicationToolbar}`}>
        <div className={`flex self-center items-center h-100`}>{massSelectionSlot}</div>
        <div className={`flex ${styles.flexEndRow} ${styles.sortBox}`}>
          <p data-cy="sort-by" className="self-center f4-5 fw6 black-60 ma0 lh-solid tr pr3">
            Sort by:
          </p>
          <div className={styles.sortDropdown} data-cy="applications-sort-criteria-dropdown">
            {sortSlot}
          </div>
        </div>

        <div className={`flex self-center ${styles.flexEndRow}`} data-cy="applications-top-match">
          {topMatchSlot}
        </div>
      </div>
      <div className="w-100 pa3 tr">{onBoardingSlot}</div>
    </>
  );
};
