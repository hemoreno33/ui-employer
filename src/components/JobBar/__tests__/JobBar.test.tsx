import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {IJobPost} from '~/services/employer/jobs.types';
import {cleanSnapshot} from '~/testUtil/enzyme';
import {JobBar} from './../JobBar';

describe('Jobs/JobBar', () => {
  it('renders correctly', () => {
    const wrapper = mount(<JobBar job={jobPostMock} />);
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });
  it('should display lock icon if a job is posted as a non-mcf job', () => {
    const inputJob: IJobPost = {
      ...jobPostMock,
      metadata: {
        ...jobPostMock.metadata,
        jobPostId: 'JOB-12345678',
      },
    };
    const wrapper = mount(<JobBar job={inputJob} />);
    expect(wrapper.find('i.iconLock')).toHaveLength(1);
  });
  it('should not display lock icon if a job is posted from mcf jobs', () => {
    const inputJob: IJobPost = {
      ...jobPostMock,
      metadata: {
        ...jobPostMock.metadata,
        jobPostId: 'MCF-12131415',
      },
    };
    const wrapper = mount(<JobBar job={inputJob} />);
    expect(wrapper.find('i.iconLock')).toHaveLength(0);
  });
});
