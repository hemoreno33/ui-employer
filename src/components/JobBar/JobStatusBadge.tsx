import React from 'react';
import {JobStatus} from '~/services/employer/jobs.types';

export interface IBadgeProps {
  status: JobStatus;
}

export const Badge: React.FunctionComponent<IBadgeProps> = ({status}: IBadgeProps) => {
  let className = 'f8 fw6 lh-solid ba br1 pv1 ph2 ';
  switch (status) {
    case JobStatus.Closed:
      className += 'b--black-50 black-50';
      break;
    case JobStatus.Draft:
    case JobStatus.Unknown:
      className += 'b--black-50 black-50';
      break;
    case JobStatus.Open:
    case JobStatus.Reopened:
      className += 'b--green green';
      break;
  }

  return (
    <div data-cy="job-post-status" className={className}>
      {status}
    </div>
  );
};
