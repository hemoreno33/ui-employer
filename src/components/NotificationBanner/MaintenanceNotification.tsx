import React from 'react';
import {IMaintenanceNotification} from '~/services/notification/getNotifications';

interface IMaintenanceNotificationProps {
  maintenance: IMaintenanceNotification;
}

export const MaintenanceNotification: React.FunctionComponent<IMaintenanceNotificationProps> = ({maintenance}) => {
  return (
    <aside className="bg-yellow pt3 pb3" data-cy="maintenance-notification">
      <div className="ph3-ns pv0 center">
        <div className="flex-ns justify-between pv0 ph3 items-center">
          <p className="f6 ma0 pv0 pr3 fw4 black v-mid lh-copy">
            <i className="icon-announcement di" /> {maintenance.message}
          </p>
        </div>
      </div>
    </aside>
  );
};
