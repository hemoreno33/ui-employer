import {addMinutes, format, formatISO} from 'date-fns';
import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {NotificationBannerContainer} from '~/components/NotificationBanner/NotificationBannerContainer';
import {cleanSnapshot} from '~/testUtil/enzyme';
import {featureNotificationMock, maintenanceNotificationMock} from '~/__mocks__/notification/notification.mocks';

describe('NotificationBannerContainer', () => {
  it('should render MaintenanceNotification if there is maintanance within range', () => {
    const reduxState = {
      notification: {
        maintenances: [
          {
            ...maintenanceNotificationMock,
            fromDate: format(new Date(), `yyyy-MM-dd'T'HH:mm:ss.SSSx`),
            toDate: format(addMinutes(new Date(), 1), `yyyy-MM-dd'T'HH:mm:ss.SSSx`),
          },
        ],
        features: [
          {
            ...featureNotificationMock,
            fromDate: format(new Date(), `yyyy-MM-dd'T'HH:mm:ss.SSSx`),
            toDate: format(addMinutes(new Date(), 1), `yyyy-MM-dd'T'HH:mm:ss.SSSx`),
          },
        ],
      },
    };
    const store = configureStore()(reduxState);
    const wrapper = mount(
      <Provider store={store}>
        <NotificationBannerContainer />
      </Provider>,
    );
    expect(wrapper.find('[data-cy="feature-notification"]')).toHaveLength(0);
    const maintenanceNotification = wrapper.find('[data-cy="maintenance-notification"]');
    expect(maintenanceNotification).toHaveLength(1);
    expect(toJson(maintenanceNotification, cleanSnapshot())).toMatchSnapshot();
  });

  it('should render FeatureNotification if there is feature within range but no maintanance within range', () => {
    const reduxState = {
      notification: {
        maintenances: [maintenanceNotificationMock],
        features: [
          {
            ...featureNotificationMock,
            fromDate: format(new Date(), `yyyy-MM-dd'T'HH:mm:ss.SSSx`),
            toDate: format(addMinutes(new Date(), 1), `yyyy-MM-dd'T'HH:mm:ss.SSSx`),
          },
        ],
      },
    };
    const store = configureStore()(reduxState);
    const wrapper = mount(
      <Provider store={store}>
        <NotificationBannerContainer />
      </Provider>,
    );
    expect(wrapper.find('[data-cy="maintenance-notification"]')).toHaveLength(0);
    const featureNotification = wrapper.find('[data-cy="feature-notification"]');
    expect(featureNotification).toHaveLength(1);
    expect(featureNotification).toMatchSnapshot();
  });

  it('should render null if both maintenance and feature are out of range', () => {
    const reduxState = {
      notification: {
        maintenances: [maintenanceNotificationMock],
        features: [featureNotificationMock],
      },
    };
    const store = configureStore()(reduxState);
    const wrapper = mount(
      <Provider store={store}>
        <NotificationBannerContainer />
      </Provider>,
    );
    expect(wrapper.find('[data-cy="feature-notification"]')).toHaveLength(0);
    expect(wrapper.find('[data-cy="maintenance-notification"]')).toHaveLength(0);
    expect(wrapper).toMatchSnapshot();
  });

  it('should not render feature if it was already acknowledged today', async () => {
    const getItemSpy = jest.spyOn(Storage.prototype, 'getItem').mockReturnValueOnce(formatISO(new Date()));
    const reduxState = {
      notification: {
        maintenances: [maintenanceNotificationMock],
        features: [
          {
            ...featureNotificationMock,
            fromDate: format(new Date(), `yyyy-MM-dd'T'HH:mm:ss.SSSx`),
            toDate: format(addMinutes(new Date(), 1), `yyyy-MM-dd'T'HH:mm:ss.SSSx`),
          },
        ],
      },
    };
    const store = configureStore()(reduxState);
    const wrapper = mount(
      <Provider store={store}>
        <NotificationBannerContainer />
      </Provider>,
    );

    expect(wrapper.find('[data-cy="feature-notification"]')).toHaveLength(0);
    expect(wrapper.find('[data-cy="maintenance-notification"]')).toHaveLength(0);
    getItemSpy.mockRestore();
  });
});
