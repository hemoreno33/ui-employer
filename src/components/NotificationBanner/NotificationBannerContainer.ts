import {connect} from 'react-redux';
import {IAppState} from '~/flux';
import {NotificationBanner} from './NotificationBanner';

const mapStateToProps = ({notification: {features, maintenances}}: IAppState) => ({
  features,
  maintenances,
});

export const NotificationBannerContainer = connect(mapStateToProps)(NotificationBanner);
