import React from 'react';

interface IErrorBoundaryProps {
  children: React.ReactNode;
  fallback: React.ReactNode;
}

interface IErrorBoundaryState {
  hasError: boolean;
}

export class ErrorBoundary extends React.Component<IErrorBoundaryProps, IErrorBoundaryState> {
  constructor(props: IErrorBoundaryProps) {
    super(props);
    this.state = {hasError: false};
  }

  public componentDidCatch() {
    this.setState({hasError: true});
  }

  public render() {
    return this.state.hasError ? this.props.fallback : this.props.children;
  }
}
