import {isEqual} from 'lodash/fp';
import React, {useEffect, useRef, useState} from 'react';
import {fetchResponse} from '~/util/fetchResponse';

export interface IFetchQuery<T> {
  input: RequestInfo;
  init?: RequestInit;
  children: (result: {loading: boolean; error?: string; data?: T}) => React.ReactElement<any> | null;
}

export function FetchQuery<T>({input, init, children}: IFetchQuery<T>) {
  const [data, setData] = useState();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState();

  const useDeepCompareMemoize = (value: [RequestInfo, RequestInit?]) => {
    const ref = useRef<[RequestInfo, RequestInit?]>();

    if (!isEqual(value)(ref.current)) {
      ref.current = value;
    }

    return ref.current;
  };

  useEffect(() => {
    const controller = new AbortController();
    const fetchData = async () => {
      setLoading(true);

      try {
        const response = await fetch(input, {...init, signal: controller.signal});
        setData(await fetchResponse(response));
      } catch (error) {
        setError(error.message);
      }

      setLoading(false);
    };

    fetchData();
    return () => {
      controller.abort();
      setData(undefined);
      setError(undefined);
    };
  }, useDeepCompareMemoize([input, init]));

  const result = () =>
    children({
      data,
      error,
      loading,
    });

  return result();
}
