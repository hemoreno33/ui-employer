import {mount} from 'enzyme';
import React from 'react';
import {ErrorBoundary} from '../ErrorBoundary';

describe('ErrorBoundary', () => {
  it('should render fallback if a child of ErrorBoundary throws an error', () => {
    const ErrorComponent: React.FunctionComponent = () => <div>Error Component</div>;
    const FallbackComponent: React.FunctionComponent = () => <div>Fallback Component</div>;
    const wrapper = mount(
      <ErrorBoundary fallback={<FallbackComponent />}>
        <ErrorComponent />
      </ErrorBoundary>,
    );

    wrapper.find(ErrorComponent).simulateError(new Error('error'));
    expect(wrapper.find(FallbackComponent)).toHaveLength(1);
  });
});
