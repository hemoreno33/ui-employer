import {mount} from 'enzyme';
import React from 'react';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {FeatureFlag} from '~/components/Core/FeatureFlag';
import {IAppState} from '~/flux';

describe('FeatureFlag', () => {
  const state: Pick<IAppState, 'features'> = {
    features: {
      jobPost: false,
      systemContact: true,
    },
  };
  test('should render component when feature flag is set to true', () => {
    const store = configureStore()(state);
    const wrapper = mount(
      <Provider store={store}>
        <FeatureFlag
          name="systemContact"
          render={() => <div>System contact is active</div>}
          fallback={() => <div>This feature is not available</div>}
        />
      </Provider>,
    );
    expect(wrapper.find('div').text()).toEqual('System contact is active');
  });
  test('should render fallback component when feature flag is set to false', () => {
    const store = configureStore()(state);
    const wrapper = mount(
      <Provider store={store}>
        <FeatureFlag
          name="jobPost"
          render={() => <div>Job post is active</div>}
          fallback={() => <div>This feature is not available</div>}
        />
      </Provider>,
    );
    expect(wrapper.find('div').text()).toEqual('This feature is not available');
  });
  test('should render fallback component when feature no feature flag is set', () => {
    const store = configureStore()(state);
    const wrapper = mount(
      <Provider store={store}>
        <FeatureFlag
          name="extraFeature"
          render={() => <div>Extra feature is active</div>}
          fallback={() => <div>This feature is not available</div>}
        />
      </Provider>,
    );
    expect(wrapper.find('div').text()).toEqual('This feature is not available');
  });
  test('should not render anything when there is no render function and feature flag is true', () => {
    const store = configureStore()(state);
    const wrapper = mount(
      <Provider store={store}>
        <FeatureFlag name="systemFeature" />
      </Provider>,
    );
    expect(wrapper.find('div').length).toEqual(0);
  });
  test('should not render anything when there is no fallback function and feature flag is false', () => {
    const store = configureStore()(state);
    const wrapper = mount(
      <Provider store={store}>
        <FeatureFlag name="jobPost" />
      </Provider>,
    );
    expect(wrapper.find('div').length).toEqual(0);
  });
});
