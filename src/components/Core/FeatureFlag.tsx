import React, {ReactElement} from 'react';
import {useSelector} from 'react-redux';
import {IAppState} from '~/flux';

interface IFeatureFlagProps {
  name: string;
  render?: () => ReactElement;
  fallback?: () => ReactElement;
}

export const FeatureFlag: React.FunctionComponent<IFeatureFlagProps> = ({name, render, fallback}) => {
  const featureFlag = useSelector((state: IAppState) => state.features[name]);
  if (featureFlag && render) {
    return render();
  }
  if (!featureFlag && fallback) {
    return fallback();
  }
  return null;
};
