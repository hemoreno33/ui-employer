import {mount} from 'enzyme';
import React from 'react';
import {BannerItem} from '../BannerItem';

describe('Banner/BannerItem', () => {
  it('should render BannerItem correctly', () => {
    const bannerItemProps = {
      id: 0,
      text: 'text',
      title: 'title',
    };
    const component = mount(<BannerItem {...bannerItemProps} />);

    expect(component).toMatchSnapshot();
  });
});
