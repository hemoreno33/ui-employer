import {connect} from 'react-redux';
import {login} from '~/flux/account';
import {onCorppassGetStartedClicked} from '~/flux/analytics';
import {Banner} from './Banner';

export interface IBannerProps {
  login: typeof login;
  onCorppassGetStartedClicked: typeof onCorppassGetStartedClicked;
}

export const BannerContainer = connect(null, {login, onCorppassGetStartedClicked})(Banner);
