import React from 'react';

interface IBannerItem {
  title: string;
  text: string;
  id: number;
}
export const BannerItem: React.FunctionComponent<IBannerItem> = ({title, text, id}) => {
  return (
    <>
      <h1 className="f2 fw6 white" data-cy={`banner-${id}-title`}>
        {title}
      </h1>
      <p className={`f3-5 fw3 lh-copy pr5`} data-cy={`banner-${id}-text`}>
        {text}
      </p>
    </>
  );
};
