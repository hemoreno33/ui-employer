import React from 'react';
import {CandidatesOnboarding} from '~/components/Onboarding/CandidatesOnboarding';
import {OnboardingContext} from '~/components/Onboarding/OnboardingContext';
import styles from './Onboarding.scss';

export class ApplicationsOnboarding extends React.Component {
  public static contextType = OnboardingContext;

  public render() {
    return (
      <CandidatesOnboarding
        title={'How do I sort applicants by suitability for my job?'}
        isViewed={this.context.isApplicationViewed}
        setAsViewed={this.context.setApplicationViewed}
      >
        <div className={`${styles.onboarderContentBox} pa3`}>
          <div className={styles.applicationStep1} />
          <p data-cy="applicant-onboarding-string-1" className="pt4 pb3 lh-copy f5">
            You may sort applicants by either one of the job matching engines, which will rank applicants based on their
            skills, work experience and education against your job description.
            <br />
            <br />
            The job matching engines are powered by WCC and JobKred.
          </p>
        </div>
        <div className={`${styles.onboarderContentBox} pa3`}>
          <div className={styles.applicationStep2} />
          <p data-cy="applicant-onboarding-string-2" className="pt4 pb3 lh-copy f5">
            Applicants bearing the ‘Among top matches’ label have been assessed to be a close match to the job by the
            job matching engines.
          </p>
        </div>
      </CandidatesOnboarding>
    );
  }
}
