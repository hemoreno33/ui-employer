import {isNil} from 'lodash/fp';
import React from 'react';
import {Query} from 'react-apollo';
import {TotalCount} from '~/components/JobList/TotalCount';
import {GetSuggestedTalentsCountQuery, GetSuggestedTalentsCountQueryVariables} from '~/graphql/__generated__/types';
import {GET_SUGGESTED_TALENTS_COUNT, SUGGESTED_TALENTS_TOTAL_LIMIT} from '~/graphql/suggestedTalents';
import {isSuggestedTalentUnavailable, IFormattedJobPost} from '~/util/jobPosts';

const SUGGESTED_TALENTS = 'Suggested Talents';

export interface ISuggestedTalentsCountProps {
  fromLastLogin: string;
  formattedJob: IFormattedJobPost;
  threshold: number;
}

export const SuggestedTalentsCount: React.FunctionComponent<ISuggestedTalentsCountProps> = ({
  fromLastLogin,
  formattedJob,
  threshold,
}) => (
  <Query<GetSuggestedTalentsCountQuery, GetSuggestedTalentsCountQueryVariables>
    query={GET_SUGGESTED_TALENTS_COUNT}
    variables={{
      fromLastLogin,
      jobId: formattedJob.uuid,
      threshold,
    }}
    skip={isSuggestedTalentUnavailable(formattedJob)}
  >
    {({loading, error, data}) => {
      if (loading || error || isNil(data) || isNil(data.suggestedTalentsForJob)) {
        return <TotalCount title={SUGGESTED_TALENTS} loading={loading} />;
      }

      const {total} = data.suggestedTalentsForJob;
      const count = total > SUGGESTED_TALENTS_TOTAL_LIMIT ? SUGGESTED_TALENTS_TOTAL_LIMIT : total;
      return <TotalCount count={count} title={SUGGESTED_TALENTS} loading={loading} />;
    }}
  </Query>
);
