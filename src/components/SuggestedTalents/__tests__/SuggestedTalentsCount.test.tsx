import {ApolloGraphQLInteraction, Matchers} from '@pact-foundation/pact';
import {mount} from 'enzyme';
import React from 'react';
import {ApolloProvider} from 'react-apollo';
import {act} from 'react-dom/test-utils';
import {PactBuilder} from '~/__mocks__/pact';
import {SuggestedTalentsCount} from '~/components/SuggestedTalents/SuggestedTalentsCount';
import {DEFAULT_SUGGESTED_TALENTS_THRESHOLD} from '~/flux/suggestedTalents/suggestedTalents.constants';
import {GET_SUGGESTED_TALENTS_COUNT, SUGGESTED_TALENTS_TOTAL_LIMIT} from '~/graphql/suggestedTalents';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {formatJob} from '~/util/jobPosts';
import {format, subYears} from 'date-fns';
import {JobStatusCodes} from '~/services/employer/jobs.types';
// temporarily suppress the following error, which will be fixed when upgrading Graphql package 14.5.8
// error TS7016: Could not find a declaration file for module 'graphql'.
// @ts-ignore
import {print} from 'graphql';

describe('SuggestedTalents/SuggestedTalentsCount', () => {
  let pactBuilder: PactBuilder;
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-profile');
    await pactBuilder.setup();
  });
  afterAll(async () => pactBuilder.provider.finalize());

  const variables = {
    fromLastLogin: '2019-01-01',
    jobId: 'R90SS0001A(2)',
    threshold: DEFAULT_SUGGESTED_TALENTS_THRESHOLD,
  };
  const getSuggestedTalentsCountQuery = new ApolloGraphQLInteraction()
    .withQuery(print(GET_SUGGESTED_TALENTS_COUNT))
    .withOperation('getSuggestedTalentsCount')
    .withRequest({
      method: 'POST',
      path: '/profile',
    })
    .withVariables(variables);

  it('render SuggestedTalentsCount', async () => {
    const queryMockResult = {
      data: {
        suggestedTalentsForJob: {
          total: Matchers.integer(),
        },
      },
    };

    getSuggestedTalentsCountQuery.uponReceiving('getSuggestedTalentsCount').willRespondWith({
      body: queryMockResult,
      status: 200,
    });
    await pactBuilder.provider.addInteraction(getSuggestedTalentsCountQuery);

    const wrapper = mountSuggestedTalentsCount();

    expect(wrapper.find('.spinnerContainer')).toHaveLength(1);

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
    wrapper.update();

    expect(wrapper.find('h4[data-cy="total-count"]').text()).toContain(
      queryMockResult.data.suggestedTalentsForJob.total.getValue(),
    );
  });

  it('render SuggestedTalentsCount with SUGGESTED_TALENTS_TOTAL_LIMIT when the total count is above SUGGESTED_TALENTS_TOTAL_LIMIT', async () => {
    const queryMockResult = {
      data: {
        suggestedTalentsForJob: {
          total: Matchers.integer(SUGGESTED_TALENTS_TOTAL_LIMIT + 1),
        },
      },
    };

    getSuggestedTalentsCountQuery.uponReceiving('getSuggestedTalentsCountAboveLimit').willRespondWith({
      body: queryMockResult,
      status: 200,
    });
    await pactBuilder.provider.addInteraction(getSuggestedTalentsCountQuery);

    const wrapper = mountSuggestedTalentsCount();

    expect(wrapper.find('.spinnerContainer')).toHaveLength(1);

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
    wrapper.update();

    expect(wrapper.find('h4[data-cy="total-count"]').text()).toContain(SUGGESTED_TALENTS_TOTAL_LIMIT);
  });

  it('should not fetch getSuggestedTalentsCount when job status is closed and original posting date past 1 year', () => {
    const wrapper = mountSuggestedTalentsCount({
      ...jobPostMock,
      status: {id: JobStatusCodes.Closed, jobStatus: 'Closed'},
      metadata: {...jobPostMock.metadata, originalPostingDate: format(subYears(new Date(), 1), 'yyyy-MM-dd')},
    });

    expect(wrapper.find('h4[data-cy="total-count"]').text()).toEqual('-');
  });

  const mountSuggestedTalentsCount = (job = {...jobPostMock, uuid: variables.jobId}) =>
    mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <SuggestedTalentsCount
          formattedJob={formatJob(job)}
          threshold={variables.threshold}
          fromLastLogin={variables.fromLastLogin}
        />
      </ApolloProvider>,
    );
});
