import {connect} from 'react-redux';
import {SuggestedTalentsCount} from '~/components/SuggestedTalents/SuggestedTalentsCount';
import {IAppState} from '~/flux/index';

const mapStateToProps = ({suggestedTalents: {fromLastLogin, threshold}}: IAppState) => ({
  fromLastLogin,
  threshold,
});

const SuggestedTalentsCountContainer = connect(mapStateToProps)(SuggestedTalentsCount);

export default SuggestedTalentsCountContainer;
