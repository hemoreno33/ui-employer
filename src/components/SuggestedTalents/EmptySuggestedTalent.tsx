import React from 'react';
import {EmptyState} from '~/components/Layouts/EmptyState';

export const EmptySuggestedTalent: React.FunctionComponent<any> = () => (
  <EmptyState className="ph5 black-30">
    <h1 data-cy="empty-suggested-talent" className="f4 ph3">
      Pick a talent from the left to view profile details.
    </h1>
  </EmptyState>
);
