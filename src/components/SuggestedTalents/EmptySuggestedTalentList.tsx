import React from 'react';
import {EmptyState} from '~/components/Layouts/EmptyState';

const SSG_URL = 'http://www.ssg-wsg.gov.sg/employers.html?activeAcc=4';

export const EmptySuggestedTalentList: React.FunctionComponent<any> = () => (
  <EmptyState className="black-30">
    <h1 className="f3">There are currently no suggested talents for this job.</h1>
    <h2 className="f4">
      Expand your hiring options with our other{' '}
      <a className="fw6 blue no-underline" href={SSG_URL} target="_blank">
        programmes and initiatives
      </a>
    </h2>
  </EmptyState>
);
