import {getOr} from 'lodash/fp';
import {connect} from 'react-redux';
import {compose, withProps} from 'recompose';
import {IAppState} from '~/flux';
import {onJobsSortingTypeClicked} from '~/flux/analytics';
import {CRITERIA_ORDER_BY, JobsSortCriteria} from '~/flux/jobPostings';
import {SortDirection} from '~/graphql/__generated__/types';
import {IJobsSortSelectionProps, JobsSortSelection} from './JobsSortSelection';

export interface IFetchJobsParams {
  value: JobsSortCriteria;
  orderDirection?: SortDirection;
}

export interface IJobsSortSelectionPassedProps {
  fetchJobs: (props: IFetchJobsParams) => void;
  jobsStatus: string;
}

export const mapStateToProps = (state: IAppState) => {
  return {
    sortCriteriaClosedJobs: getOr('', 'jobPostings.closedJobsParams.orderBy', state),
    sortCriteriaOpenJobs: getOr('', 'jobPostings.openJobsParams.orderBy', state),
  };
};

export const setSortingCriteriaWithProps = ({
  fetchJobs,
  dispatch,
  jobsStatus,
}: {
  fetchJobs: (props: IFetchJobsParams) => void;
  dispatch: (props: any) => any;
  jobsStatus: string;
}) => {
  return {
    setSortingCriteria: (criteria: JobsSortCriteria): void => {
      dispatch(onJobsSortingTypeClicked(criteria));
      const orderDirection = CRITERIA_ORDER_BY[jobsStatus][criteria];
      fetchJobs({value: criteria, orderDirection});
    },
  };
};

export const JobsSortSelectionContainer = compose<IJobsSortSelectionProps, IJobsSortSelectionPassedProps>(
  connect(mapStateToProps),
  withProps(setSortingCriteriaWithProps),
)(JobsSortSelection);
