import {isNil} from 'lodash/fp';
import React from 'react';
import {compose, lifecycle} from 'recompose';
import styles from '~/components/ApplicationsToolBar/ApplicationsToolBar.scss';
import {ITopMatchCheckboxKeyProps} from './TopMatchCheckboxContainer';

export type ITopMatchCheckboxComponentProps = ITopMatchCheckboxKeyProps & ITopMatchCheckboxComponentOwnProps;

export interface ITopMatchCheckboxComponentOwnProps {
  total?: number | null;
}

export const TopMatchCheckboxComponent = ({onTopMatchChange, checked, total}: ITopMatchCheckboxComponentProps) => {
  return (
    <label>
      <input
        data-cy="top-match-checkbox"
        className={styles.inputCheckbox}
        type="checkbox"
        onChange={onTopMatchChange}
        checked={checked}
        disabled={!total}
      />
      {total ? (
        <span className="f5 pr3 black-80 pointer">{`Show "Among top matches" (${total})`}</span>
      ) : (
        <span
          className={`f5 pr3 black-80 cursor-not-allowed ${styles.checkboxInactive}`}
        >{`Show "Among top matches" (0)`}</span>
      )}
    </label>
  );
};

const setApplicationTopMatchTotalWithLifecycle = lifecycle({
  componentDidUpdate(prevProps: ITopMatchCheckboxComponentProps) {
    const {getApplicationTopMatchTotal, total, sortCriteria, jobUuid} = this.props;
    if (!(prevProps.sortCriteria === sortCriteria && prevProps.total === total)) {
      if (!isNil(total)) {
        getApplicationTopMatchTotal(total, sortCriteria, jobUuid);
      }
    }
  },
});

export const TopMatchCheckbox = compose<ITopMatchCheckboxKeyProps, ITopMatchCheckboxComponentOwnProps>(
  setApplicationTopMatchTotalWithLifecycle,
)(TopMatchCheckboxComponent);
