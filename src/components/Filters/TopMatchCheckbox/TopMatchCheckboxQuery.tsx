import React from 'react';
import {Query} from 'react-apollo';
import {getTopMatchFilter} from '~/flux/applications';
import {
  GetApplicationsTopMatchTotalQuery,
  GetApplicationsTopMatchTotalQueryVariables,
} from '~/graphql/__generated__/types';
import {GET_APPLICATIONS_TOP_MATCH_TOTAL} from '~/graphql/applications/applications.query';
import {TopMatchCheckbox} from './TopMatchCheckbox';
import {ITopMatchCheckboxKeyProps} from './TopMatchCheckboxContainer';

export const TopMatchCheckboxQuery: React.FunctionComponent<ITopMatchCheckboxKeyProps> = (props) => {
  const {sortCriteria, jobUuid} = props;
  return (
    <Query<GetApplicationsTopMatchTotalQuery, GetApplicationsTopMatchTotalQueryVariables>
      query={GET_APPLICATIONS_TOP_MATCH_TOTAL}
      variables={{
        filterBy: getTopMatchFilter(sortCriteria),
        jobId: jobUuid,
      }}
    >
      {({data}) => {
        const total = data?.applicationsForJob?.total;
        return <TopMatchCheckbox {...props} total={total} />;
      }}
    </Query>
  );
};
