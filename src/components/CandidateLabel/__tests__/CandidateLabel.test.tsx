import {mount} from 'enzyme';
import React from 'react';
import {CandidateLabel, CandidateType} from '../CandidateLabel';

describe('CandidateLabel', () => {
  it('should render Applicant label with the right color correctly', () => {
    const candidateLabelProps = {
      label: CandidateType.applicant,
    };
    const component = mount(<CandidateLabel {...candidateLabelProps} />);

    expect(component).toMatchSnapshot();
  });
  it('should render Suggested Talent label with the right color correctly', () => {
    const talentLabel = {
      label: CandidateType.suggestedTalent,
    };
    const component = mount(<CandidateLabel {...talentLabel} />);

    expect(component).toMatchSnapshot();
  });
});
