import {mount} from 'enzyme';
import React from 'react';
import {Link, MemoryRouter as Router} from 'react-router-dom';
import {SurveyBar} from '../SurveyBar';

describe('SurveyBar', () => {
  it('should render Link to survey, and take to survey once clicked', () => {
    const surveyBarProps = {
      closeSurvey: jest.fn(),
      takeSurvey: jest.fn(),
    };
    const wrapper = mount(
      <Router>
        <SurveyBar {...surveyBarProps} />
      </Router>,
    );

    const surveyLink = wrapper.find(Link);

    surveyLink.simulate('click');
    expect(surveyLink.props().to).toBe('/survey');
    expect(surveyBarProps.takeSurvey).toHaveBeenCalledTimes(1);
  });

  it('should render Close survey', () => {
    const surveyBarProps = {
      closeSurvey: jest.fn(),
      takeSurvey: jest.fn(),
    };
    const wrapper = mount(
      <Router>
        <SurveyBar {...surveyBarProps} />
      </Router>,
    );

    const closeSurveyLink = wrapper.find('[data-cy="surveybar-close-button"]');

    closeSurveyLink.simulate('click');
    expect(surveyBarProps.closeSurvey).toHaveBeenCalledTimes(1);
  });

  it('should renders correctly', () => {
    const surveyBarProps = {
      closeSurvey: jest.fn(),
      takeSurvey: jest.fn(),
    };

    const wrapper = mount(
      <Router keyLength={0}>
        <SurveyBar {...surveyBarProps} />
      </Router>,
    );

    expect(wrapper).toMatchSnapshot();
  });
});
