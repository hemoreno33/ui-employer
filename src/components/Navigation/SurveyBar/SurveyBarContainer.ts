import {connect} from 'react-redux';
import {ActionType} from 'typesafe-actions';
import {closeSurvey, takeSurvey} from '~/flux/survey/survey.actions';
import {SurveyBar} from './SurveyBar';

export interface ISurveyBarDispatchProps {
  closeSurvey: () => ActionType<typeof closeSurvey>;
  takeSurvey: () => ActionType<typeof takeSurvey>;
}

export const SurveyBarContainer = connect(null, {
  closeSurvey,
  takeSurvey,
})(SurveyBar);
