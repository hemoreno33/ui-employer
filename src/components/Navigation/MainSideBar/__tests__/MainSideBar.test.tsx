import {mount} from 'enzyme';
import React from 'react';
import {NavLink, MemoryRouter as Router} from 'react-router-dom';
import {CreateJobPostingButton} from '~/components/Navigation/NewJobPostButton/CreateJobPostingButton';
import * as randomUtil from '~/util/generateRandomString';
import {MainSideBar} from '../MainSideBar';
import * as jobPostingsMock from '~/flux/jobPostings';

jest.spyOn(randomUtil, 'generateRandomString').mockImplementation(() => 'randomString');

jest.mock('~/components/Navigation/CompanyDetails/CompanyDetailsContainer', () => {
  return {
    CompanyDetailsContainer: () => 'CompanyDetailsContainer',
  };
});

jest.mock('~/flux/jobPostings');

describe('MainSideBar', () => {
  it('should render MainSideBar correctly', () => {
    const wrapper = mount(
      <Router keyLength={0}>
        <MainSideBar />
      </Router>,
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('renders NewJobPostButton', () => {
    const wrapper = mount(
      <Router>
        <MainSideBar />
      </Router>,
    );

    const button = wrapper.find(CreateJobPostingButton);
    expect(button).toHaveLength(1);
  });

  it('renders Link to homepage', () => {
    const wrapper = mount(
      <Router>
        <MainSideBar />
      </Router>,
    );

    const jobLink = wrapper.find(NavLink).findWhere((x) => x.prop('to') === '/jobs');
    expect(jobLink).toHaveLength(1);
    jobLink.simulate('click');
    expect(jobPostingsMock.resetJobPostingsParams).toHaveBeenCalledTimes(1);
  });
});
