import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import * as styles from './header.scss';
import {IHeaderDispatchProps, IHeaderOwnProps} from './HeaderContainer';

export type IHeaderProps = IHeaderOwnProps & IHeaderDispatchProps;

export const Header: React.FunctionComponent<IHeaderProps> = ({crumbs = [], resetJobPostingsParams}) => {
  const crumbElements = crumbs.map((c, i) => {
    const colorArray = [`bg-primary`, `bg-primary-dark ${styles.divider}`];
    const segment =
      (c as string).toLowerCase() === 'all jobs' && crumbs.length > 1 ? (
        <div data-cy="all-jobs-top-label" className={`ph4 pv3 ${colorArray[i]} ${styles.firstcrumb}`}>
          <Link className={`${styles.link} underline`} to="/jobs" onClick={resetJobPostingsParams}>
            {c}
          </Link>
        </div>
      ) : (
        <div className={`ph4 pv3 flex-auto relative truncate ${colorArray[i]}`}>{c}</div>
      );

    return <Fragment key={i}>{segment}</Fragment>;
  });
  return <header className=" white f5 flex bg-primary">{crumbElements}</header>;
};
