import {ReactChild} from 'react';
import {connect} from 'react-redux';
import {ActionType} from 'typesafe-actions';
import {resetJobPostingsParams} from '~/flux/jobPostings';
import {Header} from './Header';

export interface IHeaderDispatchProps {
  resetJobPostingsParams: () => ActionType<typeof resetJobPostingsParams>;
}
export interface IHeaderOwnProps {
  crumbs?: ReactChild[];
}

export const HeaderContainer = connect<void, IHeaderDispatchProps, IHeaderOwnProps>(null, {resetJobPostingsParams})(
  Header,
);
