import {connect} from 'react-redux';
import {getAccountUser, IUser} from '~/flux/account';
import {onLogoutClicked, onNavigationMenuLinkClicked} from '~/flux/analytics';
import {IAppState} from '~/flux/index';
import {ISurveyState} from '~/flux/survey/survey.reducer';
import {MainNavigation} from './MainNavigation';

export interface IMainNavigationProps extends IMainNavigationDispatchProps {
  user?: IUser;
  survey: ISurveyState;
}

export interface IMainNavigationDispatchProps {
  onNavigationMenuLinkClicked: typeof onNavigationMenuLinkClicked;
  onLogoutClicked: typeof onLogoutClicked;
}

export const mapStateToProps = (state: IAppState) => ({user: getAccountUser(state), survey: state.survey});

export const MainNavigationContainer = connect(mapStateToProps, {onLogoutClicked, onNavigationMenuLinkClicked})(
  MainNavigation,
);
