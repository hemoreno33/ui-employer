import {StickyHeader} from '@govtechsg/mcf-mcfui';
import React from 'react';
import {Link} from 'react-router-dom';
import {MAIN_NAVIGATION_MENU_LINK_TYPE} from '~/analytics/mainNavigationMenu';
import {Row} from '~/components/Layouts/Row';
import {PostLoginMenu, PreLoginMenuContainer} from '~/components/Navigation/MainNavigationMenu';
import styles from './MainNavigation.scss';

import {IMainNavigationProps} from '~/components/Navigation/MainNavigation';
import {SurveyBarContainer} from '~/components/Navigation/SurveyBar/SurveyBarContainer';
import {PostJobsLinkContainer} from './PostJobsLinkContainer';

export const MainNavigation: React.FunctionComponent<IMainNavigationProps> = ({
  onLogoutClicked,
  onNavigationMenuLinkClicked,
  user,
  survey,
}) => {
  return (
    <StickyHeader>
      <Row className="bg-white w-100 z-5 shadow-2 top-0 h-auto z-999">
        <>
          {survey.hasShown ? <SurveyBarContainer /> : null}
          <div id="main-nav-child" className="ph3-ns pv0 center">
            <div className={`flex flex-wrap items-center justify-between ${styles.mainNavigation}`}>
              <div className="pa2 flex items-center">
                <Link to="/" className={`no-underline pointer ${styles.logo}`} id="logo" data-cy="mcf-logo">
                  <i className={`ml2 ${styles.ccLogo}`} />
                  <span className={`pl3 pr2 fw4 f5-5 lh-solid ${styles.employerTextlogo}`}>Employer</span>
                </Link>
                <a
                  href="http://www.wsg.gov.sg/adapt-and-grow.html"
                  target="_blank"
                  data-cy="aag-logo"
                  onClick={() => onNavigationMenuLinkClicked(MAIN_NAVIGATION_MENU_LINK_TYPE.WSG_LOGO)}
                >
                  <i className={`ml2 bl b--light-silver ${styles.wsgLogo}`} />
                </a>
                <span className="clip">My careers future | An adapt and grow initiative</span>
                <a
                  data-cy="switch-jobseeker"
                  href="https://www.mycareersfuture.sg"
                  className={`fw6 no-underline pv1 ph2 mh2 lh-title f7 br1 ba b--dark-pink secondary ${styles.switchBtn}`}
                  onClick={() => onNavigationMenuLinkClicked(MAIN_NAVIGATION_MENU_LINK_TYPE.JOBSEEKER)}
                >
                  Switch to Jobseeker
                </a>
              </div>
              <nav className={`${styles.nav}`}>
                <PostJobsLinkContainer />
                <a
                  data-cy="employers-toolkit-link"
                  href="https://content.mycareersfuture.sg/category/employers-toolkit"
                  target="_blank"
                  className={`fw6 no-underline v-mid pv1 ph3 f5 black-60 ${styles.employersToolkitLink}`}
                >
                  Employers Toolkit
                </a>
                {user ? <PostLoginMenu user={user} logoutClicked={onLogoutClicked} /> : <PreLoginMenuContainer />}
              </nav>
            </div>
          </div>
        </>
      </Row>
    </StickyHeader>
  );
};
