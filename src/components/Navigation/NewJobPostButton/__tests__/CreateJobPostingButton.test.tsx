import {shallow} from 'enzyme';
import React from 'react';
import * as randomUtil from '~/util/generateRandomString';
import {CreateJobPostingButton} from '../CreateJobPostingButton';

jest.spyOn(randomUtil, 'generateRandomString').mockImplementation(() => 'randomString');

describe('CreateJobPostingButton', () => {
  it('renders correctly', () => {
    const wrapper = shallow(<CreateJobPostingButton />);
    expect(wrapper).toMatchSnapshot();
  });
});
