import React from 'react';
import styles from './CreateJobPostingButton.scss';
import {CreateJobPostingLink} from './CreateJobPostingLink';

interface ICreateJobPostingButtonProps {
  dataCy?: string;
}

export const CreateJobPostingButton: React.FunctionComponent<ICreateJobPostingButtonProps> = ({
  dataCy = 'create-job-posting-button',
}) => (
  <CreateJobPostingLink
    className={`db pr3 white fw6 no-underline lh-solid f6 bg-blue br-pill relative ${styles.newJobPost}`}
    dataCy={dataCy}
  />
);
