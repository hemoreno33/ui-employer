import React from 'react';
import {NavLoginLink} from '~/components/Login';
import {IPreLoginMenuProps} from './PreLoginMenuContainer';

export const PreLoginMenu: React.FunctionComponent<IPreLoginMenuProps> = ({login}) => {
  return <NavLoginLink onClick={login} />;
};
