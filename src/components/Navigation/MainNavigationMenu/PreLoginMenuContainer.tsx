import {connect} from 'react-redux';
import {PreLoginMenu} from '~/components/Navigation/MainNavigationMenu/PreLoginMenu';
import {login} from '~/flux/account';

export interface IPreLoginMenuProps {
  login: typeof login;
}

export const PreLoginMenuContainer = connect(null, {login})(PreLoginMenu);
