import {shallow} from 'enzyme';
import React from 'react';
import {PreLoginMenu} from '../PreLoginMenu';

describe('PreLoginMenu', () => {
  it('renders correctly', () => {
    const wrapper = shallow(<PreLoginMenu login={jest.fn()} />);
    expect(wrapper).toMatchSnapshot();
  });
});
