import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {uenUserMock} from '~/__mocks__/account/account.mocks';
import {cleanSnapshot} from '~/testUtil/enzyme';
import {PostLoginMenu} from '../PostLoginMenu';

describe('PostLoginMenu', () => {
  it('renders correctly', () => {
    const wrapper = mount(<PostLoginMenu logoutClicked={jest.fn()} user={uenUserMock} />);
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });
});
