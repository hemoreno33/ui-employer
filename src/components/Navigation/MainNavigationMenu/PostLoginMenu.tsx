import Tooltip from 'rc-tooltip';
import React from 'react';
import {Link} from 'react-router-dom';
import {IUser} from '~/flux/account';
import {onLogoutClicked} from '~/flux/analytics';
import styles from './MainNavigationMenu.scss';

interface IPostLoginMenuProps {
  user: IUser;
  logoutClicked: typeof onLogoutClicked;
}

export const PostLoginMenu: React.FunctionComponent<IPostLoginMenuProps> = ({user, logoutClicked}) => (
  <Tooltip
    placement="bottom"
    trigger={['hover']}
    overlayClassName={`absolute z-999 bg-white shadow-1 tl`}
    destroyTooltipOnHide={true}
    overlay={
      <Link
        className={`pa3 w5 no-underline fw6 f5 db ${styles.submenuUser}`}
        to="/logout"
        id="logout-button"
        onClick={() => logoutClicked()}
      >
        Logout
      </Link>
    }
  >
    <a
      href="#"
      className={`relative no-underline fw6 truncate pv1 v-mid dib ${styles.menuUser}`}
      title={user.userInfo.userFullName}
      data-cy="user-fullname"
    >
      {user.userInfo.userFullName}
    </a>
  </Tooltip>
);
