import React from 'react';
import styles from './ErrorCard.scss';

export const ErrorCard: React.FunctionComponent<any> = ({message}: any) => (
  <div data-cy="error-card" className="bg-dark-red pa3">
    <div className={`f5 fw4 white ${styles.errorIcon}`}> {message} </div>
  </div>
);
