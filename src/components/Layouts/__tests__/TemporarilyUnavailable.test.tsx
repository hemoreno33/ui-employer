import {mount} from 'enzyme';
import React from 'react';
import {TemporarilyUnavailable} from '../TemporarilyUnavailable';

describe('Layouts/TemporarilyUnavailable', () => {
  it('should render TemporarilyUnavailable correctly', () => {
    const linkAction = jest.fn();
    const component = mount(
      <TemporarilyUnavailable message="some message" link={{text: 'some link', action: linkAction}} />,
    );
    const temporarilyUnavailable = component.find(TemporarilyUnavailable);
    expect(temporarilyUnavailable).toHaveLength(1);
    expect(temporarilyUnavailable).toMatchSnapshot();

    const temporarilyUnavailableMessage = temporarilyUnavailable.find('h1');
    expect(temporarilyUnavailableMessage.text()).toEqual('some message');

    const temporarilyUnavailableLink = temporarilyUnavailable.find('a');
    temporarilyUnavailableLink.simulate('click');
    expect(temporarilyUnavailableLink.text()).toEqual('some link');
    expect(linkAction).toBeCalledTimes(1);
  });

  it('should not render link when TemporarilyUnavailable props does not specify it', () => {
    const component = mount(<TemporarilyUnavailable message="some message" />);
    const temporarilyUnavailable = component.find(TemporarilyUnavailable);
    expect(temporarilyUnavailable).toHaveLength(1);

    const temporarilyUnavailableMessage = temporarilyUnavailable.find('h1');
    expect(temporarilyUnavailableMessage.text()).toEqual('some message');

    const temporarilyUnavailableLink = temporarilyUnavailable.find('a');
    expect(temporarilyUnavailableLink).toHaveLength(0);
  });
});
