import {mount} from 'enzyme';
import React from 'react';
import {MemoryRouter as Router} from 'react-router-dom';
import {Footer} from '../Footer';

describe('Layouts/Footer', () => {
  it('should render Footer correctly', () => {
    const component = mount(
      <Router>
        <Footer />
      </Router>,
    );
    const bottomFooter = component.find(Footer);
    expect(bottomFooter).toHaveLength(1);
    expect(bottomFooter).toMatchSnapshot();
  });
});
