import {times} from 'lodash/fp';
import React from 'react';
import styles from './Loader.scss';

interface ICardLoaderProps {
  className?: string;
  cardNumber?: number;
  boxNumber?: number;
  cardGap?: number;
}

export const CardLoader: React.FunctionComponent<ICardLoaderProps> = ({
  className = '',
  cardNumber = 1,
  boxNumber = 0,
  cardGap = 2,
}) => {
  return (
    <section className={className}>
      {times(
        (num) => (
          <div className={`bg-white w-100 pa3 o-50 mb${cardGap} flex`} key={num}>
            <div data-cy="card-loader" className="flex-auto mr3">
              <div className={`w-30 h1 ${styles.animatedGradient}`} />
              <div className={`w-100 h1 mt2 ${styles.animatedGradient}`} />
              <div className={`w-90 h1 mt2 ${styles.animatedGradient}`} />
            </div>

            {times(
              (boxnum) => (
                <div key={boxnum} className={`w4 h3 ml3 ${styles.animatedGradient}`} />
              ),
              boxNumber,
            )}
          </div>
        ),
        cardNumber,
      )}
    </section>
  );
};
