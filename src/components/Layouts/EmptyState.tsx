import React from 'react';

export interface IEmptyStateProps {
  children: React.ReactNode;
  className?: string;
}

export const EmptyState: React.FunctionComponent<IEmptyStateProps> = ({children, className = ''}) => (
  <div className={`w-100 pv6 lh-copy tc ${className}`}>{children}</div>
);
