import {times} from 'lodash/fp';
import React from 'react';
import styles from './Loader.scss';

interface IFormLoaderProps {
  className?: string;
  cardNumber?: number;
  hasTextAreaLoader?: boolean;
  // hasTextAreaLoader flag determines whether text area loader is present.
  // When it is disabled, it is used as skills loader (job posting step 2), else it will be used for transitioning between forms pages
}

export const FormLoader: React.FunctionComponent<IFormLoaderProps> = ({
  className = '',
  cardNumber = 1,
  hasTextAreaLoader = true,
}) => {
  return (
    <section className={className} data-cy="form-loader">
      {times(
        (num) => (
          <div key={num} className={`w-100 pa3 mb2 flex mr2 flex-wrap`}>
            <div className="w-50 pr2">
              <div className={`w-30 h1 ${styles.animatedGradient}`} />
              <div className={`w-100 h2 mt3 ${styles.animatedGradient}`} />
            </div>

            <div className="w-50 pl2">
              <div className={`w-30 h1 ${styles.animatedGradient}`} />
              <div className={`w-100 h2 mt3 ${styles.animatedGradient}`} />
            </div>

            {hasTextAreaLoader ? (
              <>
                <div className={`w-40 h1 mt4 pt2 ${styles.animatedGradient}`} />
                <div className={`w-100 h4 mt3 pt2 ${styles.animatedGradient}`} />
              </>
            ) : (
              ''
            )}
          </div>
        ),
        cardNumber,
      )}
    </section>
  );
};
