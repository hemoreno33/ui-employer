import React from 'react';

export const PanesLayout: React.FunctionComponent<any> = ({left, right}: any) => (
  <div className="flex justify-start w-100">
    {left}
    {right}
  </div>
);
