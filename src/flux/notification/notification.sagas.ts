import {all, call, put, takeLatest, SagaReturnType} from 'redux-saga/effects';
import {getNotifications} from '~/services/notification/getNotifications';
import {notificationFetchFailed, notificationFetchSucceeded} from './notification.actions';
import {NOTIFICATION_FETCH_REQUESTED} from './notification.constants';

export function* fetchNotifications() {
  try {
    const notifications: SagaReturnType<typeof getNotifications> = yield call(getNotifications);
    yield put(notificationFetchSucceeded(notifications));
  } catch (_) {
    yield put(notificationFetchFailed());
  }
}

export function* notificationSaga() {
  yield all([takeLatest(NOTIFICATION_FETCH_REQUESTED, fetchNotifications)]);
}
