import {action} from 'typesafe-actions';
import {CLOSE_SURVEY, MAKE_SURVEY_HIDDEN, MAKE_SURVEY_VISIBLE, TAKE_SURVEY} from '~/flux/survey/survey.constants';

export const makeSurveyVisible = () => action(MAKE_SURVEY_VISIBLE);
export const makeSurveyHidden = () => action(MAKE_SURVEY_HIDDEN);
export const closeSurvey = () => action(CLOSE_SURVEY);
export const takeSurvey = () => action(TAKE_SURVEY);
