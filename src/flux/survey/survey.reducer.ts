import {ActionType} from 'typesafe-actions';
import * as actions from '~/flux/survey/survey.actions';
import {MAKE_SURVEY_HIDDEN, MAKE_SURVEY_VISIBLE} from '~/flux/survey/survey.constants';

export interface ISurveyState {
  hasShown: boolean;
}

const initialState: ISurveyState = {
  hasShown: false,
};

export type SurveyActionType = ActionType<typeof actions>;

export const survey = (state: ISurveyState = initialState, action: SurveyActionType): ISurveyState => {
  switch (action.type) {
    case MAKE_SURVEY_HIDDEN:
      return {
        ...state,
        hasShown: false,
      };
    case MAKE_SURVEY_VISIBLE:
      return {
        ...state,
        hasShown: true,
      };
    default:
      return state;
  }
};
