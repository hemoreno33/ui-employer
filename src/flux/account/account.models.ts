import {ISystemContact} from '../systemContact/systemContact.constants';

export interface IUser {
  userInfo: IUserInfo;
  exp: number;
  iss: string;
  authorization: IPermission[];
  additionalInfo?: {
    data: {
      systemContact?: Pick<ISystemContact, 'termsAndConditionsAcceptedAt'>;
    };
  };
}

export interface IUserInfo {
  accountType: string;
  userId: string;
  userCountry: string;
  userFullName: string;
  userName: string;
  singpassHolder: boolean;
  entityId: string;
  entityStatus?: EntityStatus;
  entityType: EntityType;
  entityRegNo?: string;
  entityCountry?: string;
  entityName?: string;
}

export interface IPermission {
  id: string;
  scopes: string[];
}

export type EntityStatus = 'Registered' | 'De-Registered' | 'Withdrawn';

export type EntityType = 'UEN' | 'NON-UEN';
