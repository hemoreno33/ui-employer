import {SESSION_EXPIRED} from '@govtechsg/redux-account-middleware';
import {push} from 'connected-react-router';
import {all, put, takeEvery, takeLatest} from 'redux-saga/effects';
import {config} from '~/config';
import {LoginActionTypes} from './account.constants';

export const loginRedirect = () => {
  window.location.assign(`${config.url.corppass}?target=${encodeURIComponent(window.location.origin)}`);
};

export function* sessionExpiredRedirect() {
  yield put(push('/session-timeout'));
}

export function* accountSaga() {
  yield all([
    yield takeEvery(LoginActionTypes.LOGIN, loginRedirect),
    yield takeLatest(SESSION_EXPIRED, sessionExpiredRedirect),
  ]);
}
