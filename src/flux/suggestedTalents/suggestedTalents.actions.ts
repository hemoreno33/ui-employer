import {action} from 'typesafe-actions';
import {
  ON_SUGGESTED_TALENT_CLICKED,
  SET_SUGGESTED_TALENTS_THRESHOLD,
} from '~/flux/suggestedTalents/suggestedTalents.constants';

export const onSuggestedTalentClicked = (talentId: string, position: number, jobUuid: string) =>
  action(ON_SUGGESTED_TALENT_CLICKED, {talentId, position, jobUuid});

export const setSuggestedTalentsThreshold = (threshold: number) => action(SET_SUGGESTED_TALENTS_THRESHOLD, {threshold});
