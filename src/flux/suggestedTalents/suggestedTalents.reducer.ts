import {format, subMonths} from 'date-fns';
import {ActionType} from 'typesafe-actions';
import * as actions from '~/flux/suggestedTalents/suggestedTalents.actions';
import {
  DEFAULT_SUGGESTED_TALENTS_FROM_LAST_LOGIN_BY_MONTHS,
  DEFAULT_SUGGESTED_TALENTS_THRESHOLD,
  SET_SUGGESTED_TALENTS_THRESHOLD,
} from '~/flux/suggestedTalents/suggestedTalents.constants';

export interface ISuggestedTalentsState {
  fromLastLogin: string;
  threshold: number;
}

const initialState: ISuggestedTalentsState = {
  fromLastLogin: format(subMonths(new Date(), DEFAULT_SUGGESTED_TALENTS_FROM_LAST_LOGIN_BY_MONTHS), 'yyyy-MM-dd'),
  threshold: DEFAULT_SUGGESTED_TALENTS_THRESHOLD,
};

export type SuggestedTalentsActionType = ActionType<typeof actions>;

export const suggestedTalents = (
  state: ISuggestedTalentsState = initialState,
  action: SuggestedTalentsActionType,
): ISuggestedTalentsState => {
  switch (action.type) {
    case SET_SUGGESTED_TALENTS_THRESHOLD:
      return {
        ...state,
        threshold: action.payload.threshold,
      };
    default:
      return state;
  }
};
