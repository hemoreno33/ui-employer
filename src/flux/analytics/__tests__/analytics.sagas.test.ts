import {accountReceived} from '@govtechsg/redux-account-middleware';
import {testSaga} from 'redux-saga-test-plan';
import {uenUserMock} from '~/__mocks__/account/account.mocks';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {handleEmployerLogin, handleEmployerLogout, tagUserSession} from '~/analytics/account';
import {
  handleApplicationSortingType,
  handleApplicationTopMatchFilter,
  handleApplicationTopMatchLabel,
  handleApplicationTopMatchTotal,
} from '~/analytics/applications';
import {
  handleCandidateTabClick,
  handleCandidatesPageClick,
  handleCandidateResumeClick,
  handleCandidateClick,
} from '~/analytics/candidates';
import {handleAllJobsSortingType, handlePageChange} from '~/analytics/filters';
import {handleCorpPassGetStartedClick} from '~/analytics/homeBanner';
import {handleJobsSearchQueryEvent} from '~/analytics/jobsSearchQuery';
import {mainNavigationMenuLinks, MAIN_NAVIGATION_MENU_LINK_TYPE} from '~/analytics/mainNavigationMenu';
import {startJobPosting, completeJobPosting} from '~/analytics/pages';
import {ApplicationSortCriteria} from '~/flux/applications';
import {getLocation} from '~/flux/location';
import {onSuggestedTalentClicked} from '~/flux/suggestedTalents/suggestedTalents.actions';
import {DOWNLOAD_RESUME_GA_LABEL, EVENT_CATEGORY} from '~/util/analytics';
import {
  getApplicationTopMatchTotal,
  onApplicationCandidateResumeClicked,
  onApplicationCandidateTabClicked,
  onApplicationClicked,
  onApplicationPageClicked,
  onApplicationSortingTypeClicked,
  onApplicationTopMatchFilter,
  onJobSearchQueried,
  onJobsSortingTypeClicked,
  onNavigationMenuLinkClicked,
  onPageChangeClicked,
  onSuggestedTalentCandidateTabClicked,
  onSuggestedTalentsPageClicked,
  onSuggestedTalentsResumeClicked,
  onBookmarkedCandidatesPageClicked,
  onBookmarkedCandidateResumeClicked,
  onBookmarkedCandidateTabClicked,
  onBookmarkedCandidateClicked,
} from '../analytics.actions';
import {
  corpPassGetStartedEvent,
  employerLoginEvent,
  mainNavigationMenuLinkEvent,
  sendAllJobsSortingTypeEvent,
  sendApplicationCandidateResumeEvent,
  sendApplicationCandidateTabEvent,
  sendApplicationClickEvent,
  sendApplicationPageEvent,
  sendApplicationSortingTypeEvent,
  sendApplicationTopMatchFilterEvent,
  sendApplicationTopMatchLabelEvent,
  sendApplicationTopMatchTotal,
  sendJobsSearchQueryEvent,
  sendLogoutEvent,
  sendPageChangeEvent,
  sendSuggestedTalentClickEvent,
  sendSuggestedTalentsPageEvent,
  sendSuggestedTalentsResumeEvent,
  sendSuggestedTalentTabEvent,
  tagSessionWithEntityId,
  sendStartJobPosting,
  sendCompleteJobPosting,
  sendBookmarkedCandidatesPageEvent,
  sendBookmarkedCandidateResumeEvent,
  sendBookmarkedCandidateTabEvent,
  sendBookmarkedCandidateClickEvent,
} from '../analytics.sagas';
import {
  bookmarkedApplicationMock,
  bookmarkedTalentMock,
} from '~/__mocks__/bookmarkedCandidates/bookmarkedCandidates.mock';

describe('analytics sagas', () => {
  describe('corpPassGetStartedEvent', () => {
    it('should send corpPassGetStartedEvent to GA', () => {
      testSaga(corpPassGetStartedEvent)
        .next()
        .call(handleCorpPassGetStartedClick)
        .next()
        .isDone();
    });
  });
  describe('mainNavigationMenuLinkEvent', () => {
    it('should send mainNavigationMenuLinkEvent and fires specific event to GA', () => {
      const payload = {
        title: MAIN_NAVIGATION_MENU_LINK_TYPE.WSG_LOGO,
      };
      const pathname = '/';
      testSaga(mainNavigationMenuLinkEvent, onNavigationMenuLinkClicked(payload.title))
        .next()
        .select(getLocation)
        .next({pathname})
        .call(mainNavigationMenuLinks, payload.title, pathname)
        .next()
        .isDone();
    });
  });
  describe('employerLoginEvent', () => {
    it('should send corpPassGetStartedEvent to GA', () => {
      testSaga(employerLoginEvent)
        .next()
        .call(handleEmployerLogin)
        .next()
        .isDone();
    });
  });
  describe('sendAllJobsSortingTypeEvent', () => {
    it('should sendAllJobsSortingTypeEvent and fires specific event to GA', () => {
      const payload = {
        sortType: 'descending',
      };
      testSaga(sendAllJobsSortingTypeEvent, onJobsSortingTypeClicked(payload.sortType))
        .next()
        .call(handleAllJobsSortingType, payload.sortType)
        .next()
        .isDone();
    });
  });
  describe('sendPageChangeEvent', () => {
    it('should sendPageChangeEvent to GA', () => {
      const page = 5;
      const jobStatus = 'ALL_JOBS_OPEN';
      testSaga(sendPageChangeEvent, onPageChangeClicked({jobStatus, page}))
        .next()
        .call(handlePageChange, jobStatus, page)
        .next()
        .isDone();
    });
  });
  describe('sendLogoutEvent', () => {
    it('should sendLogoutEvent to GA', () => {
      const pathname = '/jobs';
      testSaga(sendLogoutEvent)
        .next()
        .select(getLocation)
        .next({pathname})
        .call(handleEmployerLogout, pathname)
        .next()
        .isDone();
    });
  });
  describe('sendJobsSearchQueryEvent', () => {
    it('should sendJobsSearchQueryEvent to GA', () => {
      const payload = 'random query';
      testSaga(sendJobsSearchQueryEvent, onJobSearchQueried(payload))
        .next()
        .call(handleJobsSearchQueryEvent, payload)
        .next()
        .isDone();
    });
  });
  describe('sendApplicationCandidateTabEvent', () => {
    it('should sendApplicationCandidateTabEvent to GA', () => {
      const payload = 2;
      const candidateType = 'Job-Applicant';
      testSaga(sendApplicationCandidateTabEvent, onApplicationCandidateTabClicked(payload))
        .next()
        .call(handleCandidateTabClick, payload, candidateType)
        .next()
        .isDone();
    });
  });
  describe('sendApplicationPageEvent', () => {
    it('should sendApplicationPageEvent to GA', () => {
      const payload = {
        jobId: 'id1234',
        page: 2,
      };
      testSaga(sendApplicationPageEvent, onApplicationPageClicked(payload.jobId, payload.page))
        .next()
        .call(handleCandidatesPageClick, payload, EVENT_CATEGORY.JOB_APPLICANT)
        .next()
        .isDone();
    });
  });
  describe('sendApplicationCandidateResumeEvent', () => {
    it('should sendApplicationCandidateResumeEvent to GA', () => {
      testSaga(sendApplicationCandidateResumeEvent, onApplicationCandidateResumeClicked(DOWNLOAD_RESUME_GA_LABEL.LIST))
        .next()
        .call(handleCandidateResumeClick, DOWNLOAD_RESUME_GA_LABEL.LIST, EVENT_CATEGORY.JOB_APPLICANT)
        .next()
        .isDone();
    });
  });
  describe('sendApplicationSortingTypeEvent', () => {
    it('should sendApplicationSortingTypeEvent to GA', () => {
      const payload = ApplicationSortCriteria.SCORES_WCC;
      testSaga(sendApplicationSortingTypeEvent, onApplicationSortingTypeClicked(payload))
        .next()
        .call(handleApplicationSortingType, payload)
        .next()
        .isDone();
    });
  });
  describe('sendApplicationTopMatchFilterEvent', () => {
    it('should sendApplicationTopMatchFilterEvent to GA', () => {
      const payload = true;
      testSaga(sendApplicationTopMatchFilterEvent, onApplicationTopMatchFilter(payload))
        .next()
        .call(handleApplicationTopMatchFilter, payload)
        .next()
        .isDone();
    });
  });
  describe('sendApplicationTopMatchTotal', () => {
    it('should sendApplicationTopMatchTotal to GA', () => {
      const payload = {
        jobId: 'abc1234xyz',
        sortCriteria: ApplicationSortCriteria.SCORES_WCC,
        total: 12,
      };
      testSaga(
        sendApplicationTopMatchTotal,
        getApplicationTopMatchTotal(payload.total, payload.sortCriteria, payload.jobId),
      )
        .next()
        .call(handleApplicationTopMatchTotal, payload.total, payload.sortCriteria, payload.jobId)
        .next()
        .isDone();
    });
  });
  describe('sendApplicationTopMatchLabelEvent', () => {
    it('should sendApplicationTopMatchLabelEvent to GA', () => {
      testSaga(sendApplicationTopMatchLabelEvent)
        .next()
        .call(handleApplicationTopMatchLabel)
        .next()
        .isDone();
    });
  });
  describe('sendApplicationClickEvent', () => {
    it('should sendApplicationClickEvent to GA', () => {
      const mockApplication = applicationMock;
      const position = 2;

      const payload = {
        applicationId: applicationMock.id,
        candidateId: applicationMock.applicant.id,
        jobId: applicationMock.job.uuid,
        position,
      };
      testSaga(sendApplicationClickEvent, onApplicationClicked(mockApplication, position))
        .next()
        .call(handleCandidateClick, payload, EVENT_CATEGORY.JOB_APPLICANT)
        .next()
        .isDone();
    });
  });
  describe('sendSuggestedTalentsPageEvent', () => {
    it('should sendSuggestedTalentsPageEvent to GA', () => {
      const payload = {
        jobId: 'jobId-0987654321',
        page: 3,
      };
      testSaga(sendSuggestedTalentsPageEvent, onSuggestedTalentsPageClicked(payload.jobId, payload.page))
        .next()
        .call(handleCandidatesPageClick, payload, EVENT_CATEGORY.JOB_SUGGESTED_TALENT)
        .next()
        .isDone();
    });
  });
  describe('sendSuggestedTalentsResumeEvent', () => {
    it('should sendSuggestedTalentsResumeEvent to GA', () => {
      testSaga(sendSuggestedTalentsResumeEvent, onSuggestedTalentsResumeClicked(DOWNLOAD_RESUME_GA_LABEL.LIST))
        .next()
        .call(handleCandidateResumeClick, DOWNLOAD_RESUME_GA_LABEL.LIST, EVENT_CATEGORY.JOB_SUGGESTED_TALENT)
        .next()
        .isDone();
    });
  });
  describe('tagSessionWithEntityId', () => {
    it('should tagSessionWithEntityId to GA', () => {
      testSaga(tagSessionWithEntityId, accountReceived(uenUserMock))
        .next()
        .call(tagUserSession, uenUserMock.userInfo.entityId)
        .next()
        .isDone();
    });
  });
  describe('sendSuggestedTalentTabEvent', () => {
    it('should sendSuggestedTalentTabEvent to GA', () => {
      const payload = 2;
      testSaga(sendSuggestedTalentTabEvent, onSuggestedTalentCandidateTabClicked(payload))
        .next()
        .call(handleCandidateTabClick, payload, EVENT_CATEGORY.JOB_SUGGESTED_TALENT)
        .next()
        .isDone();
    });
  });
  describe('sendSuggestedTalentClickEvent', () => {
    it('should sendSuggestedTalentClickEvent to GA', () => {
      const talentIdMock = 'id';
      const positionMock = 2;
      const jobUuidMock = 'abcdefghi';

      const payload = {
        jobId: jobUuidMock,
        position: positionMock,
        candidateId: talentIdMock,
      };
      testSaga(sendSuggestedTalentClickEvent, onSuggestedTalentClicked(talentIdMock, positionMock, jobUuidMock))
        .next()
        .call(handleCandidateClick, payload, EVENT_CATEGORY.JOB_SUGGESTED_TALENT)
        .next()
        .isDone();
    });
  });
  describe('sendStartJobPosting', () => {
    it('should send startTransactionalService to WOGAA', () => {
      testSaga(sendStartJobPosting)
        .next()
        .call(startJobPosting)
        .next()
        .isDone();
    });
  });
  describe('sendCompleteJobPosting', () => {
    it('should send completeTransactionalService to WOGAA', () => {
      testSaga(sendCompleteJobPosting)
        .next()
        .call(completeJobPosting)
        .next()
        .isDone();
    });
  });
  describe('sendBookmarkedCandidatesPageEvent', () => {
    it('should sendBookmarkedCandidatesPageEvent to GA', () => {
      const payload = {
        jobId: 'jobId-0987654321',
        page: 3,
      };
      testSaga(sendBookmarkedCandidatesPageEvent, onBookmarkedCandidatesPageClicked(payload.jobId, payload.page))
        .next()
        .call(handleCandidatesPageClick, payload, EVENT_CATEGORY.JOB_SAVED_TAB)
        .next()
        .isDone();
    });
  });
  describe('sendBookmarkedCandidateResumeEvent', () => {
    it('should sendBookmarkedCandidateResumeEvent to GA', () => {
      testSaga(sendBookmarkedCandidateResumeEvent, onBookmarkedCandidateResumeClicked(DOWNLOAD_RESUME_GA_LABEL.LIST))
        .next()
        .call(handleCandidateResumeClick, DOWNLOAD_RESUME_GA_LABEL.LIST, EVENT_CATEGORY.JOB_SAVED_TAB)
        .next()
        .isDone();
    });
  });
  describe('sendBookmarkedCandidateTabEvent', () => {
    it('should sendBookmarkedCandidateTabEvent to GA', () => {
      const payload = 2;
      testSaga(sendBookmarkedCandidateTabEvent, onBookmarkedCandidateTabClicked(payload))
        .next()
        .call(handleCandidateTabClick, payload, EVENT_CATEGORY.JOB_SAVED_TAB)
        .next()
        .isDone();
    });
  });
  describe('sendBookmarkedCandidateClickEvent', () => {
    it('should sendBookmarkedCandidateClickEvent to GA for application', () => {
      const candidateMock = bookmarkedApplicationMock;
      const positionMock = 2;
      const jobUuidMock = 'abcdefghi';

      const payload = {
        jobId: jobUuidMock,
        position: positionMock,
        candidateId: candidateMock.applicant.id,
        applicationId: candidateMock.id,
      };
      testSaga(
        sendBookmarkedCandidateClickEvent,
        onBookmarkedCandidateClicked(candidateMock, positionMock, jobUuidMock),
      )
        .next()
        .call(handleCandidateClick, payload, EVENT_CATEGORY.JOB_SAVED_TAB)
        .next()
        .isDone();
    });
    it('should sendBookmarkedCandidateClickEvent to GA for talent', () => {
      const candidateMock = bookmarkedTalentMock;
      const positionMock = 2;
      const jobUuidMock = 'abcdefghi';

      const payload = {
        jobId: jobUuidMock,
        position: positionMock,
        candidateId: candidateMock.id,
        applicationId: candidateMock.id,
      };
      testSaga(
        sendBookmarkedCandidateClickEvent,
        onBookmarkedCandidateClicked(candidateMock, positionMock, jobUuidMock),
      )
        .next()
        .call(handleCandidateClick, payload, EVENT_CATEGORY.JOB_SAVED_TAB)
        .next()
        .isDone();
    });
  });
});
