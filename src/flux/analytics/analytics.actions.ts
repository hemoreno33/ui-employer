import {action} from 'typesafe-actions';
import {CandidateTabs} from '~/components/CandidateInfo';
import {GetApplicationsApplications, GetBookmarkedCandidatesBookmarkedCandidates} from '~/graphql/__generated__/types';
import {DOWNLOAD_RESUME_GA_LABEL} from '~/util/analytics';
import {ApplicationSortCriteria} from '../applications';
import {
  APPLICATIONS_SORT_TYPE,
  APPLICATIONS_TOP_MATCH_LABEL,
  APPLICATIONS_TOP_MATCH_TOTAL,
  JOBS_SORTING_TYPE,
  ON_APPLICATION_CANDIDATE_RESUME_CLICKED,
  ON_APPLICATION_CANDIDATE_TAB_CLICKED,
  ON_APPLICATION_CLICKED,
  ON_APPLICATION_PAGE_CLICKED,
  ON_APPLICATIONS_TOP_MATCH_FILTERED,
  ON_CORPPASS_GET_STARTED_CLICKED,
  ON_JOB_SEARCH_QUERY,
  ON_LOGOUT_CLICKED,
  ON_NAVIGATION_MENU_LINK_CLICKED,
  ON_PAGE_CHANGE_CLICKED,
  ON_SUGGESTED_TALENT_CANDIDATE_TAB_CLICKED,
  ON_SUGGESTED_TALENTS_PAGE_CLICKED,
  ON_SUGGESTED_TALENTS_RESUME_CLICKED,
  ON_BOOKMARKED_CANDIDATES_PAGE_CLICKED,
  ON_BOOKMARKED_CANDIDATE_RESUME_CLICKED,
  ON_BOOKMARKED_CANDIDATE_CLICKED,
  ON_BOOKMARKED_CANDIDATE_TAB_CLICKED,
} from './analytics.constants';
import {MAIN_NAVIGATION_MENU_LINK_TYPE} from '~/analytics/mainNavigationMenu';

export const onNavigationMenuLinkClicked = (title: MAIN_NAVIGATION_MENU_LINK_TYPE) =>
  action(ON_NAVIGATION_MENU_LINK_CLICKED, title);
export const onCorppassGetStartedClicked = () => action(ON_CORPPASS_GET_STARTED_CLICKED);

export const onApplicationClicked = (application: GetApplicationsApplications, position: number) =>
  action(ON_APPLICATION_CLICKED, {...application, position});
export const onApplicationCandidateTabClicked = (tabIndex: CandidateTabs) =>
  action(ON_APPLICATION_CANDIDATE_TAB_CLICKED, tabIndex);
export const onApplicationPageClicked = (jobId: string, page: number) =>
  action(ON_APPLICATION_PAGE_CLICKED, {jobId, page});
export const onApplicationCandidateResumeClicked = (label: DOWNLOAD_RESUME_GA_LABEL) =>
  action(ON_APPLICATION_CANDIDATE_RESUME_CLICKED, label);
export const onApplicationSortingTypeClicked = (sortType: ApplicationSortCriteria) =>
  action(APPLICATIONS_SORT_TYPE, sortType);
export const onApplicationTopMatchFilter = (checked: boolean) => action(ON_APPLICATIONS_TOP_MATCH_FILTERED, checked);
export const getApplicationTopMatchTotal = (total: number, sortCriteria: ApplicationSortCriteria, jobId: string) =>
  action(APPLICATIONS_TOP_MATCH_TOTAL, {total, sortCriteria, jobId});
export const onApplicationTopMatchLabelClicked = () => action(APPLICATIONS_TOP_MATCH_LABEL);
export const onJobsSortingTypeClicked = (sortType: string) => action(JOBS_SORTING_TYPE, sortType);
export const onPageChangeClicked = ({jobStatus, page}: {jobStatus: string; page: number}) =>
  action(ON_PAGE_CHANGE_CLICKED, {jobStatus, page});
export const onLogoutClicked = () => action(ON_LOGOUT_CLICKED);
export const onJobSearchQueried = (query: string) => action(ON_JOB_SEARCH_QUERY, query);
export const onSuggestedTalentsPageClicked = (jobId: string, page: number) =>
  action(ON_SUGGESTED_TALENTS_PAGE_CLICKED, {jobId, page});
export const onSuggestedTalentsResumeClicked = (label: DOWNLOAD_RESUME_GA_LABEL) =>
  action(ON_SUGGESTED_TALENTS_RESUME_CLICKED, label);

export const onSuggestedTalentCandidateTabClicked = (tabIndex: CandidateTabs) =>
  action(ON_SUGGESTED_TALENT_CANDIDATE_TAB_CLICKED, tabIndex);

export const onBookmarkedCandidatesPageClicked = (jobId: string, page: number) =>
  action(ON_BOOKMARKED_CANDIDATES_PAGE_CLICKED, {jobId, page});
export const onBookmarkedCandidateResumeClicked = (label: DOWNLOAD_RESUME_GA_LABEL) =>
  action(ON_BOOKMARKED_CANDIDATE_RESUME_CLICKED, label);
export const onBookmarkedCandidateClicked = (
  bookmarkedCandidate: GetBookmarkedCandidatesBookmarkedCandidates,
  position: number,
  jobUuid: string,
) => action(ON_BOOKMARKED_CANDIDATE_CLICKED, {bookmarkedCandidate, position, jobUuid});
export const onBookmarkedCandidateTabClicked = (tabIndex: CandidateTabs) =>
  action(ON_BOOKMARKED_CANDIDATE_TAB_CLICKED, tabIndex);
