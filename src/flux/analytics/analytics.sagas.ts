import {ACCOUNT_RECEIVED, IAccountData} from '@govtechsg/redux-account-middleware';
import {LOCATION_CHANGE, LocationChangeAction} from 'connected-react-router';
import {all, call, select, spawn, takeEvery} from 'redux-saga/effects';
import {ActionType} from 'typesafe-actions';
import {handleEmployerLogin, handleEmployerLogout, tagUserSession} from '~/analytics/account';
import {
  handleApplicationSortingType,
  handleApplicationTopMatchFilter,
  handleApplicationTopMatchLabel,
  handleApplicationTopMatchTotal,
} from '~/analytics/applications';
import {
  handleCandidateTabClick,
  handleCandidatesPageClick,
  handleCandidateResumeClick,
  handleCandidateClick,
} from '~/analytics/candidates';
import {handleAllJobsSortingType, handlePageChange} from '~/analytics/filters';
import {handleCorpPassGetStartedClick} from '~/analytics/homeBanner';
import {handleJobsSearchQueryEvent} from '~/analytics/jobsSearchQuery';
import {mainNavigationMenuLinks} from '~/analytics/mainNavigationMenu';
import {handlePageView, startJobPosting, completeJobPosting} from '~/analytics/pages';
import {IUser, LoginActionTypes} from '~/flux/account';
import {
  APPLICATIONS_SORT_TYPE,
  APPLICATIONS_TOP_MATCH_LABEL,
  APPLICATIONS_TOP_MATCH_TOTAL,
  getApplicationTopMatchTotal,
  JOBS_SORTING_TYPE,
  ON_APPLICATION_CANDIDATE_RESUME_CLICKED,
  ON_APPLICATION_CANDIDATE_TAB_CLICKED,
  ON_APPLICATION_CLICKED,
  ON_APPLICATION_PAGE_CLICKED,
  ON_APPLICATIONS_TOP_MATCH_FILTERED,
  ON_CORPPASS_GET_STARTED_CLICKED,
  ON_JOB_SEARCH_QUERY,
  ON_LOGOUT_CLICKED,
  ON_NAVIGATION_MENU_LINK_CLICKED,
  ON_PAGE_CHANGE_CLICKED,
  ON_SUGGESTED_TALENT_CANDIDATE_TAB_CLICKED,
  ON_SUGGESTED_TALENTS_PAGE_CLICKED,
  ON_SUGGESTED_TALENTS_RESUME_CLICKED,
  onApplicationCandidateTabClicked,
  onApplicationClicked,
  onApplicationPageClicked,
  onApplicationSortingTypeClicked,
  onApplicationTopMatchFilter,
  onJobSearchQueried,
  onJobsSortingTypeClicked,
  onNavigationMenuLinkClicked,
  onPageChangeClicked,
  onSuggestedTalentCandidateTabClicked,
  onSuggestedTalentsPageClicked,
  onSuggestedTalentsResumeClicked,
  onApplicationCandidateResumeClicked,
  onBookmarkedCandidatesPageClicked,
  onBookmarkedCandidateResumeClicked,
  onBookmarkedCandidateClicked,
  onBookmarkedCandidateTabClicked,
  ON_BOOKMARKED_CANDIDATE_CLICKED,
  ON_BOOKMARKED_CANDIDATES_PAGE_CLICKED,
  ON_BOOKMARKED_CANDIDATE_RESUME_CLICKED,
  ON_BOOKMARKED_CANDIDATE_TAB_CLICKED,
} from '~/flux/analytics';
import {getLocation} from '~/flux/location';
import {onSuggestedTalentClicked} from '~/flux/suggestedTalents/suggestedTalents.actions';
import {ON_SUGGESTED_TALENT_CLICKED} from '~/flux/suggestedTalents/suggestedTalents.constants';
import {START_JOB_POSTING, COMPLETE_JOB_POSTING} from '~/flux/jobPosting/jobPosting.constants';
import {EVENT_CATEGORY} from '~/util/analytics';
import {isApplication} from '~/components/BookmarkedCandidates/bookmarkedCandidatesUtil';

export function* sendPageView({payload: {location}}: LocationChangeAction) {
  yield spawn(handlePageView, location.pathname);
}

export function* mainNavigationMenuLinkEvent({payload}: ActionType<typeof onNavigationMenuLinkClicked>) {
  const {pathname} = yield select(getLocation);
  yield call(mainNavigationMenuLinks, payload, pathname);
}

export function* corpPassGetStartedEvent() {
  yield call(handleCorpPassGetStartedClick);
}

export function* employerLoginEvent() {
  yield call(handleEmployerLogin);
}

export function* sendApplicationClickEvent({payload}: ActionType<typeof onApplicationClicked>) {
  yield call(
    handleCandidateClick,
    {
      applicationId: payload.id,
      candidateId: payload.applicant.id,
      jobId: payload.job.uuid,
      position: payload.position,
    },
    EVENT_CATEGORY.JOB_APPLICANT,
  );
}

export function* sendApplicationCandidateTabEvent({payload}: ActionType<typeof onApplicationCandidateTabClicked>) {
  yield call(handleCandidateTabClick, payload, EVENT_CATEGORY.JOB_APPLICANT);
}

export function* sendApplicationPageEvent({payload}: ActionType<typeof onApplicationPageClicked>) {
  yield call(handleCandidatesPageClick, payload, EVENT_CATEGORY.JOB_APPLICANT);
}

export function* sendApplicationCandidateResumeEvent({
  payload,
}: ActionType<typeof onApplicationCandidateResumeClicked>) {
  yield call(handleCandidateResumeClick, payload, EVENT_CATEGORY.JOB_APPLICANT);
}

export function* sendApplicationSortingTypeEvent({payload}: ActionType<typeof onApplicationSortingTypeClicked>) {
  yield call(handleApplicationSortingType, payload);
}

export function* sendApplicationTopMatchFilterEvent({payload}: ActionType<typeof onApplicationTopMatchFilter>) {
  yield call(handleApplicationTopMatchFilter, payload);
}

export function* sendApplicationTopMatchTotal({
  payload: {total, sortCriteria, jobId},
}: ActionType<typeof getApplicationTopMatchTotal>) {
  yield call(handleApplicationTopMatchTotal, total, sortCriteria, jobId);
}

export function* sendAllJobsSortingTypeEvent({payload}: ActionType<typeof onJobsSortingTypeClicked>) {
  yield call(handleAllJobsSortingType, payload);
}

export function* sendPageChangeEvent({payload: {jobStatus, page}}: ActionType<typeof onPageChangeClicked>) {
  yield call(handlePageChange, jobStatus, page);
}

export function* sendLogoutEvent() {
  const {pathname} = yield select(getLocation);
  yield call(handleEmployerLogout, pathname);
}

export function* sendJobsSearchQueryEvent({payload}: ActionType<typeof onJobSearchQueried>) {
  yield call(handleJobsSearchQueryEvent, payload);
}

export function* sendSuggestedTalentsPageEvent({payload}: ActionType<typeof onSuggestedTalentsPageClicked>) {
  yield call(handleCandidatesPageClick, payload, EVENT_CATEGORY.JOB_SUGGESTED_TALENT);
}

export function* sendSuggestedTalentsResumeEvent({payload}: ActionType<typeof onSuggestedTalentsResumeClicked>) {
  yield call(handleCandidateResumeClick, payload, EVENT_CATEGORY.JOB_SUGGESTED_TALENT);
}
export function* sendSuggestedTalentClickEvent({
  payload: {talentId, jobUuid, position},
}: ActionType<typeof onSuggestedTalentClicked>) {
  yield call(
    handleCandidateClick,
    {
      jobId: jobUuid,
      position,
      candidateId: talentId,
    },
    EVENT_CATEGORY.JOB_SUGGESTED_TALENT,
  );
}

export function* tagSessionWithEntityId({payload}: {type: typeof ACCOUNT_RECEIVED; payload: IUser & IAccountData}) {
  yield call(tagUserSession, payload.userInfo.entityId);
}
export function* sendSuggestedTalentTabEvent({payload}: ActionType<typeof onSuggestedTalentCandidateTabClicked>) {
  yield call(handleCandidateTabClick, payload, EVENT_CATEGORY.JOB_SUGGESTED_TALENT);
}

export function* sendApplicationTopMatchLabelEvent() {
  yield call(handleApplicationTopMatchLabel);
}

export function* sendStartJobPosting() {
  yield call(startJobPosting);
}

export function* sendCompleteJobPosting() {
  yield call(completeJobPosting);
}

export function* sendBookmarkedCandidatesPageEvent({payload}: ActionType<typeof onBookmarkedCandidatesPageClicked>) {
  yield call(handleCandidatesPageClick, payload, EVENT_CATEGORY.JOB_SAVED_TAB);
}
export function* sendBookmarkedCandidateResumeEvent({payload}: ActionType<typeof onBookmarkedCandidateResumeClicked>) {
  yield call(handleCandidateResumeClick, payload, EVENT_CATEGORY.JOB_SAVED_TAB);
}
export function* sendBookmarkedCandidateClickEvent({
  payload: {bookmarkedCandidate, jobUuid, position},
}: ActionType<typeof onBookmarkedCandidateClicked>) {
  yield call(
    handleCandidateClick,
    {
      jobId: jobUuid,
      position,
      candidateId: isApplication(bookmarkedCandidate) ? bookmarkedCandidate.applicant.id : bookmarkedCandidate.id,
      applicationId: bookmarkedCandidate.id,
    },
    EVENT_CATEGORY.JOB_SAVED_TAB,
  );
}
export function* sendBookmarkedCandidateTabEvent({payload}: ActionType<typeof onBookmarkedCandidateTabClicked>) {
  yield call(handleCandidateTabClick, payload, EVENT_CATEGORY.JOB_SAVED_TAB);
}

export function* analyticsSaga() {
  yield all([
    takeEvery(LOCATION_CHANGE, sendPageView),
    takeEvery(ON_NAVIGATION_MENU_LINK_CLICKED, mainNavigationMenuLinkEvent),
    takeEvery(ON_CORPPASS_GET_STARTED_CLICKED, corpPassGetStartedEvent),
    takeEvery(LoginActionTypes.LOGIN, employerLoginEvent),
    takeEvery(ON_APPLICATION_CLICKED, sendApplicationClickEvent),
    takeEvery(ON_APPLICATION_CANDIDATE_TAB_CLICKED, sendApplicationCandidateTabEvent),
    takeEvery(ON_APPLICATION_PAGE_CLICKED, sendApplicationPageEvent),
    takeEvery(ON_APPLICATION_CANDIDATE_RESUME_CLICKED, sendApplicationCandidateResumeEvent),
    takeEvery(JOBS_SORTING_TYPE, sendAllJobsSortingTypeEvent),
    takeEvery(ON_PAGE_CHANGE_CLICKED, sendPageChangeEvent),
    takeEvery(ON_LOGOUT_CLICKED, sendLogoutEvent),
    takeEvery(ON_JOB_SEARCH_QUERY, sendJobsSearchQueryEvent),
    takeEvery(ON_SUGGESTED_TALENTS_PAGE_CLICKED, sendSuggestedTalentsPageEvent),
    takeEvery(ON_SUGGESTED_TALENTS_RESUME_CLICKED, sendSuggestedTalentsResumeEvent),
    takeEvery(ACCOUNT_RECEIVED, tagSessionWithEntityId),
    takeEvery(ON_SUGGESTED_TALENT_CANDIDATE_TAB_CLICKED, sendSuggestedTalentTabEvent),
    takeEvery(ON_SUGGESTED_TALENT_CLICKED, sendSuggestedTalentClickEvent),
    takeEvery(APPLICATIONS_SORT_TYPE, sendApplicationSortingTypeEvent),
    takeEvery(ON_APPLICATIONS_TOP_MATCH_FILTERED, sendApplicationTopMatchFilterEvent),
    takeEvery(APPLICATIONS_TOP_MATCH_LABEL, sendApplicationTopMatchLabelEvent),
    takeEvery(APPLICATIONS_TOP_MATCH_TOTAL, sendApplicationTopMatchTotal),
    takeEvery(START_JOB_POSTING, sendStartJobPosting),
    takeEvery(COMPLETE_JOB_POSTING, sendCompleteJobPosting),
    takeEvery(ON_BOOKMARKED_CANDIDATE_CLICKED, sendBookmarkedCandidateClickEvent),
    takeEvery(ON_BOOKMARKED_CANDIDATES_PAGE_CLICKED, sendBookmarkedCandidatesPageEvent),
    takeEvery(ON_BOOKMARKED_CANDIDATE_RESUME_CLICKED, sendBookmarkedCandidateResumeEvent),
    takeEvery(ON_BOOKMARKED_CANDIDATE_TAB_CLICKED, sendBookmarkedCandidateTabEvent),
  ]);
}
