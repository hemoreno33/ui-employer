/**
 * Keys named to be exact as API params
 *
 * @example jobs?page=0&limit=20&title=software
 */

import {JobsSortCriteria} from '~/flux/jobPostings';
import {SortDirection} from '~/graphql/__generated__/types';

export interface IFetchOpenJobs {
  page: number;
  limit: number;
  title?: string;
  jobPostId?: string;
  orderBy?: JobsSortCriteria;
  orderDirection?: SortDirection;
}
export interface IFetchClosedJobs {
  page: number;
  limit: number;
  title?: string;
  jobPostId?: string;
  orderBy?: JobsSortCriteria;
  orderDirection?: SortDirection;
}
