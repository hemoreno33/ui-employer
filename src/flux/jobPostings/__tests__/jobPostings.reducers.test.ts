import {fetchClosedJobsRequested, fetchOpenJobsRequested, resetJobPostingsParams} from '../jobPostings.actions';
import {CLOSED_JOBS_REQUESTED, OPEN_JOBS_REQUESTED} from '../jobPostings.constants';
import {jobPostings} from '../jobPostings.reducer';

describe('jobPostings reducer', () => {
  const initialState = {
    closedJobs: {
      results: [],
      total: 0,
    },
    closedJobsParams: {
      limit: 1,
      page: 0,
    },
    closedJobsTotal: 0,
    openJobs: {
      results: [],
      total: 0,
    },
    openJobsParams: {
      limit: 1,
      page: 0,
    },
    openJobsTotal: 0,
    postings: [],
  };

  describe('OPEN_JOBS_REQUESTED', () => {
    it('should set openJobsParams', () => {
      expect(jobPostings(initialState, fetchOpenJobsRequested({page: 1, limit: 1}))).toEqual({
        ...initialState,
        openJobsFetchStatus: OPEN_JOBS_REQUESTED,
        openJobsParams: {
          limit: 1,
          page: 1,
        },
      });
    });
  });

  describe('CLOSED_JOBS_REQUESTED', () => {
    it('should set closedJobsParams', () => {
      expect(jobPostings(initialState, fetchClosedJobsRequested({page: 1, limit: 1}))).toEqual({
        ...initialState,
        closedJobsFetchStatus: CLOSED_JOBS_REQUESTED,
        closedJobsParams: {
          limit: 1,
          page: 1,
        },
      });
    });
  });

  describe('RESET_JOB_POSTINGS_PARAMS', () => {
    it('should set closedJobsParams and openJobsParams to initial state', () => {
      const defaultState = jobPostings(undefined, resetJobPostingsParams());
      expect(jobPostings(initialState, resetJobPostingsParams())).toEqual({
        ...initialState,
        closedJobsParams: defaultState.closedJobsParams,
        openJobsParams: defaultState.openJobsParams,
      });
    });
  });
});
