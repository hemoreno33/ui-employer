import {IAppState} from '~/flux';

export const getFeatureFlag = (name: string) => (state: IAppState) => state.features[name];
