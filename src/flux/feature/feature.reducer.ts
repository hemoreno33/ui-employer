import {ActionType} from 'typesafe-actions';
import * as actions from './feature.actions';

export interface IFeatureToggles {
  [keys: string]: boolean;
}

export type FeatureActionType = ActionType<typeof actions>;

export const features = (state: IFeatureToggles = {}, action: FeatureActionType): IFeatureToggles => {
  switch (action.type) {
    case actions.UPDATE_FEATURE_TOGGLES:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};
