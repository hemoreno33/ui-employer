import {action} from 'typesafe-actions';
import {IFeatureToggles} from './feature.reducer';

export const UPDATE_FEATURE_TOGGLES = 'UPDATE_FEATURE_TOGGLES';
export const updateFeatureToggles = (featureToggles: IFeatureToggles = {}) =>
  action(UPDATE_FEATURE_TOGGLES, featureToggles);
