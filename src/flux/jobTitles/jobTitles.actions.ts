import {action} from 'typesafe-actions';
import {
  JOB_TITLES_RECEIVED,
  JOB_TITLES_REQUEST_FAILED,
  JOB_TITLES_REQUESTED,
  REMOVE_JOB_TITLES_RESULTS,
} from '~/flux/jobTitles/jobTitles.constants';

import {IJobTitlesResponse} from '~/flux/jobTitles/jobTitles.reducer';

export const fetchJobTitlesRequested = (query: string) => action(JOB_TITLES_REQUESTED, query);
export const fetchJobTitlesSucceeded = (jobTitles: IJobTitlesResponse) => action(JOB_TITLES_RECEIVED, jobTitles);
export const fetchJobTitlesFailed = () => action(JOB_TITLES_REQUEST_FAILED);

export const removeJobTitlesResults = () => action(REMOVE_JOB_TITLES_RESULTS);
