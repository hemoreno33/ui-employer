import {ActionType} from 'typesafe-actions';
import * as actions from '~/flux/jobTitles/jobTitles.actions';
import {JOB_TITLES_RECEIVED, REMOVE_JOB_TITLES_RESULTS} from '~/flux/jobTitles/jobTitles.constants';

const initialState: IJobTitlesResponse = {
  results: [],
  total: 0,
};

export type JobTitlesActionType = ActionType<typeof actions>;

export const jobTitles = (
  state: IJobTitlesResponse = initialState,
  action: JobTitlesActionType,
): IJobTitlesResponse => {
  switch (action.type) {
    case REMOVE_JOB_TITLES_RESULTS:
      return {
        ...initialState,
      };

    case JOB_TITLES_RECEIVED:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};

export interface IJobTitle {
  id: number;
  jobTitle: string;
  _links?: any;
}

export interface IJobTitlesResponse {
  results: IJobTitle[];
  total: number;
  _links?: any;
}
