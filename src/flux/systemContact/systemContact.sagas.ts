import {all, call, put, takeEvery, takeLatest, SagaReturnType} from 'redux-saga/effects';
import {
  systemContactFailed,
  systemContactSucceeded,
  updateSystemContactFailed,
  updateSystemContactSucceeded,
  updateTermsAndConditionsFailed,
  updateTermsAndConditionsRequested,
  updateTermsAndConditionsSucceeded,
} from '~/flux/systemContact/systemContact.actions';
import {
  SYSTEM_CONTACT_REQUESTED,
  UPDATE_SYSTEM_CONTACT_REQUESTED,
  UPDATE_TERMS_AND_CONDITIONS_REQUESTED,
} from '~/flux/systemContact/systemContact.constants';
import {
  acceptTermsAndConditions,
  fetchSystemContact,
  mutateSystemContact,
} from '~/services/systemContact/systemContact';
import {refreshRequested} from '@govtechsg/redux-account-middleware';

export function* getSystemContact() {
  try {
    const response: SagaReturnType<typeof fetchSystemContact> = yield call(fetchSystemContact);
    if (!response.data.systemContact) {
      throw new Error('no data');
    }
    yield put(systemContactSucceeded(response.data.systemContact));
  } catch {
    yield put(systemContactFailed());
  }
}

export function* updateSystemContact(action: any) {
  try {
    const response: SagaReturnType<typeof mutateSystemContact> = yield call(mutateSystemContact, action.payload);
    if (!response.data.updateSystemContact) {
      throw new Error('no data');
    }
    yield put(updateSystemContactSucceeded(response.data.updateSystemContact));
    yield put(updateTermsAndConditionsRequested());
  } catch (error) {
    yield put(updateSystemContactFailed());
  }
}

export function* updateTermsAndConditions() {
  try {
    const response: SagaReturnType<typeof acceptTermsAndConditions> = yield call(acceptTermsAndConditions);
    if (!response.data) {
      throw new Error('no data');
    }
    yield put(updateTermsAndConditionsSucceeded(response.data));
    yield put(refreshRequested());
  } catch (error) {
    yield put(updateTermsAndConditionsFailed());
  }
}

export function* systemContactSaga() {
  yield all([
    yield takeEvery(SYSTEM_CONTACT_REQUESTED, getSystemContact),
    yield takeLatest(UPDATE_SYSTEM_CONTACT_REQUESTED, updateSystemContact),
    yield takeLatest(UPDATE_TERMS_AND_CONDITIONS_REQUESTED, updateTermsAndConditions),
  ]);
}
