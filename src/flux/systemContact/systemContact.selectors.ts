import {IAppState} from '~/flux';

export const areSystemContactUpdated = (state: IAppState): boolean => !!(state.user.email && state.user.contactNumber);

export const areTermsAndConditionsAccepted = (state: IAppState): boolean =>
  !!state.user.acceptedAt && !!state.account.data?.additionalInfo?.data.systemContact?.termsAndConditionsAcceptedAt;

export const getSystemContact = (state: IAppState) => state.user;

export const isErrorOnSubmit = (state: IAppState) => state.user.error;

export const isLoadingFinished = (state: IAppState) => state.user.loaded;
