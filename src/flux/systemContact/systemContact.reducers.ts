import {ActionType} from 'typesafe-actions';
import * as actions from '~/flux/systemContact/systemContact.actions';
import {
  SYSTEM_CONTACT_FAILED,
  SYSTEM_CONTACT_SUCCEEDED,
  UPDATE_SYSTEM_CONTACT_FAILED,
  UPDATE_SYSTEM_CONTACT_SUCCEEDED,
  UPDATE_TERMS_AND_CONDITIONS_FAILED,
  UPDATE_TERMS_AND_CONDITIONS_SUCCEEDED,
} from '~/flux/systemContact/systemContact.constants';
import {SystemContact} from '~/graphql/__generated__/types';
export interface ISystemContactState {
  contactNumber: SystemContact['contactNumber'];
  email: SystemContact['email'];
  designation: SystemContact['designation'];
  individualId: string;
  isThirdParty: boolean;
  licence: string;
  error: boolean;
  acceptedAt: string;
  loaded: boolean;
}

const initialState: ISystemContactState = {
  acceptedAt: '',
  contactNumber: '',
  designation: '',
  email: '',
  error: false,
  individualId: '',
  isThirdParty: false,
  licence: '',
  loaded: false,
};

export const user = (state: ISystemContactState = initialState, action: SystemContactActionType) => {
  switch (action.type) {
    case UPDATE_SYSTEM_CONTACT_SUCCEEDED:
    case SYSTEM_CONTACT_SUCCEEDED:
      return {
        ...state,
        ...action.payload,
        acceptedAt: action.payload.termsAndConditionsAcceptedAt,
        error: false,
        loaded: true,
      };
    case SYSTEM_CONTACT_FAILED:
      return {
        ...state,
        loaded: true,
      };
    case UPDATE_TERMS_AND_CONDITIONS_SUCCEEDED:
      return {
        ...state,
        acceptedAt: action.payload,
        error: false,
      };
    case UPDATE_TERMS_AND_CONDITIONS_FAILED:
    case UPDATE_SYSTEM_CONTACT_FAILED:
      return {
        ...state,
        error: true,
      };
    default:
      return state;
  }
};

export type SystemContactActionType = ActionType<typeof actions>;
