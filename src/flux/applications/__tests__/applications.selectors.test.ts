import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {ApplicationSortCriteria, getSortedApplications} from '~/flux/applications';
import {GetApplicationsApplications} from '~/graphql/__generated__/types';

describe('application selectors', () => {
  describe('getSortedApplications()', () => {
    it('should return sorted applications based on sort created on field', () => {
      const applications: GetApplicationsApplications[] = [
        {
          ...applicationMock,
          createdOn: 'Fri Mar 10 2018 20:41:20 GMT+0000 (UTC)',
        },
        {
          ...applicationMock,
          createdOn: 'Sun Aug 02 2018 13:41:20 GMT+0000 (UTC)',
        },
      ];
      const expectedSortedApplications: GetApplicationsApplications[] = [
        {
          ...applicationMock,
          createdOn: 'Sun Aug 02 2018 13:41:20 GMT+0000 (UTC)',
        },
        {
          ...applicationMock,
          createdOn: 'Fri Mar 10 2018 20:41:20 GMT+0000 (UTC)',
        },
      ];
      expect(getSortedApplications(applications, ApplicationSortCriteria.CREATED_ON)).toEqual(
        expectedSortedApplications,
      );
    });
  });
});
