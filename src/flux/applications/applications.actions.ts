import {action} from 'typesafe-actions';
import {ON_APPLICATION_CLICKED} from '~/flux/applications';

export const onApplicationClicked = (applicationId: string) => action(ON_APPLICATION_CLICKED, {applicationId});
