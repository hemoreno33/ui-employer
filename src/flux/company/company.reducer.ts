import {ActionType} from 'typesafe-actions';
import * as actions from '~/flux/company/company.actions';
import {
  COMPANY_FETCH_REQUESTED,
  COMPANY_REQUEST_FAILED,
  COMPANY_REQUEST_SUCCEEDED,
  COMPANY_UPDATED,
} from '~/flux/company/company.constants';
import {ICompanyInfoWithAddresses} from '~/services/employer/company';

export interface ICompanyState {
  companyInfo: ICompanyInfoWithAddresses;
  fetchStatus: string;
}

const initialState: ICompanyState = {
  companyInfo: {
    addresses: [],
    description: '',
    logoFileName: '',
    logoUploadPath: '',
    name: '',
    uen: '',
  },
  fetchStatus: '',
};

export const company = (state: ICompanyState = initialState, action: CompanyActionType): ICompanyState => {
  switch (action.type) {
    case COMPANY_REQUEST_SUCCEEDED:
      return {
        ...state,
        companyInfo: action.payload || {},
        fetchStatus: action.type,
      };
    case COMPANY_UPDATED:
      return {
        ...state,
        companyInfo: {...state.companyInfo, ...action.payload} || {},
        fetchStatus: action.type,
      };
    case COMPANY_FETCH_REQUESTED:
    case COMPANY_REQUEST_FAILED:
      return {
        ...state,
        fetchStatus: action.type,
      };
    default:
      return state;
  }
};

export type CompanyActionType = ActionType<typeof actions>;
