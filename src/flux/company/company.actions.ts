import {action} from 'typesafe-actions';
import {
  COMPANY_FETCH_REQUESTED,
  COMPANY_REQUEST_FAILED,
  COMPANY_REQUEST_SUCCEEDED,
  COMPANY_UPDATED,
} from '~/flux/company/company.constants';
import {ICompanyInfo, ICompanyInfoWithAddresses} from '~/services/employer/company';

export const companyFetchRequested = () => action(COMPANY_FETCH_REQUESTED);
export const companyFetchSucceeded = (companyInfo: ICompanyInfoWithAddresses) =>
  action(COMPANY_REQUEST_SUCCEEDED, companyInfo);
export const companyFetchFailed = () => action(COMPANY_REQUEST_FAILED);
export const companyUpdated = (companyInfo: ICompanyInfo) => action(COMPANY_UPDATED, companyInfo);
