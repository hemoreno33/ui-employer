import {
  company,
  COMPANY_FETCH_REQUESTED,
  COMPANY_REQUEST_FAILED,
  COMPANY_REQUEST_SUCCEEDED,
  companyFetchFailed,
  companyFetchRequested,
  companyFetchSucceeded,
  ICompanyState,
} from '~/flux/company';

import {companyInfoMock} from '~/__mocks__/company/company.mocks';

describe('company reducer', () => {
  const initialState: ICompanyState = {
    companyInfo: {
      addresses: [],
      description: '',
      logoFileName: '',
      logoUploadPath: '',
      name: '',
      uen: '',
    },
    fetchStatus: '',
  };

  describe('COMPANY_REQUEST_SUCCEEDED', () => {
    it('should set companyInfo to the ones in action payload', () => {
      expect(company(initialState, companyFetchSucceeded(companyInfoMock))).toEqual({
        ...initialState,
        companyInfo: companyInfoMock,
        fetchStatus: COMPANY_REQUEST_SUCCEEDED,
      });
    });
  });

  describe('COMPANY_FETCH_REQUESTED', () => {
    it('should set fetchStatus to COMPANY_FETCH_REQUESTED', () => {
      expect(company(initialState, companyFetchRequested())).toEqual({
        ...initialState,
        fetchStatus: COMPANY_FETCH_REQUESTED,
      });
    });
  });

  describe('COMPANY_REQUEST_FAILED', () => {
    it('should set fetchStatus to COMPANY_REQUEST_FAILED', () => {
      expect(company(initialState, companyFetchFailed())).toEqual({
        ...initialState,
        fetchStatus: COMPANY_REQUEST_FAILED,
      });
    });
  });
});
