import {expectSaga} from 'redux-saga-test-plan';
import {all, put, takeEvery} from 'redux-saga/effects';
import {spawnWithFlag} from '~/flux/saga/spawnWithFlag';

describe('spawnWithFlags', () => {
  function* someSaga() {
    yield put({type: 'SOME_SAGA'});
  }
  function* someSagaSuite() {
    yield all([yield takeEvery('ACCOUNT_RECEIVED', someSaga)]);
  }
  test('it should spawn saga when feature flag is set to true', () => {
    return expectSaga(spawnWithFlag('anyName'), someSagaSuite)
      .provide({
        select() {
          return true;
        },
      })
      .dispatch({type: 'ACCOUNT_RECEIVED'})
      .put({
        type: 'SOME_SAGA',
      })
      .run();
  });
  test('it should not spawn saga when feature flag is set to false', () => {
    return expectSaga(spawnWithFlag('anyName'), someSagaSuite)
      .provide({
        select() {
          return false;
        },
      })
      .dispatch({type: 'ACCOUNT_RECEIVED'})
      .not.put({
        type: 'SOME_SAGA',
      })
      .run();
  });
  test('it should not spawn saga when feature flag is not set', () => {
    return expectSaga(spawnWithFlag('anyName'), someSagaSuite)
      .provide({
        select() {
          return undefined;
        },
      })
      .dispatch({type: 'ACCOUNT_RECEIVED'})
      .not.put({
        type: 'SOME_SAGA',
      })
      .run();
  });
});
