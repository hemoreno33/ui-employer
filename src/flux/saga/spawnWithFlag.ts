import {select, spawn} from 'redux-saga/effects';
import {getFeatureFlag} from '~/flux/feature';

export const spawnWithFlag = (featureFlag: string) =>
  function*(saga: (...args: any[]) => any) {
    const feature = yield select(getFeatureFlag(featureFlag));
    if (feature) {
      yield spawn(saga);
    }
  };
