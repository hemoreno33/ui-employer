import '@govtechsg/mcf-mcfui/dist/main.css';
import React from 'react';
import ReactDOM from 'react-dom';

import 'tachyons/css/tachyons.min.css';
import './styles/main.scss';

import AppContainer from '~/AppContainer';
import {createAndInitStore} from '~/flux';
import {loadFeatureToggles} from './services/featureToggles';

const init = async () => {
  const featureToggles = await loadFeatureToggles();
  const store = await createAndInitStore(featureToggles);

  ReactDOM.render(<AppContainer store={store} />, document.getElementById('root'));
};

init();
