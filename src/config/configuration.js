// @ts-check

// Only add specific environment variables (like API_URL)
// For our own safety, please don't add logic here, just use plain objects (also no functions!)

const {metaConfiguration} = require('./meta');
const {ENVIRONMENT} = require('./environment');

const devApiJobUrl = 'http://localhost:9999';

const pathConfiguration = {
  [ENVIRONMENT.development]: {
    publicPath: '/',
    url: {
      featureToggles: 'https://www.mycareersfuture.sg/features/employer/latest.json',
      corppass: 'http://localhost:8001/cp/login',
      apiJob: {
        v1: devApiJobUrl,
        v2: `${devApiJobUrl}/v2`,
      },
      apiProfile: `${devApiJobUrl}/profile`,
      notification: 'https://www.mycareersfuture.sg/notification/development.json',
      mcf: 'http://localhost:3000',
      apiVirusScanner: 'https://api-virus-scanner.ci.mcf.sh',
    },
  },
  [ENVIRONMENT.production]: {
    publicPath: '/',
    url: {
      featureToggles: 'https://www.mycareersfuture.sg/features/employer/latest.json',
      corppass: 'https://account.mycareersfuture.sg/cp/login',
      apiJob: {
        v1: 'https://api-v.mycareersfuture.sg',
        v2: 'https://api-v.mycareersfuture.sg/v2',
      },
      apiProfile: 'https://api-v.mycareersfuture.sg/profile',
      notification: 'https://www.mycareersfuture.sg/notification/production.json',
      mcf: 'https://www.mycareersfuture.sg',
      apiVirusScanner: 'https://api-scanner.mycareersfuture.sg',
    },
  },
  [ENVIRONMENT.staging]: {
    publicPath: '/',
    url: {
      featureToggles: 'https://www.mycareersfuture.sg/features/employer/latest.json',
      corppass: 'https://account.mycareersfuture.sg/cp/login',
      apiJob: {
        v1: 'https://api-v.mycareersfuture.sg',
        v2: 'https://api-v.mycareersfuture.sg/v2',
      },
      apiProfile: 'https://api-v.mycareersfuture.sg/profile',
      notification: 'https://www.mycareersfuture.sg/notification/staging.json',
      mcf: 'https://www.mycareersfuture.sg',
      apiVirusScanner: 'https://api-scanner.mycareersfuture.sg',
    },
  },
  [ENVIRONMENT.qa]: {
    publicPath: '/',
    url: {
      featureToggles: 'https://www.mycareersfuture.sg/features/employer/latest.json',
      corppass: 'https://account-qa.ci.mcf.sh/cp/login',
      apiJob: {
        v1: 'https://api-job-qa.ci.mcf.sh',
        v2: 'https://api-job-qa.ci.mcf.sh/v2',
      },
      apiProfile: 'https://api-job-qa.ci.mcf.sh/profile',
      notification: 'https://www.mycareersfuture.sg/notification/qa.json',
      mcf: 'https://jobseeker-qa.ci.mcf.sh',
      apiVirusScanner: 'https://api-virus-scanner.ci.mcf.sh',
    },
  },
  [ENVIRONMENT.uat]: {
    publicPath: '/',
    url: {
      featureToggles: 'https://www.mycareersfuture.sg/features/employer/latest.json',
      corppass: 'https://account-qa.ci.mcf.sh/cp/login', // corppass mock login
      // corppass: 'https://account-uat.ci.mcf.sh/cp/login', // corppass staging
      apiJob: {
        v1: 'https://api-job-uat.ci.mcf.sh',
        v2: 'https://api-job-uat.ci.mcf.sh/v2',
      },
      apiProfile: 'https://api-job-uat.ci.mcf.sh/profile',
      notification: 'https://www.mycareersfuture.sg/notification/uat.json',
      mcf: 'https://jobseeker-uat.ci.mcf.sh',
      apiVirusScanner: 'https://api-virus-scanner.ci.mcf.sh',
    },
  },
};

/**
 * @callback ConfigurationFunc
 * @param {import('./index.d').Environment} env
 * @return {import('./index.d').IAppConfiguration}
 */
/** @type {ConfigurationFunc} */
const configuration = (env) => {
  return {
    ...pathConfiguration[env],
    meta: {
      ...metaConfiguration[env],
    },
  };
};

module.exports = {
  configuration,
};
