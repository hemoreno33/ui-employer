export type Environment = 'development' | 'uat' | 'qa' | 'staging' | 'production';
export type EnvironmentEnum = {[k in Environment]: k};
export type getEnvironment = (s: string) => Environment;

export interface IUrlConfiguration {
  featureToggles: string;
  corppass: string;
  apiJob: {
    v1: string;
    v2: string;
  };
  apiProfile: string;
  notification: string;
  mcf: string;
  apiVirusScanner: string;
}

export interface IMetaConfiguration {
  gaTrackingCode: string;
  version: string;
  wogaaScriptUrl: string;
}

export interface IAppConfiguration {
  publicPath: string;
  url: IUrlConfiguration;
  meta: IMetaConfiguration;
}

// This generic type is here so we can extend IAppConfiguration in ExportedConfiguration
// and still provide a definition to check against in configuration.js
export type IConfigurationSection<T> = {[k in Environment]: T};

// This is the actual exported configuration
export interface IConfiguration extends IAppConfiguration {
  environment: Environment;
}
