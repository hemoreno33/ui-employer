// Inject the application configuration at buildtime so we do not reference
// the full configuration object at runtime and leak configuration details
// for other environments

import {IConfiguration} from './index.d';

declare let __WEBPACK_DEFINE_CONFIG_JS_OBJ__: IConfiguration;

export const config = __WEBPACK_DEFINE_CONFIG_JS_OBJ__;
