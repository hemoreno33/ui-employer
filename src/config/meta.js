const {ENVIRONMENT} = require('./environment');

const childProcess = require('child_process');
const path = require('path');
const fs = require('fs');
const verfile = path.join(process.cwd(), '/.version');

const getCommitShortHash = () => {
  return childProcess
    .execSync('git rev-parse --short HEAD')
    .toString()
    .trim();
};

const getVersion = () => {
  if (!fs.existsSync(verfile)) {
    return childProcess
      .execSync('git tag -l | egrep "^[0-9]+.[0-9]+.[0-9]+$" | sort -t. -k 1,1n -k 2,2n -k 3,3n -k 4,4n | tail -n 1')
      .toString()
      .trim()
      .concat(`-${getCommitShortHash()}`);
  }
  return fs
    .readFileSync(verfile)
    .toString()
    .trim();
};

const metaConfiguration = {
  [ENVIRONMENT.development]: {
    gaTrackingCode: 'UA-129908741-4',
    version: `v1.${getCommitShortHash()}`,
    wogaaScriptUrl: 'https://assets.dcube.cloud/scripts/wogaa.js',
  },
  [ENVIRONMENT.production]: {
    gaTrackingCode: 'UA-129908741-3',
    version: `v1.${getVersion()}`,
    wogaaScriptUrl: 'https://assets.wogaa.sg/scripts/wogaa.js',
  },
  [ENVIRONMENT.staging]: {
    gaTrackingCode: 'UA-129908741-3',
    version: `v1.${getVersion()}`,
    wogaaScriptUrl: 'https://assets.dcube.cloud/scripts/wogaa.js',
  },
  [ENVIRONMENT.uat]: {
    gaTrackingCode: 'UA-129908741-1',
    version: `v1.${getVersion()}`,
    wogaaScriptUrl: 'https://assets.dcube.cloud/scripts/wogaa.js',
  },
  [ENVIRONMENT.qa]: {
    gaTrackingCode: 'UA-129908741-2',
    version: `v1.${getVersion()}`,
    wogaaScriptUrl: 'https://assets.dcube.cloud/scripts/wogaa.js',
  },
};

module.exports = {
  metaConfiguration,
};
