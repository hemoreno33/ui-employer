import {LOGOUT_REQUEST_FAILED, LOGOUT_REQUESTED} from '@govtechsg/redux-account-middleware';
import {mount} from 'enzyme';
import React from 'react';
import {LogOut} from '../LogOut';

describe('Pages/LogOut', () => {
  const logOutRequestedMock = jest.fn();

  const props = {
    actionableElement: <div>actionable element</div>,
    subTitle: 'subtitle',
    title: 'title',
  };

  it('should call logOutRequested on mount and renders correctly', () => {
    const wrapper = mount(<LogOut {...props} logOutRequested={logOutRequestedMock} />);
    expect(logOutRequestedMock).toHaveBeenCalledTimes(1);
    expect(wrapper).toMatchSnapshot();
  });

  it('renders "Logging Out..." when logout request is still pending', () => {
    const wrapper = mount(<LogOut {...props} logOutRequested={logOutRequestedMock} accountStatus={LOGOUT_REQUESTED} />);
    expect(wrapper.text()).toBe('Logging Out...');
  });

  it('renders "Logging Out Failed" when logout request failed', () => {
    const wrapper = mount(
      <LogOut {...props} logOutRequested={logOutRequestedMock} accountStatus={LOGOUT_REQUEST_FAILED} />,
    );
    expect(wrapper.text()).toBe('Logging Out Failed');
  });
});
