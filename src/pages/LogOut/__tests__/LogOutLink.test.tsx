import {shallow} from 'enzyme';
import React from 'react';
import {LogOutLink} from '~/pages/LogOut/LogOutLink';

describe('Pages/LogOut/LogOutLink', () => {
  it('renders correctly', () => {
    const wrapper = shallow(<LogOutLink />);
    expect(wrapper).toMatchSnapshot();
  });
});
