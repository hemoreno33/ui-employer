import React from 'react';

export interface ITimeOutLinkProps {
  onClick: () => any;
}

export const TimeOutLink: React.FunctionComponent<ITimeOutLinkProps> = ({onClick}) => (
  <a href="#" className="f4 underline blue" id="action-button" onClick={onClick}>
    Log in again
  </a>
);
