import {logOutRequested} from '@govtechsg/redux-account-middleware';
import React from 'react';
import {connect} from 'react-redux';
import {IAppState} from '~/flux/index';
import {LogOut} from '~/pages/LogOut/LogOut';
import {LogOutLink} from '~/pages/LogOut/LogOutLink';

const mapStateToProps = ({account: {status}}: IAppState) => ({
  accountStatus: status,
  actionableElement: <LogOutLink />,
  subTitle: 'To review the candidates for your jobs, please log in again.',
  title: 'You have successfully logged out',
});

export const LogOutContainer = connect(mapStateToProps, {logOutRequested})(LogOut);
