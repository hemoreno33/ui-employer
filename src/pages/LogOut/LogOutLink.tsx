import React from 'react';
import {Link} from 'react-router-dom';

export const LogOutLink: React.FunctionComponent = () => (
  <Link to="/" className="f4 underline blue" id="action-button">
    Go back to Home
  </Link>
);
