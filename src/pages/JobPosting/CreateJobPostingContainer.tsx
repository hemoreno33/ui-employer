import {connect} from 'react-redux';
import {getAccountUser} from '~/flux/account';
import {IAppState} from '~/flux/index';
import {postJobPosting} from '~/services/employer';
import {CreateJobPosting} from './CreateJobPosting';

const mapStateToProps = (state: IAppState) => ({user: getAccountUser(state), postJobPosting});

export const CreateJobPostingContainer = connect(mapStateToProps)(CreateJobPosting);
