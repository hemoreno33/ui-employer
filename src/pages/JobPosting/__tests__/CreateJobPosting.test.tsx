import {MockedProvider} from '@apollo/react-testing';
import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {Provider} from 'react-redux';
import {MemoryRouter as Router} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {jobPostingSteps} from '~/components/JobPosting/JobPostingForm';
import {cleanSnapshot, nextTick} from '~/testUtil/enzyme';
import {CreateJobPosting} from '../CreateJobPosting';

describe('JobPosts/CreateJobPosting', () => {
  const mock: any = {};
  const routeComponentProps = {
    history: {
      ...mock,
      push: jest.fn(),
    },
    location: {
      ...mock,
      hash: jobPostingSteps[1].id,
      state: 'skywalker',
    },
    match: mock,
  };
  const store = configureStore()({
    jobTitles: {
      results: [],
    },
  });
  it('should render correctly', async () => {
    const wrapper = mount(
      <MockedProvider>
        <Provider store={store}>
          <Router initialEntries={[{pathname: '/', key: 'key'}]}>
            <CreateJobPosting {...routeComponentProps} postJobPosting={jest.fn()} />
          </Router>
        </Provider>
      </MockedProvider>,
    );

    await nextTick(wrapper);
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });
});
