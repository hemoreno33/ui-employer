import React from 'react';

export const StaticNudge = () => {
  return (
    <>
      <div data-cy="static-nudge" className="bg-washed-yellow pa3 lh-title black-80 f5">
        <p className="ma0 fw6">Wish to show your support for the SGUnited Jobs initiative?</p>
        <p className="ma0 fw4 pt2">Simply add "#SGUnitedJobs" in the job title</p>
      </div>
    </>
  );
};
