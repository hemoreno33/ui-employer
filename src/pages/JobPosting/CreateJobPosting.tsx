import {FORM_ERROR} from 'final-form';
import React, {useEffect, useState} from 'react';
import {RouteComponentProps} from 'react-router';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {JobPostingFormContainer} from '~/components/JobPosting/JobPostingFormContainer';
import {HeaderContainer} from '~/components/Navigation/Header';
import {IUser} from '~/flux/account';
import {StaticNudge} from '~/pages/JobPosting/StaticNudge';
import {postJobPosting, prepareCreateJobPayload} from '~/services/employer';
import styles from './CreateJobPosting.scss';

interface ICreateJobPostingProps extends RouteComponentProps {
  postJobPosting: typeof postJobPosting;
  user?: IUser;
}

export const CreateJobPosting: React.FunctionComponent<ICreateJobPostingProps> = (props) => {
  const [jobTitle, setJobTitle] = useState<string | undefined>();
  const title = 'Create Job Posting';
  const crumbsTitle = jobTitle ? `${title} - ${jobTitle}` : title;

  useEffect(() => {
    document.title = 'Create Job Posting | MyCareersFuture Employer';
  }, []);

  const onSubmit = async (values: IJobPostingFormState) => {
    try {
      if (!props.user) {
        throw new Error('Invalid User');
      }
      const response = await props.postJobPosting(prepareCreateJobPayload(values, props.user));
      props.history.push(`/jobs/${response.uuid}/success`, response);
    } catch (error) {
      return {[FORM_ERROR]: error.message};
    }
  };

  return (
    <section className="flex-auto-ie">
      <HeaderContainer data-cy="manage-applicants-all-jobs" crumbs={[crumbsTitle]} />
      <main className={`flex pa3 ${styles.formContainer}`}>
        <JobPostingFormContainer onTitleChange={setJobTitle} onSubmit={onSubmit} />
        <aside className={`pl3 ${styles.asideBar}`}>
          <StaticNudge />
        </aside>
      </main>
    </section>
  );
};
