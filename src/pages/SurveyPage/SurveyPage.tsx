import React from 'react';

export const SurveyPage: React.FunctionComponent = () => {
  return (
    <iframe
      style={{height: '2100px', overflowY: 'hidden'}}
      scrolling="no"
      src="https://form.gov.sg/#!/forms/ssgwsg/5bd6b21fe1ab44000f7588bc"
      width="100%"
      frameBorder="0"
      sandbox="allow-scripts allow-same-origin"
    />
  );
};
