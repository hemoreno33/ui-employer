import {connect} from 'react-redux';
import {IAppState} from '~/flux';
import {MaintenanceBranch} from './MaintenanceBranch';

const mapStateToProps = ({notification: {maintenances}}: IAppState) => ({maintenances});

export const MaintenanceBranchContainer = connect(mapStateToProps)(MaintenanceBranch);
