import {connect} from 'react-redux';
import {
  getCompanyDescription,
  getCompanyLogo,
  getCompanyName,
  getCompanyUen,
  COMPANY_FETCH_REQUESTED,
  getCompanyEmployeeCount,
  getCompanyUrl,
  companyUpdated,
  companyFetchRequested,
} from '~/flux/company';
import {IAppState} from '~/flux';
import {CompanyProfile} from './CompanyProfile';

export const mapStateToProps = ({company, account}: IAppState) => ({
  companyLogotypeSrc: getCompanyLogo(company),
  companyName: getCompanyName(company, account),
  companyUen: getCompanyUen(company),
  companyDescription: getCompanyDescription(company),
  website: getCompanyUrl(company),
  numberOfEmployees: getCompanyEmployeeCount(company),
  loading: company.fetchStatus === COMPANY_FETCH_REQUESTED,
});

export const CompanyProfileContainer = connect(mapStateToProps, {
  companyUpdated,
  companyFetchRequested,
})(CompanyProfile);
