import {connect} from 'react-redux';
import {getCompanyName, getCompanyUen, getCompanyOperatingAddress, getCompanySsic} from '~/flux/company';
import {IAppState} from '~/flux';
import {CompanyRegisteredInfo, ICompanyRegisteredInfoProps} from './CompanyRegisteredInfo';

export const mapStateToProps = ({company, account}: IAppState) => ({
  name: getCompanyName(company, account),
  uen: getCompanyUen(company),
  address: getCompanyOperatingAddress(company),
  ssic: getCompanySsic(company),
});

export const CompanyRegisteredInfoContainer = connect<ICompanyRegisteredInfoProps, {}, {}, IAppState>(
  mapStateToProps,
  {},
)(CompanyRegisteredInfo);
