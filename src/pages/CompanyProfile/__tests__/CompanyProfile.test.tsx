import {Interaction, Matchers, ApolloGraphQLInteraction} from '@pact-foundation/pact';
import {mount} from 'enzyme';
import React from 'react';
import {act} from 'react-dom/test-utils';
import {Provider} from 'react-redux';
import {MemoryRouter} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {uenUserMock} from '~/__mocks__/account/account.mocks';
import {companyInfoMock, companyAddressMock} from '~/__mocks__/company/company.mocks';
import {PactBuilder} from '~/__mocks__/pact';
import {nextTick, cleanSnapshot} from '~/testUtil/enzyme';
import {CompanyProfileContainer} from '../CompanyProfileContainer';
import {GET_SSIC_LIST} from '~/graphql/jobs/jobs.query';
import {ApolloProvider} from 'react-apollo';
import {CompanyRegisteredInfo} from '../CompanyRegisteredInfo';
import toJson from 'enzyme-to-json';
// temporarily suppress the following error, which will be fixed when upgrading Graphql package 14.5.8
// error TS7016: Could not find a declaration file for module 'graphql'.
// @ts-ignore
import {print} from 'graphql';

const v2ApiMock = jest.fn();
jest.mock('~/config', () => ({
  config: {
    url: {
      apiJob: {
        get v2() {
          return v2ApiMock();
        },
      },
    },
  },
}));

describe('CompanyProfile', () => {
  const store = configureStore()({
    account: {
      data: uenUserMock,
    },
    company: {
      companyInfo: {
        ...companyInfoMock,
        addresses: [companyAddressMock],
      },
    },
    features: {companyProfile: true},
  });

  let jobPactBuilder: PactBuilder;
  let profilePactBuilder: PactBuilder;
  beforeAll(async () => {
    jobPactBuilder = new PactBuilder('api-job');
    await jobPactBuilder.setup();
    v2ApiMock.mockImplementation(() => `http://${jobPactBuilder.host}:${jobPactBuilder.port}/v2`);

    profilePactBuilder = new PactBuilder('api-profile');
    await profilePactBuilder.setup();

    const getSsicQueryMockResult = {
      data: {
        common: {
          ssicList: Matchers.eachLike({
            code: companyInfoMock.ssicCode,
            description: 'Academic tutoring services (eg tuition centres, private tutoring services)',
          }),
        },
      },
    };
    const getSsicQuery = new ApolloGraphQLInteraction()
      .uponReceiving('getSSICList with single code')
      .withQuery(print(GET_SSIC_LIST))
      .withVariables({code: companyInfoMock.ssicCode})
      .withOperation('getCommonSsic')
      .withRequest({
        method: 'POST',
        path: '/profile',
      })
      .willRespondWith({
        body: getSsicQueryMockResult,
        status: 200,
      });
    await profilePactBuilder.provider.addInteraction(getSsicQuery);
  });

  afterAll(async () => {
    await jobPactBuilder.provider.finalize();
    await profilePactBuilder.provider.finalize();
  });

  it('should display the company registered info', async () => {
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter>
          <ApolloProvider client={profilePactBuilder.getApolloClient()}>
            <CompanyProfileContainer />
          </ApolloProvider>
        </MemoryRouter>
      </Provider>,
    );
    await act(async () => {
      await profilePactBuilder.verifyInteractions();
    });
    await nextTick(wrapper);

    const companyRegisteredInfo = wrapper.find(CompanyRegisteredInfo);
    // @ts-ignore toJson typing issue when using .find
    expect(toJson(companyRegisteredInfo, cleanSnapshot())).toMatchSnapshot();
  });

  it('should call PATCH /companies/{uen} api when saved button is clicked from the modal', async () => {
    const {addresses, ...companyResponse} = companyInfoMock;

    const interaction = new Interaction()
      .uponReceiving('companyProfileComponent')
      .withRequest({
        body: {
          description: companyInfoMock.description,
        },
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'PATCH',
        path: '/v2/companies/T08GB0046G',
      })
      .willRespondWith({
        body: Matchers.like(companyResponse),
        status: 200,
      });
    await jobPactBuilder.provider.addInteraction(interaction);

    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter>
          <ApolloProvider client={profilePactBuilder.getApolloClient()}>
            <CompanyProfileContainer />
          </ApolloProvider>
        </MemoryRouter>
      </Provider>,
    );
    await nextTick(wrapper);

    wrapper.find('[data-cy="company-profile-save"]').simulate('submit');

    await act(async () => {
      await jobPactBuilder.verifyInteractions();
    });

    wrapper.update();

    expect(wrapper.find('[data-cy="update-company-profile-modal"]')).toHaveLength(1);
  });
});
