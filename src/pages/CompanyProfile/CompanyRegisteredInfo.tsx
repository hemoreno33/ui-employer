import React from 'react';
import {Query} from 'react-apollo';
import {GetCommonSsicQuery, GetCommonSsicQueryVariables} from '~/graphql/__generated__/types';
import {GET_SSIC_LIST} from '~/graphql/jobs/jobs.query';
import {ICompanyAddress} from '~/services/employer/company';
import {formatAddress} from '~/util/addresses';

export interface ICompanyRegisteredInfoProps {
  name: string;
  uen: string;
  address?: ICompanyAddress;
  ssic?: string;
}

export const CompanyRegisteredInfo: React.FunctionComponent<ICompanyRegisteredInfoProps> = ({
  name,
  uen,
  ssic,
  address,
}) => (
  <div className="flex flex-wrap pa2 w-100">
    <h2 className="w-100 ma0 pb4 f3 lh-title black-80 fw6" data-cy="name">
      {name}
    </h2>
    <div className="pa2 pl0 w-50" data-cy="uen">
      <label className="ma0 black-50 f5-5 lh-title normal">Business UEN</label>
      <p className="f4-5 mv2 black-80 lh-copy">{uen}</p>
    </div>
    <Query<GetCommonSsicQuery, GetCommonSsicQueryVariables> query={GET_SSIC_LIST} variables={{code: ssic}} skip={!ssic}>
      {({data: getSsicListData}) => {
        const ssicDescription = getSsicListData?.common.ssicList[0]?.description;
        return (
          <div className="pa2 pl0 w-50" data-cy="industry">
            <label className="ma0 black-50 f5-5 lh-title normal">Industry</label>
            <p className="f4-5 mv2 black-80 lh-copy">{[ssic, ssicDescription].filter(Boolean).join(' - ') || '-'}</p>
          </div>
        );
      }}
    </Query>
    <div className="pa2 pl0 w-100" data-cy="address">
      <label className="ma0 black-50 f5-5 lh-title normal">Address</label>
      <p className="f4-5 mv2 black-80 lh-copy">{address ? formatAddress(address) : '-'}</p>
    </div>
    <div className="pa2 pl0 w-100 black-80 pt3" data-cy="acra-website">
      To change the registered details, please visit the{' '}
      <a href="https://www.acra.gov.sg/" title="ACRA website" target="_blank" className="blue dib">
        ACRA website
      </a>
      .
    </div>
  </div>
);
