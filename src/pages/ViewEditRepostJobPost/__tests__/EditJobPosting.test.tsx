import {MockedProvider} from '@apollo/react-testing';
import {Interaction, Matchers} from '@pact-foundation/pact';
import {mount, ReactWrapper} from 'enzyme';
import {noop} from 'lodash/fp';
import React from 'react';
import {act} from 'react-dom/test-utils';
import {Provider} from 'react-redux';
import {MemoryRouter} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {uenUserMock} from '~/__mocks__/account/account.mocks';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {PactBuilder, transformArrayToEachLikeMatcher} from '~/__mocks__/pact';
import {GET_EDUCATION_LIST, GET_SSIC_LIST, GET_SSOC_LIST} from '~/graphql/jobs/jobs.query';
import {prepareCreateJobPayload} from '~/services/employer';
import {nextTick} from '~/testUtil/enzyme';
import {transformJobPostingToJobInput} from '~/util/transformJobPostingToJobInput.ts';
import {EditJobPostingContainer} from '../EditJobPostingContainer';

const v2ApiMock = jest.fn();
jest.mock('~/config', () => ({
  config: {
    url: {
      apiJob: {
        get v2() {
          return v2ApiMock();
        },
      },
    },
  },
}));

describe('EditJobPosting', () => {
  const store = configureStore()({
    account: {
      data: uenUserMock,
    },
  });

  const previewMocks = [
    {
      request: {
        query: GET_SSOC_LIST,
      },
      result: {
        data: {
          common: {
            ssocList: [
              {
                ssoc: 1,
                ssocTitle: 'some',
              },
            ],
          },
        },
      },
    },
    {
      request: {
        query: GET_SSIC_LIST,
      },
      result: {
        data: {
          common: {
            ssicList: [
              {
                code: '1',
                description: 'some',
              },
            ],
          },
        },
      },
    },
    {
      request: {
        query: GET_EDUCATION_LIST,
      },
      result: {
        data: {
          common: {
            ssecEqaList: [{actualCode: '1', description: 'JOHN DOE'}],
            ssecFosList: [{actualCode: '1', description: 'JANE DOE'}],
          },
        },
      },
    },
  ];

  let pactBuilder: PactBuilder;
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-job');
    await pactBuilder.setup();
    v2ApiMock.mockImplementation(() => `http://${pactBuilder.host}:${pactBuilder.port}/v2`);
  });

  afterAll(async () => pactBuilder.provider.finalize());

  it('should PUT /job/{uuid} when submit button is clicked from the modal', async () => {
    const jobPosting = {
      ...jobPostMock,
      hiringCompany: undefined,
      metadata: {
        ...jobPostMock.metadata,
        expiryDate: '2020-10-08T00:00:00.000Z',
        jobPostId: 'MCF-2019-0002972',
        newPostingDate: '2020-10-01T00:00:00.000Z',
      },
      minimumYearsExperience: 1,
      salary: {
        ...jobPostMock.salary,
        minimum: 100,
      },
      ssecEqa: '42',
      ssecFos: '0521',
      uuid: '77e25f585fba0c8c212e8b9f1779755f',
    };
    const jobPostingInput = prepareCreateJobPayload(
      transformJobPostingToJobInput(jobPosting),
      uenUserMock,
      jobPosting.metadata.newPostingDate,
    );

    const interaction = new Interaction()
      .given('job 77e25f585fba0c8c212e8b9f1779755f is open and has 0 edit count')
      .uponReceiving('putJobPosting')
      .withRequest({
        body: jobPostingInput,
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'PUT',
        path: `/v2/jobs/${jobPosting.uuid}`,
      })
      .willRespondWith({
        body: Matchers.like(transformArrayToEachLikeMatcher(jobPosting)),
        status: 200,
      });
    await pactBuilder.provider.addInteraction(interaction);
    const pathname = `/jobs/${jobPosting.uuid}/edit`;
    let wrapper!: ReactWrapper;
    await act(async () => {
      wrapper = mount(
        <MockedProvider mocks={previewMocks} addTypename={false}>
          <Provider store={store}>
            <MemoryRouter initialEntries={[{pathname, hash: '#preview'}]}>
              <EditJobPostingContainer jobPosting={jobPosting} onTitleChange={noop} />
            </MemoryRouter>
          </Provider>
        </MockedProvider>,
      );
    });
    await nextTick(wrapper);

    wrapper.find('[data-cy="new-post-next"]').simulate('submit');

    await act(async () => {
      wrapper.find('[data-cy="submit-button-edit-job-submit-modal"]').simulate('click');
    });

    await nextTick(wrapper);

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
  });
});
