import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {getAccountUser} from '~/flux/account';
import {IAppState} from '~/flux/index';
import {EditJobPosting} from './EditJobPosting';

const mapStateToProps = (state: IAppState) => ({user: getAccountUser(state)});
export const EditJobPostingContainer = connect(mapStateToProps)(withRouter(EditJobPosting));
