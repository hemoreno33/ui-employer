import {FORM_ERROR} from 'final-form';
import {noop} from 'lodash/fp';
import React, {useRef, useState} from 'react';
import {RouteComponentProps} from 'react-router';
import {EditJobSubmitModal} from '~/components/EditJobPosting/EditJobSubmitModal';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {JobPostingFormContainer} from '~/components/JobPosting/JobPostingFormContainer';
import {EditKeyInformationFields} from '~/components/JobPosting/KeyInformation/EditKeyInformationFields';
import {EditJobSkillsFields} from '~/components/JobPosting/Skills/EditJobSkillsFields';
import {EditWorkplaceDetailsFields} from '~/components/JobPosting/WorkplaceDetails/EditWorkplaceDetailsFields';
import {IUser} from '~/flux/account';
import {prepareCreateJobPayload, putJobPost} from '~/services/employer';
import {IJobPost} from '~/services/employer/jobs.types';
import {transformJobPostingToJobInput} from '~/util/transformJobPostingToJobInput.ts';

interface IEditJobPostingProps extends RouteComponentProps {
  jobPosting: IJobPost;
  user?: IUser;
  onTitleChange: (values: IJobPostingFormState['jobDescription']['title']) => void;
}

export const EditJobPosting: React.FunctionComponent<IEditJobPostingProps> = ({
  history,
  jobPosting,
  onTitleChange,
  user,
}) => {
  const [displaySubmitModal, setDisplaySubmitModal] = useState(false);
  const confirmationPromiseRef = useRef<{
    resolve: () => void;
    reject: () => void;
  }>({resolve: noop, reject: noop});

  const onSubmit = async (values: IJobPostingFormState) => {
    const confirmationPromise = new Promise((resolve, reject) => {
      confirmationPromiseRef.current = {resolve, reject};
    });

    setDisplaySubmitModal(true);
    try {
      await confirmationPromise;
    } catch {
      return setDisplaySubmitModal(false);
    }

    try {
      if (!user) {
        throw new Error('Invalid User');
      }
      const response = await putJobPost(
        jobPosting.uuid,
        prepareCreateJobPayload(values, user, jobPosting.metadata.newPostingDate),
      );
      history.push(`/jobs/${jobPosting.uuid}/success`, response);
    } catch (error) {
      setDisplaySubmitModal(false);
      return {[FORM_ERROR]: error.message};
    }
  };

  return (
    <>
      <JobPostingFormContainer
        initialValues={transformJobPostingToJobInput(jobPosting)}
        jobSkills={<EditJobSkillsFields />}
        keyInformation={<EditKeyInformationFields jobPostingDate={jobPosting.metadata.newPostingDate} />}
        workplaceDetails={<EditWorkplaceDetailsFields />}
        onTitleChange={onTitleChange}
        onSubmit={onSubmit}
      />
      {displaySubmitModal && (
        <EditJobSubmitModal
          editCount={jobPosting.metadata.editCount}
          onCancel={() => confirmationPromiseRef.current.reject()}
          onSubmit={() => confirmationPromiseRef.current.resolve()}
        />
      )}
    </>
  );
};
