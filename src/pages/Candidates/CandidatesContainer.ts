import {flowRight as compose} from 'lodash';
import {graphql} from 'react-apollo';
import {connect} from 'react-redux';
import {RouteComponentProps} from 'react-router-dom';
import {IFeatureToggles} from '~/flux/feature';
import {IAppState} from '~/flux/index';
import {jobPostingFetchRequested} from '~/flux/jobPosting/jobPosting.actions';
import {
  GetApplicationsCountQuery,
  GetApplicationsCountQueryVariables,
  GetSuggestedTalentsCountQuery,
  GetSuggestedTalentsCountQueryVariables,
  GetBookmarkedCandidatesCountQuery,
  GetBookmarkedCandidatesCountQueryVariables,
} from '~/graphql/__generated__/types';
import {GET_APPLICATIONS_COUNT} from '~/graphql/applications';
import {GET_SUGGESTED_TALENTS_COUNT} from '~/graphql/suggestedTalents';
import {IJobPost} from '~/services/employer/jobs.types';
import {pathToJobUuid} from '~/util/url';
import {Candidates} from './Candidates';
import {GET_BOOKMARKED_CANDIDATES_COUNT} from '~/graphql/candidates/bookmarkedCandidates.query';
import {isSuggestedTalentUnavailable, formatJob} from '~/util/jobPosts';

export interface ICandidatesContainerStateProps {
  fromLastLogin: string;
  jobUuid: string;
  threshold: number;
  job?: IJobPost;
  features: IFeatureToggles;
}

export interface ICandidatesContainerDispatchProps {
  jobPostingFetchRequested: (uuid: string) => any;
}

export interface IApplicationsCountQueryProps {
  applicationsTotal: number;
}

export interface ISuggestedTalentsCountQueryProps {
  suggestedTalentsTotal?: number;
}

export interface IBookmarkedCandidatesCountQueryProps {
  bookmarkedCandidatesTotal?: number;
  refetchBookmarkedCandidatesTotal?: () => Promise<any>;
}

export type ICandidatesContainerReduxProps = ICandidatesContainerStateProps & ICandidatesContainerDispatchProps;

export type ICandidatesProps = ICandidatesContainerReduxProps &
  IApplicationsCountQueryProps &
  ISuggestedTalentsCountQueryProps &
  IBookmarkedCandidatesCountQueryProps &
  RouteComponentProps<any>;

export const mapStateToProps = (
  {jobPosting, suggestedTalents: {fromLastLogin, threshold}, features}: IAppState,
  {match}: RouteComponentProps<any>,
): ICandidatesContainerStateProps => {
  const titleIdParam = match.params.titleId;
  const jobUuid = pathToJobUuid(titleIdParam);
  const posting = jobPosting.jobPostings[jobUuid]?.jobPost;
  return {
    features,
    fromLastLogin,
    job: posting,
    jobUuid,
    threshold,
  };
};

export const applicationsCountQuery = graphql<
  ICandidatesContainerReduxProps,
  GetApplicationsCountQuery,
  GetApplicationsCountQueryVariables,
  ICandidatesContainerReduxProps & IApplicationsCountQueryProps
>(GET_APPLICATIONS_COUNT, {
  options: ({jobUuid}) => ({
    variables: {
      jobId: jobUuid,
    },
  }),
  props: ({data, ownProps}) => ({
    ...ownProps,
    applicationsTotal: data?.applicationsForJob?.total ?? 0,
  }),
});

export const suggestedTalentsCountQuery = graphql<
  ICandidatesContainerReduxProps,
  GetSuggestedTalentsCountQuery,
  GetSuggestedTalentsCountQueryVariables,
  ICandidatesContainerReduxProps & ISuggestedTalentsCountQueryProps
>(GET_SUGGESTED_TALENTS_COUNT, {
  options: ({fromLastLogin, jobUuid, threshold}) => ({
    variables: {
      fromLastLogin,
      jobId: jobUuid,
      threshold,
    },
  }),
  props: ({data, ownProps}) => ({
    ...ownProps,
    suggestedTalentsTotal: data?.suggestedTalentsForJob?.total,
  }),
  skip: ({job}) => !job || isSuggestedTalentUnavailable(formatJob(job)),
});

export const bookmarkedCandidatesCountQuery = graphql<
  ICandidatesContainerReduxProps,
  GetBookmarkedCandidatesCountQuery,
  GetBookmarkedCandidatesCountQueryVariables,
  ICandidatesContainerReduxProps & IBookmarkedCandidatesCountQueryProps
>(GET_BOOKMARKED_CANDIDATES_COUNT, {
  options: ({jobUuid}) => ({
    variables: {
      jobId: jobUuid,
    },
  }),
  props: ({data, ownProps}) => ({
    ...ownProps,
    bookmarkedCandidatesTotal: data?.bookmarkedCandidatesForJob?.total,
    refetchBookmarkedCandidatesTotal: data?.refetch,
  }),
  skip: ({features}) => !features.bookmarkTab,
});

export const CandidatesContainer = compose([
  connect<ICandidatesContainerStateProps, ICandidatesContainerDispatchProps, RouteComponentProps<any>, IAppState>(
    mapStateToProps,
    {jobPostingFetchRequested},
  ),
  applicationsCountQuery,
  suggestedTalentsCountQuery,
  bookmarkedCandidatesCountQuery,
])(Candidates);
