import {map, compact} from 'lodash/fp';
import React from 'react';
import {ApplicationsContainer} from '~/components/Applications/ApplicationsContainer';
import {BookmarkedCandidatesContainer} from '~/components/BookmarkedCandidates/BookmarkedCandidatesContainer';
import {JobBar} from '~/components/JobBar/JobBar';
import JobPostMenu from '~/components/Jobs/JobPostMenu';
import {CardLoader} from '~/components/Layouts/CardLoader';
import {HeaderContainer} from '~/components/Navigation/Header';
import {Tab, Tabs} from '~/components/Navigation/Tabs';
import {OnboardingProviderWithUser} from '~/components/Onboarding/OnboardingContext';
import {SuggestedTalentsContainer} from '~/components/SuggestedTalents/SuggestedTalentsContainer';
import {SUGGESTED_TALENTS_TOTAL_LIMIT} from '~/graphql/suggestedTalents';
import {CandidatesTabs} from '~/pages/Candidates/Candidates.constants';
import {ICandidatesProps} from '~/pages/Candidates/CandidatesContainer';
import {formatJob, isSuggestedTalentUnavailable} from '~/util/jobPosts';

interface ITabProp {
  tabId: CandidatesTabs;
  title: string;
  total?: number;
  children: React.ReactNode;
}

export class Candidates extends React.Component<ICandidatesProps, {}> {
  constructor(props: ICandidatesProps) {
    super(props);
    this.handleTabSelect = this.handleTabSelect.bind(this);
  }

  public handleTabSelect(index: CandidatesTabs) {
    const {history, match} = this.props;
    history.push(`/jobs/${match.params.titleId}/${index}`);
  }

  public componentDidMount() {
    this.props.jobPostingFetchRequested(this.props.jobUuid);
  }

  public componentDidUpdate() {
    if (this.props.job) {
      const formattedJob = formatJob(this.props.job);
      document.title = `${formattedJob.title} - ${formattedJob.employerName} | MyCareersFuture Employer`;
    }
  }

  public render() {
    const {
      job,
      jobUuid,
      applicationsTotal,
      suggestedTalentsTotal,
      bookmarkedCandidatesTotal,
      refetchBookmarkedCandidatesTotal,
      match,
      features,
    } = this.props;
    const formattedJob = job && formatJob(job);
    const title = formattedJob ? formattedJob.title : 'Unknown Job';
    const suggestedTalentsTotalWithLimit =
      suggestedTalentsTotal && suggestedTalentsTotal > SUGGESTED_TALENTS_TOTAL_LIMIT
        ? SUGGESTED_TALENTS_TOTAL_LIMIT
        : suggestedTalentsTotal;

    const displayTabs = map(
      (tabProp: ITabProp) => <Tab key={tabProp.title} {...tabProp} />,
      compact([
        {
          tabId: CandidatesTabs.Applications,
          title: 'Applicants',
          total: applicationsTotal,
          children: (
            <ApplicationsContainer
              jobUuid={jobUuid}
              refetchBookmarkedCandidatesTotal={refetchBookmarkedCandidatesTotal}
            />
          ),
        },
        {
          tabId: CandidatesTabs.SuggestedTalents,
          title: 'Suggested Talents',
          total: suggestedTalentsTotalWithLimit,
          children: (
            <SuggestedTalentsContainer
              jobUuid={jobUuid}
              unavailableSuggestedTalents={!!formattedJob && isSuggestedTalentUnavailable(formattedJob)}
              refetchBookmarkedCandidatesTotal={refetchBookmarkedCandidatesTotal}
            />
          ),
        },
        features.bookmarkTab
          ? {
              tabId: CandidatesTabs.Saved,
              title: 'Saved',
              total: bookmarkedCandidatesTotal,
              children: (
                <BookmarkedCandidatesContainer
                  jobUuid={jobUuid}
                  refetchBookmarkedCandidatesTotal={refetchBookmarkedCandidatesTotal}
                />
              ),
            }
          : null,
      ]),
    );

    return (
      <section className="flex-auto-ie">
        <HeaderContainer crumbs={['All Jobs', title]} />
        <div className="flex">
          {job ? (
            <>
              <JobBar job={job} />
              <JobPostMenu job={job} />
            </>
          ) : (
            <CardLoader className="flex w-100" cardNumber={3} />
          )}
        </div>
        <OnboardingProviderWithUser>
          <Tabs selectedTab={match.params.tab} onTabSelect={this.handleTabSelect}>
            {displayTabs}
          </Tabs>
        </OnboardingProviderWithUser>
      </section>
    );
  }
}
