export enum CandidatesTabs {
  Applications = 'applications',
  SuggestedTalents = 'suggested-talents',
  Saved = 'saved',
}
