import {mount} from 'enzyme';
import React from 'react';
import configureStore from 'redux-mock-store';
import {ApolloProvider} from 'react-apollo';
import {Provider} from 'react-redux';
import {ApolloGraphQLInteraction, Matchers} from '@pact-foundation/pact';
import {act} from 'react-dom/test-utils';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {IAppState} from '~/flux';
import {PactBuilder} from '~/__mocks__/pact';
import {GET_APPLICATIONS_COUNT} from '~/graphql/applications';
import {GET_SUGGESTED_TALENTS_COUNT} from '~/graphql/suggestedTalents';
import {GET_BOOKMARKED_CANDIDATES_COUNT} from '~/graphql/candidates/bookmarkedCandidates.query';
import {nextTick} from '~/testUtil/enzyme';
import {CandidatesContainer} from '../CandidatesContainer';
import {format, subYears} from 'date-fns';
import {JobStatusCodes} from '~/services/employer/jobs.types';
// temporarily suppress the following error, which will be fixed when upgrading Graphql package 14.5.8
// error TS7016: Could not find a declaration file for module 'graphql'.
// @ts-ignore
import {print} from 'graphql';
jest.mock('../Candidates', () => {
  return {
    Candidates: () => {
      return <></>;
    },
  };
});

const job = {...jobPostMock, uuid: 'R90SS0001A(2)'};
const routeComponentProps = {
  match: {
    params: {
      titleId: job.uuid,
    },
  },
};

const state: Pick<IAppState, 'jobPosting' | 'features' | 'suggestedTalents'> = {
  features: {
    bookmarkTab: true,
  },
  jobPosting: {
    jobPostings: {
      [job.uuid]: {
        jobPost: job,
      },
    },
  },
  suggestedTalents: {
    fromLastLogin: '2019-01-01',
    threshold: 1,
  },
};

let pactBuilder: PactBuilder;
describe('Pages/CandidatesContainer', () => {
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-profile');
    await pactBuilder.setup();
  });
  afterAll(async () => pactBuilder.provider.finalize());

  it('should render CandidatesContainer', async () => {
    await addApplicationsCountInteraction();
    await addSuggestedtalentsCountInteraction();
    await addBookmarkedCandidatesCountInteraction();

    const store = configureStore()(state);
    const wrapper = mount(
      <Provider store={store}>
        <ApolloProvider client={pactBuilder.getApolloClient()}>
          <CandidatesContainer {...routeComponentProps} />
        </ApolloProvider>
      </Provider>,
    );

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
    await nextTick(wrapper);

    const candidatesPage = wrapper.find('Candidates');
    expect(candidatesPage.prop('applicationsTotal')).toEqual(10);
    expect(candidatesPage.prop('suggestedTalentsTotal')).toEqual(3);
    expect(candidatesPage.prop('bookmarkedCandidatesTotal')).toEqual(6);
  });

  it('should not call getSuggestedTalentsCountQuery when job status is closed and original posting date past 1 year', async () => {
    await addApplicationsCountInteraction();
    await addBookmarkedCandidatesCountInteraction();

    const store = configureStore()({
      ...state,
      jobPosting: {
        jobPostings: {
          [job.uuid]: {
            jobPost: {
              ...job,
              status: {id: JobStatusCodes.Closed, jobStatus: 'Closed'},
              metadata: {...jobPostMock.metadata, originalPostingDate: format(subYears(new Date(), 1), 'yyyy-MM-dd')},
            },
          },
        },
      },
    });
    const wrapper = mount(
      <Provider store={store}>
        <ApolloProvider client={pactBuilder.getApolloClient()}>
          <CandidatesContainer {...routeComponentProps} />
        </ApolloProvider>
      </Provider>,
    );

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
    await nextTick(wrapper);

    const candidatesPage = wrapper.find('Candidates');
    expect(candidatesPage.prop('applicationsTotal')).toEqual(10);
    expect(candidatesPage.prop('suggestedTalentsTotal')).toBeUndefined();
    expect(candidatesPage.prop('bookmarkedCandidatesTotal')).toEqual(6);
  });

  it('should not call bookmarkedCandidatesCountQuery when bookmarkTab = false', async () => {
    await addApplicationsCountInteraction();
    await addSuggestedtalentsCountInteraction();

    const store = configureStore()({...state, features: {bookmarkTab: false}});
    const wrapper = mount(
      <Provider store={store}>
        <ApolloProvider client={pactBuilder.getApolloClient()}>
          <CandidatesContainer {...routeComponentProps} />
        </ApolloProvider>
      </Provider>,
    );

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
    await nextTick(wrapper);

    const candidatesPage = wrapper.find('Candidates');
    expect(candidatesPage.prop('applicationsTotal')).toEqual(10);
    expect(candidatesPage.prop('suggestedTalentsTotal')).toEqual(3);
    expect(candidatesPage.prop('bookmarkedCandidatesTotal')).toBeUndefined();
  });

  it('should refetch bookmarkedCandidatesCountQuery when refetchBookmarkedCandidatesTotal is triggered', async () => {
    await addApplicationsCountInteraction();
    await addSuggestedtalentsCountInteraction();
    await addBookmarkedCandidatesCountInteraction();

    const store = configureStore()(state);
    const wrapper = mount(
      <Provider store={store}>
        <ApolloProvider client={pactBuilder.getApolloClient()}>
          <CandidatesContainer {...routeComponentProps} />
        </ApolloProvider>
      </Provider>,
    );

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
    await nextTick(wrapper);

    await addBookmarkedCandidatesCountInteraction(7);
    const candidatesPage = wrapper.find('Candidates');
    (candidatesPage.prop('refetchBookmarkedCandidatesTotal') as any)();

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
    await nextTick(wrapper);

    const updatedCandidatesPage = wrapper.find('Candidates');
    expect(updatedCandidatesPage.prop('bookmarkedCandidatesTotal')).toEqual(7);
  });
});

const addApplicationsCountInteraction = async (total = 10) => {
  const applicationsCountInteraction = new ApolloGraphQLInteraction()
    .uponReceiving(`get applications count for job return total=${total}`)
    .withQuery(print(GET_APPLICATIONS_COUNT))
    .withOperation('getApplicationsCount')
    .withRequest({
      method: 'POST',
      path: '/profile',
    })
    .withVariables({jobId: job.uuid})
    .willRespondWith({
      body: {
        data: {
          applicationsForJob: {
            total: Matchers.integer(total),
            unviewedTotal: Matchers.integer(5),
          },
        },
      },
      status: 200,
    });
  await pactBuilder.provider.addInteraction(applicationsCountInteraction);
};

const addSuggestedtalentsCountInteraction = async (total = 3) => {
  const suggestedTalentsCountInteraction = new ApolloGraphQLInteraction()
    .uponReceiving(`get suggested talents count for job return total=${total}`)
    .withQuery(print(GET_SUGGESTED_TALENTS_COUNT))
    .withOperation('getSuggestedTalentsCount')
    .withRequest({
      method: 'POST',
      path: '/profile',
    })
    .withVariables({...state.suggestedTalents, jobId: job.uuid})
    .willRespondWith({
      body: {
        data: {
          suggestedTalentsForJob: {
            total: Matchers.integer(total),
          },
        },
      },
      status: 200,
    });
  await pactBuilder.provider.addInteraction(suggestedTalentsCountInteraction);
};

const addBookmarkedCandidatesCountInteraction = async (total = 6) => {
  const bookmarkedCandidatesCountInteraction = new ApolloGraphQLInteraction()
    .uponReceiving(`get bookmarked candidates count for job return total=${total}`)
    .withQuery(print(GET_BOOKMARKED_CANDIDATES_COUNT))
    .withOperation('getBookmarkedCandidatesCount')
    .withRequest({
      method: 'POST',
      path: '/profile',
    })
    .withVariables({jobId: job.uuid})
    .willRespondWith({
      body: {
        data: {
          bookmarkedCandidatesForJob: {
            total: Matchers.integer(total),
          },
        },
      },
      status: 200,
    });
  await pactBuilder.provider.addInteraction(bookmarkedCandidatesCountInteraction);
};
