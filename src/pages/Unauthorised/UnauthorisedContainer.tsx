import {connect} from 'react-redux';
import {login} from '~/flux/account';
import {Unauthorised} from './Unauthorised';

export const UnauthorisedContainer = connect(null, {login})(Unauthorised);
