import {ERROR, licence} from '~/pages/FirstTimeLogin/FirstTimeLoginValidation';

describe('FirstTimeLoginValidation', () => {
  it('Should return error if invalid licence', () => {
    const invalidLicence = '123#aws@d%';
    expect(licence(invalidLicence)).toEqual(ERROR.INVALID_LICENCE);
  });
  it('Should invalidate size of licence', () => {
    const invalidLicence = 'ABC123456';
    expect(licence(invalidLicence)).toEqual(ERROR.INVALID_LICENCE);
  });
});
