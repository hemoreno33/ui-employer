/**
 * CSS variables will not work in Internet Explorer if they are loaded during runtime (like in react-select).
 * cssStyleVars contains copies of global css variables and will be used in those scenarios instead.
 */
export const cssStyleVars = {
  borderStyle: '1px solid rgba(0, 0, 0, 0.2)',
  fontSize45: '1.125rem',
  fontSize55: '0.9375rem',
  fontSize6: '0.875rem',
  lightPrimary: '#e3d7f4',
  lightSilver: '#aaa',
  primary: '#5d2ca4',
  red: '#ff4136',
  white60: 'rgba(255, 255, 255, 0.6)',
};
