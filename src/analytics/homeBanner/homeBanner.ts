import {googleAnalytics, EVENT_ACTION, EVENT_CATEGORY} from '~/util/analytics';

const LABEL_GET_STARTED = 'corppass-get-started';

export const handleCorpPassGetStartedClick = (analytics = googleAnalytics) => {
  analytics.sendEvent({
    eventAction: EVENT_ACTION.CORPPASS_GET_STARTED,
    eventCategory: EVENT_CATEGORY.HOME_BANNER,
    eventLabel: LABEL_GET_STARTED,
  });
};
