import {handleCorpPassGetStartedClick} from '../homeBanner';
import {EVENT_ACTION, EVENT_CATEGORY} from '~/util/analytics';

describe('handleCorpPassGetStartedClick()', () => {
  const analytics = {
    sendEvent: jest.fn(),
    sendPageView: jest.fn(),
    set: jest.fn(),
  };

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should send event with corppass-getstarted when link is clicked', () => {
    handleCorpPassGetStartedClick(analytics);
    expect(analytics.sendEvent).toHaveBeenCalledTimes(1);
    expect(analytics.sendEvent).toHaveBeenCalledWith({
      eventAction: EVENT_ACTION.CORPPASS_GET_STARTED,
      eventCategory: EVENT_CATEGORY.HOME_BANNER,
      eventLabel: 'corppass-get-started',
    });
  });
});
