import {reJobPostId} from '~/pages/JobsPage/JobsPage.constants';
import {googleAnalytics, EVENT_ACTION, EVENT_CATEGORY} from '~/util/analytics';

const LABEL_WITHOUT_KEYWORD = 'without-keyword';

export const handleJobsSearchQueryEvent = (query: string, analytics = googleAnalytics) => {
  const jobSearchQueryFields = query
    ? reJobPostId.test(query)
      ? {
          eventAction: EVENT_ACTION.JOB_SEARCH_WITH_JOB_ID,
          eventLabel: query,
        }
      : {
          eventAction: EVENT_ACTION.JOB_SEARCH_WITH_KEYWORD,
          eventLabel: query.toLowerCase(),
        }
    : {
        eventAction: EVENT_ACTION.JOB_SEARCH_WITHOUT_KEYWORD,
        eventLabel: LABEL_WITHOUT_KEYWORD,
      };

  analytics.sendEvent({
    eventCategory: EVENT_CATEGORY.ALL_JOBS_OPEN,
    ...jobSearchQueryFields,
  });
};
