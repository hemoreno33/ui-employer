import {handleJobsSearchQueryEvent} from '../jobsSearchQuery';
import {EVENT_ACTION, EVENT_CATEGORY} from '~/util/analytics';

describe('analytics/jobSearchQuery', () => {
  const analytics = {
    sendEvent: jest.fn(),
    sendPageView: jest.fn(),
    set: jest.fn(),
  };

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('handleJobsSearchQueryEvent()', () => {
    it('should send event with search containing exact job Id', () => {
      const query = 'JOB-2018-0000001';
      handleJobsSearchQueryEvent(query, analytics);
      expect(analytics.sendEvent).toHaveBeenCalledTimes(1);
      expect(analytics.sendEvent).toHaveBeenCalledWith({
        eventAction: EVENT_ACTION.JOB_SEARCH_WITH_JOB_ID,
        eventCategory: EVENT_CATEGORY.ALL_JOBS_OPEN,
        eventLabel: query,
      });
    });

    it('should send event with search keywords entered', () => {
      const query = 'senior developer';
      handleJobsSearchQueryEvent(query, analytics);
      expect(analytics.sendEvent).toHaveBeenCalledTimes(1);
      expect(analytics.sendEvent).toHaveBeenCalledWith({
        eventAction: EVENT_ACTION.JOB_SEARCH_WITH_KEYWORD,
        eventCategory: EVENT_CATEGORY.ALL_JOBS_OPEN,
        eventLabel: query,
      });
    });

    it('should send event without keywords entered', () => {
      const query = '';
      handleJobsSearchQueryEvent(query, analytics);
      expect(analytics.sendEvent).toHaveBeenCalledTimes(1);
      expect(analytics.sendEvent).toHaveBeenCalledWith({
        eventAction: EVENT_ACTION.JOB_SEARCH_WITHOUT_KEYWORD,
        eventCategory: EVENT_CATEGORY.ALL_JOBS_OPEN,
        eventLabel: 'without-keyword',
      });
    });
  });
});
