import {googleAnalytics, EVENT_ACTION, EVENT_CATEGORY} from '~/util/analytics';

export enum MAIN_NAVIGATION_MENU_LINK_TYPE {
  WSG_LOGO = 'wsgLogo',
  JOBSEEKER = 'jobseeker',
}

const navMenuLinkTypeToEventActionMapping = {
  [MAIN_NAVIGATION_MENU_LINK_TYPE.WSG_LOGO]: EVENT_ACTION.WSG_LOGO,
  [MAIN_NAVIGATION_MENU_LINK_TYPE.JOBSEEKER]: EVENT_ACTION.SWITCH_TO_JOBSEEKER,
};

export const mainNavigationMenuLinks = (
  title: MAIN_NAVIGATION_MENU_LINK_TYPE,
  pathname: string,
  analytics = googleAnalytics,
) => {
  analytics.sendEvent({
    eventAction: navMenuLinkTypeToEventActionMapping[title],
    eventCategory: EVENT_CATEGORY.MAIN_NAVIGATION_MENU,
    eventLabel: pathname,
  });
};
