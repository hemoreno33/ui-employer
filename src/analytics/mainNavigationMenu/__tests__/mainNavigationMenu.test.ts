import {mainNavigationMenuLinks, MAIN_NAVIGATION_MENU_LINK_TYPE} from '../mainNavigationMenu';
import {EVENT_ACTION, EVENT_CATEGORY} from '~/util/analytics';

describe('mainNavigationMenuLinks()', () => {
  const analytics = {
    sendEvent: jest.fn(),
    sendPageView: jest.fn(),
    set: jest.fn(),
  };

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should send logo clicked event', () => {
    mainNavigationMenuLinks(MAIN_NAVIGATION_MENU_LINK_TYPE.WSG_LOGO, '/foo', analytics);
    expect(analytics.sendEvent).toHaveBeenCalledTimes(1);
    expect(analytics.sendEvent).toHaveBeenCalledWith({
      eventAction: EVENT_ACTION.WSG_LOGO,
      eventCategory: EVENT_CATEGORY.MAIN_NAVIGATION_MENU,
      eventLabel: '/foo',
    });
  });

  it('should send switch to jobseeker clicked event', () => {
    mainNavigationMenuLinks(MAIN_NAVIGATION_MENU_LINK_TYPE.JOBSEEKER, '/foo', analytics);
    expect(analytics.sendEvent).toHaveBeenCalledTimes(1);
    expect(analytics.sendEvent).toHaveBeenCalledWith({
      eventAction: EVENT_ACTION.SWITCH_TO_JOBSEEKER,
      eventCategory: EVENT_CATEGORY.MAIN_NAVIGATION_MENU,
      eventLabel: '/foo',
    });
  });
});
