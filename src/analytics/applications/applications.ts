import {ApplicationSortCriteria, CANDIDATES_SORT_CRITERIA} from '~/flux/applications';
import {CUSTOM_DIMENSIONS, googleAnalytics, EVENT_ACTION, EVENT_CATEGORY} from '~/util/analytics';

export const transformSortLabel = (sortCriteria: ApplicationSortCriteria): string | undefined => {
  const sortObj = CANDIDATES_SORT_CRITERIA.find((obj) => obj.value === sortCriteria);
  return sortObj && sortObj.label;
};

export const TOP_MATCH_FILTERED = 'TOP_MATCH_FILTERED';
export const TOP_MATCH_UNFILTERED = 'TOP_MATCH_UNFILTERED';
export const TOP_MATCH_APPLICANT = 'TOP_MATCH_APPLICANT';

export const handleApplicationSortingType = (sortCriteria: ApplicationSortCriteria, analytics = googleAnalytics) => {
  const sortType = transformSortLabel(sortCriteria);
  analytics.sendEvent({
    eventAction: EVENT_ACTION.APPLICANTS_SORTING_TYPE,
    eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
    eventLabel: sortType,
  });
};

export const handleApplicationTopMatchFilter = (checked: boolean, analytics = googleAnalytics) => {
  analytics.sendEvent({
    eventAction: EVENT_ACTION.TOP_MATCH_FILTER_CLICKED,
    eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
    eventLabel: checked ? TOP_MATCH_FILTERED : TOP_MATCH_UNFILTERED,
  });
};

export const handleApplicationTopMatchLabel = (analytics = googleAnalytics) => {
  analytics.sendEvent({
    eventAction: EVENT_ACTION.APPLICATION_TOP_MATCH_LABEL,
    eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
    eventLabel: TOP_MATCH_APPLICANT,
  });
};

export const handleApplicationTopMatchTotal = (
  total: number,
  sortCriteria: ApplicationSortCriteria,
  jobId: string,
  analytics = googleAnalytics,
) => {
  const sortType = transformSortLabel(sortCriteria);
  analytics.set({
    [CUSTOM_DIMENSIONS.JOB_HIT]: jobId,
  });
  analytics.sendEvent({
    eventAction: EVENT_ACTION.TOP_MATCH_TOTAL,
    eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
    eventLabel: `${sortType}: ${total}`,
  });
};
