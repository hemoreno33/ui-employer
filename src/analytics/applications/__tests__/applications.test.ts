import {ApplicationSortCriteria} from '~/flux/applications';
import {EVENT_ACTION, EVENT_CATEGORY} from '~/util/analytics';
import {
  handleApplicationSortingType,
  handleApplicationTopMatchFilter,
  handleApplicationTopMatchLabel,
  handleApplicationTopMatchTotal,
  transformSortLabel,
} from '../applications';

describe('analytics/applications', () => {
  const analytics = {
    sendEvent: jest.fn(),
    sendPageView: jest.fn(),
    set: jest.fn(),
  };

  afterEach(() => {
    jest.resetAllMocks();
  });
  describe('handleApplicationSortingType', () => {
    it('should send event for application sort', () => {
      const sortLabel = transformSortLabel(ApplicationSortCriteria.SCORES_JOBKRED);
      handleApplicationSortingType(ApplicationSortCriteria.SCORES_JOBKRED, analytics);
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.APPLICANTS_SORTING_TYPE,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: sortLabel,
      });
    });
  });
  describe('handleApplicationTopMatchFilter', () => {
    it('should send event for filtering applicants when checkbox is clicked', () => {
      const payload = true;
      handleApplicationTopMatchFilter(payload, analytics);
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.TOP_MATCH_FILTER_CLICKED,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: 'TOP_MATCH_FILTERED',
      });
    });
  });
  describe('handleApplicationTopMatchLabel', () => {
    it('should send event for clicking applicants with top match label', () => {
      handleApplicationTopMatchLabel(analytics);
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.APPLICATION_TOP_MATCH_LABEL,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: 'TOP_MATCH_APPLICANT',
      });
    });
  });
  describe('handleApplicationTopMatchTotal', () => {
    it('should send event for total number of applicants with top matches', () => {
      const total = 12;
      const sortLabel = transformSortLabel(ApplicationSortCriteria.CREATED_ON);
      const jobUuid = 'abc1234xyz';
      handleApplicationTopMatchTotal(total, ApplicationSortCriteria.CREATED_ON, jobUuid, analytics);
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.TOP_MATCH_TOTAL,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: `${sortLabel}: ${total}`,
      });
    });
  });
});
