import {ApolloQueryResult} from 'apollo-boost';
import {ISystemContact} from '~/flux/systemContact/systemContact.constants';
import {setupClient} from '~/graphql';
import {
  AcceptTermsAndConditionsMutation,
  GetSystemContactQuery,
  UpdateSystemContactMutation,
} from '~/graphql/__generated__/types';
import {
  GET_SYSTEM_CONTACT,
  SET_SYSTEM_CONTACT,
  SET_TERMS_AND_CONDITIONS,
} from '~/graphql/systemContact/systemContact.query';

export type ReturnTypeFetchSystemContact = ApolloQueryResult<GetSystemContactQuery>;
export const fetchSystemContact = async (): Promise<ReturnTypeFetchSystemContact> => {
  const client = setupClient();
  return client.query({
    query: GET_SYSTEM_CONTACT,
  });
};

export type ReturnTypeMutateSystemContact = ApolloQueryResult<UpdateSystemContactMutation>;
export const mutateSystemContact = async (values: Partial<ISystemContact>): Promise<ReturnTypeMutateSystemContact> => {
  const client = setupClient();
  return client.mutate({
    mutation: SET_SYSTEM_CONTACT,
    variables: {
      systemContact: {
        contactNumber: values.contactNumber,
        designation: values.designation,
        email: values.email,
      },
    },
  });
};

export type ReturnTypeAcceptTermsAndConditions = ApolloQueryResult<AcceptTermsAndConditionsMutation>;
export const acceptTermsAndConditions = async (): Promise<ReturnTypeAcceptTermsAndConditions> => {
  const client = setupClient();
  return client.mutate({
    mutation: SET_TERMS_AND_CONDITIONS,
  });
};
