import {IWccSkillsByJobResponse} from '~/components/JobPosting/Skills/skills.types';
import {config} from '~/config';
import {fetchResponse} from '~/util/fetchResponse';

export const getWccSkillsByJob = async (jobDescription = '', limit?: number): Promise<IWccSkillsByJobResponse> => {
  const url = `${config.url.apiJob.v2}/wcc/skills`;
  const response = await fetch(url, {
    body: JSON.stringify({jobDescription, limit}),
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
  });
  return fetchResponse(response);
};
