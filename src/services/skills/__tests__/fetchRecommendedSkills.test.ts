import * as jobKredServices from '~/services/skills/getSkillsByJobTitle';
import * as wccServices from '~/services/skills/getWccSkillsByJob';
import {fetchRecommendedSkills} from '../fetchRecommendedSkills';

describe('fetchRecommendedSkills', () => {
  const getSkillsByJobTitleMock = jest.spyOn(jobKredServices, 'getSkillsByJobTitle');
  const getWccSkillsByJob = jest.spyOn(wccServices, 'getWccSkillsByJob');

  afterAll(() => {
    getSkillsByJobTitleMock.mockRestore();
    getWccSkillsByJob.mockRestore();
  });

  const jobKredSkillsMock = {
    jobTitle: 'JobTitle',
    skills: [
      {id: 1, skill: 'jobKredOne'},
      {id: 2, skill: 'jobKredTwo'},
      {id: 3, skill: 'common'},
    ],
  };
  const wccSkillsMock = {
    skills: [
      {id: 4, skill: 'wccOne'},
      {id: 5, skill: 'wccTwo'},
      {id: 3, skill: 'common'},
    ],
  };

  it('should fetch wcc and jobKred skills then dedupe', async () => {
    const expectedSkills = [
      {id: 1, skill: 'jobKredOne'},
      {id: 2, skill: 'jobKredTwo'},
      {id: 3, skill: 'common'},
      {id: 4, skill: 'wccOne'},
      {id: 5, skill: 'wccTwo'},
    ];

    getSkillsByJobTitleMock.mockImplementation(() => Promise.resolve(jobKredSkillsMock));
    getWccSkillsByJob.mockImplementation(() => Promise.resolve(wccSkillsMock));
    const result = await fetchRecommendedSkills('jobTitle', 'jobDescription');
    expect(result).toEqual(expectedSkills);
  });

  it('should return the other provider skills if one provder returns an error', async () => {
    const expectedSkills = [
      {id: 4, skill: 'wccOne'},
      {id: 5, skill: 'wccTwo'},
      {id: 3, skill: 'common'},
    ];

    getSkillsByJobTitleMock.mockImplementation(() => Promise.reject(new Error()));
    getWccSkillsByJob.mockImplementation(() => Promise.resolve(wccSkillsMock));
    const result = await fetchRecommendedSkills('jobTitle', 'jobDescription');
    expect(result).toEqual(expectedSkills);
  });

  it('should return an empty array if all providers returned an error', async () => {
    getSkillsByJobTitleMock.mockImplementation(() => Promise.reject(new Error()));
    getWccSkillsByJob.mockImplementation(() => Promise.reject(new Error()));
    const result = await fetchRecommendedSkills('jobTitle', 'jobDescription');
    expect(result).toEqual([]);
  });
});
