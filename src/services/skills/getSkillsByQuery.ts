import {escape} from 'lodash';
import {IJobSkill} from '~/components/JobPosting/Skills/skills.types';
import {config} from '~/config';

export const getSkillsByQuery = (query = ''): Promise<IJobSkill[]> => {
  const url = `${config.url.apiJob.v1}/autocomplete-skill?query=${escape(query)}`;
  return fetch(url, {
    credentials: 'include',
  }).then((res) => res.json());
};
