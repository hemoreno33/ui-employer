import {addDays, format} from 'date-fns';
import * as qs from 'qs';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {fosRequired} from '~/components/JobPosting/KeyInformation/keyInformationValidations';
import {IPillSkill} from '~/components/JobPosting/Skills/skills.types';
import {Location, LocationType} from '~/components/JobPosting/WorkplaceDetails/workplaceDetails.types';
import {config} from '~/config';
import {IUser} from '~/flux/account';
import {IJobsResponse} from '~/flux/jobPostings';
import {IFetchClosedJobs, IFetchOpenJobs} from '~/flux/jobPostings/jobPostings.types';
import {IExtendJob, IJobPost, IJobPostInput, JobStatusCodes, SalaryType} from '~/services/employer/jobs.types';
import {fetchResponse} from '~/util/fetchResponse';
import {includeCredentials} from '~/services/employer/includeCredentials';

export const PAGE_SIZE = 20;

export const getJobPosting = (uuid: string): Promise<IJobPost> => {
  return fetch(`${config.url.apiJob.v2}/jobs/${uuid}`, includeCredentials).then((res) => fetchResponse(res));
};

export const getJobs = (uen: string, query = ''): Promise<IJobsResponse> => {
  const url = `${config.url.apiJob.v2}/companies/${uen}/jobs${query}`;
  return fetch(url, includeCredentials).then((res) => fetchResponse(res));
};

export const getOpenJobs = (uen: string, payload: IFetchOpenJobs): Promise<IJobsResponse> => {
  const query = `?jobStatuses=${JobStatusCodes.Open},${JobStatusCodes.Reopened}&${qs.stringify(payload)}`;
  return getJobs(uen, query);
};

export const getClosedJobs = (uen: string, payload: IFetchClosedJobs): Promise<IJobsResponse> => {
  const query = `?jobStatuses=${JobStatusCodes.Closed}&${qs.stringify(payload)}`;
  return getJobs(uen, query);
};

export const getSelectedSkillIds = (skills: IPillSkill[]) => {
  return skills.filter((skill) => skill.selected).map(({id}) => ({id}));
};

export const prepareCreateJobPayload = (
  newPosting: IJobPostingFormState,
  account: IUser,
  jobPostingDate?: string,
): IJobPostInput => {
  const {jobDescription, keyInformation, jobPostingSkills, workplaceDetails} = newPosting;
  const {title, description, occupation, thirdPartyEmployer, thirdPartyEmployerEntityId} = jobDescription;
  const {
    jobPostDuration,
    numberOfVacancies,
    jobCategories,
    positionLevel,
    minimumYearsExperience,
    employmentType,
    qualification,
    fieldOfStudy,
    minimumSalary,
    maximumSalary,
    schemes,
  } = keyInformation;
  const {location, locationType, ...address} = workplaceDetails;
  return {
    address:
      locationType === LocationType.Multiple
        ? undefined
        : {
            ...address,
            isOverseas: location === Location.Overseas,
          },
    categories: jobCategories.map((category) => ({id: category})),
    description,
    employmentTypes: employmentType
      ? employmentType.map((employment) => {
          return {id: employment};
        })
      : [],
    hiringCompany: thirdPartyEmployer && thirdPartyEmployerEntityId ? {uen: thirdPartyEmployerEntityId} : undefined,
    metadata: {
      expiryDate: jobPostDuration
        ? format(addDays(jobPostingDate ? new Date(jobPostingDate) : new Date(), jobPostDuration), 'yyyy-MM-dd')
        : undefined,
    },
    minimumYearsExperience,
    numberOfVacancies,
    positionLevels: positionLevel ? [{id: positionLevel}] : [],
    postedCompany: {uen: account.userInfo.entityId},
    salary: {
      maximum: maximumSalary,
      minimum: minimumSalary,
      type: {id: SalaryType.Monthly},
    },
    schemes: schemes.filter((scheme) => scheme.selected).map(({id, subSchemeId}) => ({id, subSchemeId})),
    skills: getSelectedSkillIds(jobPostingSkills.addedSkills).concat(
      getSelectedSkillIds(jobPostingSkills.recommendedSkills),
    ),
    ssecEqa: qualification,
    ssecFos: qualification && fosRequired(qualification) ? fieldOfStudy : undefined,
    ssocCode: occupation,
    title,
  };
};

export const postJobPosting = async (newPosting: IJobPostInput): Promise<IJobPost> => {
  const url = `${config.url.apiJob.v2}/jobs`;
  const response = await fetch(url, {
    ...includeCredentials,
    body: JSON.stringify(newPosting),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
  });
  return fetchResponse(response);
};

export const closeJobPosting = async (uuid: string, isVacant: boolean): Promise<void> => {
  const url = `${config.url.apiJob.v2}/jobs/${uuid}/close`;
  const res = await fetch(url, {
    ...includeCredentials,
    body: JSON.stringify({isVacant}),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PUT',
  });

  if (!res.ok) {
    throw new Error(await res.text());
  }
};

export const extendJobPost = async (uuid: string, expiryDate: IExtendJob): Promise<IJobPost> => {
  const url = `${config.url.apiJob.v2}/jobs/${uuid}`;
  const res = await fetch(url, {
    ...includeCredentials,
    body: JSON.stringify(expiryDate),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PATCH',
  });
  return fetchResponse(res);
};

export const putJobPost = async (uuid: string, jobPosting: IJobPostInput): Promise<IJobPost> => {
  const url = `${config.url.apiJob.v2}/jobs/${uuid}`;
  const response = await fetch(url, {
    ...includeCredentials,
    body: JSON.stringify(jobPosting),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PUT',
  });
  return fetchResponse(response);
};

export const repostJobPost = async (uuid: string, expiryDate: string): Promise<IJobPost> => {
  const url = `${config.url.apiJob.v2}/jobs/${uuid}/repost`;
  const res = await fetch(url, {
    ...includeCredentials,
    body: JSON.stringify({expiryDate}),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PATCH',
  });
  return fetchResponse(res);
};
