import {Interaction, Matchers} from '@pact-foundation/pact';
import {jobPostInputMock, jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {PactBuilder, transformArrayToEachLikeMatcher} from '~/__mocks__/pact';
import {IPillSkill} from '~/components/JobPosting/Skills/skills.types';
import {getJobs, getSelectedSkillIds, postJobPosting} from '../jobs';
import {IJobPostInput} from '../jobs.types';

const v2ApiMock = jest.fn();

jest.mock('~/config', () => ({
  config: {
    url: {
      apiJob: {
        get v2() {
          return v2ApiMock();
        },
      },
    },
  },
}));

describe('services/employer/jobs', () => {
  let pactBuilder: PactBuilder;
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-job');
    await pactBuilder.setup();
    v2ApiMock.mockImplementation(() => `http://${pactBuilder.host}:${pactBuilder.port}/v2`);
  });
  const companyUen = '198701269D';
  afterAll(async () => pactBuilder.provider.finalize());

  describe('getJobs', () => {
    it('should GET /v2/companies/{uen}/jobs', async () => {
      const {schemes, ...jobWithoutScheme} = jobPostMock;
      const queryMockResult = {
        results: Matchers.eachLike(transformArrayToEachLikeMatcher(jobWithoutScheme)),
        total: Matchers.like(200),
      };

      const interaction = new Interaction()
        .uponReceiving('fetchJobs')
        .withRequest({
          method: 'GET',
          path: `/v2/companies/${companyUen}/jobs`,
          query: {
            limit: '2',
            page: '4',
          },
        })
        .willRespondWith({
          body: queryMockResult,
          status: 200,
        });
      await pactBuilder.provider.addInteraction(interaction);

      const response = await getJobs(companyUen, '?limit=2&page=4');
      await pactBuilder.verifyInteractions();
      expect(response.results).toHaveLength(1);
      expect(response.total).toEqual(200);
    });
  });

  describe('getSelectedSkillIds', () => {
    it('filter selected skills and return their respective ids', () => {
      const pillSkills: IPillSkill[] = [
        {id: 1, skill: 'foo', selected: false},
        {id: 2, skill: 'bar', selected: true},
        {id: 3, skill: 'baz', selected: true},
        {id: 4, skill: 'quz', selected: false},
      ];
      expect(getSelectedSkillIds(pillSkills)).toEqual([{id: 2}, {id: 3}]);
    });
  });

  describe('postJobPosting', () => {
    it('should POST /v2/job', async () => {
      const jobInput: IJobPostInput = {...jobPostInputMock, postedCompany: {uen: companyUen}};
      const queryMockResult = transformArrayToEachLikeMatcher(jobPostMock);

      const interaction = new Interaction()
        .uponReceiving('postJobPosting')
        .withRequest({
          body: jobInput,
          headers: {
            'Content-Type': 'application/json',
          },
          method: 'POST',
          path: '/v2/jobs',
        })
        .willRespondWith({
          body: Matchers.like(queryMockResult),
          status: 200,
        });
      await pactBuilder.provider.addInteraction(interaction);

      const response = await postJobPosting(jobInput);
      await pactBuilder.verifyInteractions();
      expect(response).toEqual(
        transformArrayToEachLikeMatcher(jobPostMock, (val: any) => expect.arrayContaining([val])),
      );
    });
  });
});
