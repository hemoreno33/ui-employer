import * as qs from 'qs';
import {FetchQuery, IFetchQuery} from '~/components/Core/FetchQuery';
import {ICompanyInfo, ICompanyInfoWithAddresses} from '~/services/employer/company';
import {config} from '~/config';
import {fetchResponse} from '~/util/fetchResponse';
import {includeCredentials} from '~/services/employer/includeCredentials';

export const getCompanyInfo = async (uen?: string): Promise<ICompanyInfoWithAddresses> => {
  if (!uen) {
    throw new Error('uen required');
  }
  const url = `${config.url.apiJob.v2}/companies/${uen}`;
  const response = await fetch(url, {
    credentials: 'include',
    cache: 'no-cache',
  });
  return fetchResponse(response);
};

interface IGetCompanyInfoQuery extends Pick<IFetchQuery<ICompanyInfoWithAddresses>, 'children'> {
  uen: string;
}
export function GetCompanyInfoQuery({uen, children}: IGetCompanyInfoQuery) {
  if (uen === '') {
    return children({error: 'uen required', loading: false});
  }
  return FetchQuery<ICompanyInfoWithAddresses>({
    children,
    init: {credentials: 'include'},
    input: `${config.url.apiJob.v2}/companies/${uen}`,
  });
}

interface IGetCompanySuggestionQuery extends Pick<IFetchQuery<{results: ICompanySuggestion[]}>, 'children'> {
  name: string;
}
export function GetCompanySuggestionQuery({name, children}: IGetCompanySuggestionQuery) {
  if (name.length < 3) {
    return children({error: 'minimum length 3', loading: false});
  }
  return FetchQuery<{results: ICompanySuggestion[]}>({
    children,
    input: `${config.url.apiJob.v2}/companies/suggestions?${qs.stringify({name})}`,
  });
}

export const updateCompanyProfile = async (uen: string, companyProfile: ICompanyInput): Promise<ICompanyInfo> => {
  const url = `${config.url.apiJob.v2}/companies/${uen}`;
  const response = await fetch(url, {
    ...includeCredentials,
    body: JSON.stringify(companyProfile),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PATCH',
  });
  return fetchResponse(response);
};

export interface ICompanySuggestion {
  name: string;
  uen: string;
}

export interface ICompanyInput {
  description: string;
  companyUrl?: string;
  employeeCount?: number;
  logoBase64?: string;
  virusScanResult?: string;
}
