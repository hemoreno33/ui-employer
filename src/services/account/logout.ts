import {config} from '~/config';

export const logout = async (): Promise<Response> => {
  const response = await fetch(`${config.url.apiJob.v2}/logout`, {
    credentials: 'include',
  });
  if (!response.ok) {
    throw new Error();
  }
  return response;
};
