import {config} from '~/config';
import {IUser} from '~/flux/account';

export const getAccount = async (): Promise<IUser> => {
  const response = await fetch(`${config.url.apiJob.v2}/account`, {
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
  });
  if (!response.ok) {
    throw new Error();
  }
  return response.json();
};
