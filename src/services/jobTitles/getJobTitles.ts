import {config} from '~/config';
import {IJobTitlesResponse} from '~/flux/jobTitles/jobTitles.reducer';

export const getJobTitles = (query = ''): Promise<IJobTitlesResponse> => {
  const url = `${config.url.apiJob.v2}/job-titles/suggestions?name=${query}`;
  return fetch(url, {
    credentials: 'include',
  }).then((res) => res.json());
};
