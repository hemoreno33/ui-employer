import {isNil} from 'lodash/fp';
import {ApplicationSortCriteria} from '~/flux/applications';
import {GetApplicationsScores} from '~/graphql/__generated__/types';

enum TopMatcherScore {
  WCC = 0.6,
  JOBKRED = 0.7,
}

export const isTopMatch = (scores?: GetApplicationsScores, sortCriteria?: ApplicationSortCriteria) => {
  const jobKredTopMatch = scores && !isNil(scores.jobkred) && scores.jobkred >= TopMatcherScore.JOBKRED;
  const wccTopMatch = scores && !isNil(scores.wcc) && scores.wcc >= TopMatcherScore.WCC;
  return sortCriteria === ApplicationSortCriteria.SCORES_JOBKRED
    ? jobKredTopMatch
    : sortCriteria === ApplicationSortCriteria.SCORES_WCC
    ? wccTopMatch
    : jobKredTopMatch || wccTopMatch;
};
