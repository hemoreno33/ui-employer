import {ApplicationSortCriteria} from '~/flux/applications';
import {GetApplicationsScores} from '~/graphql/__generated__/types';
import {isTopMatch} from '~/services/scores/isTopMatch';

describe('TopMatcherBadge', () => {
  describe('showTopMatch()', () => {
    it('should return true when sortCriteria is job match by WCC and when wcc score >= 0.6', () => {
      const sortCriteria = ApplicationSortCriteria.SCORES_WCC;
      const scores: GetApplicationsScores = {
        jobkred: 0.201,
        wcc: 0.876,
      };
      expect(isTopMatch(scores, sortCriteria)).toBe(true);
    });

    it('should render Among top matchers when sortCriteria is job match by JobKred and when jobkred score >= 0.7', () => {
      const sortCriteria = ApplicationSortCriteria.SCORES_JOBKRED;
      const scores: GetApplicationsScores = {
        jobkred: 0.7,
        wcc: 0.01,
      };
      expect(isTopMatch(scores, sortCriteria)).toBe(true);
    });

    it('should not render Among top matchers when jobKred score is 0 and when sortCriteria is job match by JobKred', () => {
      const sortCriteria = ApplicationSortCriteria.SCORES_JOBKRED;
      const scores: GetApplicationsScores = {
        jobkred: 0,
        wcc: 0.8,
      };
      expect(isTopMatch(scores, sortCriteria)).toBe(false);
    });

    it('should render null when neither jobkred nor wcc scores >= 0.7 or 0.6 respectively and when sortCriteria is Date Applied', () => {
      const sortCriteria = ApplicationSortCriteria.CREATED_ON;
      const scores: GetApplicationsScores = {
        jobkred: 0.699,
        wcc: 0.01,
      };
      expect(isTopMatch(scores, sortCriteria)).toBe(false);
    });
    it('should render Among top matchers when either jobkred or wcc scores >= 0.7 or 0.6 respectively and when sortCriteria is Date Applied', () => {
      const sortCriteria = ApplicationSortCriteria.CREATED_ON;
      const scores: GetApplicationsScores = {
        jobkred: 0.7,
        wcc: 0.65,
      };
      expect(isTopMatch(scores, sortCriteria)).toBe(true);
    });
  });
});
