import {config} from '~/config';

export interface IScanForVirusResult {
  isInfected: boolean;
  viruses: Array<string>;
  sha256Hash: string;
  filesize: number;
  iat: number; // 'issued at' epoch time
}

export const scanForVirus = async (buffer: string): Promise<string> => {
  const response = await fetch(config.url.apiVirusScanner, {
    body: buffer,
    method: 'POST',
    headers: {
      'Content-Type': 'application/octet-stream',
    },
  });
  return response.text();
};
