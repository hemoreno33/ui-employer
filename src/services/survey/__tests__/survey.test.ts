import {
  resetSurveyStorage,
  saveApplicationClick,
  saveTalentClick,
  shouldCount,
  shouldSurvey,
  stopCount,
} from '~/services/survey/survey';

const localStorageGetItemSpy = jest.spyOn(Storage.prototype, 'getItem');
const localStorageSetItemSpy = jest.spyOn(Storage.prototype, 'setItem');

describe('survey Service', () => {
  it('shouldCount with empty storage', () => {
    expect(shouldCount(null)).toBe(true);
    expect(shouldCount('')).toBe(true);
  });

  it('shouldCount with survey object as string', () => {
    expect(shouldCount('{"isCounting": true}')).toBe(true);
    expect(shouldCount('{"isCounting": false}')).toBe(false);
  });

  it('shouldSurvey will not if application or talents are empty', () => {
    expect(shouldSurvey({applications: [], talents: []})).toBe(false);
    expect(shouldSurvey({applications: ['ids'], talents: []})).toBe(false);
  });

  it('shouldSurvey if applications or talents above threshold', () => {
    expect(shouldSurvey({applications: ['ids', 'ids', 'ids', 'ids', 'ids'], talents: []})).toBe(true);
    expect(shouldSurvey({applications: ['ids', 'ids', 'ids', 'ids'], talents: ['ids', 'ids']})).toBe(true);
  });

  it('resetSurveyStorage', () => {
    resetSurveyStorage('test');

    const mock = JSON.stringify({
      applications: [],
      isCounting: true,
      talents: [],
    });

    expect(localStorageSetItemSpy).toBeCalledWith('survey-test', mock);
  });

  it('stopCount', () => {
    stopCount('test');

    const mock = JSON.stringify({
      applications: [],
      isCounting: false,
      talents: [],
    });

    expect(localStorageSetItemSpy).toBeCalledWith('survey-test', mock);
  });

  it('should saveApplicationClick', () => {
    const expectedOneClick = {
      applications: ['id1'],
      isCounting: true,
      talents: [],
    };

    localStorageGetItemSpy.mockReturnValueOnce(null);

    let surveyObj = saveApplicationClick('test', 'id1');
    expect(surveyObj).toEqual(expectedOneClick);

    const mockWithInitalValue = {
      applications: ['id1'],
      isCounting: true,
      talents: [],
    };

    const expectedSecondClick = {
      applications: ['id1'],
      isCounting: true,
      talents: [],
    };

    localStorageGetItemSpy.mockReturnValueOnce(JSON.stringify(mockWithInitalValue));

    surveyObj = saveApplicationClick('test', 'id1');
    expect(surveyObj).toEqual(expectedSecondClick);
  });

  it('should saveTalentClick', () => {
    const expectedOneClick = {
      applications: [],
      isCounting: true,
      talents: ['id1'],
    };

    localStorageGetItemSpy.mockReturnValueOnce(null);

    let surveyObj = saveTalentClick('test', 'id1');
    expect(surveyObj).toEqual(expectedOneClick);

    const mockWithInitalValue = {
      applications: [],
      isCounting: true,
      talents: ['id1'],
    };

    const expectedSecondClick = {
      applications: [],
      isCounting: true,
      talents: ['id1'],
    };

    localStorageGetItemSpy.mockReturnValueOnce(JSON.stringify(mockWithInitalValue));

    surveyObj = saveTalentClick('test', 'id1');
    expect(surveyObj).toEqual(expectedSecondClick);
  });
});
