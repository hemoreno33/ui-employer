import {cloneDeep} from 'lodash/fp';
import {getFromStorage, saveToStorage} from '~/util/localStorage';

interface ISurveyStorage {
  isCounting?: boolean;
  applications: string[];
  talents: string[];
}

const defaultStorage: ISurveyStorage = {
  applications: [],
  isCounting: true,
  talents: [],
};

const SURVEY_PREFIX = 'survey';
const saveSurveyToStorage = (username: string, value: ISurveyStorage) => saveToStorage(SURVEY_PREFIX, username, value);
const getSurveyFromStorage = (username: string) => getFromStorage(SURVEY_PREFIX, username);

export const shouldCount = (surveyStorageString: string | null) => {
  if (surveyStorageString) {
    const {isCounting}: ISurveyStorage = JSON.parse(surveyStorageString);
    return isCounting;
  }

  return true;
};

const addFirstClickTo = (prop: 'applications' | 'talents', username: string, id: string) => {
  const first = cloneDeep(defaultStorage);
  first[prop].push(id);
  saveSurveyToStorage(username, first);
  return first;
};

const addClickTo = (prop: 'applications' | 'talents', surveyStorage: string, username: string, id: string) => {
  const surveyStorageValue: ISurveyStorage = JSON.parse(surveyStorage);

  if (!surveyStorageValue[prop].includes(id)) {
    surveyStorageValue[prop].push(id);
    saveSurveyToStorage(username, surveyStorageValue);
  }

  return surveyStorageValue;
};

export const saveApplicationClick = (username: string, applicationId: string) => {
  const surveyStorage = getSurveyFromStorage(username);

  return surveyStorage
    ? addClickTo('applications', surveyStorage, username, applicationId)
    : addFirstClickTo('applications', username, applicationId);
};

export const saveTalentClick = (username: string, talentId: string) => {
  const surveyStorage = getSurveyFromStorage(username);

  return surveyStorage
    ? addClickTo('talents', surveyStorage, username, talentId)
    : addFirstClickTo('talents', username, talentId);
};

const APPLICATION_CLICKS_TRESHOLD = 5;
const TALENTS_CLICKS_TRESHOLD = 2;

export const shouldSurvey = ({applications, talents}: ISurveyStorage): boolean => {
  return applications.length >= APPLICATION_CLICKS_TRESHOLD || talents.length >= TALENTS_CLICKS_TRESHOLD;
};

export const resetSurveyStorage = (username: string) => {
  saveSurveyToStorage(username, defaultStorage);
};

export const stopCount = (username: string) => {
  const stopCountStorage: ISurveyStorage = {
    applications: [],
    isCounting: false,
    talents: [],
  };
  saveSurveyToStorage(username, stopCountStorage);
};
