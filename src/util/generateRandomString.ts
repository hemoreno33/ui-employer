// NOTE: Via https://stackoverflow.com/a/27747377/995735
export const generateRandomString = (strLength = 10) => {
  const browserCrypto = window.crypto || window.msCrypto; // for IE 11

  const uint8 = new Uint8Array(strLength / 2);
  browserCrypto.getRandomValues(uint8);
  return Array.from(uint8, (val) => val.toString(16).padStart(2, '0')).join('');
};
