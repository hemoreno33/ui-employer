import {ICandidate} from '~/graphql/candidates';

/*
  Check if the candidate has any empty values for required education and work entries
    Education:
    - at least one entry
    - has non-empty values for year attained, institution, and EQA description
    Work experience:
    - can have 0 work experience
    - has non-empty values for job title, institution, and EQA description
 */
export const isCorrupted = (candidate: ICandidate) => {
  if (candidate.education && candidate.education.length > 0) {
    for (const education of candidate.education) {
      if (
        !(
          education &&
          education.yearAttained &&
          education.institution &&
          education.ssecEqaCode &&
          education.ssecEqaDescription
        )
      ) {
        return true;
      }
    }

    if (candidate.workExperiences && candidate.workExperiences.length > 0) {
      for (const workExperience of candidate.workExperiences) {
        if (!(workExperience && workExperience.jobTitle && workExperience.companyName && workExperience.startDate)) {
          return true;
        }
      }
    }
  } else {
    return true;
  }
  return false;
};
