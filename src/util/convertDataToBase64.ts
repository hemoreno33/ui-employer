export const convertDataToBase64 = (data: Blob | File): Promise<string> =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = (_) => reject(new Error('Encountered error while converting to base64'));
    reader.onloadend = () => {
      resolve(reader.result as string);
    };
    reader.readAsDataURL(data);
  });
