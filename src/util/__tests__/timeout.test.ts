import {setPromiseTimeout} from '../timeout';

describe('setPromiseTimeout', () => {
  it('should resolve if promise duration is less than timeout', async () => {
    const promise = () =>
      new Promise<string>((resolve) => {
        setTimeout(() => resolve('resolved'), 100);
      });
    const result = await setPromiseTimeout(promise(), 500);
    expect(result).toEqual('resolved');
  });

  it('should throw error if promise duration is more than timeout', async () => {
    const promise = () =>
      new Promise<string>((resolve) => {
        setTimeout(() => resolve('resolved'), 100);
      });
    await expect(setPromiseTimeout(promise(), 10)).rejects.toMatchObject({message: 'Timeout'});
  });

  it('should throw error if promise throws an error', async () => {
    const promise = () =>
      new Promise<string>((_, reject) => {
        setTimeout(() => reject('rejected'), 100);
      });
    await expect(setPromiseTimeout(promise(), 500)).rejects.toEqual('rejected');
  });
});
