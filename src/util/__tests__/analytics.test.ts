import {googleAnalytics, EVENT_ACTION, EVENT_CATEGORY} from '../analytics';

declare const global: any;

describe('Util/analytics', () => {
  const ga = jest.fn();

  beforeEach(() => {
    global.ga = ga;
  });

  afterEach(() => {
    ga.mockReset();
  });

  it('should set ga with the right parameters', () => {
    const fieldsObject = {
      page: 'bar',
      title: 'foo',
    };
    googleAnalytics.set(fieldsObject);
    expect(ga).toHaveBeenCalledWith('set', {
      page: 'bar',
      title: 'foo',
    });
  });

  it('should call ga with correct pageView parameters', () => {
    const pageViewFields = {
      page: 'bar',
      title: 'foo',
    };
    googleAnalytics.sendPageView(pageViewFields);
    expect(ga).toHaveBeenCalledTimes(1);
    expect(ga).toHaveBeenCalledWith('send', {
      hitType: 'pageview',
      page: 'bar',
      title: 'foo',
    });
  });

  it('should call ga with correct event parameters', () => {
    const eventFields = {
      eventAction: EVENT_ACTION.ITEM_POSITION,
      eventCategory: EVENT_CATEGORY.JOB_SUGGESTED_TALENT,
      eventLabel: 'suggested talents',
      nonInteraction: true,
    };
    googleAnalytics.sendEvent(eventFields);
    expect(ga).toHaveBeenCalledTimes(1);
    expect(ga).toHaveBeenCalledWith('send', {
      ...eventFields,
      hitType: 'event',
    });
  });
});
