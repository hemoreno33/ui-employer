import {debounceAsync} from '../debounceAsync';

describe('debounceAsync', () => {
  it('should not call debounced function if the previous call has not yet finished', async () => {
    const spyFn = jest.fn();
    const asyncFn = () =>
      new Promise<void>((resolve) => {
        spyFn();
        setTimeout(resolve, 100);
      });
    const debouncedAsyncFn = debounceAsync(asyncFn);

    await Promise.all([debouncedAsyncFn(), debouncedAsyncFn(), debouncedAsyncFn()]);
    expect(spyFn).toHaveBeenCalledTimes(1);
  });

  it('should be able to call the debounced function if the previous call has finished', async () => {
    const spyFn = jest.fn();
    const asyncFn = () =>
      new Promise<void>((resolve) => {
        spyFn();
        setTimeout(resolve, 100);
      });
    const debouncedAsyncFn = debounceAsync(asyncFn);

    await debouncedAsyncFn();
    expect(spyFn).toHaveBeenCalledTimes(1);
    await debouncedAsyncFn();
    expect(spyFn).toHaveBeenCalledTimes(2);
  });
});
