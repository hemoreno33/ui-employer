import {isMCFJob} from '~/util/isMCFJob';

describe('Util/isMCFJob', () => {
  it('should return true when jobPost is from MCF job', () => {
    const mcfJobPostId = 'MCF-2019-1234000';
    expect(isMCFJob(mcfJobPostId)).toBe(true);
  });
  it('should return false when jobPost is from MSF or careers@Gov', () => {
    const msfJobPostId = 'JOB-2019-1234000';
    expect(isMCFJob(msfJobPostId)).toBe(false);
  });
  it('should return false when jobPost has prefix that does not start with MCF', () => {
    const msfJobPostId = 'FOO-2019-1234000';
    expect(isMCFJob(msfJobPostId)).toBe(false);
  });
});
