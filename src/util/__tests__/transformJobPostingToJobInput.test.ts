import {differenceInDays, parseISO} from 'date-fns';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {Location, LocationType} from '~/components/JobPosting/WorkplaceDetails/workplaceDetails.types';
import {IJobPost} from '~/services/employer/jobs.types';
import {transformJobPostingToJobInput} from '../transformJobPostingToJobInput';

describe('transformJobPostingToJobInput', () => {
  it('should transform jobpost to jobinput', () => {
    const mockedJobPost: IJobPost = {
      ...jobPostMock,
      hiringCompany: {
        ...jobPostMock.hiringCompany,
        name: 'Republic Polytechnic',
        registrationTypeId: 1,
        ssicCode: '79101',
        uen: 'T08GB0046G',
      },
      minimumYearsExperience: 1,
      ssecEqa: '42',
      ssecFos: '0521',
    };

    const skillsWithSelectedTrue = mockedJobPost.skills.map((skill) => {
      return {
        ...skill,
        selected: true,
      };
    });

    const checkLocationType = !(
      jobPostMock.address.postalCode &&
      jobPostMock.address.block &&
      jobPostMock.address.street
    )
      ? LocationType.Multiple
      : LocationType.None;

    const jobPostEditMock: IJobPostingFormState = {
      jobDescription: {
        description: mockedJobPost.description,
        industry: '79101',
        occupation: mockedJobPost.ssocCode,
        registrationType: 'Registry of Company',
        thirdPartyEmployer: Boolean(mockedJobPost.hiringCompany && mockedJobPost.hiringCompany.uen),
        thirdPartyEmployerEntityId: mockedJobPost.hiringCompany && mockedJobPost.hiringCompany.uen,
        title: mockedJobPost.title,
      },
      jobPostingSkills: {
        addedSkills: [...skillsWithSelectedTrue],
        recommendedSkills: [],
      },
      keyInformation: {
        employmentType: mockedJobPost.employmentTypes ? mockedJobPost.employmentTypes.map((type) => type.id) : [],
        fieldOfStudy: mockedJobPost.ssecFos,
        jobCategories: mockedJobPost.categories ? mockedJobPost.categories.map((category) => category.id) : [],
        jobPostDuration: differenceInDays(
          parseISO(mockedJobPost.metadata.expiryDate),
          parseISO(mockedJobPost.metadata.newPostingDate),
        ),
        maximumSalary: mockedJobPost.salary.maximum,
        minimumSalary: mockedJobPost.salary.minimum,
        minimumYearsExperience: 1,
        numberOfVacancies: 1,
        positionLevel: mockedJobPost.positionLevels[0].id,
        qualification: mockedJobPost.ssecEqa,
        schemes: [
          {
            ...mockedJobPost.schemes[0].scheme,
            selected: true,
          },
        ],
      },
      workplaceDetails: {
        ...mockedJobPost.address,
        location: mockedJobPost.address.isOverseas ? Location.Overseas : Location.Local,
        locationType: checkLocationType,
      },
    };

    expect(transformJobPostingToJobInput(mockedJobPost)).toEqual(jobPostEditMock);
  });
});
