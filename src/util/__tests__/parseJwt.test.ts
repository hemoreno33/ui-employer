import {parseJwt} from '~/util/parseJwt';

describe('util/parseJwt', () => {
  it('should return a decoded payload', () => {
    const mockToken =
      'eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc0luZmVjdGVkIjpmYWxzZSwidmlydXNlcyI6W10sInNoYTI1Nkhhc2giOiJiMjdkZDg4M2NhMWM0ODYyNjI4MWVmOGZjNGJiY2U2ZGJlNzE5MjEwNzU2ZjA1YjVkODFiOGU3OTBkOTFhZTk2IiwiZmlsZXNpemUiOjE1NTYsImlhdCI6MTU4OTc5NTc4MX0.o8mMmvjKSJe0jiu3S6RBsoDAjk8DSHd4uYkQWsSot1pgBLZJVad0DGpl_2FCmhzzTqBXYD_0FdgpfaaigZ0i2B2KX7SLykcdvSolYzY2Rt50oRB5hwBklMX8veoxCXRNcyTTNuhc6ZNmGOZaBeHxr09hfHAPLBmS-MNqgLs5fV5H5iu8EXgqNF03SwsIYl5UwwnJc-0mwxdFTNUviu27dzT8xUSjrPnj-8wfEfpm9mmyz31fq7w3Mkpm8-3S-kVnbsqqZCuwQZA6kZ0Zq9HwwWswkK5aI2h9KNaSugR-xuyetar5kBSabnNrwxzz7_S1zrFssKURpP-wfWg1-9YHVg';
    const decodedMockResult = {
      isInfected: false,
      viruses: [],
      sha256Hash: 'b27dd883ca1c48626281ef8fc4bbce6dbe719210756f05b5d81b8e790d91ae96',
      filesize: 1556,
      iat: 1589795781,
    };
    expect(parseJwt(mockToken)).toEqual(decodedMockResult);
  });
});
