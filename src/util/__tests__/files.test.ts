import {formatFileSize} from '../files';

describe('files util', () => {
  describe('formatFileSize', () => {
    it.each`
      input             | output
      ${3}              | ${'3bytes'}
      ${123}            | ${'123bytes'}
      ${1028}           | ${'1.0KB'}
      ${12485761}       | ${'11.9MB'}
      ${10737418248}    | ${'10.0GB'}
      ${10777418248234} | ${'10037.3GB'}
    `('should return $output when given $input', ({input, output}) => {
      expect(formatFileSize(input)).toEqual(output);
    });
  });
});
