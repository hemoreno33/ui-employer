import {applicationMock} from '~/__mocks__/applications/applications.mocks.ts';
import {isCorrupted} from '../candidate';

describe('candidate util', () => {
  describe('isCorrupted()', () => {
    const baseCandidate = applicationMock.applicant;

    it('should return false for candidate with valid education and valid work experience', () => {
      expect(isCorrupted(baseCandidate)).toEqual(false);
    });

    describe('education validation', () => {
      it('should return true for candidate without education', () => {
        const candidate = {...baseCandidate, education: []};
        expect(isCorrupted(candidate)).toEqual(true);
      });

      it('should return true for candidate with invalid education', () => {
        const candidate = {
          ...baseCandidate,
          education: [
            {
              institution: '',
              isHighest: false,
              name: 'Optional name or description',
              ssecEqaCode: '60',
              ssecEqaDescription: "Bachelor's Degree or equivalent",
              ssecFosDescription: '',
              yearAttained: 0,
            },
          ],
        };

        expect(isCorrupted(candidate)).toEqual(true);
      });

      it('should return false for candidate with ssecEqa but no ssecFos', () => {
        const candidate = {
          ...baseCandidate,
          education: [
            {
              institution: 'Institution',
              isHighest: false,
              name: 'Optional name or description',
              ssecEqaCode: '60',
              ssecEqaDescription: "Bachelor's Degree or equivalent",
              yearAttained: 2000,
            },
          ],
        };

        expect(isCorrupted(candidate)).toEqual(false);
      });
    });

    describe('workExperience validation', () => {
      it('should return false for candidate without work experience', () => {
        const candidate = {...baseCandidate, workExperiences: []};
        expect(isCorrupted(candidate)).toEqual(false);
      });

      it('should return true for candidate with invalid work experience', () => {
        const candidate = {
          ...baseCandidate,
          workExperiences: [
            {
              companyName: '',
              endDate: '',
              jobDescription: 'some desc',
              jobTitle: 'Test Subject',
              startDate: '',
            },
          ],
        };

        expect(isCorrupted(candidate)).toEqual(true);
      });
    });
  });
});
