import {config} from '~/config';

export const getDocumentUrl = (filePath: string): string => config.url.apiProfile + filePath;
