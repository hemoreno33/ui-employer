import {mcf} from '@mcf/constants';
import {differenceInDays, parseISO} from 'date-fns';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {IJobSkill} from '~/components/JobPosting/Skills/skills.types';
import {Location, LocationType} from '~/components/JobPosting/WorkplaceDetails/workplaceDetails.types';
import {IJobPost, IJobPostAddress, IJobPostScheme} from '~/services/employer/jobs.types';

export const transformJobPostingToJobInput = (jobPosting: IJobPost): IJobPostingFormState => {
  const {
    address,
    title,
    description,
    ssocCode,
    hiringCompany,
    skills,
    metadata,
    numberOfVacancies,
    categories,
    positionLevels,
    minimumYearsExperience,
    employmentTypes,
    ssecEqa,
    ssecFos,
    salary,
    schemes,
    otherRequirements,
  } = jobPosting;

  const selectedSkills = skills.map((skill: IJobSkill) => ({
    ...skill,
    selected: true,
  }));

  const selectedSchemes = schemes.map((schemeObj: IJobPostScheme) => {
    const {scheme} = schemeObj;
    return {
      ...schemeObj,
      scheme: {
        ...scheme,
        selected: true,
      },
    };
  });

  const checkLocationType = (jobPostAddress: IJobPostAddress) => {
    const {postalCode, block, street} = jobPostAddress;
    if (!postalCode && !block && !street) {
      return LocationType.Multiple;
    } else {
      return LocationType.None;
    }
  };

  const getRegistrationType = (id: number) => {
    const registrationType = mcf.COMPANY_REGISTRATION_TYPES.find((data) => id === data.id);
    return registrationType ? registrationType.registrationType : id.toString();
  };

  return {
    jobDescription: {
      description,
      industry: hiringCompany && hiringCompany.ssicCode,
      occupation: ssocCode,
      otherRequirements,
      registrationType:
        hiringCompany && hiringCompany.registrationTypeId
          ? getRegistrationType(hiringCompany.registrationTypeId)
          : undefined,
      thirdPartyEmployer: Boolean(hiringCompany && hiringCompany.uen),
      thirdPartyEmployerEntityId: hiringCompany && hiringCompany.uen,
      title,
    },
    jobPostingSkills: {
      addedSkills: selectedSkills,
      recommendedSkills: [],
    },
    keyInformation: {
      employmentType: employmentTypes.map((type) => type.id),
      fieldOfStudy: ssecFos,
      jobCategories: categories.map((category) => category.id),
      jobPostDuration: differenceInDays(parseISO(metadata.expiryDate), parseISO(metadata.newPostingDate)),
      maximumSalary: salary.maximum,
      minimumSalary: salary.minimum,
      minimumYearsExperience,
      numberOfVacancies,
      positionLevel: positionLevels.map((position) => position.id)[0],
      qualification: ssecEqa,
      schemes: selectedSchemes.map((scheme) => scheme.scheme),
    },
    workplaceDetails: {
      ...address,
      location: address.isOverseas ? Location.Overseas : Location.Local,
      locationType: checkLocationType(address),
    },
  };
};
