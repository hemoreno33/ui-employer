import {isNil} from 'lodash/fp';
export const isNotNil = <TValue>(value: TValue | null | undefined): value is TValue => !isNil(value);
