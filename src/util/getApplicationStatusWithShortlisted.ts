import {mcf} from '@mcf/constants';
import {updateApplicationStatusOptions} from '~/components/CandidateInfo/ApplicationStatus';
import {Maybe} from '~/graphql/__generated__/types';

export const getApplicationStatusWithShortlisted = (
  statusId?: mcf.JOB_APPLICATION_STATUS,
  isShortlisted: Maybe<boolean> = false,
) => {
  const applicationStatus = updateApplicationStatusOptions.find((statusOption) => statusOption.value === statusId);
  if (applicationStatus && (applicationStatus?.value !== mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW || isShortlisted)) {
    return applicationStatus;
  }
};
