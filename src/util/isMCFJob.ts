export const isMCFJob = (jobId: string) => /^MCF/.test(jobId);
