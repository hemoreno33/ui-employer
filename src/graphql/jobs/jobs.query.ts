import gql from 'graphql-tag';

export const GET_SSOC_LIST = gql`
  query getCommon {
    common {
      ssocList {
        ssoc
        ssocTitle
      }
    }
  }
`;

export const GET_EDUCATION_LIST = gql`
  query getCommonEducation {
    common {
      ssecEqaList {
        actualCode
        description
      }
      ssecFosList {
        actualCode
        description
      }
    }
  }
`;
export const GET_COUNTRY_LIST = gql`
  query getCommonCountry {
    common {
      countryList {
        code
        description
      }
    }
  }
`;
export const GET_SSIC_LIST = gql`
  query getCommonSsic($code: String) {
    common {
      ssicList(code: $code) {
        code
        description
      }
    }
  }
`;
