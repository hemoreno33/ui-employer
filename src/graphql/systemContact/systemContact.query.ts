import gql from 'graphql-tag';

export const GET_SYSTEM_CONTACT = gql`
  query getSystemContact {
    systemContact {
      contactNumber
      email
      designation
      individualId
      termsAndConditionsAcceptedAt
    }
  }
`;

export const SET_SYSTEM_CONTACT = gql`
  mutation UpdateSystemContact($systemContact: SystemContactInput!) {
    updateSystemContact(systemContact: $systemContact) {
      contactNumber
      designation
      email
    }
  }
`;

export const SET_TERMS_AND_CONDITIONS = gql`
  mutation AcceptTermsAndConditions {
    acceptTermsAndConditions
  }
`;
