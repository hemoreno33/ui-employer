import {InMemoryCache, IntrospectionFragmentMatcher} from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import {ApolloLink, from} from 'apollo-link';
import {HttpLink} from 'apollo-link-http';
import {Store} from 'redux';
import {action} from 'typesafe-actions';
import {config} from '~/config/index';
import introspectionResult from '~/graphql/__generated__/types';

export const setupClient = (...middleware: ApolloLink[]) => {
  const fragmentMatcher = new IntrospectionFragmentMatcher({introspectionQueryResultData: introspectionResult});
  const cache = new InMemoryCache({fragmentMatcher});

  return new ApolloClient({
    cache,
    link: from([...middleware, new HttpLink({uri: config.url.apiProfile, credentials: 'include'})]),
    connectToDevTools: process.env.NODE_ENV === 'development',
  });
};

export const activityMiddleware = (store: Store) =>
  new ApolloLink((operation, forward) => {
    // dispatch an action so that redux-account-middleware know that there is still activity on-going and refresh auth token when necessary
    store.dispatch(action('GRAPHQL_ACTIVITY'));
    return forward ? forward(operation) : null;
  });
