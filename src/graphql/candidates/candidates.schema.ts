import {
  GetApplicationsApplicant,
  GetApplicationsEducation,
  GetApplicationsResume,
  GetApplicationsScores,
  GetApplicationsSkills,
  GetApplicationsWorkExperiences,
  GetSuggestedTalentsEducation,
  GetSuggestedTalentsResume,
  GetSuggestedTalentsScores,
  GetSuggestedTalentsSkills,
  GetSuggestedTalentsTalent,
  GetSuggestedTalentsWorkExperiences,
  Maybe,
} from '~/graphql/__generated__/types';
import {isNotNil} from '~/util/isNotNil';

export type ICandidate = GetSuggestedTalentsTalent | GetApplicationsApplicant;
export type IEducation = GetSuggestedTalentsEducation | GetApplicationsEducation;
export type ISkill = GetSuggestedTalentsSkills | GetApplicationsSkills;
export type IWorkExperience = GetSuggestedTalentsWorkExperiences | GetApplicationsWorkExperiences;
export type IScores = GetApplicationsScores | GetSuggestedTalentsScores;
export type IResume = GetApplicationsResume | GetSuggestedTalentsResume;

export const isApplicationScores = (scores: IScores): scores is GetApplicationsScores => {
  return isNotNil((scores as GetApplicationsScores).jobkred);
};
export const isApplicationResume = (resume: Maybe<IResume>): resume is GetApplicationsResume => {
  return isNotNil(resume) && isNotNil((resume as GetApplicationsResume).lastDownloadedAt);
};
