import {createBrowserHistory} from 'history';
import {config} from '~/config';

// this has been moved out of router.ts file because it was causing HMR to refresh the whole page
export const history = createBrowserHistory({basename: config.publicPath});
