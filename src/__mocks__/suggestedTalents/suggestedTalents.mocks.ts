/* tslint:disable:object-literal-sort-keys */
import {GetSuggestedTalentsTalents} from '~/graphql/__generated__/types';

export const suggestedTalentsMock: GetSuggestedTalentsTalents[] = [
  {
    talent: {
      email: 'blackJack@example.com',
      name: 'Black Jack',
      lastLogin: '2018-09-18T16:04:50',
      id: 'A-0',
      mobileNumber: '+65 9991 2345',
      education: [
        {
          isHighest: false,
          institution: 'Yale',
          yearAttained: 2010,
          ssecEqaCode: '81',
          ssecEqaDescription: "Master's Degree or equivalent",
          ssecFosDescription: 'Accommodation Services',
          name: 'Optional name or description',
          isVerified: null,
        },
        {
          isHighest: false,
          institution: 'Harvard',
          yearAttained: 2014,
          ssecEqaCode: '60',
          ssecEqaDescription: "Bachelor's Degree or equivalent",
          ssecFosDescription: 'Accommodation Services',
          name: 'Second Bachelors',
          isVerified: null,
        },
        {
          isHighest: true,
          institution: 'Harvard',
          yearAttained: 2012,
          ssecEqaCode: '82',
          ssecEqaDescription: 'Doctorate or equivalent',
          ssecFosDescription: 'Actuarial Science',
          name: 'Optional name or description',
          isVerified: null,
        },
        {
          isHighest: false,
          institution: 'Stanford',
          yearAttained: 2008,
          ssecEqaCode: '60',
          ssecEqaDescription: "Bachelor's Degree or equivalent",
          ssecFosDescription:
            'Biological Sciences & Technologies n.e.c. (including molecular biology, cell & molecular biology, computational biology)',
          name: 'Optional name or description',
          isVerified: null,
        },
      ],
      skills: [
        {
          id: 1,
          skill: 'guitar',
        },
        {
          id: 2,
          skill: 'keyboard',
        },
        {
          id: 3,
          skill: 'bass',
        },
        {
          id: 4,
          skill: 'drums',
        },
        {
          id: 5,
          skill: 'ocarina',
        },
        {
          id: 6,
          skill: 'erhu',
        },
        {
          id: 7,
          skill: 'pipa',
        },
        {
          id: 8,
          skill: 'keytar',
        },
        {
          id: 9,
          skill: 'shamisen',
        },
        {
          id: 10,
          skill: 'gong',
        },
        {
          id: 11,
          skill: 'marimba',
        },
        {
          id: 12,
          skill: 'timpani',
        },
        {
          id: 13,
          skill: 'harp',
        },
        {
          id: 14,
          skill: 'cello',
        },
        {
          id: 15,
          skill: 'potato',
        },
      ],
      workExperiences: [
        {
          jobTitle: 'Music Teacher',
          companyName: 'School of Rock',
          startDate: '2017-01-01T00:00:00.000',
          endDate: '2017-02-01T00:00:00.000',
          jobDescription:
            'Lorem ipsum dolor sit amet, quot viris qui no, hendrerit sadipscing necessitatibus cum in, id probo denique vix. Prima reformidans nam eu. Mei choro mucius ullamcorper cu. An eam quem quodsi, perfecto imperdiet nec cu, vel an oportere temporibus. Cu pri noster tamquam inermis, id facete facilis eos. Vim ei quot aeque, officiis quaerendum interpretaris ea mel, an pro case diceret. Deleniti partiendo mei ne.',
        },
        {
          jobTitle: 'Job One',
          companyName: 'Uno Company',
          startDate: '2016-01-01T00:00:00.000',
          endDate: '2017-01-01T00:00:00.000',
          jobDescription:
            'Lorem ipsum dolor sit amet, quot viris qui no, hendrerit sadipscing necessitatibus cum in, id probo denique vix. Prima reformidans nam eu. Mei choro mucius ullamcorper cu. An eam quem quodsi, perfecto imperdiet nec cu, vel an oportere temporibus. Cu pri noster tamquam inermis, id facete facilis eos. Vim ei quot aeque, officiis quaerendum interpretaris ea mel, an pro case diceret. Deleniti partiendo mei ne.',
        },
        {
          jobTitle: 'Job Two',
          companyName: 'Dos Company',
          startDate: '2015-01-01T00:00:00.000',
          endDate: '2016-01-01T00:00:00.000',
          jobDescription:
            'Lorem ipsum dolor sit amet, quot viris qui no, hendrerit sadipscing necessitatibus cum in, id probo denique vix. Prima reformidans nam eu. Mei choro mucius ullamcorper cu. An eam quem quodsi, perfecto imperdiet nec cu, vel an oportere temporibus. Cu pri noster tamquam inermis, id facete facilis eos. Vim ei quot aeque, officiis quaerendum interpretaris ea mel, an pro case diceret. Deleniti partiendo mei ne.',
        },
        {
          jobTitle: 'Job Three',
          companyName: 'Tres Company',
          startDate: '2014-01-01T00:00:00.000',
          endDate: '2015-01-01T00:00:00.000',
          jobDescription:
            'Lorem ipsum dolor sit amet, quot viris qui no, hendrerit sadipscing necessitatibus cum in, id probo denique vix. Prima reformidans nam eu. Mei choro mucius ullamcorper cu. An eam quem quodsi, perfecto imperdiet nec cu, vel an oportere temporibus. Cu pri noster tamquam inermis, id facete facilis eos. Vim ei quot aeque, officiis quaerendum interpretaris ea mel, an pro case diceret. Deleniti partiendo mei ne.',
        },
      ],
      resume: {
        fileName: 'cool_resume.pdf',
        filePath: 'somepath',
      },
    },
    scores: {
      wcc: 0.504,
    },
    bookmarkedOn: '2018-09-18T16:04:50',
  },
  {
    talent: {
      email: 'catHerder@example.com',
      id: 'A-1',
      lastLogin: '2018-09-18T16:04:50',
      name: 'Kow Ah Tan',
      mobileNumber: '+65 9991 2345',
      education: [
        {
          institution: 'School Name',
          yearAttained: 2014,
          ssecEqaCode: '60',
          ssecEqaDescription: "Bachelor's Degree or equivalent",
          ssecFosDescription: 'Accommodation Services',
          isHighest: true,
          name: 'Optional name or description',
          isVerified: null,
        },
      ],
      skills: [{id: 1, skill: 'some skill'}],
      workExperiences: [
        {
          companyName: 'TESLA LTD',
          jobDescription: 'description',
          endDate: '2018-01-01T00:00:00.000',
          jobTitle: 'Some Job',
          startDate: '2017-01-01T00:00:00.000',
        },
      ],
      resume: {
        fileName: 'cool_resume.pdf',
        filePath: 'somepath',
      },
    },
    scores: {
      wcc: 0.496,
    },
    bookmarkedOn: null,
  },
  {
    talent: {
      email: 'monicaChen@example.com',
      id: 'A-2',
      lastLogin: '2018-09-24T16:04:50',
      name: 'Monica Chen',
      mobileNumber: '+65 9991 2345',
      education: [],
      skills: [{id: 234, skill: 'farming'}],
      workExperiences: [
        {
          companyName: 'TESLA LTD',
          endDate: '2010-12-01T00:00:00.000',
          jobTitle: 'Engineer',
          startDate: '2010-01-01T00:00:00.000',
          jobDescription: 'Work Description',
        },
      ],
      resume: {
        fileName: 'weak_resume.pdf',
        filePath: 'anotherpath',
      },
    },
    scores: {
      wcc: 0.464,
    },
    bookmarkedOn: null,
  },
  {
    talent: {
      email: 'VENKATA.KRISHNA.RAGHAVENDRA.MANIKANTA.VARA.PRADESH.ALIAS.IERIEL.MIRZA@example.com',
      id: 'A-3',
      name: 'VENKATA KRISHNA RAGHAVENDRA MANIKANTA VARA PRADESH @ IERIEL MIRZA',
      lastLogin: '2018-09-18T16:04:50',
      mobileNumber: '+65 9991 2345',
      skills: [],
      education: [
        {
          institution: 'School Name',
          ssecFosDescription: 'Accommodation Services',
          isHighest: true,
          name: 'Doctorate',
          ssecEqaCode: '60',
          ssecEqaDescription: 'Bachelors Degree or equivalent',
          yearAttained: 2010,
          isVerified: null,
        },
      ],
      workExperiences: [
        {
          companyName: 'TESLA LTD',
          endDate: '2010-12-01T00:00:00.000',
          jobTitle: 'Engineer',
          startDate: '2010-01-01T00:00:00.000',
          jobDescription: 'Work Description',
        },
      ],
      resume: {
        fileName: 'questionable_resume.pdf',
        filePath: 'apath',
      },
    },
    scores: {
      wcc: 0.451,
    },
    bookmarkedOn: null,
  },
];

export interface ISuggestedTalentsByJobPostMock {
  [key: string]: GetSuggestedTalentsTalents[];
}

export const suggestedTalentsByJobPostsMock: ISuggestedTalentsByJobPostMock = {
  afab75dafd1c63f8dc3a8d909be8ef4f: suggestedTalentsMock,
};
