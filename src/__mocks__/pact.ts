import {Matchers, Pact} from '@pact-foundation/pact';
import {PactWeb} from '@pact-foundation/pact/pact-web';
import {ApolloClient, HttpLink, InMemoryCache} from 'apollo-boost';
import {isPlainObject, reduce} from 'lodash';
import path from 'path';

export class PactBuilder {
  public port = 0;
  public host: string;
  public provider: PactWeb | Pact;

  constructor(providerName: 'api-profile' | 'api-job') {
    if (process.env.PACT_CI) {
      this.host = `pact-mock-service-${providerName}`;
      this.port = 4444;
      this.provider = new PactWeb({
        cors: true,
        host: this.host,
        port: this.port,
      });
    } else {
      const logFilename = module.parent ? path.basename(module.parent.filename, '.tsx') : 'pact';
      this.host = 'localhost';
      this.provider = new Pact({
        consumer: 'ui-employer',
        cors: true,
        dir: './pacts',
        host: 'localhost',
        log: `./pacts/logs/${providerName}-${logFilename}.log`,
        logLevel: 'error',
        pactfileWriteMode: 'update',
        provider: providerName,
        spec: 2,
      });
    }
  }

  public async setup() {
    if (this.provider instanceof Pact) {
      const opts = await this.provider.setup();
      this.port = opts.port;
    }
  }

  public getApolloClient() {
    const cache = new InMemoryCache({
      addTypename: false,
    });

    return new ApolloClient({
      cache,
      link: new HttpLink({uri: `http://${this.host}:${this.port}/profile`}),
    });
  }

  /**
   * This method is to retry the pact provider verify at least x times before failing verification check
   * It is introduced because on graphql query component mount, the request is not fired and registered by the provider immediately.
   * So a retry on the verification to cater for the delay, to prevent test flakiness
   * @param {number} retry to specify the number of times to retry for interactions verification
   */
  public async verifyInteractions(retry = 3): Promise<void> {
    try {
      await this.provider.mockService.verify();
      await this.provider.mockService.removeInteractions();
    } catch (err) {
      if (retry > 0) {
        return this.verifyInteractions(retry - 1);
      }
      throw err;
    }
  }
}

export const transformArrayToEachLikeMatcher = <T extends {}>(
  mockData: T,
  eachLikeMatcher: any = Matchers.eachLike,
): {} => {
  return isPlainObject(mockData)
    ? reduce(
        mockData,
        (prev, curr, key) => {
          if (isPlainObject(curr)) {
            return {...prev, [key]: transformArrayToEachLikeMatcher(curr, eachLikeMatcher)};
          } else if (Array.isArray(curr) && curr.length > 0) {
            return {...prev, [key]: eachLikeMatcher(transformArrayToEachLikeMatcher(curr[0], eachLikeMatcher))};
          }
          return {...prev, [key]: curr};
        },
        {},
      )
    : mockData;
};
