const path = require('path');

module.exports = {
  process(_: any, filename: string): string {
    return 'module.exports = ' + JSON.stringify(path.basename(filename)) + ';';
  },
};
