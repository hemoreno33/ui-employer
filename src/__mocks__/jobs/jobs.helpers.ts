import {IJobPost} from '~/services/employer/jobs.types';
import {jobPostMock} from './jobs.mocks';

export const createJobPost = (options: Partial<IJobPost>): IJobPost => ({
  ...jobPostMock,
  ...options,
});
