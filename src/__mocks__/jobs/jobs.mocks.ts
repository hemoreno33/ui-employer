import {addDays, format} from 'date-fns';
import {IJobPost, IJobPostInput} from '~/services/employer/jobs.types';

/* tslint:disable:object-literal-sort-keys */
export const jobPostMock: IJobPost = {
  uuid: 'afab75dafd1c63f8dc3a8d909be8ef4f',
  sourceCode: 'Careers@Gov',
  title: 'School of Technology for the Arts &amp; Manager (Technical)',
  description:
    '<p><strong><span style="text-decoration: underline;">Job Responsibilities</span></strong></p> <p>Manage a team of Technical Executives in:</p> <ul style="list-style-type: disc;"> <li>Day to day operations of studios, workshops &amp; loan centre to support lecturers &amp; students in teaching and learning</li> <li>Set up and operation of audio-visual lighting systems and/or rigging for school-wide events</li> <li>Administration of good safety practices in compliance to QEHS (Quality Environment Health Safety), WSH (Workplace Safety and Health) policies and procedures, including working at heights</li> </ul> <p>Inventory and Asset Management:</p> <ul style="list-style-type: disc;"> <li>Oversee overall maintenance, repair, scheduled servicing or replacement of audio, video &amp; lighting equipment, studio systems and computer systems for the school</li> <li>Maintain proper asset inventory records and documentation for internal/external audit</li> <li>Coordinate with vendors and suppliers on : <ul style="list-style-type: circle;"> <li>Installation&nbsp; and integration of new equipment &amp; software</li> <li>Maintenance and repairs of existing equipment</li> <li>Receipt of purchased technical equipment and consumables</li> </ul> </li> </ul> <p><strong><span style="text-decoration: underline;">Requirements </span></strong></p> <ul style="list-style-type: disc;"> <li>Minimum eight (8) years relevant industry experience</li> <li>Proven track record in leadership with at least five (5) years&rsquo; experience in heading a team and/ or managing audio video facilities such as recording studios or equivalent set-ups</li> <li>Operational knowledge in studio/video/audio production and post-production</li> <li>Proficient in testing equipment for use in integration, installation, maintenance and repair of broadcast studio systems</li> <li>BizSafe certification, Work-at-Heights, Occupational First Aid will be an advantage</li> </ul>',
  ssocCode: 2209,
  numberOfVacancies: 1,
  categories: [
    {
      id: 10,
      category: 'Education and Training',
    },
    {
      id: 30,
      category: 'Public / Civil Service',
    },
  ],
  employmentTypes: [
    {
      id: 8,
      employmentType: 'Full Time',
    },
  ],
  positionLevels: [
    {
      id: 1,
      position: 'Senior Management',
    },
  ],
  status: {
    id: 102,
    jobStatus: 'Open',
  },
  postedCompany: {
    uen: 'T08GB0046G',
    description:
      ' <p style="text-align: justify;">   <strong>About Republic Polytechnic</strong></p>  <p style="margin: 0cm 0cm 0pt; text-align: justify;">   <span style="font-size: 11pt;"><font color="#000000" face="Times New Roman">The first educational institution in Singapore to leverage the Problem-based Learning approach for all its diploma programmes, Republic Polytechnic (RP) has seven schools and one academic centre offering forty-two diplomas in Applied Science, Engineering, Management and Communication, Events and Hospitality, Infocomm, Sports, Health &amp; Leisure, and Technology for the Arts.</font></span></p>  <p style="text-align: justify;">   <span style="font-size: 11pt;"><font color="#000000" face="Times New Roman">In addition to offering full-time diploma courses, our Academy for Continuing Education @RP (ACE@RP) is committed to promoting lifelong learning among working adults.</font></span></p>  <p style="text-align: justify;">   <span style="font-size: 11pt;"><font color="#000000" face="Times New Roman">At Republic Polytechnic, you will discover a collaborative culture that supports your pursuit in nurturing learners to be problem-solvers with innovative and entrepreneurial minds.&nbsp; We believe in transforming not only the students but also yourself, as you seek and inspire life-long learning and acquisition of skills.&nbsp; As an organisation that scores many firsts, RP welcomes you to achieve your potential in our dynamic environment.</font></span></p>  <p style="text-align: justify;">   <span style="font-size: 11pt;"><font color="#000000" face="Times New Roman">For more information, please visit</font></span><font size="3"><span style="color: black; font-family: Roboto;"> </span></font><span style="font-size: 11pt;"><font color="#0000ff" face="Times New Roman"><a href="http://www.rp.edu.sg/careers">http://www.rp.edu.sg/careers</a></font></span><span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;"><span style="color: rgb(0, 0, 0);">.</span></span></span></p>  ',
    name: 'Republic Polytechnic Recruiter',
    _links: {
      self: {
        href: 'http://localhost:4000/v2/companies/T08GB0046G',
      },
    },
  },
  hiringCompany: {
    uen: 'T08GB0046G',
    description:
      ' <p style="text-align: justify;">   <strong>About Republic Polytechnic</strong></p>  <p style="margin: 0cm 0cm 0pt; text-align: justify;">   <span style="font-size: 11pt;"><font color="#000000" face="Times New Roman">The first educational institution in Singapore to leverage the Problem-based Learning approach for all its diploma programmes, Republic Polytechnic (RP) has seven schools and one academic centre offering forty-two diplomas in Applied Science, Engineering, Management and Communication, Events and Hospitality, Infocomm, Sports, Health &amp; Leisure, and Technology for the Arts.</font></span></p>  <p style="text-align: justify;">   <span style="font-size: 11pt;"><font color="#000000" face="Times New Roman">In addition to offering full-time diploma courses, our Academy for Continuing Education @RP (ACE@RP) is committed to promoting lifelong learning among working adults.</font></span></p>  <p style="text-align: justify;">   <span style="font-size: 11pt;"><font color="#000000" face="Times New Roman">At Republic Polytechnic, you will discover a collaborative culture that supports your pursuit in nurturing learners to be problem-solvers with innovative and entrepreneurial minds.&nbsp; We believe in transforming not only the students but also yourself, as you seek and inspire life-long learning and acquisition of skills.&nbsp; As an organisation that scores many firsts, RP welcomes you to achieve your potential in our dynamic environment.</font></span></p>  <p style="text-align: justify;">   <span style="font-size: 11pt;"><font color="#000000" face="Times New Roman">For more information, please visit</font></span><font size="3"><span style="color: black; font-family: Roboto;"> </span></font><span style="font-size: 11pt;"><font color="#0000ff" face="Times New Roman"><a href="http://www.rp.edu.sg/careers">http://www.rp.edu.sg/careers</a></font></span><span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;"><span style="color: rgb(0, 0, 0);">.</span></span></span></p>  ',
    name: 'Republic Polytechnic',
    _links: {
      self: {
        href: 'http://localhost:4000/v2/companies/T08GB0046G',
      },
    },
  },
  address: {
    isOverseas: false,
    block: '9',
    street: 'WOODLANDS AVENUE 9',
    floor: '8',
    unit: '77',
    building: 'Sandcrawler',
    postalCode: '738964',
  },
  skills: [
    {
      id: 8355,
      skill: 'Software Development',
    },
    {
      id: 215,
      skill: 'Agile Methodologies',
    },
    {
      id: 7975,
      skill: 'Scrum',
    },
    {
      id: 4794,
      skill: 'Java',
    },
    {
      id: 8359,
      skill: 'Software Engineering',
    },
    {
      id: 8366,
      skill: 'Software Project Management',
    },
    {
      id: 9726,
      skill: 'Web Services',
    },
  ],
  metadata: {
    jobPostId: 'JOB-2018-0015396',
    createdAt: '2018-03-27T07:15:18.000Z',
    updatedAt: '2018-03-27T07:15:18.000Z',
    totalNumberJobApplication: 9999,
    totalNumberOfView: 1,
    newPostingDate: '2018-01-25',
    originalPostingDate: '2018-01-25',
    expiryDate: '2020-10-10',
    isPostedOnBehalf: false,
    isHideSalary: false,
    isHideCompanyAddress: false,
    isHideHiringEmployerName: false,
    isHideEmployerName: false,
    editCount: 0,
    repostCount: 0,
  },
  salary: {
    maximum: 10000.0,
    minimum: 0.0,
    type: {
      id: 4,
      salaryType: 'Monthly',
    },
  },
  schemes: [
    {
      scheme: {
        id: 4,
        scheme: 'Career Support',
      },
    },
  ],
  _links: {
    self: {
      href: 'http://localhost:4000/v2/jobs/afab75dafd1c63f8dc3a8d909be8ef4f',
    },
  },
};

export const jobsMock: IJobPost[] = [jobPostMock];

export const jobPostInputMock: IJobPostInput = {
  title: 'Lead Application Consultant (PEGA)',
  description:
    "<p>As trustee of the nation’s savings, our work shapes the future of Singaporeans.</p> <p>We foster an environment of trust and encourage positive relationships for effective teamwork. We offer you the space to develop with meaningful opportunities and fresh challenges. We adopt a total rewards focus comprising competitive remuneration, attractive benefits and non-monetary recognition.</p> <p>You will be involved in the analysis, design and development, implementation and maintenance of applications to support the Board's scheme.</p> <p>You will participate in the various stages of the project SDLC and your responsibilities may include the followings:</p> <ul> <li>Gather and analyse business requirements</li> <li>Involve in the requirement analysis phase to ensure feasibility of functional specifications</li> <li>Build and manage project schedules, quality and risk management plans</li> <li>Oversee the implementation of project on daily basis to ensure on time and quality are met</li> <li>Actively contribute to architectural decisions and ensure software meets business requirements</li> <li>Perform code reviews and hands-on coding (if necessary) and ensure best practises are followed</li> <li>Provide timely resolution of incidents and mitigation of potential risks</li> <li>Coordinate communications activities and provide timely project progress updates to stakeholders</li> </ul> <p>Ideally you should have/be:</p> <ul> <li>Tertiary qualification in Computer Science, Engineering or equivalent</li> <li>At least 12 years of IT working experiences in end to end SDLC cycle from requirements gathering, analysis, testing and implementation</li> <li>Extensive working knowledge in Pega application development and Pega v7</li> <li>Experience in designing solutions using Pega framework and or supporting various Pega components</li> <li>Certified in Pega CLSA/ CSSA</li> <li>Good working experience in Java, J2EE, relational database (DB2/Oracle)</li> <li>Good grasp of IT technologies, methodologies and best practises</li> <li>Strong collaboration, prioritization and adaptability skills</li> <li>Comfortable in building new applications and maintaining existing systems</li> <li>Strong communicator with excellent written and oral skills</li> <li>Flexible in assignment and willingness to learn new skills</li> <li>Passionate in IT and willing to stay hands-on</li> </ul> <p><strong>TRUST. Everything We Stand For. </strong></p> <p>Find out more at CPF Careers page.</p>",
  minimumYearsExperience: 1,
  ssocCode: 2512,
  numberOfVacancies: 1,
  categories: [{id: 21}, {id: 30}],
  employmentTypes: [{id: 8}],
  positionLevels: [{id: 2}],
  skills: [{id: 7571}, {id: 7983}, {id: 8355}, {id: 8366}],
  schemes: [{id: 4}],
  postedCompany: {uen: 'T08GB0046G'},
  hiringCompany: {uen: 'T08GB0046G'},
  address: {
    block: '238B',
    street: 'THOMSON ROAD',
    floor: '#08',
    unit: '00',
    building: 'TOWER B NOVENA SQUARE',
    postalCode: '307685',
    isOverseas: false,
  },
  metadata: {
    expiryDate: format(addDays(new Date(), 7), 'd MMM yyyy'),
  },
  salary: {
    maximum: 10000,
    minimum: 200,
    type: {id: 4},
  },
  ssecEqa: '92',
  ssecFos: '0521',
};
