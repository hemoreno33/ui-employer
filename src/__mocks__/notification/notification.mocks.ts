import {
  IFeatureNotification,
  IMaintenanceNotification,
  MaintenanceSystem,
} from '~/services/notification/getNotifications';

export const maintenanceNotificationMock: IMaintenanceNotification = {
  downtime: {
    fromDate: '2018-10-30T14:00:00.000Z',
    system: MaintenanceSystem.CORPPASS,
    toDate: '2018-10-30T22:00:00.000Z',
  },
  fromDate: '2018-10-25T14:00:00.000Z',
  message:
    'You will not be able to apply for jobs from 30 Nov 2018 10:00 pm to 01 Dec 2018 6:00 am due to scheduled maintenance for Jobs Bank.',
  title: 'Announcement',
  toDate: '2018-10-30T22:00:00.000Z',
  type: 'maintenance',
};

export const featureNotificationMock: IFeatureNotification = {
  bannerColor: '#00EFE4',
  fromDate: '2018-09-24T00:00:00.000Z',
  message: 'Log in to save jobs that interest you and view them in the Saved Jobs page. ',
  textColor: 'black',
  title: 'Announcement',
  toDate: '2018-10-01T15:59:00.000Z',
  type: 'feature',
  portal: 'employer',
};
