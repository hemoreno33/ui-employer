import {IConfiguration} from '~/config/index.d';

export const config: IConfiguration = {
  environment: 'development',
  meta: {
    gaTrackingCode: '',
    version: 'v1.abc',
    wogaaScriptUrl: '',
  },
  publicPath: '',
  url: {
    apiJob: {
      v1: '',
      v2: '',
    },
    apiProfile: '',
    corppass: '',
    featureToggles: '',
    mcf: '',
    notification: '',
    apiVirusScanner: '',
  },
};
