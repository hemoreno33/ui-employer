import {Matchers} from '@pact-foundation/pact';
import {transformArrayToEachLikeMatcher} from '../pact';

describe('__mocks__/pact', () => {
  it('should transform Array type into Matchers.eachLike', () => {
    const input = {
      array: ['value'],
      child: {
        array: ['value'],
        childValue: 'test',
      },
      children: [
        {
          array: ['value'],
        },
      ],
      id: 1,
      value: 'test',
    };
    const expected = {
      array: Matchers.eachLike('value'),
      child: {
        array: Matchers.eachLike('value'),
        childValue: 'test',
      },
      children: Matchers.eachLike({
        array: Matchers.eachLike('value'),
      }),
      id: 1,
      value: 'test',
    };
    expect(JSON.stringify(transformArrayToEachLikeMatcher(input))).toEqual(JSON.stringify(expected));
  });

  it('should transform Array type into Matchers.eachLike using only the first array value', () => {
    const input = {
      array: ['value1', 'value2'],
    };
    const expected = {
      array: Matchers.eachLike('value1'),
    };
    expect(JSON.stringify(transformArrayToEachLikeMatcher(input))).toEqual(JSON.stringify(expected));
  });

  it('should not transform empty Array', () => {
    const input = {
      array: [],
    };
    expect(transformArrayToEachLikeMatcher(input)).toEqual(input);
  });

  it('should transform with custom eachLike matcher', () => {
    const customMatcher = (val: any) => [val];
    const input = {
      array: ['value'],
      child: {
        array: ['value'],
        childValue: 'test',
      },
      children: [
        {
          array: ['value'],
        },
      ],
      id: 1,
      value: 'test',
    };
    const expected = {
      array: customMatcher('value'),
      child: {
        array: customMatcher('value'),
        childValue: 'test',
      },
      children: customMatcher({
        array: customMatcher('value'),
      }),
      id: 1,
      value: 'test',
    };
    expect(transformArrayToEachLikeMatcher(input, customMatcher)).toEqual(expected);
  });
});
