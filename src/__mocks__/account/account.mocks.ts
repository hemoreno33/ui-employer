import {IUser} from '~/flux/account';

export const uenUserMock: IUser = {
  userInfo: {
    accountType: 'Admin',
    entityId: 'T08GB0046G',
    entityStatus: 'Registered',
    entityType: 'UEN',
    singpassHolder: true,
    userCountry: 'SG',
    userFullName: 'Tan Ah Koy',
    userId: 'F1234567P',
    userName: 'RP192',
  },
  authorization: [],
  exp: 2523438765,
  iss: 'Corppass',
  additionalInfo: {
    data: {
      systemContact: {
        termsAndConditionsAcceptedAt: '2018-01-01T00:00:00.000Z',
      },
    },
  },
};

export const nonUenUserMock: IUser = {
  userInfo: {
    accountType: 'Admin',
    entityCountry: 'CO',
    entityId: 'C18000545L',
    entityName: 'CRAZY RICH ASIANS PTE LTD',
    entityRegNo: '999999999',
    entityType: 'NON-UEN',
    singpassHolder: true,
    userCountry: 'SG',
    userFullName: 'Tan Ah Koy',
    userId: 'F1234567P',
    userName: 'RP192',
  },
  authorization: [],
  exp: 2523438765,
  iss: 'Corppass',
};
